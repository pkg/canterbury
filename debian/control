Source: canterbury
Section: libs
Priority: extra
Maintainer: Apertis package maintainers <packagers@lists.apertis.org>
Build-Depends:
 autoconf-archive,
 autotools-dev,
 dbus,
 debhelper (>= 10),
 desktop-file-utils,
 dh-apparmor,
 dh-exec,
 flatpak,
 gobject-introspection (>= 1.30),
 hotdoc-0.8 (>= 0.8.3),
 hotdoc-c-extension-0.8 (>= 0.8.2),
 hotdoc-dbus-extension-0.8,
 libapparmor-dev,
 libappstream-glib-dev,
 libgirepository1.0-dev (>= 1.30),
 libglib2.0-dev,
 libgtk-3-dev,
 libpulse-dev,
 libsystemd-dev,
 shapwick-dev (>= 0.2.0),
Standards-Version: 3.9.2

Package: canterbury
Section: oldlibs
Architecture: any
Depends:
 canterbury-full (= ${binary:Version}),
 ${misc:Depends},
Description: Application management service - transitional package
 This package arranges for canterbury-full to be installed on upgrade.

Package: canterbury-common
Architecture: any
Depends:
 apparmor (>= 2.11.0-2ubuntu5~),
 canterbury-update-component-index (= ${binary:Version}),
 libcanterbury-0-0 (= ${binary:Version}),
 libcanterbury-platform-0-0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 canterbury (<< ${binary:Version}),
Replaces:
 canterbury (<< ${binary:Version}),
Description: Application management service - common files
 Canterbury is the Apertis application management service. It arranges for
 agents and other programs to be managed by systemd, starts agents
 during startup, and arranges for agents to be restarted if they crash.
 .
 This package contains supporting files used by both the canterbury-full
 and canterbury-core versions of the Canterbury service, including the
 canterbury-init service that loads AppArmor profiles during boot,
 and the canterbury-exec wrapper that sets up appropriate environment
 variables and directories to run programs from app-bundles.

Package: canterbury-core
Architecture: any
Depends:
 apparmor (>= 2.11.0-2ubuntu5~),
 canterbury-common (= ${binary:Version}),
 canterbury-update-component-index (= ${binary:Version}),
 libcanterbury-0-0 (= ${binary:Version}),
 libcanterbury-platform-0-0 (= ${binary:Version}),
 ribchester-core | ribchester-full,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Application management service - lightweight version
 Canterbury is the Apertis application management service. It arranges for
 agents and other programs to be managed by systemd, starts agents
 during startup, and arranges for agents to be restarted if they crash.
 .
 This version of Canterbury does not include GUI-related features such
 as application launching and a "back" stack.

Package: canterbury-core-tests
Section: libdevel
Architecture: any
Depends:
 apparmor (>= 2.11.0-2ubuntu5~),
 canterbury-common (= ${binary:Version}),
 canterbury-core (= ${binary:Version}),
 canterbury-update-component-index (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 canterbury-tests (<< ${binary:Version}),
Replaces:
 canterbury-tests (<< ${binary:Version}),
Description: Canterbury application manager - tests (without GUI support)
 Canterbury is the Apertis application management service.
 .
 This package contains automated tests suitable for minimal (non-GUI)
 environments.

Package: canterbury-dev
Section: oldlibs
Architecture: any
Depends:
 canterbury (= ${binary:Version}),
 libapparmor-dev,
 libappstream-glib-dev,
 libcanterbury-0-dev (= ${binary:Version}),
 libcanterbury-gdbus-0-dev (= ${binary:Version}),
 libglib2.0-dev,
 libpulse-dev,
 libsystemd-dev,
 shapwick-dev,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 canterbury-doc,
Description: Transitional package for Canterbury development files
 This package contains the deprecated canterbury.pc pkg-config file,
 which pulls in both canterbury-0 and canterbury-gdbus-0.
 .
 New code should depend on canterbury-0.pc (libcanterbury-0-dev) or on
 canterbury-gdbus-0.pc (libcanterbury-gdbus-0-dev) as appropriate.

Package: canterbury-doc
Section: doc
Architecture: all
Depends:
 canterbury(= ${binary:Version}),
 ${misc:Depends},
Description: This package contains the API reference for the canterbury service.

Package: canterbury-full
Architecture: any
Depends:
 apparmor (>= 2.11.0-2ubuntu5~),
 canterbury-common (= ${binary:Version}),
 canterbury-update-component-index (= ${binary:Version}),
 desktop-file-utils,
 gtk-update-icon-cache,
 libcanterbury-0-0 (= ${binary:Version}),
 libcanterbury-gdbus-0-9 (= ${binary:Version}),
 ribchester (>= 0.1612.3-0co1),
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 canterbury (<< ${binary:Version}),
Replaces:
 canterbury (<< ${binary:Version}),
Description: Application management service - full-functionality version
 Canterbury is the Apertis application management service. It arranges for
 agents and other programs to be managed by systemd, starts agents
 during startup, and arranges for agents to be restarted if they crash.
 .
 This version of Canterbury also includes various GUI-related features,
 some of which should likely be moved to the system compositor in future:
 it launches graphical programs on request, handles hardware button events,
 manages the "back" stack for applications and the activity stack, and
 communicates with the Mildenhall reference UI suite, particularly the
 launcher and compositor.

Package: canterbury-tests
Section: libdevel
Architecture: any
Depends:
 apparmor (>= 2.11.0-2ubuntu5~),
 canterbury-core-tests (= ${binary:Version}),
 canterbury (= ${binary:Version}),
 libgtk-3-bin,
 python3-dbus,
 python3-dbusmock,
 python3-gi,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Canterbury application manager - tests (with full GUI support)
 Canterbury is the Apertis application management service.
 .
 This package contains automated tests.

Package: canterbury-tests-additions
Section: libdevel
Architecture: any
Depends:
 apparmor (>= 2.11.0-2ubuntu5~),
 canterbury-tests (= ${binary:Version}),
 canterbury (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Canterbury application manager - additional tests
 Canterbury is the Apertis application management service.
 .
 This package contains additional automated tests.

Package: canterbury-update-component-index
Architecture: any
Depends:
 apparmor (>= 2.11.0-2ubuntu5~),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Canterbury application manager - index update tool
 This package creates and maintains a cache of installed software components.
 It has been separated from the canterbury package to avoid circular
 dependencies.

Package: gir1.2-canterbury-0
Section: introspection
Architecture: any
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: application manager service - introspection data
 This package contains GObject-Introspection data for utility functions for
 apps and services related to the Canterbury application framework.

Package: gir1.2-canterburygdbus-0
Section: introspection
Architecture: any
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 gir1.2-canterbury-0 (<< 0.1703.7~),
Replaces:
 gir1.2-canterbury-0 (<< 0.1703.7~),
Description: application manager D-Bus API - introspection data
 This package contains GObject-Introspection data for older D-Bus APIs of the
 Canterbury application framework.

Package: gir1.2-canterbury-platform-0
Section: oldlibs
Architecture: any
Depends:
 gir1.2-canterburyplatform-0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: transitional package for introspection data
 This package depends on gir1.2-canterburyplatform-0, which was renamed.

Package: gir1.2-canterburyplatform-0
Section: introspection
Architecture: any
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 gir1.2-canterbury-platform-0 (<< 0.1703.7~),
Replaces:
 gir1.2-canterbury-platform-0 (<< 0.1703.7~),
Description: application manager service - introspection data
 This package contains GObject-Introspection data for utility functions for
 apps and services related to the Canterbury application framework.

Package: libcanterbury-0-0
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: application manager service - shared library
 This package contains a shared library with utility functions for
 apps and services related to the Canterbury application framework.

Package: libcanterbury-0-dev
Section: libdevel
Architecture: any
Depends:
 gir1.2-canterbury-0 (= ${binary:Version}),
 libapparmor-dev,
 libappstream-glib-dev,
 libcanterbury-0-0 (= ${binary:Version}),
 libglib2.0-dev,
 libpulse-dev,
 libsystemd-dev,
 shapwick-dev,
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 canterbury-dev (<< 0.1703.7~),
Replaces:
 canterbury-dev (<< 0.1703.7~),
Suggests:
 canterbury-doc,
Description: application manager service - shared library development files
 This package contains development files for the libcanterbury-0
 shared library.

Package: libcanterbury-gdbus-0-9
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 libcanterbury-platform-common,
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 canterbury (<< 0.1703.7~),
Replaces:
 canterbury (<< 0.1703.7~),
Description: application manager service - GDBus shared library
 This package contains a shared library with GDBus wrappers for
 the Canterbury application manager's D-Bus interfaces.

Package: libcanterbury-gdbus-0-dev
Section: libdevel
Architecture: any
Depends:
 gir1.2-canterburygdbus-0 (= ${binary:Version}),
 libcanterbury-gdbus-0-9 (= ${binary:Version}),
 libglib2.0-dev,
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 canterbury-dev (<< 0.1703.7~),
Replaces:
 canterbury-dev (<< 0.1703.7~),
Suggests:
 canterbury-doc,
Description: application manager service - D-Bus API library development files
 This package contains development files for the libcanterbury-gdbus-0
 shared library.

Package: libcanterbury-platform-0-0
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 libcanterbury-platform-common,
 shared-mime-info,
 ${misc:Depends},
 ${shlibs:Depends},
Description: application manager service - shared library for platform services
 This package contains a shared library with utility functions for
 platform services related to the Canterbury application framework.

Package: libcanterbury-platform-common
Section: misc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 canterbury (<< ${binary:Version}),
Replaces:
 canterbury (<< ${binary:Version}),
Description: application manager service - supporting files
 This package contains supporting files for the libcanterbury-platform
 library, including the canterbury-platform-read-entry-points AppArmor
 abstraction.

Package: libcanterbury-platform-0-dev
Section: libdevel
Architecture: any
Depends:
 libcanterbury-0-dev (= ${binary:Version}),
 gir1.2-canterburyplatform-0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 canterbury-doc,
Description: application manager service - headers for platform services
 This package contains the static libraries and header files needed to
 use libcanterbury-platform-0-0.
