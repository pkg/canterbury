/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef EXAMPLE_REQUEST_MANAGER_H
#define EXAMPLE_REQUEST_MANAGER_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <canterbury/canterbury-platform.h>

#include "tests/example-request-manager-generated.h"

G_BEGIN_DECLS

#define EXAMPLE_TYPE_REQUEST_MANAGER (example_request_manager_get_type ())

G_DECLARE_FINAL_TYPE (ExampleRequestManager,
                      example_request_manager,
                      EXAMPLE,
                      REQUEST_MANAGER,
                      TestsRequestManagerSkeleton)

ExampleRequestManager *example_request_manager_new (
    CbyComponentIndex *component_index);

guint example_request_manager_count_activities (ExampleRequestManager *self);
const gchar *example_request_manager_get_message (ExampleRequestManager *self,
                                                  const gchar *activity_path);
GStrv example_request_manager_list_messages (ExampleRequestManager *self);

G_END_DECLS

#endif
