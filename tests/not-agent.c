/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * not-agent — an instrumented fake UI process for the Canterbury
 *             test suite
 *
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <gio/gio.h>

#define BUNDLE_ID "org.apertis.CanterburyTests.Agent"
#define ENTRY_POINT_ID BUNDLE_ID ".NotAgent"

#define TESTS_TYPE_NOT_AGENT (tests_not_agent_get_type ())
G_DECLARE_FINAL_TYPE (TestsNotAgent, tests_not_agent, TESTS, NOT_AGENT,
                      GApplication)

struct _TestsNotAgent
{
  GApplication parent;
};

G_DEFINE_TYPE (TestsNotAgent, tests_not_agent, G_TYPE_APPLICATION)

static void
tests_not_agent_class_init (TestsNotAgentClass *cls)
{
}

static void
tests_not_agent_init (TestsNotAgent *self)
{
}

int
main (int argc, char **argv)
{
  g_autoptr (GApplication) app = NULL;

  app = G_APPLICATION (g_object_new (TESTS_TYPE_NOT_AGENT,
                                     "application-id", ENTRY_POINT_ID,
                                     "flags", G_APPLICATION_IS_SERVICE,
                                     NULL));

  return g_application_run (app, argc, argv);
}
