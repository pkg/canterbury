/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <glib/gstdio.h>

#include <canterbury/canterbury-platform.h>

#include "canterbury/gdbus/canterbury.h"
#include "canterbury/gdbus/enumtypes.h"
#include "canterbury/platform/component-index-internal.h"
#include "canterbury/platform/entry-point-internal.h"
#include "canterbury/platform/entry-point-index.h"
#include "canterbury/platform/entry-point-index-internal.h"

#include "tests/common.h"

typedef struct
{
  gchar *tmpdir;
  gchar *xdg_data_dir;
} TestsTempDir;

typedef struct
{
  TestsTempComponentIndex component_index;
} Fixture;

static gchar *
tests_temp_dir_init (TestsTempDir *self)
{
  g_autoptr (GError) error = NULL;

  g_assert_nonnull (self);

  self->tmpdir = g_dir_make_tmp ("canterbury.XXXXXX", &error);
  g_assert_no_error (error);
  g_assert_nonnull (self->tmpdir);

  self->xdg_data_dir =
      g_build_filename (self->tmpdir, "share", NULL);

  return NULL;
}

static void
tests_fix_permissions (const gchar *dir)
{
  g_autoptr (GError) error = NULL;
  gint exit_status;

  /* We need to chmod it in case it's a copy of the read-only data
   * during distcheck. */
  const gchar *argv[] = { "chmod", "-R", "u+w", "<tmpdir>", NULL };

  argv[3] = dir;

  g_spawn_sync (NULL, (gchar **) argv, NULL,     /* envp */
                G_SPAWN_SEARCH_PATH, NULL, NULL, /* child setup */
                NULL,                            /* stdout */
                NULL,                            /* stderr */
                &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);
}

static void
tests_remove_dir (const gchar *dir)
{
  g_autoptr (GError) error = NULL;
  const gchar *argv[] = { "rm", "-fr", "<dir>", NULL };
  gint exit_status;

  g_assert_nonnull (dir);

  tests_fix_permissions (dir);

  argv[2] = dir;
  g_spawn_sync (NULL, (gchar **) argv, NULL,     /* envp */
                G_SPAWN_SEARCH_PATH, NULL, NULL, /* child setup */
                NULL,                            /* stdout */
                NULL,                            /* stderr */
                &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);
}

static void
tests_temp_dir_cleanup (TestsTempDir *self)
{
  g_assert_nonnull (self);
  g_assert_nonnull (self->tmpdir);
  g_assert_nonnull (self->xdg_data_dir);

  tests_remove_dir (self->tmpdir);
  g_free (self->tmpdir);
  g_free (self->xdg_data_dir);
}

static void
tests_temp_dir_setup (const TestsTempDir *self)
{
  g_assert_nonnull (self);
  g_assert_nonnull (self->tmpdir);
  g_assert_nonnull (self->xdg_data_dir);

  g_mkdir_with_parents (self->xdg_data_dir, 0755);
}

static void
tests_temp_dir_teardown (const TestsTempDir *self)
{
  g_assert_nonnull (self);
  g_assert_nonnull (self->tmpdir);
  g_assert_nonnull (self->xdg_data_dir);

  tests_remove_dir (self->xdg_data_dir);
}

static void
tests_temp_dir_prepare_for_bundle (const TestsTempDir *self,
                                   const gchar *bundle_id)
{
  const gchar *argv[] = { "cp", "-R", "<source>", "<tmpdir>", NULL };
  g_autoptr (GError) error = NULL;
  g_autofree gchar *bundle_apps_path = NULL;
  gint exit_status;

  g_assert_nonnull (self);
  g_assert_nonnull (self->tmpdir);
  g_assert_nonnull (self->xdg_data_dir);
  g_assert_nonnull (bundle_id);

  tests_fix_permissions (self->tmpdir);

  bundle_apps_path = g_build_filename ("tests", "Applications", bundle_id,
                                       "share", "applications", NULL);
  g_test_message ("%s: xdg_data_dir: %s; copying from: %s/%s",
                  G_STRFUNC, self->xdg_data_dir,
                  g_test_get_dir (G_TEST_DIST), bundle_apps_path);
  argv[2] = bundle_apps_path;
  argv[3] = self->xdg_data_dir;

  g_spawn_sync (g_test_get_dir (G_TEST_DIST), (gchar **) argv, NULL, /* envp */
                G_SPAWN_SEARCH_PATH, NULL, NULL, /* child setup */
                NULL,                            /* stdout */
                NULL,                            /* stderr */
                &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);
}

static void
tests_temp_dir_cleanup_for_bundle (const TestsTempDir *self,
                                   const gchar *bundle_id)
{
  g_autoptr (GDir) dir = NULL;
  g_autofree gchar *bundle_apps_path = NULL;
  g_autofree gchar *target_path = NULL;
  g_autoptr (GError) error = NULL;
  const gchar *name;

  g_assert_nonnull (self);
  g_assert_nonnull (self->tmpdir);
  g_assert_nonnull (self->xdg_data_dir);
  g_assert_nonnull (bundle_id);

  tests_fix_permissions (self->tmpdir);

  bundle_apps_path = g_build_filename (g_test_get_dir (G_TEST_DIST),
                                       "tests", "Applications", bundle_id,
                                       "share", "applications", NULL);
  dir = g_dir_open (bundle_apps_path, 0, &error);
  g_assert_nonnull (dir);

  target_path = g_build_filename (self->xdg_data_dir, "applications", NULL);
  for (name = g_dir_read_name (dir);
       name != NULL;
       name = g_dir_read_name (dir))
    {
      if (g_str_has_suffix (name, ".desktop"))
        {
          g_autofree gchar *desktop_file = NULL;

          desktop_file = g_build_filename (target_path, name, NULL);
          tests_assert_syscall_0_or_enoent (unlink (desktop_file));
        }
    }
}

static void
setup (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  /* be killed by SIGALRM if it takes unreasonably long */
  alarm (30);
}

static void
setup_index (Fixture *f, gconstpointer user_data)
{
  const TestsTempDir *temp_dir = user_data;

  setup (f, NULL);

  tests_temp_component_index_setup (&f->component_index);
  tests_temp_dir_setup (temp_dir);
}

typedef struct
{
  /** Entry Point **/
  CbyServiceType service_type;
  const gchar *bundle_id;
  const gchar *app_name;
  /* Path */
  const gchar *working_directory;
  /* X-Apertis-AudioChannelName */
  const gchar *audio_channel_name;
  /* X-Apertis-AudioResourceOwner */
  const gchar *audio_resource_owner;
  /* X-Apertis-CategoryLabel (mangled => uppercase + spaces (kerning)) */
  const gchar *category;
  /* X-Apertis-SplashScreen */
  const gchar *splash_screen;
  /* Appinfo icon */
  const gchar *app_info_icon;
  /* argv[0..N] from Exec */
  const gchar * const *argv;
  /* DisplayName non mangled */
  const gchar * const *app_entry_display_names;
  /* GenericName|DisplayName|Name (mangled => uppercase + spaces (kerning)) */
  const gchar * const *app_entry_names;
  /* MimeType */
  const gchar * const *mimetypes;
  /* Appinfo desktop entry string */
  const gchar * const *app_info_string;
  /* X-Apertis-BackgroundState */
  guint bkg_state;
  /* X-Apertis-Type */
  guint executable_type;
  /* X-Apertis-AudioRole */
  guint audio_resource_type;
  /* X-Apertis-BandwidthPriority */
  guint bandwidth_priority;
  /* NoDisplay */
  gboolean nodisplay;

  /** Test specific **/
  /* Only install bundles matching the ids of main entry points */
  gboolean main_entry_point;

  /* whether the bundle or entry point ID is invalid */
  gboolean invalid_entry_point;
} GetEntryPointsTestCase;

static GetEntryPointsTestCase entry_point_index_test_cases[] = {
  { APPSTORE,                                             /* app type */
    "com.example.MusicPlayer",                            /* bundle id */
    "com.example.MusicPlayer",                            /* app name */
    "/Applications/com.example.MusicPlayer",              /* working dir */
    "com.example.MusicPlayer",                            /* audio channel name */
    "com.example.MusicPlayer",                            /* audio resource owner */
    /* CbyEntryPoint mangles the category with spaces */
    "A U D I O",                                          /* category */
    "music-player-splash.png",                            /* spĺash screen */
    "audio-x-generic",                                    /* app_info icon */
    /* Note that the executable must exist otherwise GAppInfo doesnt
     * list the entry point, so using /bin/true for testing purposes */
    (const char *[]) { "/bin/true", "param1", "param2", NULL }, /* argv */
    /* Human readable display name (from .desktop file) */
    (const char *[]) { "Music Player", NULL }, /* app entry display names*/
    /* CbyEntryPoint mangles the display name with spaces */
    (const char *[]) { "M U S I C   P L A Y E R", NULL }, /* app entry names */
    (const char *[]) { "audio/mpeg", "audio/mp3", NULL }, /* mime types */
    (const char *[]) { "X-Test-ExistingField", "found" }, /* app_info desktop entry string */
    /* CbyEntryPoint sets state to running per default if no X-Apertis-BackgroundState key is found */
    CANTERBURY_BKG_STATE_RUNNING,                         /* background state */
    CANTERBURY_EXECUTABLE_TYPE_APPLICATION,               /* executable type */
    CANTERBURY_AUDIO_RESOURCE_TYPE_MUSIC,                 /* audio resource type */
    CANTERBURY_BANDWIDTH_PRIORITY_HIGH,                   /* internet bandwidth priority */
    FALSE,                                                /* nodisplay */
    TRUE,                                                 /* main_entry_point */
    FALSE
  },

  { APPSTORE,                                             /* app type */
    "com.example.MusicPlayer",                            /* bundle id */
    "com.example.MusicPlayer.Agent",                      /* app name */
    NULL,                                                 /* working dir */
    NULL,                                                 /* audio channel name */
    NULL,                                                 /* audio resource owner */
    NULL,                                                 /* category */
    NULL,                                                 /* spĺash screen */
    NULL,                                                 /* app_info icon */
    /* Note that the executable must exist otherwise GAppInfo doesnt
     * list the entry point, so using /bin/true for testing purposes */
    (const char *[]) { "/bin/true", "param1", NULL },     /* argv */
    /* Human readable display name (from .desktop file) */
    (const char *[]) { "Example Music Player - Playback Agent", NULL }, /* app entry display names*/
    /* CbyEntryPoint mangles the display name with spaces */
    (const char *[]) { "E X A M P L E   M U S I C   P L A Y E R   -   P L A Y B A C K   A G E N T", NULL }, /* app entry names */
    (const char *[]) { NULL },                            /* mime types */
    (const char *[]) { "X-Test-UndefinedField", NULL },   /* app_info desktop entry string */
    /* CbyEntryPoint sets state to running per default if no X-Apertis-BackgroundState key is found */
    CANTERBURY_BKG_STATE_RUNNING,                         /* background state */
    CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE,             /* executable type */
    CANTERBURY_AUDIO_RESOURCE_TYPE_NONE,                  /* audio resource type */
    CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN,                /* internet bandwidth priority */
    TRUE,                                                 /* nodisplay */
    FALSE,                                                /* main_entry_point */
    FALSE
  },

  { APPSTORE,                                             /* app type */
    "com.example.Masked",                                 /* bundle id */
    "com.example.Masked",                                 /* app name */
    NULL,                                                 /* working dir */
    NULL,                                                 /* audio channel name */
    NULL,                                                 /* audio resource owner */
    NULL,                                                 /* category */
    NULL,                                                 /* spĺash screen */
    NULL,                                                 /* app_info icon */
    /* Note that the executable must exist otherwise GAppInfo doesnt
     * list the entry point, so using /bin/true for testing purposes */
    (const char *[]) { "/bin/true", "param1", NULL },     /* argv */
    /* Human readable display name (from .desktop file) */
    (const char *[]) { "Masked by built-in app", NULL }, /* app entry display names*/
    /* CbyEntryPoint mangles the display name with spaces */
    (const char *[]) { "M A S K E D   B Y   B U I L T - I N   A P P", NULL }, /* app entry names */
    (const char *[]) { NULL },                            /* mime types */
    (const char *[]) { "X-Test-UndefinedField", NULL },   /* app_info desktop entry string */
    /* CbyEntryPoint sets state to running per default if no X-Apertis-BackgroundState key is found */
    CANTERBURY_BKG_STATE_RUNNING,                         /* background state */
    CANTERBURY_EXECUTABLE_TYPE_APPLICATION,               /* executable type */
    CANTERBURY_AUDIO_RESOURCE_TYPE_NONE,                  /* audio resource type */
    CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN,                /* internet bandwidth priority */
    FALSE,                                                /* nodisplay */
    TRUE,                                                 /* main_entry_point */
    FALSE
  },

  { APPSTORE,
    "com.example.Masked",
    "com.example.InvalidBundleId",
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    CANTERBURY_BKG_STATE_UNKNOWN,
    CANTERBURY_EXECUTABLE_TYPE_UNKNOWN,
    CANTERBURY_AUDIO_RESOURCE_TYPE_NONE,
    CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN,
    FALSE,
    FALSE,
    TRUE
  },

  { APPSTORE,
    "com.example.Masked",
    "invalid-entry-point",
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    CANTERBURY_BKG_STATE_UNKNOWN,
    CANTERBURY_EXECUTABLE_TYPE_UNKNOWN,
    CANTERBURY_AUDIO_RESOURCE_TYPE_NONE,
    CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN,
    FALSE,
    FALSE,
    TRUE
  },

  { APPSTORE,
    "com.example.Masked",
    "com.example.NotMyService",
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    CANTERBURY_BKG_STATE_UNKNOWN,
    CANTERBURY_EXECUTABLE_TYPE_UNKNOWN,
    CANTERBURY_AUDIO_RESOURCE_TYPE_NONE,
    CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN,
    FALSE,
    FALSE,
    TRUE
  },
};

#define assert_cmpstrv_equal(a, b) \
    G_STMT_START {\
      gchar **__a = (a), **__b = (b); \
      if (__a == NULL) g_assert_null (__b); \
      else \
        { \
          guint len; \
          guint i; \
          g_assert_nonnull (__b); \
          len = g_strv_length (__a); \
          g_assert_cmpint (len, ==, g_strv_length (__b)); \
          for (i = 0; i < len; ++i) \
            g_assert_cmpstr (__a[i], ==, __b[i]); \
        } \
    } G_STMT_END

static void
test_entry_point (CbyEntryPoint *entry_point,
                  const GetEntryPointsTestCase *tc)
{
  g_assert_cmpint (_cby_entry_point_get_service_type (entry_point), ==, tc->service_type);

  g_assert_cmpstr (cby_entry_point_get_id (entry_point), ==, tc->app_name);
  g_assert_cmpstr (_cby_entry_point_get_working_directory (entry_point), ==, tc->working_directory);
  g_assert_cmpstr (_cby_entry_point_get_audio_channel (entry_point), ==, tc->audio_channel_name);
  g_assert_cmpstr (_cby_entry_point_get_audio_resource_owner_id (entry_point), ==, tc->audio_resource_owner);
  g_assert_cmpstr (_cby_entry_point_get_category_label (entry_point), ==, tc->category);
  g_assert_cmpstr (_cby_entry_point_get_splash_screen (entry_point), ==, tc->splash_screen);
  g_assert_cmpstr (_cby_entry_point_get_mangled_display_name (entry_point), ==, tc->app_entry_names[0]);
  g_assert_cmpstr (cby_entry_point_get_display_name (entry_point), ==, tc->app_entry_display_names[0]);
  g_assert_cmpstr (cby_entry_point_get_string (entry_point, tc->app_info_string[0]), ==, tc->app_info_string[1]);

  if (tc->app_info_icon)
    {
      GStrv icon_names;
      GIcon *icon = cby_entry_point_get_icon (entry_point);
      g_assert_nonnull (icon);
      g_object_get (G_OBJECT (icon), "names", &icon_names, NULL);
      g_assert_nonnull (icon_names);
      g_assert_cmpstr (icon_names[0], ==,tc->app_info_icon);
      g_strfreev (icon_names);
    }

  assert_cmpstrv_equal ((gchar **) _cby_entry_point_get_argv (entry_point), (gchar **) tc->argv);
  g_assert_cmpstr (cby_entry_point_get_executable (entry_point), ==, tc->argv[0]);
  assert_cmpstrv_equal ((gchar **) _cby_entry_point_get_arguments (entry_point), (gchar **) (&tc->argv[1]));
  assert_cmpstrv_equal ((gchar **)cby_entry_point_get_supported_types (entry_point), (gchar **) tc->mimetypes);

  g_assert_cmpint (_cby_entry_point_get_background_state (entry_point), ==, tc->bkg_state);
  g_assert_cmpint (_cby_entry_point_get_executable_type (entry_point), ==, tc->executable_type);
  g_assert_cmpint (_cby_entry_point_get_audio_resource_type (entry_point), ==, tc->audio_resource_type);
  g_assert_cmpint (_cby_entry_point_get_internet_bandwidth_priority (entry_point), ==, tc->bandwidth_priority);
  g_assert_cmpint (cby_entry_point_should_show (entry_point), ==, !tc->nodisplay);
}

static void
test_get_entry_points (Fixture *f, gconstpointer user_data)
{
  const TestsTempDir *temp_dir = user_data;
  g_autoptr (CbyComponentIndex) component_index = NULL;
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GPtrArray) entry_points = NULL;
  guint i, j;

  /* Install entry points for each test case representing a
   * main entry point to temporary data dir */
  for (i = 0; i < G_N_ELEMENTS (entry_point_index_test_cases); i++)
    {
      const GetEntryPointsTestCase *tc = &entry_point_index_test_cases[i];
      if (tc->main_entry_point)
        tests_temp_dir_prepare_for_bundle (temp_dir, tc->bundle_id);
    }

  /* For change notification */
  component_index = _cby_component_index_new_full (
      CBY_COMPONENT_INDEX_FLAGS_NONE, f->component_index.platform_cache,
      f->component_index.store_cache, &error);
  g_assert_no_error (error);
  g_assert (CBY_IS_COMPONENT_INDEX (component_index));

  entry_point_index = cby_entry_point_index_new (component_index);
  g_assert (CBY_IS_ENTRY_POINT_INDEX (entry_point_index));

  /* Check if each entry point defined in test cases are available and match */
  for (i = 0; i < G_N_ELEMENTS (entry_point_index_test_cases); i++)
    {
      const GetEntryPointsTestCase *tc = &entry_point_index_test_cases[i];
      g_autoptr (CbyEntryPoint) entry_point = NULL;

      entry_point = cby_entry_point_index_get_by_id (entry_point_index, tc->app_name);

      if (!tc->invalid_entry_point)
        {
          if (!CBY_IS_ENTRY_POINT (entry_point))
            g_error ("Test case %d \"%s\" should have had an entry point",
                     i, tc->app_name);

          test_entry_point (entry_point, tc);
        }
      else if (entry_point != NULL)
        {
          g_error ("Test case %d \"%s\" should not have had an entry point",
                   i, tc->app_name);
        }
    }

  /* Check again, but now using the list of all available entry points */
  entry_points = cby_entry_point_index_get_entry_points (entry_point_index);
  g_assert_nonnull (entry_points);
  for (i = 0; i < G_N_ELEMENTS (entry_point_index_test_cases); i++)
    {
      const GetEntryPointsTestCase *tc = &entry_point_index_test_cases[i];
      gboolean found = FALSE;

      for (j = 0; j < entry_points->len; j++)
        {
          CbyEntryPoint *entry_point = g_ptr_array_index (entry_points, j);

          g_assert_true (CBY_IS_ENTRY_POINT (entry_point));
          if (!tc->invalid_entry_point &&
              g_strcmp0 (cby_entry_point_get_id (entry_point), tc->app_name) == 0)
            {
              test_entry_point (entry_point, tc);
              found = TRUE;
            }
        }
      if (!tc->invalid_entry_point)
        g_assert_true (found);
      else
        g_assert_false (found);
    }
}

static void
append_to_ptr_array (CbyEntryPointIndex *entry_point_index,
                     CbyEntryPoint *entry_point,
                     GPtrArray *arr)
{
  g_assert (arr != NULL);

  g_ptr_array_add (arr, g_object_ref (entry_point));
}

static void
test_changes (Fixture *f, gconstpointer user_data)
{
  const TestsTempDir *temp_dir = user_data;
  g_autoptr (CbyComponentIndex) component_index = NULL;
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  /* com.example.MusicPlayer */
  g_autoptr (CbyEntryPoint) entry_point1 = NULL;
  /* com.example.MusicPlayer.Agent */
  g_autoptr (CbyEntryPoint) entry_point2 = NULL;
  /* com.example.Masked */
  g_autoptr (CbyEntryPoint) entry_point3 = NULL;
  g_autoptr (GError) error = NULL;
  /* com.example.MusicPlayer */
  const GetEntryPointsTestCase *tc1 = NULL;
  /* com.example.MusicPlayer.Agent */
  const GetEntryPointsTestCase *tc2 = NULL;
  /* com.example.Masked */
  const GetEntryPointsTestCase *tc3 = NULL;
  gulong added_signal_id;
  gulong changed_signal_id;
  gulong removed_signal_id;
  g_autoptr (GPtrArray) added = g_ptr_array_new_with_free_func (g_object_unref);
  g_autoptr (GPtrArray) changed =
      g_ptr_array_new_with_free_func (g_object_unref);
  g_autoptr (GPtrArray) removed =
      g_ptr_array_new_with_free_func (g_object_unref);
  guint i;
  g_autoptr (GKeyFile) editor = g_key_file_new ();
  g_autofree gchar *desktop_file = NULL;
  g_autofree gchar *custom_field = NULL;

  tc1 = &entry_point_index_test_cases[0];
  tc2 = &entry_point_index_test_cases[1];
  tc3 = &entry_point_index_test_cases[2];

  /* Install just com.example.MusicPlayer and check if its entry points are found */
  tests_temp_dir_prepare_for_bundle (temp_dir, tc1->bundle_id);

  component_index = _cby_component_index_new_full (
      CBY_COMPONENT_INDEX_FLAGS_NONE, f->component_index.platform_cache,
      f->component_index.store_cache, &error);
  g_assert_no_error (error);
  g_assert (CBY_IS_COMPONENT_INDEX (component_index));

  entry_point_index = cby_entry_point_index_new (component_index);
  g_assert (CBY_IS_ENTRY_POINT_INDEX (entry_point_index));
  added_signal_id =
      g_signal_connect (entry_point_index, "added",
                        G_CALLBACK (append_to_ptr_array), added);
  changed_signal_id =
      g_signal_connect (entry_point_index, "changed",
                        G_CALLBACK (append_to_ptr_array), changed);
  removed_signal_id =
      g_signal_connect (entry_point_index, "removed",
                        G_CALLBACK (append_to_ptr_array), removed);

  /* Check if com.example.MusicPlayer is available */
  entry_point1 = cby_entry_point_index_get_by_id (entry_point_index, tc1->app_name);
  g_assert_true (CBY_IS_ENTRY_POINT (entry_point1));
  test_entry_point (entry_point1, tc1);

  /* com.example.MusicPlayer.Agent comes from com.example.MusicPlayer
   * that just got installed */
  entry_point2 = cby_entry_point_index_get_by_id (entry_point_index, tc2->app_name);
  g_assert_true (CBY_IS_ENTRY_POINT (entry_point2));
  test_entry_point (entry_point2, tc2);

  /* com.example.Masked is not available yet */
  entry_point3 = cby_entry_point_index_get_by_id (entry_point_index, tc3->app_name);
  g_assert_true (entry_point3 == NULL);

  /* Test installation: adding com.example.Masked to list */
  g_clear_object (&entry_point1);
  g_clear_object (&entry_point2);
  g_clear_object (&entry_point3);

  /* Now install com.example.Masked */
  tests_temp_dir_prepare_for_bundle (temp_dir, tc3->bundle_id);

  /* Nothing changed yet */
  g_assert_cmpuint (added->len, ==, 0);
  g_assert_cmpuint (changed->len, ==, 0);
  g_assert_cmpuint (removed->len, ==, 0);

  /* Trigger an update on component index so that the entry point
   * index gets reloaded */
  tests_temp_component_index_update (
      &f->component_index, TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_PLATFORM);

  while (added->len < 1)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (added->len, ==, 1);
  g_assert_cmpstr (cby_entry_point_get_id (g_ptr_array_index (added, 0)), ==,
                   tc3->app_name);
  g_assert_cmpuint (changed->len, ==, 0);
  g_assert_cmpuint (removed->len, ==, 0);

  /* Still there */
  entry_point1 = cby_entry_point_index_get_by_id (entry_point_index, tc1->app_name);
  g_assert_true (CBY_IS_ENTRY_POINT (entry_point1));
  test_entry_point (entry_point1, tc1);

  entry_point2 = cby_entry_point_index_get_by_id (entry_point_index, tc2->app_name);
  g_assert_true (CBY_IS_ENTRY_POINT (entry_point2));
  test_entry_point (entry_point2, tc2);

  /* And now it is available */
  entry_point3 = cby_entry_point_index_get_by_id (entry_point_index, tc3->app_name);
  g_assert_true (CBY_IS_ENTRY_POINT (entry_point3));
  test_entry_point (entry_point3, tc3);

  g_ptr_array_set_size (added, 0);
  g_ptr_array_set_size (changed, 0);
  g_ptr_array_set_size (removed, 0);
  g_clear_object (&entry_point1);
  g_clear_object (&entry_point2);
  g_clear_object (&entry_point3);

  /* Test removal: removing com.example.MusicPlayer from list */
  tests_temp_dir_cleanup_for_bundle (temp_dir, tc1->bundle_id);

  g_assert_cmpuint (added->len, ==, 0);
  g_assert_cmpuint (changed->len, ==, 0);
  g_assert_cmpuint (removed->len, ==, 0);

  tests_temp_component_index_update (
      &f->component_index, TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_PLATFORM);

  while (removed->len < 1)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (added->len, ==, 0);
  g_assert_cmpuint (changed->len, ==, 0);
  g_assert_cmpuint (removed->len, ==, 2);

  for (i = 0; i < removed->len; i++)
    {
      CbyEntryPoint *ep = g_ptr_array_index (removed, i);

      if (g_strcmp0 (cby_entry_point_get_id (ep), tc1->app_name))
        {
          entry_point1 = g_object_ref (ep);
        }
      else if (g_strcmp0 (cby_entry_point_get_id (ep), tc2->app_name))
        {
          entry_point2 = g_object_ref (ep);
        }
      else
        {
          g_test_message ("Unexpected entry point \"%s\" removed",
                          cby_entry_point_get_id (ep));
          g_test_fail ();
        }
    }

  g_assert_nonnull (entry_point1);
  g_assert_nonnull (entry_point2);
  g_clear_object (&entry_point1);
  g_clear_object (&entry_point2);

  entry_point1 = cby_entry_point_index_get_by_id (entry_point_index, tc1->app_name);
  g_assert_true (entry_point1 == NULL);

  /* Ok. check if we also removed com.example.MusicPlayer.Agent as expected */
  entry_point2 = cby_entry_point_index_get_by_id (entry_point_index, tc2->app_name);
  g_assert_true (entry_point2 == NULL);

  /* com.example.Masked is still there */
  entry_point3 = cby_entry_point_index_get_by_id (entry_point_index, tc3->app_name);
  g_assert_true (CBY_IS_ENTRY_POINT (entry_point3));
  test_entry_point (entry_point3, tc3);

  g_ptr_array_set_size (added, 0);
  g_ptr_array_set_size (changed, 0);
  g_ptr_array_set_size (removed, 0);
  g_clear_object (&entry_point1);
  g_clear_object (&entry_point2);
  g_clear_object (&entry_point3);

  /* Test changes in an recognised field */
  desktop_file = g_build_filename (temp_dir->xdg_data_dir, "applications",
                                   "com.example.Masked.desktop", NULL);
  g_key_file_load_from_file (editor, desktop_file, G_KEY_FILE_NONE, &error);
  g_assert_no_error (error);
  /* This is a field that Canterbury does parse and recognise */
  g_key_file_set_string (editor, G_KEY_FILE_DESKTOP_GROUP,
                         G_KEY_FILE_DESKTOP_KEY_PATH, "/foo");
  g_key_file_save_to_file (editor, desktop_file, &error);
  g_assert_no_error (error);

  tests_temp_component_index_update (
      &f->component_index, TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_PLATFORM);

  while (changed->len < 1)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (added->len, ==, 0);
  g_assert_cmpuint (changed->len, ==, 1);
  g_assert_cmpstr (cby_entry_point_get_id (g_ptr_array_index (changed, 0)), ==,
                   tc3->app_name);
  g_assert_cmpuint (removed->len, ==, 0);

  entry_point1 = cby_entry_point_index_get_by_id (entry_point_index, tc1->app_name);
  g_assert_true (entry_point1 == NULL);

  entry_point2 = cby_entry_point_index_get_by_id (entry_point_index, tc2->app_name);
  g_assert_true (entry_point2 == NULL);

  entry_point3 = cby_entry_point_index_get_by_id (entry_point_index,
                                                  tc3->app_name);
  g_assert_true (CBY_IS_ENTRY_POINT (entry_point3));
  /* Do not call test_entry_point() with tc3 here: a field has changed,
   * so it would fail. Just test one arbitrary field that did not change,
   * and one that did. */
  g_assert_cmpstr (cby_entry_point_get_display_name (entry_point3), ==,
                   tc3->app_entry_display_names[0]);
  g_assert_cmpstr (_cby_entry_point_get_working_directory (entry_point3), ==,
                   "/foo");
  custom_field =
      cby_entry_point_get_string (entry_point3,
                                  "X-WayneIndustries-BatmobileCompatible");
  g_assert_cmpstr (custom_field, ==, NULL);

  g_ptr_array_set_size (added, 0);
  g_ptr_array_set_size (changed, 0);
  g_ptr_array_set_size (removed, 0);
  g_clear_object (&entry_point1);
  g_clear_object (&entry_point2);
  g_clear_object (&entry_point3);

  /* Test changes in an unrecognised field */
  g_key_file_load_from_file (editor, desktop_file, G_KEY_FILE_NONE, &error);
  g_assert_no_error (error);
  /* This is a field that Canterbury does not parse or recognise */
  g_key_file_set_boolean (editor, G_KEY_FILE_DESKTOP_GROUP,
                          "X-WayneIndustries-BatmobileCompatible", TRUE);
  g_key_file_save_to_file (editor, desktop_file, &error);
  g_assert_no_error (error);

  tests_temp_component_index_update (
      &f->component_index, TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_PLATFORM);

  while (changed->len < 1)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (added->len, ==, 0);
  g_assert_cmpuint (changed->len, ==, 1);
  g_assert_cmpstr (cby_entry_point_get_id (g_ptr_array_index (changed, 0)), ==,
                   tc3->app_name);
  g_assert_cmpuint (removed->len, ==, 0);

  entry_point1 = cby_entry_point_index_get_by_id (entry_point_index, tc1->app_name);
  g_assert_true (entry_point1 == NULL);

  entry_point2 = cby_entry_point_index_get_by_id (entry_point_index, tc2->app_name);
  g_assert_true (entry_point2 == NULL);

  entry_point3 = cby_entry_point_index_get_by_id (entry_point_index,
                                                  tc3->app_name);
  g_assert_true (CBY_IS_ENTRY_POINT (entry_point3));
  /* Nothing changed among the known fields, except that "/foo" is still
   * as it was. */
  g_assert_cmpstr (cby_entry_point_get_display_name (entry_point3), ==,
                   tc3->app_entry_display_names[0]);
  g_assert_cmpstr (_cby_entry_point_get_working_directory (entry_point3), ==,
                   "/foo");
  /* The custom field is now present, though. */
  custom_field =
      cby_entry_point_get_string (entry_point3,
                                  "X-WayneIndustries-BatmobileCompatible");
  g_assert_cmpstr (custom_field, ==, "true");

  g_ptr_array_set_size (added, 0);
  g_ptr_array_set_size (changed, 0);
  g_ptr_array_set_size (removed, 0);
  g_clear_object (&entry_point1);
  g_clear_object (&entry_point2);
  g_clear_object (&entry_point3);
  g_clear_pointer (&custom_field, g_free);

  /* Test removal */
  tests_temp_dir_cleanup_for_bundle (temp_dir, tc3->bundle_id);

  g_assert_cmpuint (added->len, ==, 0);
  g_assert_cmpuint (changed->len, ==, 0);
  g_assert_cmpuint (removed->len, ==, 0);

  tests_temp_component_index_update (
      &f->component_index, TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_PLATFORM);

  while (removed->len < 1)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (added->len, ==, 0);
  g_assert_cmpuint (changed->len, ==, 0);
  g_assert_cmpuint (removed->len, ==, 1);

  entry_point1 = cby_entry_point_index_get_by_id (entry_point_index, tc1->app_name);
  g_assert_true (entry_point1 == NULL);

  entry_point2 = cby_entry_point_index_get_by_id (entry_point_index, tc2->app_name);
  g_assert_true (entry_point2 == NULL);

  /* Check if the com.example.Masked was removed */
  entry_point3 = cby_entry_point_index_get_by_id (entry_point_index, tc3->app_name);
  g_assert_true (entry_point3 == NULL);

  g_signal_handler_disconnect (entry_point_index, added_signal_id);
  g_signal_handler_disconnect (entry_point_index, changed_signal_id);
  g_signal_handler_disconnect (entry_point_index, removed_signal_id);
}

static void
teardown (Fixture *f, gconstpointer user_data)
{
  const TestsTempDir *temp_dir = user_data;

  tests_temp_component_index_teardown (&f->component_index);
  tests_temp_dir_teardown (temp_dir);

  alarm (0);
}

int
main (int argc, char **argv)
{
  TestsTempDir temp_dir;
  int exit_status;

  tests_temp_dir_init (&temp_dir);
  g_setenv ("XDG_DATA_HOME", temp_dir.xdg_data_dir, TRUE);

  g_test_init (&argc, &argv, NULL);

  g_test_add ("/entry-point-index/get-entry-points", Fixture, &temp_dir, setup_index,
              test_get_entry_points, teardown);
  g_test_add ("/entry-point-index/changes", Fixture, &temp_dir, setup_index,
              test_changes, teardown);

  exit_status = g_test_run ();

  tests_temp_dir_cleanup (&temp_dir);

  return exit_status;
}
