/*
 * activate: open entry points, possibly with files
 *
 * Typical usage:
 *
 * /usr/lib/installed-tests/canterbury-0/activate org.apertis.Eye
 * /usr/lib/installed-tests/canterbury-0/activate org.apertis.Rhayader https://www.collabora.com/
 *
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gio/gio.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>

#include "src/util.h"

static gchar *opt_launched_by;

static GOptionEntry entries[] = {
  { "launched-by", '\0', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING,
    &opt_launched_by,
    "A string representing the entity that initiated this activation, for "
    "example 'Launcher' or 'now-playing-key'. See canterbury_app_handler.h "
    "for interesting values.",
    "STRING" },
  { NULL }
};

static void
save_result (GObject *source_object,
             GAsyncResult *result,
             gpointer user_data)
{
  GAsyncResult **r = user_data;

  g_assert (*r == NULL);
  *r = g_object_ref (result);
}

int
main (int argc, char **argv)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GOptionContext) context = NULL;
  g_autoptr (GDBusConnection) session_bus = NULL;
  g_autoptr (CbyComponentIndex) components = NULL;
  g_autoptr (CbyEntryPointIndex) entry_points = NULL;
  g_autoptr (CbyEntryPoint) entry_point = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  GVariantDict vardict;
  GVariant *platform_data = NULL; /* floating */

  _cby_setenv_disable_services ();

  context = g_option_context_new ("- open entry points, possibly with files");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    return 2;

  session_bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);

  if (session_bus == NULL)
    goto error;

  components = cby_component_index_new (CBY_COMPONENT_INDEX_FLAGS_NONE, &error);

  if (components == NULL)
    goto error;

  entry_points = cby_entry_point_index_new (components);

  if (argc < 2)
    {
      g_printerr ("Usage: canterbury-activate ENTRY_POINT [FILE...]\n");
      return 1;
    }

  entry_point = cby_entry_point_index_get_by_id (entry_points, argv[1]);

  if (entry_point == NULL)
    {
      g_printerr ("Entry point “%s” not found\n", argv[1]);
      return 1;
    }

  g_variant_dict_init (&vardict, NULL);

  if (opt_launched_by != NULL)
    g_variant_dict_insert (&vardict, "X-Apertis-LaunchedBy", "s",
                           opt_launched_by);

  platform_data = g_variant_dict_end (&vardict);

  if (argc == 2)
    {
      cby_entry_point_activate_async (entry_point, platform_data, NULL,
                                      save_result, &result);

      while (result == NULL)
        g_main_context_iteration (NULL, TRUE);

      if (!cby_entry_point_activate_finish (entry_point, result, &error))
        goto error;
    }
  else
    {
      cby_entry_point_open_uris_async (entry_point,
                                       (const gchar * const *) &argv[2],
                                       platform_data, NULL, save_result,
                                       &result);

      while (result == NULL)
        g_main_context_iteration (NULL, TRUE);

      if (!cby_entry_point_open_uris_finish (entry_point, result, &error))
        goto error;
    }

  return 0;

error:
  g_printerr ("%s\n", error->message);
  return 1;
}
