/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <glib.h>

static gchar *unlink_on_usr1;
static gchar *unlink_on_usr2;

/*
 * prerm-apparmor-helper DIRECTORY
 *
 * Helper program for tests/postinst-prerm, to verify that AppArmor profiles
 * can be removed.
 *
 * When setup has finished, close stdout.
 *
 * On receiving SIGUSR1, attempt to unlink DIRECTORY/unlink-on-usr1 and
 * otherwise ignore the signal. In the test, we do this before the
 * AppArmor profile is removed, so it should succeed.
 *
 * On receiving SIGUSR2, attempt to unlink DIRECTORY/unlink-on-usr2,
 * then exit with status SIGUSR2. In the test, we do this after the
 * AppArmor profile is removed, so it should fail.
 */

/* An async-signal-safe version of fputs (message, stderr) */
static void
stderr_write_loop (const gchar *message)
{
  ssize_t len = 0;
  const gchar *p;

  /* Strictly speaking, strlen() is not guaranteed to be async-signal-safe */
  for (p = message; *p != '\0'; p++)
    len++;

  while (len > 0)
    {
      ssize_t written = write (STDERR_FILENO, message, len);

      /* Just give up on error, it's not as if we can complain to stderr */
      if (written <= 0)
        return;

      message += written;
      len -= written;
    }
}

static void
handler (int number)
{
  const gchar *file = NULL;

  switch (number)
    {
      case SIGUSR1:
        file = unlink_on_usr1;
        break;

      case SIGUSR2:
        file = unlink_on_usr2;
        break;

      default:
        abort ();
    }

  if (unlink (file) == 0)
    stderr_write_loop ("# Removed: ");
  else
    stderr_write_loop ("# Failed to remove: ");

  stderr_write_loop (file);
  stderr_write_loop ("\n");

  if (number == SIGUSR2)
    _exit (SIGUSR2);
}

int
main (int argc,
      char **argv)
{
  const char *dir = argv[1];
  struct sigaction action;

  /* Deliberately not freed: this process never exits normally */
  unlink_on_usr1 = g_build_filename (dir, "unlink-on-usr1", NULL);
  unlink_on_usr2 = g_build_filename (dir, "unlink-on-usr2", NULL);

  memset (&action, '\0', sizeof (action));

  action.sa_handler = handler;

  if (sigaction (SIGUSR1, &action, NULL))
    {
      perror ("sigaction SIGUSR1");
      return 1;
    }

  if (sigaction (SIGUSR2, &action, NULL))
    {
      perror ("sigaction SIGUSR2");
      return 1;
    }

  /* Signal that we are ready. */
  if (close (STDOUT_FILENO) != 0)
    {
      perror ("close stdout");
      return 1;
    }

  for (;;)
    pause ();

  /* not reached */
  return 2;
}
