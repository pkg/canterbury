/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <glib/gstdio.h>

#include <canterbury/canterbury-platform.h>

#include "canterbury/platform/component-index-internal.h"

#include "tests/common.h"

typedef struct
{
  TestsTempComponentIndex component_index;
} Fixture;

static void
setup (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  /* be killed by SIGALRM if it takes unreasonably long */
  alarm (30);
}

static void
setup_index (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  setup (f, NULL);

  tests_temp_component_index_setup (&f->component_index);
}

typedef struct
{
  const gchar *id;
  CbyProcessType type;
} GetBundleTestCase;

static const GetBundleTestCase get_bundle_test_cases[] = {
  { "com.example.MusicPlayer", CBY_PROCESS_TYPE_STORE_BUNDLE },
  { "com.example.Masked", CBY_PROCESS_TYPE_BUILT_IN_BUNDLE },
  { "net.example.wayneindustries.Theme", CBY_PROCESS_TYPE_BUILT_IN_BUNDLE },
};

static void
test_get_bundle (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (CbyComponentIndex) index = NULL;
  g_autoptr (GError) error = NULL;
  guint i;

  index = _cby_component_index_new_full (
      CBY_COMPONENT_INDEX_FLAGS_NONE, f->component_index.platform_cache,
      f->component_index.store_cache, &error);
  g_assert_no_error (error);
  g_assert (index != NULL);

  for (i = 0; i < G_N_ELEMENTS (get_bundle_test_cases); i++)
    {
      const GetBundleTestCase *tc = &get_bundle_test_cases[i];
      g_autoptr (CbyComponent) component = NULL;

      component = cby_component_index_get_app_bundle (index, tc->id);

      g_assert_cmpstr (cby_component_get_bundle_id (component), ==, tc->id);
      g_assert_cmpuint (cby_component_get_component_type (component), ==,
                        tc->type);
    }
}

typedef struct
{
  const gchar *path;
  const gchar *id;
  CbyProcessType type;
} GetProcessTestCase;

static const GetProcessTestCase get_process_test_cases[] = {
  { "/Applications/com.example.MusicPlayer/bin/player",
    "com.example.MusicPlayer", CBY_PROCESS_TYPE_STORE_BUNDLE },
  { "/usr/Applications/net.example.wayneindustries.App/bin/main",
    "net.example.wayneindustries.App", CBY_PROCESS_TYPE_BUILT_IN_BUNDLE },
  { "/tmp/foo.sh", NULL, CBY_PROCESS_TYPE_UNKNOWN },

  /* The one that masks a store app bundle */
  { "/usr/Applications/com.example.Masked/bin/main", "com.example.Masked",
    CBY_PROCESS_TYPE_BUILT_IN_BUNDLE },
  /* The one that is masked */
  { "/Applications/com.example.Masked/bin/main", NULL,
    CBY_PROCESS_TYPE_UNKNOWN },

  /* Platform processes aren't matched yet */
  { "/usr/bin/example", NULL, CBY_PROCESS_TYPE_UNKNOWN },
};

static void
test_get_process (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (CbyComponentIndex) index = NULL;
  g_autoptr (GError) error = NULL;
  guint i;

  index = _cby_component_index_new_full (
      CBY_COMPONENT_INDEX_FLAGS_NONE, f->component_index.platform_cache,
      f->component_index.store_cache, &error);
  g_assert_no_error (error);
  g_assert (index != NULL);

  for (i = 0; i < G_N_ELEMENTS (get_process_test_cases); i++)
    {
      const GetProcessTestCase *tc = &get_process_test_cases[i];
      g_autoptr (CbyComponent) component = NULL;
      g_autoptr (CbyProcessInfo) process_info = NULL;

      process_info = cby_process_info_new_for_path_and_user (
          tc->path, CBY_PROCESS_INFO_NO_USER_ID);
      component = cby_component_index_get_by_process (index, process_info);

      if (tc->id == NULL)
        {
          g_assert_null (component);
        }
      else
        {
          g_assert_true (CBY_IS_COMPONENT (component));
          g_assert_cmpstr (cby_component_get_id (component), ==, tc->id);
          g_assert_cmpuint (cby_component_get_component_type (component), ==,
                            tc->type);
        }
    }
}

static void
test_get_all (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (CbyComponentIndex) index = NULL;
  g_autoptr (GPtrArray) components = NULL;
  g_autoptr (GHashTable) hash =
      g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
  g_autoptr (GError) error = NULL;
  g_autoptr (AsApp) app_property = NULL;
  g_autofree gchar *id_property = NULL;
  CbyProcessType type;
  guint i;
  const gchar *const *categories;
  CbyComponent *component;
  AsApp *app;

  index = _cby_component_index_new_full (
      CBY_COMPONENT_INDEX_FLAGS_NONE, f->component_index.platform_cache,
      f->component_index.store_cache, &error);
  g_assert_no_error (error);
  g_assert (index != NULL);

  components = cby_component_index_get_installed_components (index);
  g_assert_nonnull (components);

  for (i = 0; i < components->len; i++)
    {
      const gchar *id;

      component = g_ptr_array_index (components, i);
      g_assert_true (CBY_IS_COMPONENT (component));
      id = cby_component_get_id (component);
      g_test_message ("Any component: %s", id);
      g_assert_false (g_hash_table_lookup_extended (hash, id, NULL, NULL));
      g_hash_table_replace (hash, g_strdup (id), g_object_ref (component));
    }

  component = g_hash_table_lookup (hash, "com.example.MusicPlayer");
  g_assert_true (CBY_IS_COMPONENT (component));
  g_assert_cmpuint (cby_component_get_component_type (component), ==,
                    CBY_PROCESS_TYPE_STORE_BUNDLE);
  categories = cby_component_get_categories (component);
  g_assert_nonnull (categories);
  g_assert_cmpstr (categories[0], ==, "Audio");
  g_assert_cmpstr (categories[1], ==, "AudioVideo");
  g_assert_cmpstr (categories[2], ==, "Music");
  g_assert_cmpstr (categories[3], ==, "X-Apertis-Foo");
  g_assert_cmpstr (categories[4], ==, "X-WayneIndustries-Foobar");
  g_assert_cmpstr (categories[5], ==, NULL);
  app = cby_component_get_appstream_app (component);
  g_assert_cmpstr (as_app_get_url_item (app, AS_URL_KIND_HOMEPAGE), ==,
                   "http://www.example.com/music-player/");
  g_assert_cmpstr (cby_component_get_settings_schema_id (component), ==,
                   "com.example.MusicPlayer");

  g_object_get (component,
                "appstream-app", &app_property,
                "component-type", &type,
                "id", &id_property,
                NULL);
  g_assert_cmpstr (id_property, ==, "com.example.MusicPlayer");
  g_assert_cmpuint (type, ==, CBY_PROCESS_TYPE_STORE_BUNDLE);
  g_assert_nonnull (app_property);
  g_assert_true (app_property == app);

  component = g_hash_table_lookup (hash, "net.example.wayneindustries.App");
  g_assert_true (CBY_IS_COMPONENT (component));
  g_assert_cmpuint (cby_component_get_component_type (component), ==,
                    CBY_PROCESS_TYPE_BUILT_IN_BUNDLE);
  categories = cby_component_get_categories (component);
  g_assert_nonnull (categories);
  g_assert_cmpstr (categories[0], ==, "X-Apertis-Example");
  g_assert_cmpstr (categories[1], ==, "X-WayneIndustries-Test");
  g_assert_cmpstr (categories[2], ==, NULL);
  g_assert_cmpstr (cby_component_get_settings_schema_id (component), ==, NULL);

  component = g_hash_table_lookup (hash, "net.example.wayneindustries.Empty");
  g_assert_true (CBY_IS_COMPONENT (component));
  g_assert_cmpuint (cby_component_get_component_type (component), ==,
                    CBY_PROCESS_TYPE_BUILT_IN_BUNDLE);
  categories = cby_component_get_categories (component);
  g_assert_nonnull (categories);
  g_assert_cmpstr (categories[0], ==, NULL);
  g_assert_cmpstr (cby_component_get_settings_schema_id (component), ==, NULL);

  component = g_hash_table_lookup (hash, "net.example.wayneindustries.Theme");
  g_assert_true (CBY_IS_COMPONENT (component));
  g_assert_cmpuint (cby_component_get_component_type (component), ==,
                    CBY_PROCESS_TYPE_BUILT_IN_BUNDLE);
  categories = cby_component_get_categories (component);
  g_assert_nonnull (categories);
  g_assert_cmpstr (categories[0], ==, NULL);
  g_assert_cmpstr (cby_component_get_settings_schema_id (component), ==, NULL);

  component = g_hash_table_lookup (hash, "example.desktop");
  g_assert_true (CBY_IS_COMPONENT (component));
  g_assert_cmpuint (cby_component_get_component_type (component), ==,
                    CBY_PROCESS_TYPE_PLATFORM);
  categories = cby_component_get_categories (component);
  g_assert_nonnull (categories);
  g_assert_cmpstr (categories[0], ==, NULL);
  g_assert_cmpstr (cby_component_get_settings_schema_id (component), ==, NULL);

  component = g_hash_table_lookup (hash, "com.example.Masked");
  g_assert_true (CBY_IS_COMPONENT (component));
  g_assert_cmpuint (cby_component_get_component_type (component), ==,
                    CBY_PROCESS_TYPE_BUILT_IN_BUNDLE);
  categories = cby_component_get_categories (component);
  g_assert_nonnull (categories);
  g_assert_cmpstr (categories[0], ==, NULL);
  g_assert_cmpstr (cby_component_get_settings_schema_id (component), ==, NULL);

  component = g_hash_table_lookup (
      hash, "org.apertis.CanterburyTests.RequestManagerClient");
  g_assert_true (CBY_IS_COMPONENT (component));
  g_assert_cmpuint (cby_component_get_component_type (component), ==,
                    CBY_PROCESS_TYPE_STORE_BUNDLE);

  component = g_hash_table_lookup (
      hash, "org.apertis.CanterburyTests.ImportantClient");
  g_assert_true (CBY_IS_COMPONENT (component));
  g_assert_cmpuint (cby_component_get_component_type (component), ==,
                    CBY_PROCESS_TYPE_STORE_BUNDLE);

  component = g_hash_table_lookup (
      hash, "org.apertis.CanterburyTests.LaunchPreferences");
  g_assert_true (CBY_IS_COMPONENT (component));
  g_assert_cmpuint (cby_component_get_component_type (component), ==,
                    CBY_PROCESS_TYPE_BUILT_IN_BUNDLE);

  g_assert_cmpuint (g_hash_table_size (hash), ==, 9);

  /* Filtered subsets */

  g_hash_table_remove_all (hash);
  g_ptr_array_unref (g_steal_pointer (&components));
  components = cby_component_index_get_built_in_bundles (index);
  g_assert_nonnull (components);

  for (i = 0; i < components->len; i++)
    {
      const gchar *id;

      component = g_ptr_array_index (components, i);
      g_assert_true (CBY_IS_COMPONENT (component));
      id = cby_component_get_id (component);
      g_test_message ("Built-in bundle: %s", id);
      g_assert_false (g_hash_table_lookup_extended (hash, id, NULL, NULL));
      g_hash_table_replace (hash, g_strdup (id), g_object_ref (component));
    }

  component = g_hash_table_lookup (hash, "net.example.wayneindustries.App");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (hash, "net.example.wayneindustries.Empty");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (hash, "net.example.wayneindustries.Theme");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (hash, "com.example.Masked");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (hash, "org.apertis.CanterburyTests.LaunchPreferences");
  g_assert_true (CBY_IS_COMPONENT (component));

  component = g_hash_table_lookup (hash, "com.example.MusicPlayer");
  g_assert_null (component);
  component = g_hash_table_lookup (hash, "example.desktop");
  g_assert_null (component);

  g_assert_cmpuint (g_hash_table_size (hash), ==, 5);

  g_hash_table_remove_all (hash);
  g_ptr_array_unref (g_steal_pointer (&components));
  components = cby_component_index_get_installed_bundles (index);
  g_assert_nonnull (components);

  for (i = 0; i < components->len; i++)
    {
      const gchar *id;

      component = g_ptr_array_index (components, i);
      g_assert_true (CBY_IS_COMPONENT (component));
      id = cby_component_get_id (component);
      g_test_message ("Any bundle: %s", id);
      g_assert_false (g_hash_table_lookup_extended (hash, id, NULL, NULL));
      g_hash_table_replace (hash, g_strdup (id), g_object_ref (component));
    }

  component = g_hash_table_lookup (hash, "net.example.wayneindustries.App");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (hash, "net.example.wayneindustries.Empty");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (hash, "net.example.wayneindustries.Theme");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (hash, "com.example.Masked");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (hash, "com.example.MusicPlayer");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (
      hash, "org.apertis.CanterburyTests.LaunchPreferences");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (hash, "example.desktop");
  g_assert_null (component);
  component = g_hash_table_lookup (
      hash, "org.apertis.CanterburyTests.RequestManagerClient");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (
      hash, "org.apertis.CanterburyTests.ImportantClient");
  g_assert_true (CBY_IS_COMPONENT (component));

  g_assert_cmpuint (g_hash_table_size (hash), ==, 8);

  g_hash_table_remove_all (hash);
  g_ptr_array_unref (g_steal_pointer (&components));
  components = cby_component_index_get_installed_store_bundles (index);
  g_assert_nonnull (components);

  for (i = 0; i < components->len; i++)
    {
      const gchar *id;

      component = g_ptr_array_index (components, i);
      g_assert_true (CBY_IS_COMPONENT (component));
      id = cby_component_get_id (component);
      g_test_message ("Store bundle: %s", id);
      g_assert_false (g_hash_table_lookup_extended (hash, id, NULL, NULL));
      g_hash_table_replace (hash, g_strdup (id), g_object_ref (component));
    }

  component = g_hash_table_lookup (hash, "net.example.wayneindustries.App");
  g_assert_null (component);
  component = g_hash_table_lookup (hash, "net.example.wayneindustries.Empty");
  g_assert_null (component);
  component = g_hash_table_lookup (hash, "net.example.wayneindustries.Theme");
  g_assert_null (component);
  /* com.example.Masked does not show up as a store app-bundle, because it's
   * masked by a built-in app-bundle of the same name. */
  component = g_hash_table_lookup (hash, "com.example.Masked");
  g_assert_null (component);
  component = g_hash_table_lookup (hash, "com.example.MusicPlayer");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (hash, "example.desktop");
  g_assert_null (component);
  component = g_hash_table_lookup (
      hash, "org.apertis.CanterburyTests.RequestManagerClient");
  g_assert_true (CBY_IS_COMPONENT (component));
  component = g_hash_table_lookup (
      hash, "org.apertis.CanterburyTests.ImportantClient");
  g_assert_true (CBY_IS_COMPONENT (component));

  g_assert_cmpuint (g_hash_table_size (hash), ==, 3);
}

static void
test_eacces (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (CbyComponentIndex) index;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *forbidden;
  gchar tmpdir[] = "cby-test-component-index-XXXXXX";

  /* Create a directory that we can't read. Any attempt to load app-bundles
   * where we can't read either the store app-bundles or the built-in app
   * bundles will fail with EACCES rather than returning incomplete data. */

  g_assert_true (g_mkdtemp_full (tmpdir, 0000) == tmpdir);

  forbidden = g_build_filename (tmpdir, "whatever", NULL);

  index = _cby_component_index_new_full (CBY_COMPONENT_INDEX_FLAGS_NONE,
                                         f->component_index.platform_cache,
                                         forbidden, &error);
  g_assert_nonnull (error);
  g_assert_null (index);
  g_clear_error (&error);

  index =
      _cby_component_index_new_full (CBY_COMPONENT_INDEX_FLAGS_NONE, forbidden,
                                     f->component_index.store_cache, &error);
  g_assert_nonnull (error);
  g_assert_null (index);
  g_clear_error (&error);

  g_assert_true (g_rmdir (tmpdir) == 0);
}

static void
inc_changes (CbyComponentIndex *index, guint *changes)
{
  g_assert (changes != NULL);

  *changes += 1;
}

static gboolean
quit_loop (gpointer data)
{
  g_main_loop_quit (data);
  return G_SOURCE_REMOVE;
}

static void
test_changes (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (CbyComponentIndex) index = NULL;
  g_autoptr (GError) error = NULL;
  gulong signal_id;
  guint changes = 0;

  index = _cby_component_index_new_full (
      CBY_COMPONENT_INDEX_FLAGS_NONE, f->component_index.platform_cache,
      f->component_index.store_cache, &error);
  g_assert_no_error (error);
  g_assert (index != NULL);

  signal_id =
      g_signal_connect (index, "changed", G_CALLBACK (inc_changes), &changes);

  g_assert_cmpuint (changes, ==, 0);

  tests_temp_component_index_update (
      &f->component_index, TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_PLATFORM);

  while (changes < 1)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (changes, ==, 1);

  if (g_test_slow ())
    {
      g_autoptr (GMainLoop) loop = g_main_loop_new (NULL, FALSE);

      g_test_message ("Tests in slow mode. Waiting 10 seconds to make sure "
                      "there is no duplicate signal.");
      g_timeout_add (10000, quit_loop, loop);
      g_main_loop_run (loop);

      g_assert_cmpuint (changes, ==, 1);
    }

  tests_temp_component_index_update (
      &f->component_index, TESTS_RUN_UPDATE_COMPONENT_INDEX_MODE_STORE);

  while (changes < 2)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (changes, ==, 2);

  if (g_test_slow ())
    {
      g_autoptr (GMainLoop) loop = g_main_loop_new (NULL, FALSE);

      g_test_message ("Tests in slow mode. Waiting 10 seconds to make sure "
                      "there is no duplicate signal.");
      g_timeout_add (10000, quit_loop, loop);
      g_main_loop_run (loop);

      g_assert_cmpuint (changes, ==, 2);
    }

  g_signal_handler_disconnect (index, signal_id);
}

static void
teardown (Fixture *f, gconstpointer unused G_GNUC_UNUSED)
{
  tests_temp_component_index_teardown (&f->component_index);

  /* cancel watchdog */
  alarm (0);
}

int
main (int argc, char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/component-index/get-bundle", Fixture, NULL, setup_index,
              test_get_bundle, teardown);
  g_test_add ("/component-index/get-process", Fixture, NULL, setup_index,
              test_get_process, teardown);
  g_test_add ("/component-index/get-all", Fixture, NULL, setup_index,
              test_get_all, teardown);
  g_test_add ("/component-index/eacces", Fixture, NULL, setup_index,
              test_eacces, teardown);
  g_test_add ("/component-index/changes", Fixture, NULL, setup_index,
              test_changes, teardown);

  return g_test_run ();
}
