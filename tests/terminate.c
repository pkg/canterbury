/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <canterbury/canterbury-platform.h>

#include "src/foreign-errors.h"
#include "tests/common.h"

#define BUNDLE_ID "org.apertis.CanterburyTests.ReluctantToExit"
#define OTHER_BUNDLE_ID "org.apertis.CanterburyTests.DoesNotExist"
#define ENTRY_POINT_ID BUNDLE_ID
#define LOGGING_IFACE BUNDLE_ID
#define SYSTEMD_UNIT ENTRY_POINT_ID ".service"
#define SYSTEMD_SLICE ENTRY_POINT_ID ".slice"
#define CBY_SYSTEM_BUS_NAME "org.apertis.Canterbury"

typedef struct
{
  TestsAppBundle bundle;
  GDBusProxy *bundle_manager1_proxy;
  guint ordinary_uid;
  guint ordinary_gid;
  guint unrelated_uid;
  guint unrelated_gid;
  GDBusConnection *session_bus;
  GDBusConnection *system_bus;
  TestsNameWatch name_watch;
  TestsSignalSubscription sigterm_subscription;
  gchar *app_unique_name;
  guint sigterm_received_times;
} Fixture;

static const gchar * const version_numbers[] = { "1.0" };

static void
app_appeared_cb (GDBusConnection *connection G_GNUC_UNUSED,
                 const gchar *name G_GNUC_UNUSED,
                 const gchar *name_owner,
                 gpointer user_data)
{
  Fixture *f = user_data;

  g_free (f->app_unique_name);
  f->app_unique_name = g_strdup (name_owner);
}

static void
app_vanished_cb (GDBusConnection *connection G_GNUC_UNUSED,
                 const gchar *name G_GNUC_UNUSED,
                 gpointer user_data)
{
  Fixture *f = user_data;

  g_free (f->app_unique_name);
  f->app_unique_name = NULL;
}

static void
sigterm_cb (GDBusConnection *connection G_GNUC_UNUSED,
            const gchar *sender G_GNUC_UNUSED,
            const gchar *path G_GNUC_UNUSED,
            const gchar *iface G_GNUC_UNUSED,
            const gchar *member G_GNUC_UNUSED,
            GVariant *parameters G_GNUC_UNUSED,
            gpointer user_data)
{
  Fixture *f = user_data;

  f->sigterm_received_times++;
}

static void
setup (Fixture *f,
       gconstpointer nil G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autofree gchar *xdg_runtime_dir = NULL;
  g_autofree gchar *session_address = NULL;
  g_autofree gchar *drop_in_dir = NULL;
  g_autofree gchar *drop_in_path = NULL;

  /* be killed by SIGALRM if it takes unreasonably long */
  alarm (60);

  tests_signal_subscription_init (&f->sigterm_subscription);

  if (!tests_require_running_as_root ())
    return;

  if (!tests_require_ordinary_uid (&f->ordinary_uid, &f->ordinary_gid,
                                   &f->unrelated_uid, &f->unrelated_gid))
    return;

  if (!tests_require_ribchester (NULL, NULL, &f->bundle_manager1_proxy))
    return;

  if (!tests_app_bundle_set_up (&f->bundle, NULL, f->bundle_manager1_proxy,
                                BUNDLE_ID, f->ordinary_uid,
                                version_numbers,
                                G_N_ELEMENTS (version_numbers)))
    return;

  /* We hard-code this on the assumption that Apertis continues to use
   * systemd-logind, which does not arbitrarily change this path, and that
   * the ordinary user already has a login session. In practice this is true
   * on all Apertis images. */
  xdg_runtime_dir = g_strdup_printf ("/run/user/%u", f->ordinary_uid);
  session_address = g_strdup_printf ("unix:path=/run/user/%u/bus",
                                     f->ordinary_uid);

  /* Connect to the ordinary user's session bus as that ordinary user.
   * We can't do that as root. Use the saved uid to keep the ability to
   * come back to being root. */
  tests_assert_syscall_0 (setresuid (f->ordinary_uid, f->ordinary_uid, 0));
  f->session_bus =
      g_dbus_connection_new_for_address_sync (session_address,
                                              (G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
                                               G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION),
                                              NULL, NULL, &error);
  tests_assert_syscall_0 (setresuid (0, 0, 0));

  g_assert_no_error (error);
  g_assert_nonnull (f->session_bus);

  f->system_bus = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (f->system_bus);

  /* Make sure the app is not initially running.
   * TODO: After fully implementing https://phabricator.apertis.org/T2621
   * we shouldn't need to do this, because removing the app-bundle would
   * stop the app automatically. */
  tuple = g_dbus_connection_call_sync (f->session_bus, SYSTEMD_BUS_NAME,
                                       SYSTEMD_PATH_MANAGER,
                                       SYSTEMD_IFACE_MANAGER,
                                       "StopUnit",
                                       g_variant_new ("(ss)", SYSTEMD_SLICE,
                                                      "replace"),
                                       NULL, G_DBUS_CALL_FLAGS_NONE, -1, NULL,
                                       &error);
  g_assert_no_error (error);
  g_clear_pointer (&tuple, g_variant_unref);

  f->name_watch =
      g_bus_watch_name_on_connection (f->session_bus, ENTRY_POINT_ID,
                                      G_BUS_NAME_WATCHER_FLAGS_NONE,
                                      app_appeared_cb, app_vanished_cb,
                                      f, NULL);

  tests_signal_subscription_set (&f->sigterm_subscription,
      f->session_bus,
      g_dbus_connection_signal_subscribe (f->session_bus,
          NULL, LOGGING_IFACE, "ReceivedTerminateSignal", NULL, NULL,
          G_DBUS_SIGNAL_FLAGS_NONE, sigterm_cb, f, NULL));

  if (!g_test_slow ())
    {
      /* Make this test less slow by making systemd less patient:
       * it would normally wait up to 10 seconds to stop something. */
      drop_in_dir = g_build_filename (xdg_runtime_dir, "systemd",
                                      "transient", SYSTEMD_UNIT ".d", NULL);
      drop_in_path = g_build_filename (drop_in_dir, "impatience.conf", NULL);
      g_mkdir_with_parents (drop_in_dir, 0755);
      g_file_set_contents (drop_in_path,
                           "[Service]\n"
                           "TimeoutStopSec=3s\n",
                           -1, &error);
      g_assert_no_error (error);
      tests_assert_syscall_0 (chown (drop_in_dir, f->ordinary_uid,
                                     f->ordinary_gid));
      tests_assert_syscall_0 (chown (drop_in_path, f->ordinary_uid,
                                     f->ordinary_gid));
    }
}

static void
test_basic (Fixture *f,
            gconstpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;
  const TestsVersion *version = &f->bundle.versions[0];

  if (g_test_failed ())
    return;

  /* If using autopkgtest, we started Canterbury in the test wrapper script.
   * If not, we assume that the image contains Canterbury and it will be
   * started automatically. */
  g_test_message ("Waiting for Canterbury...");
  tests_wait_for_name_owner (f->system_bus,
                             CBY_SYSTEM_BUS_NAME_PER_USER_APP_MANAGER1,
                             10, NULL);
  /* We told systemd to terminate this app-bundle's slice in setup(). */
  g_test_message ("Waiting for app-bundle to not be running...");
  tests_wait_for_no_name_owner (f->session_bus, ENTRY_POINT_ID, 20);
  g_test_message ("Installing app-bundle...");
  tests_app_bundle_install_bundle (&f->bundle, NULL, version);

  /* Start the app up. We do this by going behind Canterbury's back, to
   * demonstrate that this functionality is not limited to situations
   * where Canterbury actually launched it. */
  {
    g_auto (TestsScopedTimeout) timeout = 0;

    tests_scoped_timeout_start (&timeout, 20);

    while (TRUE)
      {
        g_test_message ("Trying to start app...");
        tuple = g_dbus_connection_call_sync (f->session_bus, SYSTEMD_BUS_NAME,
                                             SYSTEMD_PATH_MANAGER,
                                             SYSTEMD_IFACE_MANAGER,
                                             "StartUnit",
                                             g_variant_new ("(ss)", SYSTEMD_UNIT,
                                                            "replace"),
                                             NULL, G_DBUS_CALL_FLAGS_NONE, -1,
                                             NULL, &error);

        /* Canterbury might not have generated the unit yet. If it hasn't,
         * wait a moment and try again. */
        if (g_error_matches (error, CBY_SYSTEMD_ERROR,
                             CBY_SYSTEMD_ERROR_NO_SUCH_UNIT))
          {
            g_test_message ("No such unit - will retry");
            g_clear_error (&error);
            g_usleep (G_USEC_PER_SEC / 10);
            continue;
          }

        g_assert_no_error (error);
        g_clear_pointer (&tuple, g_variant_unref);
        break;
      }
  }

  {
    g_auto (TestsScopedTimeout) timeout = 0;

    g_test_message ("Waiting for app to start...");
    tests_scoped_timeout_start (&timeout, 20);

    while (f->app_unique_name == NULL)
      g_main_context_iteration (NULL, TRUE);
  }

  g_assert_cmpuint (f->sigterm_received_times, ==, 0);

  if (g_strcmp0 (user_data, "during-remove") == 0)
    {
      /* Integration test for T2621: removing the app-bundle causes it to be
       * terminated as a side-effect */
      tests_app_bundle_remove (&f->bundle);
    }
  else
    {
      tuple =
          g_dbus_connection_call_sync (f->system_bus,
                                       CBY_SYSTEM_BUS_NAME_PER_USER_APP_MANAGER1,
                                       CBY_OBJECT_PATH_PER_USER_APP_MANAGER1,
                                       CBY_INTERFACE_PER_USER_APP_MANAGER1,
                                       "TerminateBundle",
                                       g_variant_new ("(s)", BUNDLE_ID),
                                       G_VARIANT_TYPE_UNIT, G_DBUS_CALL_FLAGS_NONE,
                                       -1, NULL, &error);
      g_assert_no_error (error);
      g_clear_pointer (&tuple, g_variant_unref);
    }

  {
    g_auto (TestsScopedTimeout) timeout = 0;

    g_test_message ("Waiting for app to be killed...");

    /* Allow up to twice as long as it should have taken, to account for
     * D-Bus message delay etc. */
    if (g_test_slow ())
      tests_scoped_timeout_start (&timeout, 20);
    else
      tests_scoped_timeout_start (&timeout, 6);

    while (f->app_unique_name != NULL)
      g_main_context_iteration (NULL, TRUE);
  }

  /* It received SIGTERM (and lived for long enough to tell us about it)
   * before systemd became impatient and sent SIGKILL */
  g_assert_cmpuint (f->sigterm_received_times, ==, 1);
}

static void
teardown (Fixture *f,
          gconstpointer unused G_GNUC_UNUSED)
{
  /* TODO: For now, all tests that start an agent are expected to stop it
   * and wait for it to stop. When we have finished
   * https://phabricator.apertis.org/T2621 we should adjust this, because
   * Canterbury itself will stop the agent as a side-effect of removal. */
  g_assert_cmpstr (f->app_unique_name, ==, NULL);

  tests_app_bundle_tear_down (&f->bundle);
  g_clear_object (&f->bundle_manager1_proxy);

  tests_name_watch_clear (&f->name_watch);
  tests_signal_subscription_clear (&f->sigterm_subscription);
  g_clear_object (&f->session_bus);
  g_clear_object (&f->system_bus);

  /* cancel watchdog */
  alarm (0);
}

int
main (int argc,
      char **argv)
{
  cby_init_environment ();
  /* Ensure GDBus error domain is registered */
  (void) _cby_systemd_error_quark ();

  g_test_init (&argc, &argv, NULL);

  g_test_add ("/terminate/basic", Fixture, NULL, setup, test_basic, teardown);
  g_test_add ("/terminate/during-remove", Fixture, "during-remove",
              setup, test_basic, teardown);

  return g_test_run ();
}
