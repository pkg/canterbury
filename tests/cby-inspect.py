#!/usr/bin/python3
# vim:set fileencoding=utf-8:

# Copyright © 2015 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import gi
from gi.repository import (GLib, Gio)

gi.require_version('Canterbury', '0')
from gi.repository import Canterbury as Cby
gi.require_version('CanterburyPlatform', '0')
from gi.repository import CanterburyPlatform as Platform

conn = Gio.bus_get_sync(Gio.BusType.SESSION, None)

CBY_NAME_BASE = 'org.apertis.Canterbury'
CBY_BUS_NAME = 'org.apertis.Canterbury'
CBY_PATH = '/org/apertis/Canterbury'
DBUS_PROPERTIES_IFACE = 'org.freedesktop.DBus.Properties'

KNOWN_APPS = (
    'AudioPlayer',
    'Frampton-Agent',
    'Launcher',
    'Popup-Layer',
    'Rhayader-WebBrowser',
    'StatusBar',
    'VideoPlayer',
    'mildenhall-settings',
)

print('-------- Launcher menu --------')
print('')

last_cat = ''
last_cat_icon = ''

for cat, cat_icon, label, icon, thumb in sorted(
        zip(*conn.call_sync(CBY_BUS_NAME,
                            CBY_PATH + '/Launcher',
                            CBY_NAME_BASE + '.Launcher',
                            'PopulateItems',
                            None,
                            GLib.VariantType.new('(' + ('as' * 5) + ')'),
                            Gio.DBusCallFlags.NONE,
                            -1,
                            None))):
    if cat != last_cat or cat_icon != last_cat_icon:
        print(cat)
        print('[%s]' % cat_icon)

    print('    %s' % label)
    print('        [%s]' % icon)
    print('        %s' % thumb)

    last_cat = cat
    last_cat_icon = cat_icon

print('')
print('')
print('-------- Content-type handlers --------')
print('')

mime, argmime = conn.call_sync(CBY_BUS_NAME, CBY_PATH + '/AppDbHandler',
                               CBY_NAME_BASE + '.AppDbHandler', 'AppLaunchDb',
                               None, GLib.VariantType.new('(a{ss}a{ss})'),
                               Gio.DBusCallFlags.NONE, -1, None)

print('App ID => content-type:')

for k, v in sorted(mime.items()):
    print('    %s' % k)
    print('        %s' % v)

print('App ID => "argument mimes" (whatever that means):')

for k, v in sorted(argmime.items()):
    print('    %s' % k)
    print('        %s' % v)

print('')
print('')
print('-------- App info --------')
print('')

for a in sorted(KNOWN_APPS):
    icon, type_, background_state, category, cat_icon, audio_resource_type = \
        conn.call_sync(CBY_BUS_NAME, CBY_PATH + '/AppManager',
                       CBY_NAME_BASE + '.AppManager', 'GetApplicationInfo',
                       GLib.Variant('(s)', (a,)),
                       GLib.VariantType.new('(suussu)'),
                       Gio.DBusCallFlags.NONE, -1, None)

    print('%s:' % a)

    print('    icon: %s' % icon)
    print('    type: %u' % type_)
    print('    background state: %u' % background_state)
    print('    category: %s' % category)
    print('    category icon: %s' % cat_icon)
    print('    audio resource type: %s' % audio_resource_type)
    print('')

print('')
print('')
print('-------- State --------')
print('')

props, = conn.call_sync(CBY_BUS_NAME, CBY_PATH + '/AppManager',
                        DBUS_PROPERTIES_IFACE, 'GetAll',
                        GLib.Variant('(s)', (CBY_NAME_BASE + '.AppManager',)),
                        GLib.VariantType.new('(a{sv})'),
                        Gio.DBusCallFlags.NONE, -1, None)

print('Current active app: %s' % props.get('CurrentActiveApp'))
print('Current active audio source: %s' %
      props.get('CurrentActiveAudioSource'))
print('Current active app requires Internet: %s' %
      ('yes' if props.get('CurrentActiveAppRequiresInternet') else 'no'))

props, = conn.call_sync(CBY_BUS_NAME, CBY_PATH + '/Launcher',
                        DBUS_PROPERTIES_IFACE, 'GetAll',
                        GLib.Variant('(s)', (CBY_NAME_BASE + '.Launcher',)),
                        GLib.VariantType.new('(a{sv})'),
                        Gio.DBusCallFlags.NONE, -1, None)

print('Current active app: %s' % props.get('CurrentActiveMenuEntry'))

print('')
print('')

print('-------- Component index --------')

components = Platform.ComponentIndex.new(Platform.ComponentIndexFlags.NONE)
print('')
print('all components:')

for bundle in components.get_installed_components():
    print('\t{}:'.format(bundle.get_id()))
    print('\t\ttype: {}'.format(bundle.get_component_type().value_nick))
    print('\t\tcategories: {}'.format(bundle.get_categories()))

components = Platform.ComponentIndex.new(Platform.ComponentIndexFlags.NONE)
print('')
print('built-in app bundles:')

# As well as being a different list, this also demonstrates property-based
# access
for bundle in components.get_installed_bundles():
    print('\t{}:'.format(bundle.props.bundle_id))
    print('\t\ttype: {}'.format(bundle.props.component_type.value_nick))

print('')
print('')
