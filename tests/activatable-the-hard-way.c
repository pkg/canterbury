/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <gio/gio.h>

#include "canterbury/messages-internal.h"
#include "canterbury/platform/org.freedesktop.Application.h"

/*
 * activatable-the-hard-way — implement org.freedesktop.Activatable from
 * first principles, without using GApplication, to prove that it can be
 * done.
 */

#define BUNDLE_ID "org.apertis.CanterburyTests.Activatable"
#define ENTRY_POINT_ID BUNDLE_ID ".TheHardWay"
#define OBJECT_PATH "/org/apertis/CanterburyTests/Activatable/TheHardWay"
#define LOGGING_IFACE BUNDLE_ID ".Log"

static gboolean
activate_cb (_CbyOrgFreedesktopApplication *skeleton,
             GDBusMethodInvocation *invocation,
             GVariant *platform_data,
             gpointer user_data)
{
  GDBusConnection *session_bus = G_DBUS_CONNECTION (user_data);

  g_dbus_connection_emit_signal (session_bus, NULL, OBJECT_PATH, LOGGING_IFACE,
                                 "ReceivedActivate",
                                 g_variant_new ("(s)", ENTRY_POINT_ID), NULL);

  _cby_org_freedesktop_application_complete_activate (skeleton, invocation);
  return TRUE;  /* handled */
}

static gboolean
open_cb (_CbyOrgFreedesktopApplication *skeleton,
         GDBusMethodInvocation *invocation,
         const gchar * const *uris,
         GVariant *platform_data,
         gpointer user_data)
{
  GDBusConnection *session_bus = G_DBUS_CONNECTION (user_data);

  g_dbus_connection_emit_signal (session_bus, NULL, OBJECT_PATH, LOGGING_IFACE,
                                 "ReceivedOpen",
                                 g_variant_new ("(s^as)", ENTRY_POINT_ID, uris),
                                 NULL);

  _cby_org_freedesktop_application_complete_open (skeleton, invocation);
  return TRUE;  /* handled */
}

static void
new_app_state_cb (GDBusConnection *session_bus,
                  const gchar *sender,
                  const gchar *object_path,
                  const gchar *iface,
                  const gchar *signal,
                  GVariant *parameters,
                  gpointer user_data)
{
  g_autoptr (GPtrArray) arr = g_ptr_array_new_with_free_func (g_free);
  g_autoptr (GVariant) map = NULL;
  g_autofree gchar *entry_point_id = NULL;
  g_autofree gchar *k = NULL;
  g_autofree gchar *v = NULL;
  guint app_state;
  GVariantIter iter;

  if (!g_variant_is_of_type (parameters, G_VARIANT_TYPE ("(sua{ss})")))
    {
      WARNING ("Ignoring unexpected signature for NewAppState");
      return;
    }

  g_variant_get (parameters, "(su@a{ss})", &entry_point_id, &app_state, &map);

  g_variant_iter_init (&iter, map);

  while (g_variant_iter_next (&iter, "{ss}", &k, &v))
    {
      g_ptr_array_add (arr, g_steal_pointer (&k));
      g_ptr_array_add (arr, g_steal_pointer (&v));
    }

  g_ptr_array_add (arr, NULL);

  g_dbus_connection_emit_signal (session_bus, NULL, OBJECT_PATH, LOGGING_IFACE,
                                 "ReceivedNewAppState",
                                 g_variant_new ("(su^as)", entry_point_id,
                                                app_state, arr->pdata),
                                 NULL);
}

static void
register_my_app_cb (GObject *source_object,
                    GAsyncResult *result,
                    gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple =
    g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                   result, &error);

  if (tuple != NULL)
    MESSAGE ("RegisterMyApp() succeeded");
  else
    MESSAGE ("RegisterMyApp() failed: %s", error->message);
}

static GMainLoop *loop = NULL;

static void
name_acquired_cb (GDBusConnection *session_bus,
                  const gchar *name,
                  gpointer user_data)
{
}

static void
name_lost_cb (GDBusConnection *session_bus,
              const gchar *name,
              gpointer user_data)
{
  WARNING ("Name %s lost", name);
  g_main_loop_quit (user_data);
}

int
main (int argc,
      char **argv)
{
  g_autoptr (GDBusConnection) session_bus = NULL;
  g_autoptr (_CbyOrgFreedesktopApplication) skeleton = NULL;
  g_autoptr (GError) error = NULL;
  guint subscription = 0;
  guint name_ownership = 0;

  MESSAGE ("Starting");

  session_bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);

  if (session_bus == NULL)
    {
      WARNING ("Unable to conect to D-Bus: %s", error->message);
      return 1;
    }

  skeleton = _cby_org_freedesktop_application_skeleton_new ();

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (skeleton),
                                         session_bus,
                                         OBJECT_PATH,
                                         &error))
    {
      CRITICAL ("Unable to export object: %s", error->message);
      return 1;
    }

  subscription =
    g_dbus_connection_signal_subscribe (session_bus,
                                        "org.apertis.Canterbury",
                                        "org.apertis.Canterbury.AppManager",
                                        "NewAppState",
                                        "/org/apertis/Canterbury/AppManager",
                                        ENTRY_POINT_ID,
                                        G_DBUS_SIGNAL_FLAGS_NONE,
                                        new_app_state_cb, NULL, NULL);

  g_signal_connect_object (skeleton, "handle-activate",
                           G_CALLBACK (activate_cb), session_bus, 0);
  g_signal_connect_object (skeleton, "handle-open",
                           G_CALLBACK (open_cb), session_bus, 0);

  loop = g_main_loop_new (NULL, TRUE);
  name_ownership =
      g_bus_own_name_on_connection (session_bus,
                                    ENTRY_POINT_ID,
                                    G_BUS_NAME_OWNER_FLAGS_NONE,
                                    name_acquired_cb,
                                    name_lost_cb,
                                    g_main_loop_ref (loop),
                                    (GDestroyNotify) g_main_loop_unref);

  if (argc != 2 || g_strcmp0 (argv[1], "--daemon") != 0)
    {
      /* We were invoked other than via D-Bus activation, so let's work the
       * old way - use the historical Canterbury API. */

      MESSAGE ("Started via Exec=");

      /* For simplicity we do this with message-passing rather than making a
       * proxy */
      g_dbus_connection_call (session_bus, "org.apertis.Canterbury",
                              "/org/apertis/Canterbury/AppManager",
                              "org.apertis.Canterbury.AppManager",
                              "RegisterMyApp",
                              g_variant_new ("(st)", ENTRY_POINT_ID,
                                             G_GUINT64_CONSTANT (0)),
                              G_VARIANT_TYPE_UNIT,
                              G_DBUS_CALL_FLAGS_NONE,
                              -1,
                              NULL,
                              register_my_app_cb,
                              NULL);

      g_dbus_connection_emit_signal (session_bus, NULL, OBJECT_PATH,
                                     LOGGING_IFACE, "ReceivedCommandLine",
                                     g_variant_new ("(s^as)", ENTRY_POINT_ID,
                                                    argv),
                                     NULL);
    }
  else
    {
      MESSAGE ("Started via X-Apertis-ServiceExec= with --daemon command-line "
               "argument, will wait for activation");
    }

  g_main_loop_run (loop);
  g_main_loop_unref (loop);

  g_bus_unown_name (name_ownership);
  g_dbus_connection_signal_unsubscribe (session_bus, subscription);
  return 1;
}
