/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <canterbury/canterbury-platform.h>

#include "src/foreign-errors.h"
#include "tests/common.h"

#define BUNDLE_ID "org.apertis.CanterburyTests.Agent"
#define ENTRY_POINT_ID BUNDLE_ID
#define NOT_AGENT_ENTRY_POINT_ID BUNDLE_ID ".NotAgent"
#define AGENT_IFACE BUNDLE_ID
#define SYSTEMD_UNIT ENTRY_POINT_ID ".service"
#define SYSTEMD_SLICE ENTRY_POINT_ID ".slice"

/* The agent "crashes" if it notices this name on the session bus. */
#define TRIGGER BUNDLE_ID ".PleaseCrash"

typedef struct
{
  TestsAppBundle bundle;
  GDBusProxy *bundle_manager1_proxy;
  guint ordinary_uid;
  guint ordinary_gid;
  guint unrelated_uid;
  guint unrelated_gid;
  GDBusConnection *session_bus;
  TestsNameWatch agent_watch;
  TestsNameWatch not_agent_watch;
  gchar *agent_unique_name;
  gsize agent_appeared_count;
  gsize agent_vanished_count;
  gsize agent_started_count;
  TestsSignalSubscription agent_started_subscription;
  gsize agent_crash_triggered_count;
  TestsSignalSubscription agent_crash_triggered_subscription;
  /* The monotonic time (microseconds since an arbitrary point) at which we
   * most recently saw the agent's bus name appear or disappear */
  gint64 agent_appeared_time;
  gint64 agent_vanished_time;
} Fixture;

static const gchar * const version_numbers[] = { "1.0" };

static void
agent_appeared_cb (GDBusConnection *connection G_GNUC_UNUSED,
                   const gchar *name G_GNUC_UNUSED,
                   const gchar *name_owner,
                   gpointer user_data)
{
  Fixture *f = user_data;

  g_free (f->agent_unique_name);
  f->agent_unique_name = g_strdup (name_owner);
  f->agent_appeared_count++;
  f->agent_appeared_time = g_get_monotonic_time ();
}

static void
agent_vanished_cb (GDBusConnection *connection G_GNUC_UNUSED,
                   const gchar *name G_GNUC_UNUSED,
                   gpointer user_data)
{
  Fixture *f = user_data;

  g_free (f->agent_unique_name);
  f->agent_unique_name = NULL;
  f->agent_vanished_count++;
  f->agent_vanished_time = g_get_monotonic_time ();
}

static void
not_agent_appeared_cb (GDBusConnection *connection G_GNUC_UNUSED,
                       const gchar *name G_GNUC_UNUSED,
                       const gchar *name_owner G_GNUC_UNUSED,
                       gpointer user_data G_GNUC_UNUSED)
{
  g_error ("The non-agent should not have appeared at any time during this "
           "test");
}

static void
agent_started_cb (GDBusConnection *connection G_GNUC_UNUSED,
                  const gchar *sender G_GNUC_UNUSED,
                  const gchar *path G_GNUC_UNUSED,
                  const gchar *iface G_GNUC_UNUSED,
                  const gchar *member G_GNUC_UNUSED,
                  GVariant *parameters G_GNUC_UNUSED,
                  gpointer user_data)
{
  Fixture *f = user_data;

  f->agent_started_count++;
}

static void
agent_crash_triggered_cb (GDBusConnection *connection G_GNUC_UNUSED,
                          const gchar *sender G_GNUC_UNUSED,
                          const gchar *path G_GNUC_UNUSED,
                          const gchar *iface G_GNUC_UNUSED,
                          const gchar *member G_GNUC_UNUSED,
                          GVariant *parameters G_GNUC_UNUSED,
                          gpointer user_data)
{
  Fixture *f = user_data;

  f->agent_crash_triggered_count++;
}

static void
setup (Fixture *f,
       gconstpointer nil G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;

  /* be killed by SIGALRM if it takes unreasonably long */
  if (g_test_slow ())
    alarm (300);
  else
    alarm (60);

  tests_signal_subscription_init (&f->agent_started_subscription);
  tests_signal_subscription_init (&f->agent_crash_triggered_subscription);

  if (!tests_require_ordinary_uid (&f->ordinary_uid, &f->ordinary_gid,
                                   &f->unrelated_uid, &f->unrelated_gid))
    return;

  if (!tests_require_ribchester (NULL, NULL, &f->bundle_manager1_proxy))
    return;

  if (!tests_app_bundle_set_up (&f->bundle, NULL, f->bundle_manager1_proxy,
                                BUNDLE_ID, f->ordinary_uid,
                                version_numbers,
                                G_N_ELEMENTS (version_numbers)))
    return;

  if (!tests_require_real_session_bus ())
    return;

  f->session_bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (f->session_bus);

  /* Make sure systemd has forgotten about any previous failures. */
  g_dbus_connection_call_sync (f->session_bus, SYSTEMD_BUS_NAME,
                               SYSTEMD_PATH_MANAGER,
                               SYSTEMD_IFACE_MANAGER,
                               "ResetFailed",
                               NULL,
                               G_VARIANT_TYPE_UNIT, G_DBUS_CALL_FLAGS_NONE, -1,
                               NULL, &error);
  g_assert_no_error (error);

  /* The non-agent should not appear at any time during this test. This
   * is part of the test for T2711: we want to start agents, but *only*
   * agents. */
  f->not_agent_watch =
      g_bus_watch_name_on_connection (f->session_bus, NOT_AGENT_ENTRY_POINT_ID,
                                      G_BUS_NAME_WATCHER_FLAGS_NONE,
                                      not_agent_appeared_cb,
                                      NULL, NULL, NULL);

  /* Make sure the agent is not initially running.
   * TODO: After implementing https://phabricator.apertis.org/T2621
   * we shouldn't need to do this, because removing the app-bundle would
   * stop the agent automatically. */
  g_dbus_connection_call_sync (f->session_bus, SYSTEMD_BUS_NAME,
                               SYSTEMD_PATH_MANAGER,
                               SYSTEMD_IFACE_MANAGER,
                               "StopUnit",
                               g_variant_new ("(ss)", SYSTEMD_SLICE, "replace"),
                               NULL, G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);

  if (g_error_matches (error, CBY_SYSTEMD_ERROR,
                       CBY_SYSTEMD_ERROR_NO_SUCH_UNIT))
    g_clear_error (&error);

  g_assert_no_error (error);
  tests_wait_for_no_name_owner (f->session_bus, ENTRY_POINT_ID, 10);

  f->agent_watch =
      g_bus_watch_name_on_connection (f->session_bus, ENTRY_POINT_ID,
                                      G_BUS_NAME_WATCHER_FLAGS_NONE,
                                      agent_appeared_cb,
                                      agent_vanished_cb,
                                      f, NULL);

  /* The agent emits Started before it takes its well-known bus name,
   * so we have to accept any bus name here. */
  tests_signal_subscription_set (&f->agent_started_subscription,
      f->session_bus,
      g_dbus_connection_signal_subscribe (f->session_bus,
          NULL, AGENT_IFACE, "Started", NULL, NULL,
          G_DBUS_SIGNAL_FLAGS_NONE, agent_started_cb, f, NULL));

  tests_signal_subscription_set (&f->agent_crash_triggered_subscription,
      f->session_bus,
      g_dbus_connection_signal_subscribe (f->session_bus,
          ENTRY_POINT_ID, AGENT_IFACE, "CrashTriggered", NULL, NULL,
          G_DBUS_SIGNAL_FLAGS_NONE, agent_crash_triggered_cb, f, NULL));
}

static void
install_bundle_and_wait_for_agent (Fixture *f)
{
  const TestsVersion *version = &f->bundle.versions[0];
  g_autoptr (GError) error = NULL;

  /* The agent is not initially running. GDBus represents this by reporting
   * it as having vanished. */

  {
    g_auto (TestsScopedTimeout) timeout = 0;

    tests_scoped_timeout_start (&timeout, 10);

    while (f->agent_vanished_count == 0)
      g_main_context_iteration (NULL, TRUE);
  }

  g_assert_cmpstr (f->agent_unique_name, ==, NULL);
  g_assert_cmpuint (f->agent_appeared_count, ==, 0);
  g_assert_cmpuint (f->agent_appeared_time, ==, 0);
  g_assert_cmpuint (f->agent_vanished_count, ==, 1);
  g_assert_cmpuint (f->agent_vanished_time, >, 0);
  g_assert_cmpuint (f->agent_started_count, ==, 0);
  g_assert_cmpuint (f->agent_crash_triggered_count, ==, 0);

  tests_app_bundle_install_bundle (&f->bundle, NULL, version);

  /* Now that we have implemented T2711, the agent should start as a
   * side-effect of the installation of the bundle. Be a bit generous
   * with the timing, since for simplicity tests_app_bundle_install_bundle()
   * doesn't actually wait for the transaction to finish. */
  {
    g_auto (TestsScopedTimeout) timeout = 0;

    tests_scoped_timeout_start (&timeout, 30);

    while (f->agent_unique_name == NULL)
      g_main_context_iteration (NULL, TRUE);
  }

  g_assert_cmpstr (f->agent_unique_name, !=, NULL);
  g_assert_cmpuint (f->agent_appeared_count, ==, 1);
  g_assert_cmpuint (f->agent_appeared_time, >=, f->agent_vanished_time);
  g_assert_cmpuint (f->agent_vanished_count, ==, 1);
  g_assert_cmpuint (f->agent_vanished_time, >, 0);
  g_assert_cmpuint (f->agent_started_count, ==, 1);
  g_assert_cmpuint (f->agent_crash_triggered_count, ==, 0);
}

static void
remove_agent_and_wait (Fixture *f)
{
  g_auto (TestsScopedTimeout) timeout = 0;
  g_autoptr (GError) error = NULL;

  tests_app_bundle_remove (&f->bundle);

  tests_scoped_timeout_start (&timeout, 10);

  while (f->agent_unique_name != NULL)
    g_main_context_iteration (NULL, TRUE);
}

static void
test_basic (Fixture *f,
            gconstpointer nil G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;

  if (g_test_failed ())
    return;

  install_bundle_and_wait_for_agent (f);
  /* Now that we've implemented T2621, removing the agent terminates it
   * as a side-effect. */
  remove_agent_and_wait (f);

  g_assert_cmpstr (f->agent_unique_name, ==, NULL);
  g_assert_cmpuint (f->agent_appeared_count, ==, 1);
  /* 2, not 1, because detecting that it was not initially running counts as
   * vanishing */
  g_assert_cmpuint (f->agent_vanished_count, ==, 2);
  g_assert_cmpuint (f->agent_vanished_time, >=, f->agent_appeared_time);
  g_assert_cmpuint (f->agent_started_count, ==, 1);
  g_assert_cmpuint (f->agent_crash_triggered_count, ==, 0);
}

static gboolean
set_boolean_cb (gpointer data)
{
  gboolean *p = data;

  g_assert_nonnull (p);
  g_assert_cmpint (*p, ==, FALSE);
  *p = TRUE;
  return G_SOURCE_REMOVE;
}

static void
test_crash_once (Fixture *f,
                 gconstpointer nil G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;

  if (g_test_failed ())
    return;

  install_bundle_and_wait_for_agent (f);

  /* Make it crash, but only once. */

  g_dbus_connection_call_sync (f->session_bus, SYSTEMD_BUS_NAME,
                               SYSTEMD_PATH_MANAGER,
                               SYSTEMD_IFACE_MANAGER,
                               "KillUnit",
                               g_variant_new ("(ssi)", SYSTEMD_UNIT, "all",
                                              SIGSEGV),
                               G_VARIANT_TYPE_UNIT, G_DBUS_CALL_FLAGS_NONE, -1,
                               NULL, &error);
  g_assert_no_error (error);

  while (f->agent_unique_name != NULL)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpstr (f->agent_unique_name, ==, NULL);
  /* 2, not 1, because detecting that it was not initially running counts as
   * vanishing */
  g_assert_cmpuint (f->agent_vanished_count, ==, 2);
  g_assert_cmpuint (f->agent_vanished_time, >=, f->agent_appeared_time);
  g_assert_cmpuint (f->agent_appeared_count, ==, 1);
  g_assert_cmpuint (f->agent_started_count, ==, 1);
  g_assert_cmpuint (f->agent_crash_triggered_count, ==, 0);

  /* Wait for it to come back */
  while (f->agent_unique_name == NULL)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpstr (f->agent_unique_name, !=, NULL);
  /* 2, not 1, because detecting that it was not initially running counts as
   * vanishing */
  g_assert_cmpuint (f->agent_vanished_count, ==, 2);
  g_assert_cmpuint (f->agent_appeared_count, ==, 2);
  /* When the agent gets restarted, there's no guarantee that the dbus-daemon
   * will have processed the end-of-stream event on the socket from the
   * previous instance; so it's possible that the new instance fails to
   * acquire the bus name because the old instance still owns it. That's
   * OK, because the same restart-on-failure logic will make systemd retry
   * launching it. This would not normally be observable by client code
   * except as a short delay, but our test agent emits Started during
   * startup, before it takes the bus name. */
  g_assert_cmpuint (f->agent_started_count, >=, 2);
  g_assert_cmpuint (f->agent_crash_triggered_count, ==, 0);
  /* It should have restarted 5 seconds later, but have some tolerance:
   * allow between 3 and 15. */
  g_assert_cmpuint (f->agent_appeared_time, >=,
                    f->agent_vanished_time + (3 * G_USEC_PER_SEC));
  g_assert_cmpuint (f->agent_appeared_time, <=,
                    f->agent_vanished_time + (15 * G_USEC_PER_SEC));

  if (g_test_slow ())
    {
      /* It does not crash again within the next 10 seconds */
      gboolean done = FALSE;

      g_timeout_add (15000, set_boolean_cb, &done);

      while (!done)
        g_main_context_iteration (NULL, TRUE);

      g_assert_cmpstr (f->agent_unique_name, !=, NULL);
      g_assert_cmpuint (f->agent_vanished_count, ==, 2);
      g_assert_cmpuint (f->agent_appeared_count, ==, 2);
    }

  /* Now that we've implemented T2621, removing the agent terminates it
   * as a side-effect. */
  remove_agent_and_wait (f);

  g_assert_cmpstr (f->agent_unique_name, ==, NULL);
  g_assert_cmpuint (f->agent_appeared_count, ==, 2);
  g_assert_cmpuint (f->agent_started_count, >=, 2);
  g_assert_cmpuint (f->agent_vanished_count, ==, 3);
  g_assert_cmpuint (f->agent_vanished_time, >=, f->agent_appeared_time);
  /* We killed it ourselves rather than triggering it to crash. */
  g_assert_cmpuint (f->agent_crash_triggered_count, ==, 0);
}

static void
test_crash_repeatedly (Fixture *f,
                       gconstpointer nil G_GNUC_UNUSED)
{
  g_auto (TestsNameOwnership) ownership = 0;

  if (g_test_failed ())
    return;

  install_bundle_and_wait_for_agent (f);

  /* Make it crash, repeatedly. If it is restarted automatically,
   * it will notice that we are still holding this bus name and crash
   * again. */
  ownership =
      g_bus_own_name_on_connection (f->session_bus, TRIGGER,
                                    G_BUS_NAME_OWNER_FLAGS_NONE,
                                    NULL, NULL, NULL, NULL);

  while (f->agent_unique_name != NULL)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpstr (f->agent_unique_name, ==, NULL);
  /* 2, not 1, because detecting that it was not initially running counts as
   * vanishing */
  g_assert_cmpuint (f->agent_vanished_count, ==, 2);
  g_assert_cmpuint (f->agent_appeared_count, ==, 1);
  g_assert_cmpuint (f->agent_vanished_time, >=, f->agent_appeared_time);
  g_assert_cmpuint (f->agent_started_count, ==, f->agent_appeared_count);
  g_assert_cmpuint (f->agent_crash_triggered_count, ==, f->agent_started_count);

  /* It restarts, and crashes again, some number of times. */

  while (f->agent_appeared_count < (g_test_slow () ? 5 : 1))
    {
      /* It's restarted. This should be 5 seconds later, but have some
       * tolerance. */
      while (f->agent_unique_name == NULL)
        g_main_context_iteration (NULL, TRUE);

      g_assert_cmpstr (f->agent_unique_name, !=, NULL);
      g_assert_cmpuint (f->agent_vanished_count, ==, f->agent_appeared_count);
      g_assert_cmpuint (f->agent_started_count, ==, f->agent_appeared_count);
      g_assert_cmpuint (f->agent_appeared_time, >=,
                        f->agent_vanished_time + (3 * G_USEC_PER_SEC));
      g_assert_cmpuint (f->agent_appeared_time, <=,
                        f->agent_vanished_time + (10 * G_USEC_PER_SEC));

      /* It crashes again */
      while (f->agent_unique_name != NULL)
        g_main_context_iteration (NULL, TRUE);

      g_assert_cmpstr (f->agent_unique_name, ==, NULL);
      /* +1 because detecting that it was not initially running counts as
       * vanishing */
      g_assert_cmpuint (f->agent_vanished_count, ==,
                        f->agent_appeared_count + 1);
      g_assert_cmpuint (f->agent_vanished_time, >=, f->agent_appeared_time);
      g_assert_cmpuint (f->agent_started_count, ==, f->agent_appeared_count);
      g_assert_cmpuint (f->agent_crash_triggered_count, ==,
                        f->agent_started_count);
    }

  if (g_test_slow ())
    {
      /* It has reached its restart limit and does not restart again. */
      gboolean done = FALSE;

      g_timeout_add (30000, set_boolean_cb, &done);

      while (!done)
        g_main_context_iteration (NULL, TRUE);

      g_assert_cmpstr (f->agent_unique_name, ==, NULL);
      /* +1 because detecting that it was not initially running counts as
       * vanishing */
      g_assert_cmpuint (f->agent_vanished_count, ==,
                        f->agent_appeared_count + 1);
      g_assert_cmpuint (f->agent_appeared_count, ==, 5);
    }

  /* Now that we've implemented T2621, removing the agent terminates it
   * as a side-effect. */
  remove_agent_and_wait (f);

  g_assert_cmpstr (f->agent_unique_name, ==, NULL);

  if (g_test_slow ())
    g_assert_cmpuint (f->agent_appeared_count, ==, 5);
  else
    g_assert_cmpuint (f->agent_appeared_count, >=, 1);

  g_assert_cmpuint (f->agent_vanished_count, ==, f->agent_appeared_count + 1);
  g_assert_cmpuint (f->agent_vanished_time, >=, f->agent_appeared_time);
}

static void
teardown (Fixture *f,
          gconstpointer unused G_GNUC_UNUSED)
{
  /* We already removed the agent, which terminated it as a side-effect */
  g_assert_cmpstr (f->agent_unique_name, ==, NULL);

  tests_app_bundle_tear_down (&f->bundle);
  g_clear_object (&f->bundle_manager1_proxy);

  tests_name_watch_clear (&f->agent_watch);
  tests_name_watch_clear (&f->not_agent_watch);
  tests_signal_subscription_clear (&f->agent_started_subscription);
  tests_signal_subscription_clear (&f->agent_crash_triggered_subscription);
  g_clear_object (&f->session_bus);

  /* cancel watchdog */
  alarm (0);
}

int
main (int argc,
    char **argv)
{
  cby_init_environment ();
  /* Ensure GDBus error domain is registered */
  (void) _cby_systemd_error_quark ();

  g_test_init (&argc, &argv, NULL);

  g_test_add ("/agents/basic", Fixture, NULL, setup, test_basic, teardown);
  g_test_add ("/agents/crash/once", Fixture, NULL,
              setup, test_crash_once, teardown);
  g_test_add ("/agents/crash/repeatedly", Fixture, NULL,
              setup, test_crash_repeatedly, teardown);

  return g_test_run ();
}
