/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <canterbury/canterbury-platform.h>

#include "src/apparmor.h"
#include "tests/common.h"

typedef struct
{
  TestsAppBundle bundles[5];
  GDBusProxy *app_store_proxy;
  GDBusProxy *bundle_manager1_proxy;
  guint ordinary_uid;
  guint ordinary_gid;
  guint unrelated_uid;
  guint unrelated_gid;
} Fixture;

static const gchar * const version_numbers[] = { "1.0" };

/*
 * Common setup for all tests.
 */
static void
setup (Fixture *f,
       gconstpointer unused G_GNUC_UNUSED)
{
  guint i;

  if (!tests_require_ordinary_uid (&f->ordinary_uid, &f->ordinary_gid,
                                   &f->unrelated_uid, &f->unrelated_gid))
    return;

  if (!tests_require_ribchester (NULL, &f->app_store_proxy,
                                 &f->bundle_manager1_proxy))
    return;

  for (i = 0; i < G_N_ELEMENTS (f->bundles); i++)
    {
      if (!tests_app_bundle_set_up (&f->bundles[i], f->app_store_proxy,
                                    f->bundle_manager1_proxy, NULL,
                                    f->ordinary_uid, version_numbers,
                                    G_N_ELEMENTS (version_numbers)))
        return;
    }

  if (!tests_require_apparmor_active ())
    return;
}

static void
assert_apparmor_profile_can_run (const gchar *profile_name)
{
  g_autoptr (GSubprocess) child = NULL;
  g_autoptr (GError) error = NULL;

  g_test_message ("Running /bin/true under %s", profile_name);
  child = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                            &error,
                            "aa-exec",
                            "-p", profile_name,
                            "--",
                            "/bin/true",
                            NULL);
  g_assert_no_error (error);
  g_subprocess_wait_check (child, NULL, &error);
  g_assert_no_error (error);
}

static void
assert_apparmor_profile_cannot_run (const gchar *profile_name)
{
  g_autoptr (GSubprocess) child = NULL;
  g_autoptr (GError) error = NULL;

  g_test_message ("Trying to run /bin/true under %s", profile_name);
  child = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                            &error,
                            "aa-exec",
                            "-p", profile_name,
                            "--",
                            "/bin/true",
                            NULL);
  g_assert_no_error (error);
  g_subprocess_wait_check (child, NULL, &error);

  if (error == NULL)
    {
      g_test_message ("Expected error, but /bin/true exited 0");
      g_test_fail ();
      return;
    }

  if (g_error_matches (error, G_SPAWN_EXIT_ERROR, 1))
    {
      g_test_message ("→ exit 1 (probably aa-exec failed)");
      return;
    }

  if (g_error_matches (error, G_SPAWN_EXIT_ERROR, 127))
    {
      g_test_message ("→ exit 127 (probably access to executable or shared "
                      "libraries was forbidden)");
      return;
    }

  /* If we get a different exit status after upgrading AppArmor, accepting
   * that is probably OK too, but please check what changed in AppArmor
   * before changing this test's expectations. */

  g_test_message ("Expected exit status 1 or 127, but got %s %d: %s",
                  g_quark_to_string (error->domain), error->code,
                  error->message);
  g_test_fail ();
}

static void
test_apparmor (Fixture *f,
               gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (CbyAppArmorStore) store = NULL;
  g_autofree gchar *apparmor_d = NULL;
  g_autofree gchar *ubercache = NULL;
  gchar *profile_names[G_N_ELEMENTS (f->bundles)];
  gchar *profile_basenames[G_N_ELEMENTS (f->bundles)];
  gchar *source_files[G_N_ELEMENTS (f->bundles)];
  gchar *profile_contents[G_N_ELEMENTS (f->bundles)];
  gchar *symlinks[G_N_ELEMENTS (f->bundles)];
  gchar *binary_caches[G_N_ELEMENTS (f->bundles)];
  gchar template[] = "/tmp/cby-apparmor-unit.XXXXXX";
  const gchar *tmpdir;
  const gchar *rm_argv[] = { "rm", "-fr", "<tmpdir>", NULL };
  int exit_status;
  GStatBuf stat_buf;
  guint i;
  gboolean ok;

  if (g_test_failed ())
    return;

  tmpdir = g_mkdtemp (template);
  g_assert_true (tmpdir == template);
  apparmor_d = g_build_filename (tmpdir, "apparmor.d", NULL);
  tests_assert_syscall_0 (g_mkdir (apparmor_d, 0700));

  for (i = 0; i < G_N_ELEMENTS (f->bundles); i++)
    {
      profile_names[i] = g_strdup_printf ("%s/%s/**",
                                          CBY_PATH_PREFIX_STORE_BUNDLE,
                                          f->bundles[i].id);
      profile_basenames[i] = g_strdup_printf ("Applications.%s",
                                              f->bundles[i].id);
    }

  if (getuid () == 0)
    {
      g_test_message ("Loading syntactically invalid profile...");
      ok = _cby_apparmor_load_stub_profile ("«not syntactically valid»",
                                            &error);
      g_assert_error (error, G_SPAWN_EXIT_ERROR, 1);
      g_test_message ("→ %s", error->message);
      g_clear_error (&error);
      g_assert_false (ok);

      g_test_message ("Loading blank profile...");
      ok = _cby_apparmor_load_stub_profile (profile_names[0], &error);
      g_assert_no_error (error);
      g_assert_true (ok);
    }

  store = _cby_apparmor_store_open ("/nonexistent", &error);
  g_assert_error (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND);
  g_clear_error (&error);
  g_assert_null (store);

  store = _cby_apparmor_store_open (apparmor_d, &error);
  g_assert_no_error (error);
  g_assert_nonnull (store);

  ubercache = g_build_filename (apparmor_d, "cache", ".ubercache", NULL);
  g_file_set_contents (ubercache, "hello", -1, &error);
  g_assert_no_error (error);

  g_test_message ("Disabling a profile that isn't there...");
  /* ENOENT is ignored, so disabling a profile that isn't there should work. */
  ok = _cby_apparmor_store_disable_profile (store, f->bundles[0].id, &error);
  g_assert_no_error (error);
  g_assert_true (ok);

  /* Disabling a profile (even one that does not, in fact, exist) invalidates
   * the ubercache */
  tests_assert_type_nofollow (ubercache, S_IFREG);
  tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
  g_assert_cmpint (stat_buf.st_size, ==, 0);

  for (i = 0; i < G_N_ELEMENTS (f->bundles); i++)
    {
      symlinks[i] = g_build_filename (apparmor_d, profile_basenames[i], NULL);
      binary_caches[i] = g_build_filename (apparmor_d, "cache",
                                           profile_basenames[i], NULL);
      source_files[i] = g_build_filename (tmpdir, profile_basenames[i], NULL);
    }

  /* Assert that disabling a profile clears out its text source and
   * binary cache, even if the text source is not in fact a symlink,
   * and even if the other one is absent */
  g_test_message ("Disabling profiles that have only text or only binary...");
  g_file_set_contents (symlinks[0], "hello", -1, &error);
  g_assert_no_error (error);
  g_file_set_contents (binary_caches[1], "\x12\x34\x56\x78", -1, &error);
  g_assert_no_error (error);

  ok = _cby_apparmor_store_disable_profile (store, f->bundles[0].id, &error);
  g_assert_no_error (error);
  g_assert_true (ok);

  ok = _cby_apparmor_store_disable_profile (store, f->bundles[1].id, &error);
  g_assert_no_error (error);
  g_assert_true (ok);

  tests_assert_no_file (symlinks[0]);
  tests_assert_no_file (symlinks[1]);
  tests_assert_no_file (binary_caches[0]);
  tests_assert_no_file (binary_caches[1]);

  g_file_set_contents (source_files[0], "«not a valid profile»", -1, &error);
  g_assert_no_error (error);

  if (getuid () == 0)
    {
      g_file_set_contents (ubercache, "hello", -1, &error);
      g_assert_no_error (error);

      g_test_message ("Trying enabling a profile that is syntactically "
                      "invalid...");
      ok = _cby_apparmor_store_enable_and_load_profile (store, source_files[0],
                                                        f->bundles[0].id,
                                                        &error);
      /* Because it's syntactically invalid, the kernel says no. */
      g_assert_error (error, G_SPAWN_EXIT_ERROR, 1);
      g_test_message ("→ %s", error->message);
      g_clear_error (&error);
      g_assert_false (ok);
      g_test_message ("Checking that it got disabled automatically...");
      tests_assert_no_file (symlinks[0]);
      tests_assert_no_file (binary_caches[0]);

      /* Enabling a profile (even one that is not, in fact, valid) invalidates
       * the ubercache */
      tests_assert_type_nofollow (ubercache, S_IFREG);
      tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
      g_assert_cmpint (stat_buf.st_size, ==, 0);
    }

  /* Invalid because it has the wrong (single) name */
  profile_contents[0] =
    g_strdup_printf ("#include <tunables/global>\n"
                     "/Applications/com.example.TheWrongName/bin/foo {\n"
                     "  #include <abstractions/base>\n"
                     "  /bin/true mr,\n"
                     "}\n");

  /* Invalid because in addition to the expected name, it has a child
   * profile */
  profile_contents[1] =
    g_strdup_printf ("#include <tunables/global>\n"
                     "%s {\n"
                     "  #include <abstractions/base>\n"
                     "  /bin/true mr,\n"
                     "  profile some_child {\n"
                     "    #include <abstractions/base>\n"
                     "    /bin/true mr,\n"
                     "  }\n"
                     "}\n",
                     profile_names[1]);

  /* Invalid because in addition to the expected name, it has another
   * (sibling) profile */
  profile_contents[2] =
    g_strdup_printf ("#include <tunables/global>\n"
                     "%s {\n"
                     "  #include <abstractions/base>\n"
                     "  /bin/true mr,\n"
                     "}\n"
                     "/Applications/com.example.Another/nope {\n"
                     "  #include <abstractions/base>\n"
                     "  /bin/true mr,\n"
                     "}\n",
                     profile_names[2]);

  /* Invalid because we don't handle built-in application bundles. */
  profile_contents[3] =
    g_strdup_printf ("#include <tunables/global>\n"
                     "/usr%s {\n"
                     "  #include <abstractions/base>\n"
                     "  /bin/true mr,\n"
                     "}\n",
                     profile_names[3]);

  /* Invalid because the bundle ID in the name is not syntactically valid. */
  profile_contents[4] =
    g_strdup_printf ("#include <tunables/global>\n"
                     "/Applications/not-a-valid-bundle-id/** {\n"
                     "  #include <abstractions/base>\n"
                     "  /bin/true mr,\n"
                     "}\n");

  for (i = 0; i < G_N_ELEMENTS (f->bundles); i++)
    {
      if (profile_contents[i] == NULL)
        break;

      g_file_set_contents (source_files[i], profile_contents[i], -1, &error);
      g_assert_no_error (error);

      if (getuid () == 0)
        {
          g_file_set_contents (ubercache, "hello", -1, &error);
          g_assert_no_error (error);

          g_test_message ("Trying enabling a profile that does not contain "
                          "(only) the desired name (%d)...", i);
          ok = _cby_apparmor_store_enable_and_load_profile (store,
                                                            source_files[i],
                                                            f->bundles[i].id,
                                                            &error);
          /* Because it's semantically invalid, our code says no. */
          g_assert_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_DATA);
          g_test_message ("→ %s", error->message);
          g_clear_error (&error);
          g_assert_false (ok);
          g_test_message ("Checking that it got disabled automatically...");
          tests_assert_no_file (symlinks[i]);
          tests_assert_no_file (binary_caches[i]);

          /* Enabling a profile (even one that is not, in fact, valid)
           * invalidates the ubercache */
          tests_assert_type_nofollow (ubercache, S_IFREG);
          tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
          g_assert_cmpint (stat_buf.st_size, ==, 0);
        }

      g_clear_pointer (&profile_contents[i], g_free);
    }

  for (i = 0; i < G_N_ELEMENTS (f->bundles); i++)
    {
      profile_contents[i] = g_strdup_printf ("#include <tunables/global>\n"
                                             "%s {\n"
                                             "  #include <abstractions/base>\n"
                                             "  /bin/true mr,\n"
                                             "}\n", profile_names[i]);
      g_file_set_contents (source_files[i], profile_contents[i], -1, &error);
    }

  if (getuid () == 0)
    {
      for (i = 0; i < G_N_ELEMENTS (f->bundles); i++)
        {
          assert_apparmor_profile_cannot_run (profile_names[i]);

          g_test_message ("Enabling valid AppArmor profile %d...", i);
          ok = _cby_apparmor_store_enable_and_load_profile (store,
                                                            source_files[i],
                                                            f->bundles[i].id,
                                                            &error);
          g_assert_no_error (error);
          g_assert_true (ok);
          tests_assert_symlink_to (symlinks[i], source_files[i]);
          tests_assert_type_nofollow (binary_caches[i], S_IFREG);

          g_test_message ("Checking AppArmor profile %d...", i);
          assert_apparmor_profile_can_run (profile_names[i]);
          tests_remove_apparmor_profile (profile_names[i], &error);
          assert_apparmor_profile_cannot_run (profile_names[i]);
          g_assert_no_error (error);
        }
    }

  g_test_message ("Invalidating ubercache...");

  g_file_set_contents (ubercache, "hello", -1, &error);
  g_assert_no_error (error);

  ok = _cby_apparmor_store_invalidate_ubercache (store, &error);
  g_assert_no_error (error);
  g_assert_true (ok);
  tests_assert_type_nofollow (ubercache, S_IFREG);
  tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
  g_assert_cmpint (stat_buf.st_size, ==, 0);

  if (getuid () == 0)
    {
      goffset size = 0;

      /* Unlink one of the caches. It will be recompiled. */
      tests_assert_syscall_0 (g_unlink (binary_caches[0]));

      /* Make one of the caches bad. It too will be recompiled. */
      g_file_set_contents (binary_caches[1], "hello", -1, &error);

      g_test_message ("Rebuilding ubercache...");
      ok = _cby_apparmor_store_ensure_and_load_ubercache (store, &error);
      g_assert_no_error (error);
      g_assert_true (ok);

      for (i = 0; i < G_N_ELEMENTS (f->bundles); i++)
        {
          tests_assert_syscall_0 (g_stat (binary_caches[i], &stat_buf));
          size += stat_buf.st_size;
        }

      /* The ubercache is the concatenation of all the binary caches. */
      tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
      g_assert_cmpint (stat_buf.st_size, ==, size);

      for (i = 0; i < G_N_ELEMENTS (f->bundles); i++)
        {
          g_test_message ("Checking/removing AppArmor profile %d...", i);
          assert_apparmor_profile_can_run (profile_names[i]);
          tests_remove_apparmor_profile (profile_names[i], &error);
          assert_apparmor_profile_cannot_run (profile_names[i]);
          g_assert_no_error (error);

          /* Delete it, without affecting the ubercache. */
          tests_assert_syscall_0 (g_unlink (symlinks[i]));
          tests_assert_syscall_0 (g_unlink (binary_caches[i]));
        }

      /* Reload from the ubercache, which is still there (until we
       * invalidate it). */
      g_test_message ("Reloading ubercache (with profiles missing)...");
      ok = _cby_apparmor_store_ensure_and_load_ubercache (store, &error);
      g_assert_no_error (error);
      g_assert_true (ok);

      for (i = 0; i < G_N_ELEMENTS (f->bundles); i++)
        {
          g_test_message ("Checking/removing AppArmor profile %d...", i);
          assert_apparmor_profile_can_run (profile_names[i]);
          tests_remove_apparmor_profile (profile_names[i], &error);
          assert_apparmor_profile_cannot_run (profile_names[i]);
          g_assert_no_error (error);
        }

      g_test_message ("Rebuilding ubercache (without the profiles)...");
      ok = _cby_apparmor_store_invalidate_ubercache (store, &error);
      g_assert_no_error (error);
      ok = _cby_apparmor_store_ensure_and_load_ubercache (store, &error);
      g_assert_no_error (error);
      g_assert_true (ok);
      tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
      g_assert_cmpint (stat_buf.st_size, ==, 0);
    }

  for (i = 0; i < G_N_ELEMENTS (f->bundles); i++)
    {
      g_free (profile_names[i]);
      g_free (profile_basenames[i]);
      g_free (source_files[i]);
      g_free (profile_contents[i]);
      g_free (symlinks[i]);
      g_free (binary_caches[i]);
    }

  rm_argv[2] = tmpdir;
  g_spawn_sync (NULL, (gchar **) rm_argv, NULL, G_SPAWN_SEARCH_PATH,
                NULL, NULL, NULL, NULL, &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);

  if (getuid () != 0)
    {
      /* We tested a little bit if we weren't root, but not enough to really
       * be useful, so mark it as skipped. */
      g_test_skip ("Re-run as root for much better coverage");
    }
}

static void
teardown (Fixture *f,
          gconstpointer unused G_GNUC_UNUSED)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (f->bundles); i++)
    tests_app_bundle_tear_down (&f->bundles[i]);
}

int
main (int argc,
    char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/apparmor", Fixture, NULL, setup, test_apparmor, teardown);

  return g_test_run ();
}
