#!/usr/bin/python3
# vim:set fileencoding=utf-8:

# Copyright © 2017 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import gi
from gi.repository import (GLib, Gio)

import argparse
import os
import sys

import dbus.service
import dbus.mainloop.glib

import dbusmock.mockobject


class SystemdManagerMock():

    def __init__(self, bus):
        self.bus_name = dbus.service.BusName("org.freedesktop.systemd1",
                                             bus,
                                             allow_replacement=True,
                                             replace_existing=True,
                                             do_not_queue=True)

        self.object = dbusmock.mockobject.DBusMockObject(self.bus_name,
                                                         "/org/freedesktop/systemd1",
                                                         "org.freedesktop.systemd1.Manager",
                                                         props={})
        self.object.AddMethod('', 'Subscribe', '', '', '')
        self.object.AddMethod('', 'Reload', '', '', '')
        # using any valid path as we wont be checking anyway
        self.object.AddMethod('', 'StartUnit', 'ss', 'o', 'ret="/org/freedesktop/freedesktop/system1/job/123"')
        self.object.AddMethod('', 'StopUnit', 'ss', 'o', 'ret="/org/freedesktop/freedesktop/system1/job/123"')


class SubprocessLauncher():

    def __init__(self, program, finished_callback, finished_callback_params=None):
        self.finished_callback = finished_callback
        self.finished_callback_params = finished_callback_params

        self.proc = Gio.Subprocess.new(args.program, Gio.SubprocessFlags.NONE)
        self.proc.wait_check_async(None, self._on_process_finished)

    def get_exit_status(self):
        return self.proc.get_exit_status()

    def _on_process_finished(self, proc, results):
        self.finished_callback(self.finished_callback_params)


if __name__ == '__main__':

    def print_usage():
        return "%(prog)s [-h] [-s] [--] PROGRAM [ARGUMENTS]"

    def _on_process_finished(main_loop):
        main_loop.quit();

    parser = argparse.ArgumentParser(usage=print_usage())
    parser.add_argument('-s', '--system', action='store_true',
                        help='put object(s) on system bus (default: session bus)')
    parser.add_argument('program', nargs="*", help="program to run")

    args = parser.parse_args()

    main_loop = GLib.MainLoop()
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    if args.system:
        bus = dbus.SystemBus()
    else:
        bus = dbus.SessionBus()

    os.environ["USING_SYSTEMD_MOCK"] = "1";

    systemd_mock = SystemdManagerMock(bus)

    launcher = SubprocessLauncher(args.program, _on_process_finished, main_loop)

    main_loop.run()

    sys.exit(launcher.get_exit_status())
