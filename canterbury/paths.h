/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CANTERBURY_PATHS_H__
#define __CANTERBURY_PATHS_H__

#if !defined(__CANTERBURY_IN_META_HEADER__) && !defined(CANTERBURY_COMPILATION)
#error "Only <canterbury/canterbury.h> can be included directly"
#endif

#include <glib.h>

G_BEGIN_DECLS

const gchar *cby_get_persistence_path (GError **error);

G_END_DECLS

#endif
