/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CANTERBURY_PLATFORM_COMPONENT_INDEX_H
#define CANTERBURY_PLATFORM_COMPONENT_INDEX_H

#if !defined(__CANTERBURY_IN_META_HEADER__) && !defined(CANTERBURY_COMPILATION)
#error "Only <canterbury/canterbury-platform.h> can be included directly"
#endif

#include <glib.h>

#include <canterbury/bundle.h>
#include <canterbury/platform/component.h>
#include <canterbury/process-info.h>

G_BEGIN_DECLS

/**
 * CbyComponentIndexFlags:
 * @CBY_COMPONENT_INDEX_FLAGS_NONE: No special behaviour
 * @CBY_COMPONENT_INDEX_FLAGS_ONCE: Do not watch for changes to the index
 *
 * Flags affecting how a #CbyComponentIndex object is configured.
 */
typedef enum /*< flags >*/
{
  CBY_COMPONENT_INDEX_FLAGS_NONE = 0,
  CBY_COMPONENT_INDEX_FLAGS_ONCE = (1 << 0),
} CbyComponentIndexFlags;

#define CBY_TYPE_COMPONENT_INDEX (cby_component_index_get_type ())
G_DECLARE_FINAL_TYPE (
    CbyComponentIndex, cby_component_index, CBY, COMPONENT_INDEX, GObject)

CbyComponentIndex *cby_component_index_new (CbyComponentIndexFlags flags,
                                            GError **error);

GPtrArray *cby_component_index_get_built_in_bundles (CbyComponentIndex *self);
GPtrArray *cby_component_index_get_installed_bundles (CbyComponentIndex *self);
GPtrArray *cby_component_index_get_installed_components (
    CbyComponentIndex *self);
GPtrArray *cby_component_index_get_installed_store_bundles (
    CbyComponentIndex *self);
CbyComponent *cby_component_index_get_app_bundle (CbyComponentIndex *self,
                                                  const gchar *bundle_id);
CbyComponent *cby_component_index_get_by_process (CbyComponentIndex *self,
                                                  CbyProcessInfo *process_info);

G_END_DECLS

#endif
