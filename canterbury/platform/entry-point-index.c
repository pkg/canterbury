/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "entry-point-index.h"
#include "entry-point-index-internal.h"

#include "canterbury/errors.h"
#include "canterbury/gdbus/canterbury_app_handler.h"
#include "canterbury/messages-internal.h"
#include "canterbury/platform/enumtypes.h"
#include "entry-point-internal.h"

#include <string.h>

/**
 * SECTION:platform/entry-point-index.h
 * @title: CbyEntryPointIndex
 * @short_description: An index of identifiable entry points on the system
 *
 * An index of identifiable entry points on the system, represented
 * by #CbyEntryPoint objects, with change notification.
 *
 * #CbyEntryPointIndex is a #GInitable object, so it is an error to use its
 * APIs when g_initable_init() has not completed successfully.
 * The convenience constructors such as cby_entry_point_index_new()
 * and g_initable_new() do this automatically, and will not return an object
 * that cannot be used.
 *
 * Services wishing to use this API must include
 * `<abstractions/canterbury-platform-read-entry-points>`
 * in their AppArmor abstractions.
 *
 * Since: 0.1703.4
 */

struct _CbyEntryPointIndex
{
  GObject parent;

  CbyComponentIndex *component_index;

  /* Cache of { owned ID: owned CbyEntryPoint } */
  GHashTable *entries;

  gulong component_index_changed_id;
};

enum
{
  SIGNAL_BEFORE_CHANGES,
  SIGNAL_AFTER_CHANGES,
  SIGNAL_ADDED,
  SIGNAL_REMOVED,
  SIGNAL_CHANGED,
  LAST_SIGNAL
};

static guint cby_entry_point_index_signals[LAST_SIGNAL] = { 0 };

enum
{
  PROP_0,
  PROP_COMPONENT_INDEX,
  PROP_LAST
};

G_DEFINE_TYPE (CbyEntryPointIndex, cby_entry_point_index, G_TYPE_OBJECT);

static void
add_entry_point (CbyEntryPointIndex *self,
                 CbyEntryPoint *entry_point)
{
  g_hash_table_insert (self->entries,
                       g_strdup (cby_entry_point_get_id (entry_point)),
                       g_object_ref (entry_point));
  g_signal_emit (self, cby_entry_point_index_signals[SIGNAL_ADDED], 0, entry_point);
  DEBUG ("Added entry point \"%s\"", cby_entry_point_get_id (entry_point));
}

static void
remove_entry_point (CbyEntryPointIndex *self,
                    CbyEntryPoint *entry_point,
                    GHashTableIter *iter)
{
  const gchar *id = cby_entry_point_get_id (entry_point);
  g_object_ref (entry_point);

  g_return_if_fail (iter == NULL ||
                    g_hash_table_iter_get_hash_table (iter) == self->entries);

  if (iter == NULL)
    g_hash_table_remove (self->entries, id);
  else
    g_hash_table_iter_remove (iter);

  g_signal_emit (self, cby_entry_point_index_signals[SIGNAL_REMOVED], 0, entry_point);
  DEBUG ("Removed entry point \"%s\"", id);
  g_object_unref (entry_point);
}

static void
reload_entry_points (CbyEntryPointIndex *self)
{
  GList *l;
  GList *apps = NULL;
  g_autoptr (GHashTable) temp_apps = NULL; /* owner ID (without .dekstop) => unowned GAppInfo */
  GHashTableIter temp_apps_iter;
  GHashTableIter entries_iter;
  const gchar *id;
  CbyEntryPoint *entry_point_to_remove;
  GAppInfo *app;

  DEBUG ("Rebuilding application database");

  g_signal_emit (self, cby_entry_point_index_signals[SIGNAL_BEFORE_CHANGES], 0);

  /* Build a temporary hash of { CbyEntryPoint ID => GAppInfo } */
  apps = g_app_info_get_all ();
  temp_apps = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
  for (l = apps; l != NULL; l = g_list_next (l))
    {
      gchar *app_id;

      app = l->data;

      app_id = g_strdup (g_app_info_get_id (app));

      /* This is documented to be possible, but probably can't happen here */
      if (G_UNLIKELY (app_id == NULL))
        continue;

      /* chop off trailing ".desktop" */
      if (g_str_has_suffix (app_id, ".desktop"))
        {
          char *dot = strrchr (app_id, '.');

          g_assert (dot != NULL);
          *dot = '\0';
        }

      g_hash_table_insert (temp_apps, app_id, app);
    }

  /* Remove apps no longer available from the list of entry points */
  g_hash_table_iter_init (&entries_iter, self->entries);
  while (g_hash_table_iter_next (&entries_iter,
                                 (gpointer) &id,
                                 (gpointer) &entry_point_to_remove))
    {
      app = g_hash_table_lookup (temp_apps, id);
      if (app != NULL)
        continue;

      remove_entry_point (self, entry_point_to_remove, &entries_iter);
    }

  /* Update list of entry points with current and new installed apps */
  g_hash_table_iter_init (&temp_apps_iter, temp_apps);
  while (g_hash_table_iter_next (&temp_apps_iter,
                                 (gpointer) &id,
                                 (gpointer) &app))
    {
      g_autoptr (CbyEntryPoint) entry_point = NULL;
      g_autoptr (GError) error = NULL;
      gboolean update = FALSE;
      gboolean actually_changed = FALSE;

      entry_point = cby_entry_point_index_get_by_id (self, id);
      if (entry_point != NULL)
        {
          update = TRUE;

          if (!_cby_entry_point_update_from_app_info (entry_point, app,
                                                      &actually_changed,
                                                      &error))
            {
              if (g_error_matches (error, CBY_ERROR, CBY_ERROR_INVALID_ENTRY_POINT) ||
                  g_error_matches (error, CBY_ERROR, CBY_ERROR_NOT_OUR_ENTRY_POINT))
                {
                  /* We don't warn here to avoid breaking tests */
                  DEBUG ("Unable to update entry point \"%s\": \"%s\" - removing from database",
                         id, error->message);
                }
              else
                {
                  WARNING ("Unable to update entry point \"%s\": \"%s\" - removing from database",
                           id, error->message);
                }
              remove_entry_point (self, entry_point, NULL);
              continue;
            }
        }
      else
        {
          entry_point = _cby_entry_point_new_from_app_info (APPSTORE, id, app, &error);
          if (entry_point == NULL)
            {
              if (g_error_matches (error, CBY_ERROR, CBY_ERROR_INVALID_ENTRY_POINT) ||
                  g_error_matches (error, CBY_ERROR, CBY_ERROR_NOT_OUR_ENTRY_POINT))
                {
                  /* We don't warn here to avoid breaking tests */
                  DEBUG ("Unable to load entry point \"%s\": \"%s\" - ignoring entry point",
                         id, error->message);
                }
              else
                {
                  WARNING ("Unable to load entry point \"%s\": \"%s\" - ignoring entry point",
                           id, error->message);
                }
              continue;
            }
        }

      /* TODO: We could probably return some specific error if the executable type is
       * unknown and treat the error above as done for INVALID or NOT_OUR */
      if (_cby_entry_point_get_executable_type (entry_point) == CANTERBURY_EXECUTABLE_TYPE_UNKNOWN)
        {
          DEBUG ("Ignoring entry point \"%s\": unknown Apertis app type", id);
          if (update)
            remove_entry_point (self, entry_point, NULL);
        }
      else
        {
          if (!update)
            {
              add_entry_point (self, entry_point);
            }
          else if (actually_changed)
            {
              DEBUG ("Updated entry point \"%s\"", id);
              g_signal_emit (self, cby_entry_point_index_signals[SIGNAL_CHANGED], 0, entry_point);
            }
        }
    }

  g_list_free_full (apps, g_object_unref);

  g_signal_emit (self, cby_entry_point_index_signals[SIGNAL_AFTER_CHANGES], 0);
}

static void
cby_entry_point_index_init (CbyEntryPointIndex *self)
{
  self->entries =
      g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);

  reload_entry_points (self);
}

static void
component_index_changed_cb (CbyComponentIndex *index,
                            gpointer user_data)
{
  CbyEntryPointIndex *self = CBY_ENTRY_POINT_INDEX (user_data);

  reload_entry_points (self);
}

static void
cby_entry_point_index_constructed (GObject *object)
{
  CbyEntryPointIndex *self = CBY_ENTRY_POINT_INDEX (object);

  /* Every time a store app-bundle is installed, removed, upgraded or rolled back,
   * the component index gets updated. We deliberately do not use #GAppInfoMonitor
   * because that uses inotify and can update halfway through an upgrade, whereas
   * the component index only emits ::changed when we are back to a consistent state.
   */
  self->component_index_changed_id = g_signal_connect (self->component_index,
                                                       "changed",
                                                       G_CALLBACK (component_index_changed_cb),
                                                       self);
}

static void
cby_entry_point_index_dispose (GObject *object)
{
  CbyEntryPointIndex *self = CBY_ENTRY_POINT_INDEX (object);

  if (self->component_index_changed_id != 0)
    {
      g_signal_handler_disconnect (self->component_index,
                                   self->component_index_changed_id);
      self->component_index_changed_id = 0;
    }
  g_clear_pointer (&self->component_index, g_object_unref);

  g_clear_pointer (&self->entries, g_hash_table_unref);

  G_OBJECT_CLASS (cby_entry_point_index_parent_class)->dispose (object);
}

static void
cby_entry_point_index_set_property (GObject *object,
                                    guint prop_id,
                                    const GValue *value,
                                    GParamSpec *pspec)
{
  CbyEntryPointIndex *self = CBY_ENTRY_POINT_INDEX (object);

  g_return_if_fail (CBY_IS_ENTRY_POINT_INDEX (object));

  switch (prop_id)
    {
    case PROP_COMPONENT_INDEX:
      g_assert (self->component_index == NULL);
      self->component_index = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
cby_entry_point_index_class_init (CbyEntryPointIndexClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  object_class->constructed = cby_entry_point_index_constructed;

  /* all properties are write-only so there is no getter */
  object_class->set_property = cby_entry_point_index_set_property;

  object_class->dispose = cby_entry_point_index_dispose;

  /**
   * CbyEntryPointIndex::before-changes:
   * @self: the index
   *
   * Emitted when the index is about to (re)load entry points.
   *
   * Since: 0.1703.8
   */
  cby_entry_point_index_signals[SIGNAL_BEFORE_CHANGES] =
      g_signal_new ("before-changes", G_TYPE_FROM_CLASS (cls), G_SIGNAL_RUN_LAST,
                    0, NULL, NULL,
                    g_cclosure_marshal_VOID__VOID,
                    G_TYPE_NONE, 0);
  /**
   * CbyEntryPointIndex::after-changes:
   * @self: the index
   *
   * Emitted when the index is finished (re)loading entry points.
   *
   * Since: 0.1703.8
   */
  cby_entry_point_index_signals[SIGNAL_AFTER_CHANGES] =
      g_signal_new ("after-changes", G_TYPE_FROM_CLASS (cls), G_SIGNAL_RUN_LAST,
                    0, NULL, NULL,
                    g_cclosure_marshal_VOID__VOID,
                    G_TYPE_NONE, 0);
  /**
   * CbyEntryPointIndex::added:
   * @self: the index
   * @entry_point: the entry point that changed
   *
   * Emitted when a new entry point is added.
   *
   * Since: 0.1703.4
   */
  cby_entry_point_index_signals[SIGNAL_ADDED] =
      g_signal_new ("added", G_TYPE_FROM_CLASS (cls), G_SIGNAL_RUN_LAST,
                    0, NULL, NULL,
                    g_cclosure_marshal_VOID__OBJECT,
                    G_TYPE_NONE, 1, CBY_TYPE_ENTRY_POINT);
  /**
   * CbyEntryPointIndex::removed:
   * @self: the index
   * @entry_point: the entry point that changed
   *
   * Emitted when an entry point is removed.
   *
   * Since: 0.1703.4
   */
  cby_entry_point_index_signals[SIGNAL_REMOVED] =
      g_signal_new ("removed", G_TYPE_FROM_CLASS (cls), G_SIGNAL_RUN_LAST,
                    0, NULL, NULL,
                    g_cclosure_marshal_VOID__OBJECT,
                    G_TYPE_NONE, 1, CBY_TYPE_ENTRY_POINT);
  /**
   * CbyEntryPointIndex::changed:
   * @self: the index
   * @entry_point: the entry point that changed
   *
   * Emitted when an entry point is updated.
   *
   * Since: 0.1703.4
   */
  cby_entry_point_index_signals[SIGNAL_CHANGED] =
      g_signal_new ("changed", G_TYPE_FROM_CLASS (cls), G_SIGNAL_RUN_LAST,
                    0, NULL, NULL,
                    g_cclosure_marshal_VOID__OBJECT,
                    G_TYPE_NONE, 1, CBY_TYPE_ENTRY_POINT);

  g_object_class_install_property (object_class, PROP_COMPONENT_INDEX,
      g_param_spec_object ("component-index",
          "Component index", "Component index",
          CBY_TYPE_COMPONENT_INDEX,
          G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
}

CbyEntryPointIndex *
cby_entry_point_index_new (CbyComponentIndex *component_index)
{
  g_return_val_if_fail (CBY_IS_COMPONENT_INDEX (component_index), NULL);

  return g_object_new (CBY_TYPE_ENTRY_POINT_INDEX,
                       "component-index", component_index,
                       NULL);
}

/**
 * cby_entry_point_index_get_entry_points:
 * @self: Entry point index
 *
 * Builds the list of entry points in this index. The list holds a reference
 * to all entry points in the index.
 *
 * Returns: (nullable) (transfer container) (element-type CbyEntryPoint): A new array of entry
 * points in the index, or %NULL
 *
 * Since: 0.1703.4
 */
GPtrArray *
cby_entry_point_index_get_entry_points (CbyEntryPointIndex *self)
{
  GPtrArray *ret = NULL;
  GHashTableIter iter;
  gpointer value;

  g_return_val_if_fail (self != NULL, NULL);

  ret = g_ptr_array_new_with_free_func (g_object_unref);
  g_return_val_if_fail (ret != NULL, NULL);

  g_hash_table_iter_init (&iter, self->entries);
  while (g_hash_table_iter_next (&iter, NULL, &value))
    {
      g_ptr_array_add (ret, g_object_ref (value));
    }

  return ret;
}

/**
 * cby_entry_point_index_get_by_id:
 * @self: Entry point index
 * @id: Entry point identifier
 *
 * Lookup the entry point identified by @id
 *
 * Returns: (nullable) (transfer full): The entry point, or %NULL if not found
 * points in the index, or %NULL
 *
 * Since: 0.1703.4
 */
CbyEntryPoint *
cby_entry_point_index_get_by_id (CbyEntryPointIndex *self,
                                 const gchar *id)
{
  CbyEntryPoint *entry_point;

  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (id != NULL, NULL);

  entry_point = g_hash_table_lookup (self->entries, id);

  if (entry_point != NULL)
    return g_object_ref (entry_point);

  return NULL;
}
