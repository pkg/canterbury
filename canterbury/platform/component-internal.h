/*<private_header>*/
/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CANTERBURY_PLATFORM_COMPONENT_INTERNAL_H__
#define __CANTERBURY_PLATFORM_COMPONENT_INTERNAL_H__

#include "canterbury/platform/component.h"

#define CBY_APPSTREAM_X_BUNDLE_ID "X-Apertis-BundleID"
#define CBY_APPSTREAM_X_SETTINGS_SCHEMA "X-Apertis-SettingsSchema"

#endif
