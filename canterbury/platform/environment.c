/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "canterbury/platform/environment.h"

#include "canterbury/platform/paths.h"

/**
 * SECTION:platform/environment.h
 * @title: Environment related functions
 * @short_description: Functions to init/setup environment
 */

/**
 * cby_init_environment:
 *
 * Alter the process environment (process-global state) so that standard
 * GLib and GTK+ APIs such as #GtkIconTheme and #GAppInfo will find
 * all installed app-bundles.
 *
 * As this function uses g_setenv() to set some environment variables, you
 * should be mindful that this method is not thread-safe, making it only safe
 * to use at the very start of your program, before creating any other threads
 * (or creating objects that create worker threads of their own).
 * Please refer to g_setenv() docs for more info.
 *
 * Also note that Apertis has a general security policy that unprivileged
 * app-bundles are not allowed to enumerate other app-bundles, either directly
 * or by enumerating entry points. For this function to be useful, the
 * calling process must have sufficient privileges to bypass that
 * security policy, for example by adding
 * `#include <abstractions/canterbury-platform-read-entry-points>`
 * to its AppArmor profile.
 *
 * Since: 0.1612.3
 */
void
cby_init_environment (void)
{
  static gsize initialized = 0;

  if (g_once_init_enter (&initialized))
    {
      /* Tell GLib/GTK+ to read .desktop files/icons from the app framework's
       * paths */
      g_setenv ("XDG_DATA_DIRS",
                CBY_PATH_SYSTEM_EXTENSIONS ":"
                "/var/lib/MILDENHALL_extensions:"
                "/usr/local/share:/usr/share", TRUE);
      /* For the purposes of OnlyShowIn, we are Apertis */
      g_setenv ("XDG_CURRENT_DESKTOP", "Apertis", TRUE);

      g_once_init_leave (&initialized, 1);
    }
}
