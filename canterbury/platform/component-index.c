/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "canterbury/platform/component-index-internal.h"

#include <appstream-glib.h>

#include "canterbury/bundle.h"
#include "canterbury/enumtypes.h"
#include "canterbury/platform/component-internal.h"
#include "canterbury/platform/enumtypes.h"
#include "canterbury/process-info.h"

/**
 * SECTION:platform/component-index.h
 * @title: CbyComponentIndex
 * @short_description: An index of identifiable software components on the system
 *
 * An index of identifiable software components on the system, represented
 * by #CbyComponent objects, with change notification.
 *
 * #CbyComponentIndex is a #GInitable object, so it is an error to use its
 * APIs when g_initable_init() has not completed successfully.
 * The convenience constructors such as cby_component_index_new()
 * and g_initable_new() do this automatically, and will not return an object
 * that cannot be used.
 *
 * Services wishing to use this API must include
 * `<abstractions/canterbury-platform-read-entry-points>`
 * in their AppArmor abstractions.
 *
 * Since: 0.11.0
 */

struct _CbyComponentIndex
{
  GObject parent;

  gchar *store_cache_path;
  AsStore *store_cache;

  gchar *platform_cache_path;
  AsStore *platform_cache;

  /* Cache of { owned ID: owned CbyComponent }, invalidated when one of
   * the stores emits changed */
  GHashTable *components;

  GError *init_error;

  gulong store_cache_changed_id;
  gulong platform_cache_changed_id;
  CbyComponentIndexFlags flags;
  gboolean init_success;
};

static guint signal_changed;
static void initable_iface_init (GInitableIface *initable_iface);

typedef enum
{
  PROP_FLAGS = 1,
  PROP_PLATFORM_CACHE_PATH,
  PROP_STORE_CACHE_PATH,
} Property;

static GParamSpec *property_specs[PROP_STORE_CACHE_PATH + 1] = { NULL };

G_DEFINE_TYPE_WITH_CODE (CbyComponentIndex,
                         cby_component_index,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                initable_iface_init))

static void
invalidate_cache_cb (AsStore *source, CbyComponentIndex *self)
{
  g_hash_table_remove_all (self->components);
  g_signal_emit (self, signal_changed, 0);
}

static void
cby_component_index_init (CbyComponentIndex *self)
{
  self->components =
      g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
}

static void
cby_component_index_dispose (GObject *object)
{
  CbyComponentIndex *self = CBY_COMPONENT_INDEX (object);

  if (self->store_cache_changed_id != 0)
    {
      g_signal_handler_disconnect (self->store_cache,
                                   self->store_cache_changed_id);
      self->store_cache_changed_id = 0;
    }

  g_clear_object (&self->store_cache);

  if (self->platform_cache_changed_id != 0)
    {
      g_signal_handler_disconnect (self->platform_cache,
                                   self->platform_cache_changed_id);
      self->platform_cache_changed_id = 0;
    }

  g_clear_object (&self->platform_cache);

  G_OBJECT_CLASS (cby_component_index_parent_class)->dispose (object);
}

static void
cby_component_index_set_property (GObject *object,
                                  guint prop_id,
                                  const GValue *value,
                                  GParamSpec *pspec)
{
  CbyComponentIndex *self = CBY_COMPONENT_INDEX (object);

  g_return_if_fail (CBY_IS_COMPONENT_INDEX (object));
  g_return_if_fail (self->init_error == NULL);
  /* cannot assert self->init_success because properties are set before the
   * object is ready to initialize */

  switch ((Property) prop_id)
    {
    case PROP_FLAGS:
      g_assert (self->flags == 0);
      self->flags = g_value_get_flags (value);
      break;

    case PROP_PLATFORM_CACHE_PATH:
      g_assert (self->platform_cache_path == NULL);
      self->platform_cache_path = g_value_dup_string (value);
      break;

    case PROP_STORE_CACHE_PATH:
      g_assert (self->store_cache_path == NULL);
      self->store_cache_path = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
cby_component_index_class_init (CbyComponentIndexClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  /* all properties are write-only so there is no getter */
  object_class->set_property = cby_component_index_set_property;
  object_class->dispose = cby_component_index_dispose;

  /**
   * CbyComponentIndex::changed:
   *
   * Emitted when the list of installed components has changed. The
   * results of most methods are invalidated and should be re-retrieved.
   *
   * Since: 0.11.0
   */
  signal_changed =
      g_signal_new ("changed", G_TYPE_FROM_CLASS (cls), G_SIGNAL_RUN_LAST, 0,
                    NULL, NULL, NULL, G_TYPE_NONE, 0);

  /**
   * CbyComponentIndex:flags:
   *
   * Flags affecting how the bundle registry behaves.
   *
   * Since: 0.11.0
   */
  property_specs[PROP_FLAGS] = g_param_spec_flags (
      "flags", "Flags", "Flags", CBY_TYPE_COMPONENT_INDEX_FLAGS,
      CBY_COMPONENT_INDEX_FLAGS_NONE,
      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  /**
   * CbyComponentIndex:built-in-cache-path:
   *
   * The cache file to monitor for built-in app bundles, or %NULL to use
   * a default.
   *
   * Since: 0.11.0
   */
  property_specs[PROP_PLATFORM_CACHE_PATH] = g_param_spec_string (
      "platform-cache-path", "Platform cache path",
      "Path to cache file containing the platform and built-in app-bundles",
      NULL, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  /**
   * CbyComponentIndex:store-cache-path:
   *
   * The cache file to monitor for installed store app bundles, or %NULL to
   * use a default.
   *
   * Since: 0.11.0
   */
  property_specs[PROP_STORE_CACHE_PATH] = g_param_spec_string (
      "store-cache-path", "Store cache path",
      "Path to cache file containing installed store app-bundles", NULL,
      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties (
      object_class, G_N_ELEMENTS (property_specs), property_specs);
}

static gboolean
cby_component_index_initable_init (GInitable *initable,
                                   GCancellable *cancellable,
                                   GError **error)
{
  CbyComponentIndex *self = CBY_COMPONENT_INDEX (initable);

  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!self->init_success)
    {
      g_autoptr (GFile) platform_cache_file = NULL;
      g_autoptr (GFile) store_cache_file = NULL;

      if (self->platform_cache_path == NULL)
        platform_cache_file =
            g_file_new_for_path (CBY_APPSTREAM_CACHE_PLATFORM);
      else
        platform_cache_file = g_file_new_for_path (self->platform_cache_path);

      if (self->store_cache_path == NULL)
        store_cache_file =
            g_file_new_for_path (CBY_APPSTREAM_CACHE_INSTALLED_STORE);
      else
        store_cache_file = g_file_new_for_path (self->store_cache_path);

      self->store_cache = as_store_new ();
      self->platform_cache = as_store_new ();

      if (!(self->flags & CBY_COMPONENT_INDEX_FLAGS_ONCE))
        {
          self->store_cache_changed_id =
              g_signal_connect (self->store_cache, "changed",
                                G_CALLBACK (invalidate_cache_cb), self);
          self->platform_cache_changed_id =
              g_signal_connect (self->platform_cache, "changed",
                                G_CALLBACK (invalidate_cache_cb), self);

          as_store_set_watch_flags (
              self->store_cache,
              (AS_STORE_WATCH_FLAG_ADDED | AS_STORE_WATCH_FLAG_REMOVED));
          as_store_set_watch_flags (
              self->platform_cache,
              (AS_STORE_WATCH_FLAG_ADDED | AS_STORE_WATCH_FLAG_REMOVED));
        }

      as_store_set_origin (self->platform_cache, "apertis-built-in");

      if (!as_store_from_file (self->platform_cache, platform_cache_file, NULL,
                               NULL, &self->init_error))
        goto fail;

      as_store_set_origin (self->platform_cache, "apertis-installed-store");

      if (!as_store_from_file (self->store_cache, store_cache_file, NULL, NULL,
                               &self->init_error))
        goto fail;

      self->init_success = TRUE;
    }

  if (self->init_error == NULL)
    return TRUE;

fail:
  g_set_error_literal (error, self->init_error->domain, self->init_error->code,
                       self->init_error->message);
  return FALSE;
}

static CbyComponent *
ensure_component (CbyComponentIndex *self,
                  const gchar *id,
                  CbyProcessType type,
                  AsApp *app)
{
  CbyComponent *component;

  component = g_hash_table_lookup (self->components, id);

  if (component != NULL)
    {
      g_assert (cby_component_get_component_type (component) == type);
      return g_object_ref (component);
    }

  /* a new reference, which is returned */
  component = cby_component_new_for_appstream_app (type, app, NULL);

  if (component == NULL)
    return NULL;

  g_hash_table_insert (self->components, g_strdup (id),
                       g_object_ref (component));
  return component;
}

typedef enum
{
  GET_STORE = (1 << 0),
  GET_BUILT_IN = (1 << 1),
  GET_PLATFORM = (1 << 2),
} GetFlags;

static GPtrArray *
cby_component_index_get (CbyComponentIndex *self, GetFlags flags)
{
  g_autoptr (GPtrArray) arr = g_ptr_array_new_with_free_func (g_object_unref);

  if (flags & (GET_BUILT_IN | GET_PLATFORM))
    {
      GPtrArray *apps = as_store_get_apps (self->platform_cache);
      guint i;

      for (i = 0; i < apps->len; i++)
        {
          AsApp *app = g_ptr_array_index (apps, i);
          CbyProcessType type;
          g_autoptr (CbyComponent) component = NULL;
          const gchar *id;

          id = as_app_get_metadata_item (app, CBY_APPSTREAM_X_BUNDLE_ID);

          if (id != NULL)
            {
              if (g_strcmp0 (as_app_get_id (app), id) != 0)
                {
                  g_warn_if_reached ();
                  continue;
                }

              type = CBY_PROCESS_TYPE_BUILT_IN_BUNDLE;

              if (!(flags & GET_BUILT_IN))
                continue;
            }
          else
            {
              id = as_app_get_id (app);
              type = CBY_PROCESS_TYPE_PLATFORM;

              if (!(flags & GET_PLATFORM))
                continue;
            }

          component = ensure_component (self, id, type, app);

          if (component != NULL)
            g_ptr_array_add (arr, g_steal_pointer (&component));
        }
    }

  if (flags & GET_STORE)
    {
      GPtrArray *apps = as_store_get_apps (self->store_cache);
      guint i;

      for (i = 0; i < apps->len; i++)
        {
          AsApp *app = g_ptr_array_index (apps, i);
          CbyProcessType type = CBY_PROCESS_TYPE_STORE_BUNDLE;
          g_autoptr (CbyComponent) component = NULL;
          const gchar *id;

          id = as_app_get_metadata_item (app, CBY_APPSTREAM_X_BUNDLE_ID);

          if (G_UNLIKELY (id == NULL ||
                          g_strcmp0 (as_app_get_id (app), id) != 0))
            {
              g_warn_if_reached ();
              continue;
            }

          /* Built-in apps and platform components mask store apps of the same
           * name. */
          if (as_store_get_app_by_id (self->platform_cache, id) != NULL)
            continue;

          component = ensure_component (self, id, type, app);

          if (component != NULL)
            g_ptr_array_add (arr, g_steal_pointer (&component));
        }
    }

  return g_steal_pointer (&arr);
}

/**
 * cby_component_index_get_installed_components:
 * @self: the index
 *
 * List all the installed components regardless of their types.
 *
 * The returned array is no longer up-to-date after #CbyComponentIndex::changed
 * has been emitted.
 *
 * Returns: (transfer container) (element-type CanterburyPlatform.Component):
 *  an array of software components
 *
 * Since: 0.11.0
 */
GPtrArray *
cby_component_index_get_installed_components (CbyComponentIndex *self)
{
  g_return_val_if_fail (CBY_IS_COMPONENT_INDEX (self), NULL);
  g_return_val_if_fail (self->init_error == NULL, NULL);
  g_return_val_if_fail (self->init_success, NULL);

  return cby_component_index_get (self,
                                  GET_STORE | GET_BUILT_IN | GET_PLATFORM);
}

/**
 * cby_component_index_get_installed_bundles:
 * @self: the index
 *
 * List all the installed app-bundles, whether they are store or built-in
 * app-bundles.
 *
 * The returned array is no longer up-to-date after #CbyComponentIndex::changed
 * has been emitted.
 *
 * Returns: (transfer container) (element-type CanterburyPlatform.Component):
 *  an array of built-in and store app-bundles
 *
 * Since: 0.11.0
 */
GPtrArray *
cby_component_index_get_installed_bundles (CbyComponentIndex *self)
{
  g_return_val_if_fail (CBY_IS_COMPONENT_INDEX (self), NULL);
  g_return_val_if_fail (self->init_error == NULL, NULL);
  g_return_val_if_fail (self->init_success, NULL);

  return cby_component_index_get (self, GET_STORE | GET_BUILT_IN);
}

/**
 * cby_component_index_get_installed_store_bundles:
 * @self: the index
 *
 * List all the installed store app-bundles.
 *
 * The returned array is no longer up-to-date after #CbyComponentIndex::changed
 * has been emitted.
 *
 * Returns: (transfer container) (element-type CanterburyPlatform.Component):
 *  an array of store app-bundles
 *
 * Since: 0.11.0
 */
GPtrArray *
cby_component_index_get_installed_store_bundles (CbyComponentIndex *self)
{
  g_return_val_if_fail (CBY_IS_COMPONENT_INDEX (self), NULL);
  g_return_val_if_fail (self->init_error == NULL, NULL);
  g_return_val_if_fail (self->init_success, NULL);

  return cby_component_index_get (self, GET_STORE);
}

/**
 * cby_component_index_get_built_in_bundles:
 * @self: the index
 *
 * List all the built-in app-bundles.
 *
 * The returned array is no longer up-to-date after #CbyComponentIndex::changed
 * has been emitted.
 *
 * Returns: (transfer container) (element-type CanterburyPlatform.Component):
 *  an array of built-in app-bundles
 *
 * Since: 0.11.0
 */
GPtrArray *
cby_component_index_get_built_in_bundles (CbyComponentIndex *self)
{
  g_return_val_if_fail (CBY_IS_COMPONENT_INDEX (self), NULL);
  g_return_val_if_fail (self->init_error == NULL, NULL);
  g_return_val_if_fail (self->init_success, NULL);

  return cby_component_index_get (self, GET_BUILT_IN);
}

/**
 * cby_component_index_new:
 * @flags: flags affecting how to set up the index
 * @error: used to raise an error on failure
 *
 * Return a new index using the standard cache paths.
 *
 * Returns: (transfer full): a new index of components
 *
 * Since: 0.11.0
 */
CbyComponentIndex *
cby_component_index_new (CbyComponentIndexFlags flags, GError **error)
{
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return _cby_component_index_new_full (flags, NULL, NULL, error);
}

/**
 * cby_component_index_get_app_bundle:
 * @self: the index
 * @bundle_id: the bundle in question
 *
 * Return the #CbyComponent corresponding to the installed app-bundle with
 * the given identifier.
 *
 * Returns: (nullable) (transfer full): a new reference to a #CbyComponent,
 *  or %NULL if not found
 *
 * Since: 0.11.0
 */
CbyComponent *
cby_component_index_get_app_bundle (CbyComponentIndex *self,
                                    const gchar *bundle_id)
{
  AsApp *app = NULL; /* unowned */

  g_return_val_if_fail (CBY_IS_COMPONENT_INDEX (self), NULL);
  g_return_val_if_fail (self->init_error == NULL, NULL);
  g_return_val_if_fail (self->init_success, NULL);
  g_return_val_if_fail (cby_is_bundle_id (bundle_id), NULL);

  app = as_store_get_app_by_id (self->platform_cache, bundle_id);

  if (app != NULL)
    {
      if (g_strcmp0 (as_app_get_metadata_item (app, CBY_APPSTREAM_X_BUNDLE_ID),
                     bundle_id) == 0)
        return ensure_component (self, bundle_id,
                                 CBY_PROCESS_TYPE_BUILT_IN_BUNDLE, app);
      else /* platform component, not a bundle */
        return NULL;
    }

  app = as_store_get_app_by_id (self->store_cache, bundle_id);

  if (app != NULL)
    {
      if (g_strcmp0 (as_app_get_metadata_item (app, CBY_APPSTREAM_X_BUNDLE_ID),
                     bundle_id) == 0)
        return ensure_component (self, bundle_id,
                                 CBY_PROCESS_TYPE_STORE_BUNDLE, app);
      else
        return NULL;
    }

  return NULL;
}

/**
 * cby_component_index_get_by_process:
 * @self: the index
 * @process_info: information about a process
 *
 * Return the #CbyComponent corresponding to the installed component matching
 * the given process information, or %NULL if it was not found or there was
 * not enough information to make the decision.
 *
 * Returns: (nullable) (transfer full): a new reference to a #CbyComponent,
 *  or %NULL
 *
 * Since: 0.11.0
 */
CbyComponent *
cby_component_index_get_by_process (CbyComponentIndex *self,
                                    CbyProcessInfo *process_info)
{
  g_autoptr (CbyComponent) component = NULL;
  CbyProcessType type;
  const gchar *bundle_id;

  g_return_val_if_fail (CBY_IS_COMPONENT_INDEX (self), NULL);
  g_return_val_if_fail (self->init_error == NULL, NULL);
  g_return_val_if_fail (self->init_success, NULL);
  g_return_val_if_fail (CBY_IS_PROCESS_INFO (process_info), NULL);

  type = cby_process_info_get_process_type (process_info);

  switch (type)
    {
    case CBY_PROCESS_TYPE_UNKNOWN:
      /* not enough information */
      return NULL;

    case CBY_PROCESS_TYPE_PLATFORM:
      /* TODO: work out a way to relate a platform process like Newport to a
       * component, perhaps by adding X-Apertis-AppArmorProfile metadata?
       * Part of https://phabricator.apertis.org/T2376 */
      return NULL;

    case CBY_PROCESS_TYPE_BUILT_IN_BUNDLE:
    case CBY_PROCESS_TYPE_STORE_BUNDLE:
      bundle_id = cby_process_info_get_bundle_id (process_info);
      component = cby_component_index_get_app_bundle (self, bundle_id);

      if (component != NULL &&
          cby_component_get_component_type (component) == type)
        return g_steal_pointer (&component);
      else
        return NULL;

    default:
      g_return_val_if_reached (NULL);
    }
}

static void
initable_iface_init (GInitableIface *initable_iface)
{
  initable_iface->init = cby_component_index_initable_init;
}
