/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#if !defined(__CANTERBURY_IN_META_HEADER__) && !defined(CANTERBURY_COMPILATION)
#error "Only <canterbury/canterbury-platform.h> can be included directly"
#endif

#include <glib.h>

G_BEGIN_DECLS

/**
 * SECTION:platform/dbus.h
 * @title: Platform D-Bus constants
 * @short_description: Constants related to D-Bus interfaces
 */

/**
 * CBY_SESSION_BUS_NAME:
 *
 * Canterbury's bus name on the session bus.
 *
 * Since: 0.1709.0
 */
#define CBY_SESSION_BUS_NAME \
  "org.apertis.Canterbury"

/**
 * CBY_SYSTEM_BUS_NAME_RIBCHESTER:
 *
 * Ribchester's bus name on the system bus, used to implement the
 * #CbyPrivilegedAppHelper1.
 *
 * Since: 0.1709.0
 */
#define CBY_SYSTEM_BUS_NAME_RIBCHESTER \
  "org.apertis.Ribchester"

/**
 * CBY_OBJECT_PATH_PRIVILEGED_APP_HELPER1:
 *
 * The object path at which implementations of #CbyPrivilegedAppHelper1 are
 * to be exported on the system bus.
 *
 * Since: 0.1709.0
 */
#define CBY_OBJECT_PATH_PRIVILEGED_APP_HELPER1 \
  "/org/apertis/Canterbury/PrivilegedAppHelper1"

/**
 * CBY_INTERFACE_PRIVILEGED_APP_HELPER1:
 *
 * The interface implemented by #CbyPrivilegedAppHelper1.
 *
 * Since: 0.1709.0
 */
#define CBY_INTERFACE_PRIVILEGED_APP_HELPER1 \
  (cby_privileged_app_helper1_interface_info ()->name)

/**
 * CBY_OBJECT_PATH_PER_USER_APP_MANAGER1:
 *
 * The object path at which implementations of #CbyPerUserAppManager1 are
 * to be exported on the system bus.
 *
 * Since: 0.1709.0
 */
#define CBY_OBJECT_PATH_PER_USER_APP_MANAGER1 \
  "/org/apertis/Canterbury/PerUserAppManager1"

/**
 * CBY_INTERFACE_PER_USER_APP_MANAGER1:
 *
 * The interface implemented by #CbyPerUserAppManager1.
 *
 * Since: 0.1709.0
 */
#define CBY_INTERFACE_PER_USER_APP_MANAGER1 \
  (cby_per_user_app_manager1_interface_info ()->name)

/**
 * CBY_SYSTEM_BUS_NAME_PER_USER_APP_MANAGER1:
 *
 * The system bus name used to mediate access to #CbyPerUserAppManager1.
 * All implementations of #CbyPerUserAppManager1 queue for ownership of
 * this name.
 *
 * Since: 0.1709.0
 */
#define CBY_SYSTEM_BUS_NAME_PER_USER_APP_MANAGER1 \
  CBY_INTERFACE_PER_USER_APP_MANAGER1

/**
 * CBY_SERVICE_STOP_TIMEOUT_SEC:
 *
 * The arbitrary timeout used when terminating app-bundles, in seconds.
 *
 * It is recommended that D-Bus calls to
 * cby_per_user_app_manager1_call_terminate_bundle()
 * should use a timeout in milliseconds somewhat longer than 1000 times
 * this, to allow for the termination of the app-bundle via SIGKILL
 * and the D-Bus message-passing overhead.
 */
#define CBY_SERVICE_STOP_TIMEOUT_SEC 10

G_END_DECLS
