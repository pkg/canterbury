/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "entry-point.h"
#include "entry-point-internal.h"

#include <stdlib.h>
#include <string.h>

#include <gio/gdesktopappinfo.h>

#include "canterbury/bundle.h"
#include "canterbury/errors.h"
#include "canterbury/gdbus/canterbury_app_handler.h"
#include "canterbury/gdbus/enumtypes.h"
#include "canterbury/messages-internal.h"
#include "canterbury/platform/dbus.h"
#include "canterbury/process-info-internal.h"

/**
 * SECTION:platform/entry-point.h
 * @title: CbyEntryPoint
 * @short_description: Object representing an entry point
 *
 * Since: 0.1703.4
 */

typedef enum
{
  CBY_ENTRY_POINT_FLAGS_NONE = 0,
  CBY_ENTRY_POINT_FLAGS_NODISPLAY = (1 << 0),
  CBY_ENTRY_POINT_FLAGS_DBUS_ACTIVATABLE = (1 << 1),
} CbyEntryPointFlags;

typedef struct
{
  gchar *id;
  CanterburyAppBkgState background_state;
  gchar *working_directory;
  CanterburyExecutableType executable_type;
  gchar **argv;
  CanterburyAudioType audio_resource_type;
  gchar* audio_channel;
  gchar* audio_resource_owner_id;
  gchar* category_label;
  gchar *display_name;
  gchar *mangled_display_name;
  CanterburyBandwidthPriority internet_bandwidth_priority;
  gchar **supported_types;
  gchar *splash_screen;
  CbyServiceType service_type;
  GAppInfo *app_info; /* owned */
  CbyProcessInfo *process_info; /* owned */

  /* Used to detect file changes, or NULL if not file-backed.
   * This is not cryptographically strong, but that isn't what we
   * need here anyway so we might as well use something fast. */
  gchar *md5;

  CbyEntryPointFlags flags;
} CbyEntryPointPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (CbyEntryPoint, cby_entry_point, G_TYPE_OBJECT)

static void
cby_entry_point_init (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  priv->service_type = INVALID_TYPE;
  priv->internet_bandwidth_priority = CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN;
  priv->audio_resource_type = CANTERBURY_AUDIO_RESOURCE_NONE;
}

static void
cby_entry_point_reset (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  DEBUG ("Resetting app %s", priv->id);

  priv->executable_type = 0;
  priv->audio_resource_type = CANTERBURY_AUDIO_RESOURCE_NONE;
  priv->internet_bandwidth_priority = CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN;
  priv->flags = CBY_ENTRY_POINT_FLAGS_NONE;

  g_clear_object (&priv->app_info);
  g_clear_object (&priv->process_info);
  g_clear_pointer (&priv->audio_channel, g_free);
  g_clear_pointer (&priv->audio_resource_owner_id, g_free);
  g_clear_pointer (&priv->category_label, g_free);
  g_clear_pointer (&priv->splash_screen, g_free);
  g_clear_pointer (&priv->argv, g_strfreev);
  g_clear_pointer (&priv->display_name, g_free);
  g_clear_pointer (&priv->mangled_display_name, g_free);
  g_clear_pointer (&priv->supported_types, g_strfreev);

  g_clear_pointer (&priv->working_directory, g_free);
  priv->background_state = 0;
}

static void
cby_entry_point_dispose (GObject *object)
{
  CbyEntryPoint *self = CBY_ENTRY_POINT (object);
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  priv->service_type = INVALID_TYPE;
  cby_entry_point_reset (self);
  g_clear_pointer (&priv->id, g_free);

  G_OBJECT_CLASS (cby_entry_point_parent_class)->dispose (object);
}

static void
cby_entry_point_class_init (CbyEntryPointClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  object_class->dispose = cby_entry_point_dispose;
}

static CbyEntryPoint *
_cby_entry_point_new (void)
{
  return g_object_new (CBY_TYPE_ENTRY_POINT, NULL);
}

/*
 * desktop_parse_enum:
 * @desktop: a parsed desktop entry (.desktop file)
 * @key: a key in the [Desktop Entry] group
 * @type: the GType of a GEnum, whose values' nicknames
 *   (as in g_enum_get_value_by_nick()) are strings that can appear
 *   as values of @key
 * @if_unspecified: what to return if the desktop entry
 *   does not specify @key at all
 * @if_unparsed: what to return if the desktop entry does specify @key,
 *   but we do not recognise the value
 *
 * Parse a string value in a desktop entry that represents a member of
 * the enum @type. To accept multiple strings as synonyms for a value,
 * the enum may have items with distinct nicknames but different
 * numeric values.
 *
 * For example, "video" is the nickname of
 * %CANTERBURY_AUDIO_RESOURCE_TYPE_VIDEO, so when this function is
 * called with @key `X-Apertis-AudioRole` and @type
 * %CANTERBURY_TYPE_AUDIO_RESOURCE_TYPE, and a desktop entry @desktop
 * containing `X-Apertis-AudioRole=video`, it will return
 * %CANTERBURY_AUDIO_RESOURCE_TYPE_VIDEO.
 *
 * In addition to the generic GEnum-based logic, this function
 * currently has some special cases for audio resource types,
 * so that it can parse all PulseAudio media roles.
 *
 * Returns: @if_unspecified, @if_unparsed or a member of the enum @type
 */
static gint
desktop_parse_enum (GDesktopAppInfo *desktop,
                    const gchar *key,
                    GType type,
                    gint if_unspecified,
                    gint if_unparsed)
{
  gchar *s;
  GEnumClass *cls;
  GEnumValue *value;
  gint ret = if_unspecified;

  g_return_val_if_fail (G_TYPE_IS_ENUM (type), if_unparsed);

  s = g_desktop_app_info_get_string (desktop, key);

  if (s == NULL)
    return ret;

  cls = g_type_class_ref (type);
  value = g_enum_get_value_by_nick (cls, s);
  g_type_class_unref (cls);

  if (value != NULL)
    {
      ret = value->value;
    }
  else if (type == CANTERBURY_TYPE_AUDIO_RESOURCE_TYPE)
    {
      /* We document X-Apertis-AudioRole as being a PulseAudio media role
       * <http://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/Developer/Clients/ApplicationProperties/>
       * so we should understand those, even though our enum is different
       * in some places. */

      /* FIXME: are these the most appropriate values of
       * CanterburyAudioResourceType for the corresponding PA roles?
       * Should we add more values? */
      if (strcmp (s, "game") == 0)
        {
          /* A game */
          ret = CANTERBURY_AUDIO_RESOURCE_TYPE_UNKNOWN;
        }
      else if (strcmp (s, "event") == 0)
        {
          /* A button click or similar event feedback - is this what
           * Apertis' "interrupt" also means? */
          ret = CANTERBURY_AUDIO_RESOURCE_TYPE_INTERRUPT;
        }
      else if (strcmp (s, "animation") == 0)
        {
          /* Adobe Flash or similar */
          ret = CANTERBURY_AUDIO_RESOURCE_TYPE_VIDEO;
        }
      else if (strcmp (s, "production") == 0)
        {
          /* Audio production app, like a sound recorder */
          ret = CANTERBURY_AUDIO_RESOURCE_TYPE_RECORD;
        }
      else if (strcmp (s, "a11y") == 0)
        {
          /* Screen readers and other accessibility components */
          ret = CANTERBURY_AUDIO_RESOURCE_TYPE_UNKNOWN;
        }
      else
        {
          WARNING ("unknown value \"%s\" for \"%s\"", s, key);
          ret = CANTERBURY_AUDIO_RESOURCE_TYPE_UNKNOWN;
        }
    }
  else
    {
      WARNING ("unknown value \"%s\" for %s", s, key);
      ret = if_unparsed;
    }

  g_free (s);
  return ret;
}

/*
 * If @name appears to have already been subjected to
 * M A N U A L   K E R N I N G, return an unaltered copy.
 * Otherwise, add spaces and translate to upper case.
 *
 * Returns: (transfer full): @name, in upper case, with spaces inserted
 */
static gchar *
mangle_display_name (const gchar *name)
{
  gchar *ret;
  guint i;

  if (name == NULL)
    return NULL;

  /* Heuristic to make sure we don't mangle pre-mangled names. */
  if (name[0] != '\0' && name[1] == ' ' && name[2] != '\0' &&
      name[3] == ' ')
    return g_strdup (name);

  ret = g_new0 (gchar, strlen (name) * 2);

  for (i = 0; name[i] != '\0'; i++)
    {
      ret[i * 2] = g_ascii_toupper (name[i]);

      if (name[i + 1] == '\0')
        ret[i * 2 + 1] = '\0';
      else
        ret[i * 2 + 1] = ' ';
    }

  return ret;
}

CbyEntryPoint *
_cby_entry_point_new_from_app_info (CbyServiceType service_type,
                                    const gchar *app_name,
                                    GAppInfo *app_info,
                                    GError **error)
{
  g_autoptr (CbyEntryPoint) self = NULL;
  CbyEntryPointPrivate *priv;

  g_return_val_if_fail (G_IS_APP_INFO (app_info), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  self = _cby_entry_point_new ();
  priv = cby_entry_point_get_instance_private (self);

  priv->id = g_strdup (app_name);
  priv->service_type = service_type;

  if (_cby_entry_point_update_from_app_info (self, app_info, NULL, error))
    return g_steal_pointer (&self);
  else
    return NULL;
}

static gboolean
cby_entry_point_copy_from_app_info (CbyEntryPoint *self,
                                    GAppInfo *app_info,
                                    GError **error)
{
  gint argc;
  g_auto (GStrv) argv = NULL;
  gchar **argvp;
  const char **mime_types;
  const gchar *label = NULL;
  GDesktopAppInfo *desktop = NULL;
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);
  CbyProcessType process_type;

  if (G_IS_DESKTOP_APP_INFO (app_info))
    desktop = G_DESKTOP_APP_INFO (app_info);

  if (!g_shell_parse_argv (g_app_info_get_commandline (app_info),
                           &argc, &argv, error))
    return FALSE;

  if (argv == NULL || argv[0] == NULL)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
          "No Exec= command-line found");
      return FALSE;
    }

  /* Lets strip 'canterbury-exec', if any, from Exec= line so that we can
   * get the real executable path to be used when creating a CbyProcessInfo
   * for this entry point
   */
  if (g_strcmp0 (argv[0], "canterbury-exec") == 0)
    {
      guint skip_args_count = 1;

      if (argc == 1)
        {
          g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
              "Invalid Exec= command-line");
          return FALSE;
        }

      if (g_strcmp0 (argv[1], "--") == 0)
        {
          if (argc == 2)
            {
              g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                  "Invalid Exec= command-line");
              return FALSE;
            }

          skip_args_count++;
        }

      argvp = argv + skip_args_count;
    }
  else
    {
      argvp = argv;
    }

  cby_entry_point_reset (self);
  priv->app_info = g_object_ref (app_info);

  if (g_path_is_absolute (argvp[0]))
    {
      priv->process_info = cby_process_info_new_for_path_and_user (
          argvp[0], getuid());
    }
  else if (desktop)
    {
      /* A non-absolute Exec command-line (PATH search) is only valid for the
       * platform layer or built-in bundles. Use the location of the .desktop
       * file itself, after resolving symlinks, to decide which one this is. */
      const gchar *filename = g_desktop_app_info_get_filename (desktop);

      if (filename != NULL)
        {
          g_autofree gchar *resolved_path = NULL;

          resolved_path = realpath (filename, NULL);
          priv->process_info = cby_process_info_new_for_path_and_user (
              resolved_path, getuid());
        }
    }

  if (priv->process_info == NULL)
    {
      /* We could not identity the process type, ignoring entry point */
      g_set_error (error, CBY_ERROR, CBY_ERROR_NOT_OUR_ENTRY_POINT,
                   "Could not identify process type");
      return FALSE;
    }

  process_type = cby_process_info_get_process_type (priv->process_info);
  switch (process_type)
    {
    case CBY_PROCESS_TYPE_STORE_BUNDLE:
      {
        g_autofree gchar *service_exec = NULL;
        const gchar *bundle_id;

        bundle_id = cby_process_info_get_bundle_id (priv->process_info);
        /* Only consider a store bundle's entry point valid if both the
         * entry point and bundle ID are valid */
        if (!bundle_id ||
            !cby_is_bundle_id (bundle_id) ||
            !cby_is_entry_point_id (priv->id))
          {
            g_set_error (error, CBY_ERROR, CBY_ERROR_INVALID_ENTRY_POINT,
                         "Invalid bundle or entry point ID");
            return FALSE;
          }

        /* For simplicity we treat the service command line (if present) as
         * one big string, require it to be absolute (we don't remove
         * "canterbury-exec --" from the beginning), and don't fully
         * parse it. */
        if (desktop != NULL)
          service_exec =
              g_desktop_app_info_get_string (desktop,
                                             CBY_ENTRY_POINT_KEY_X_APERTIS_SERVICE_EXEC);

        if (service_exec != NULL)
          {
            g_autoptr (CbyProcessInfo) service_exec_info = NULL;

            if (!g_path_is_absolute (service_exec))
              {
                g_set_error (error, CBY_ERROR, CBY_ERROR_INVALID_ENTRY_POINT,
                             "Invalid non-absolute %s \"%s\" for "
                             "store app-bundle",
                             CBY_ENTRY_POINT_KEY_X_APERTIS_SERVICE_EXEC,
                             service_exec);
                return FALSE;
              }

            service_exec_info =
                cby_process_info_new_for_path_and_user (service_exec,
                                                        getuid ());

            if (service_exec_info == NULL ||
                cby_process_info_get_process_type (service_exec_info) != process_type ||
                g_strcmp0 (cby_process_info_get_bundle_id (service_exec_info),
                           bundle_id) != 0)
              {
                g_set_error (error, CBY_ERROR, CBY_ERROR_INVALID_ENTRY_POINT,
                             "Store app-bundle executable path \"%s\" not "
                             "consistent with bundle ID \"%s\"",
                             argvp[0], bundle_id);
                return FALSE;
              }
          }
      }
      break;
    case CBY_PROCESS_TYPE_PLATFORM:
    case CBY_PROCESS_TYPE_BUILT_IN_BUNDLE:
      /* We skip entry point and bundle ID validation for platform and built-in bundles.
       * Platform processes don't follow the bundle specification and built-in bundles have a
       * special treatment (e.g. Launcher.desktop is special and hard-coded).
       *
       * TODO: we could probably mandate that at least the bundle ID is valid for built-in
       * bundles.
       * */
      break;
    case CBY_PROCESS_TYPE_UNKNOWN:
    default:
      /* We could not identity the process type, ignoring entry point */
      g_set_error (error, CBY_ERROR, CBY_ERROR_NOT_OUR_ENTRY_POINT,
                   "Could not identify process type");
      return FALSE;
    }

  priv->argv = g_strdupv (argvp);

  /* FIXME: it should ideally be mildenhall-launcher, not Canterbury, that
   * chooses which variation of the name we want. We might want to use
   * the generic name for built-in apps but the full (display) name
   * for store apps? */

  if (desktop != NULL)
    {
      label = g_desktop_app_info_get_generic_name (desktop);
    }

  if (label == NULL)
    label = g_app_info_get_display_name (priv->app_info);

  if (label == NULL)
    label = g_app_info_get_name (priv->app_info);

  priv->display_name = g_strdup (label);
  priv->mangled_display_name = mangle_display_name (label);

  mime_types = g_app_info_get_supported_types (priv->app_info);

  /* We have historically guaranteed that this isn't NULL */
  if (mime_types == NULL)
    priv->supported_types = g_new0 (gchar *, 1);
  else
    priv->supported_types = g_strdupv ((gchar **) mime_types);

  if (desktop != NULL)
    {
      gchar *tmp;

      if (g_desktop_app_info_get_nodisplay (desktop))
        priv->flags |= CBY_ENTRY_POINT_FLAGS_NODISPLAY;

      if (g_desktop_app_info_get_boolean (desktop,
          G_KEY_FILE_DESKTOP_KEY_DBUS_ACTIVATABLE))
        {
          if (g_dbus_is_interface_name (priv->id))
            priv->flags |= CBY_ENTRY_POINT_FLAGS_DBUS_ACTIVATABLE;
          else
            WARNING ("\"%s\" has an invalid name so cannot be DBusActivatable",
                     priv->id);
        }

      priv->working_directory = g_desktop_app_info_get_string (desktop,
          G_KEY_FILE_DESKTOP_KEY_PATH);

      priv->executable_type = desktop_parse_enum (desktop,
          "X-Apertis-Type", CANTERBURY_TYPE_EXECUTABLE_TYPE,
          /* We specifically want to use UNKNOWN so that non-Apertis apps
           * aren't counted as anything else. */
          CANTERBURY_EXECUTABLE_TYPE_UNKNOWN,
          CANTERBURY_EXECUTABLE_TYPE_UNKNOWN);

      if (priv->executable_type == CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE ||
          priv->executable_type == CANTERBURY_EXECUTABLE_TYPE_SERVICE)
        {
          /* It would make no sense for a background service to be terminated
           * when running in the background! */
          priv->background_state = CANTERBURY_BKG_STATE_RUNNING;
        }
      else
        {
          priv->background_state = desktop_parse_enum (desktop,
              "X-Apertis-BackgroundState", CANTERBURY_TYPE_APP_BKG_STATE,
              /* If no background state is specified, we assume it wants to be
               * left intact when not the foreground app, since that is the
               * conservative assumption for apps that might not save their
               * state correctly. */
              CANTERBURY_BKG_STATE_RUNNING,
              CANTERBURY_BKG_STATE_UNKNOWN);
        }

      priv->audio_resource_type = desktop_parse_enum (desktop,
          "X-Apertis-AudioRole", CANTERBURY_TYPE_AUDIO_RESOURCE_TYPE,
          CANTERBURY_AUDIO_RESOURCE_TYPE_NONE,
          CANTERBURY_AUDIO_RESOURCE_TYPE_UNKNOWN);

      priv->audio_channel = g_desktop_app_info_get_string (desktop,
          "X-Apertis-AudioChannelName");
      priv->audio_resource_owner_id = g_desktop_app_info_get_string (desktop,
          "X-Apertis-AudioResourceOwner");

      /* FIXME: we should derive the category from the Categories instead;
       * if mildenhall-launcher wants to use M A N U A L   K E R N I N G
       * then it should do it itself; and mildenhall-launcher should be
       * responsible for choosing the icon.
       * (see T489 and https://wiki.apertis.org/Application_Entry_Points) */
      tmp = g_desktop_app_info_get_string (desktop, "X-Apertis-CategoryLabel");
      priv->category_label = mangle_display_name (tmp);
      g_free (tmp);

      priv->internet_bandwidth_priority = desktop_parse_enum (desktop,
          "X-Apertis-BandwidthPriority", CANTERBURY_TYPE_BANDWIDTH_PRIORITY,
          CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN,
          CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN);
      priv->splash_screen = g_desktop_app_info_get_string (desktop,
          "X-Apertis-SplashScreen");
    }
  else
    {
      WARNING ("Unable to process non-GDesktopAppInfo");
    }

  return TRUE;
}

gboolean
_cby_entry_point_update_from_app_info (CbyEntryPoint *self,
                                       GAppInfo *app_info,
                                       gboolean *changed,
                                       GError **error)
{
  const gchar *id;
  g_autofree gchar *temp_app_id = NULL;
  g_autofree gchar *md5 = NULL;
  g_autoptr (GAppInfo) new_app_info = NULL;
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (G_IS_APP_INFO (app_info), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  id = cby_entry_point_get_id (self);

  temp_app_id = g_strdup (g_app_info_get_id (app_info));
  g_return_val_if_fail (temp_app_id != NULL, FALSE);

  /* chop off trailing ".desktop" */
  if (g_str_has_suffix (temp_app_id, ".desktop"))
    {
      char *dot = strrchr (temp_app_id, '.');

      g_assert (dot != NULL);
      *dot = '\0';
    }
  g_return_val_if_fail (g_strcmp0 (temp_app_id, id) == 0, FALSE);

  if (G_IS_DESKTOP_APP_INFO (app_info))
    {
      const gchar *filename;
      g_autofree gchar *contents;
      g_autoptr (GError) local_error = NULL;
      GDesktopAppInfo *desktop;
      gsize length;

      desktop = G_DESKTOP_APP_INFO (app_info);
      filename = g_desktop_app_info_get_filename (desktop);

      if (filename == NULL ||
          !g_file_get_contents (filename, &contents, &length, &local_error))
        {
          if (filename)
            {
              g_set_error (error, CBY_ERROR, CBY_ERROR_FAILED,
                           "Unable to load \"%s\": %s", filename,
                           local_error->message);
            }
          else
            {
              g_set_error (error, CBY_ERROR, CBY_ERROR_FAILED,
                           "Unable to load: fail loading file contents");
            }

          return FALSE;
        }

      md5 = g_compute_checksum_for_data (G_CHECKSUM_MD5,
                                         (const guchar *) contents,
                                         length);

      if (priv->md5 != NULL && strcmp (md5, priv->md5) == 0)
        {
          if (changed)
            *changed = FALSE;
          return TRUE;
        }

      /* We construct a new GDesktopAppInfo just in case the file changed
       * between the original GDesktopAppInfo loading it, and our hashing. */
      new_app_info =
          G_APP_INFO (g_desktop_app_info_new_from_filename (filename));

      if (new_app_info == NULL)
        {
          g_set_error (error, CBY_ERROR, CBY_ERROR_FAILED,
                       "Unable to parse \"%s\"", filename);
          return FALSE;
        }
    }
  else
    {
      /* Assume it has changed - we can't really tell */
      new_app_info = g_object_ref (app_info);
    }

  if (!cby_entry_point_copy_from_app_info (self, new_app_info, error))
    return FALSE;

  g_free (priv->md5);
  priv->md5 = g_steal_pointer (&md5);

  if (changed)
    *changed = TRUE;
  return TRUE;
}

gboolean
cby_entry_point_should_show (CbyEntryPoint *self)
{
  const gchar *id;
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_ENTRY_POINT (self), FALSE);

  id = cby_entry_point_get_id (self);

  if ((id == NULL) ||
      ((priv->flags & CBY_ENTRY_POINT_FLAGS_NODISPLAY) != 0) ||
      (priv->display_name == NULL) ||
      (priv->mangled_display_name == NULL) ||
      (priv->executable_type != CANTERBURY_EXECUTABLE_TYPE_APPLICATION &&
       priv->executable_type != CANTERBURY_EXECUTABLE_TYPE_EXT_APP))
    return FALSE;
  return TRUE;
}

gboolean
_cby_entry_point_get_dbus_activatable (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_ENTRY_POINT (self), FALSE);

  if (priv->flags & CBY_ENTRY_POINT_FLAGS_DBUS_ACTIVATABLE)
    return TRUE;

  return FALSE;
}

/**
 * cby_entry_point_get_id:
 * @self: the Canterbury entry point
 *
 * Get the entry point ID for the `CbyEntryPoint` self.
 *
 * Returns: (transfer none): Entry point ID
 *
 * Since: 0.1703.4
 */
const gchar *
cby_entry_point_get_id (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_ENTRY_POINT (self), NULL);

  return priv->id;
}

/*
 * _cby_entry_point_get_background_state:
 * @self: the Canterbury entry point
 *
 * Returns: background state of @self
 */
CanterburyAppBkgState
_cby_entry_point_get_background_state (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->background_state;
}

/*
 * _cby_entry_point_get_working_directory:
 * @self: the Canterbury entry point
 *
 * Returns: (transfer none): working directory for this entry point.
 */
const gchar *
_cby_entry_point_get_working_directory (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->working_directory;
}

/**
 * cby_entry_point_get_executable:
 * @self: the Canterbury entry point
 *
 * Returns: (transfer none): executable for this entry point.
 *
 * Since: 0.1703.4
 */
const gchar *
cby_entry_point_get_executable (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  /* The only public constructor ensures this */
  g_return_val_if_fail (priv->argv != NULL, NULL);
  g_return_val_if_fail (priv->argv[0] != NULL, NULL);

  return priv->argv[0];
}

/*
 * _cby_entry_point_get_executable_type:
 * @self: the Canterbury entry point
 *
 * Returns: executable type for this entry point.
 */
CanterburyExecutableType
_cby_entry_point_get_executable_type (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->executable_type;
}

/*
 * _cby_entry_point_get_argv:
 * @self: the Canterbury entry point
 *
 * Return the executable and its arguments.
 *
 * Returns: (array zero-terminated=1) (transfer none): a complete argument list, for example `{"/usr/bin/myapp", "--verbose", NULL}`
 */
const gchar *const *
_cby_entry_point_get_argv (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  /* The only public constructor ensures this */
  g_return_val_if_fail (priv->argv != NULL, NULL);
  g_return_val_if_fail (priv->argv[0] != NULL, NULL);

  return (const gchar *const *) priv->argv;
}

/*
 * _cby_entry_point_get_arguments:
 * @self: the Canterbury entry point
 *
 * Return arguments to be passed to the executable, not including the executable name itself.
 *
 * Returns: (array zero-terminated=1) (transfer none): arguments, for example `{"--verbose", NULL}`
 */
const gchar *const *
_cby_entry_point_get_arguments (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_ENTRY_POINT (self), NULL);
  /* The only public constructor ensures this */
  g_return_val_if_fail (priv->argv != NULL, NULL);
  g_return_val_if_fail (priv->argv[0] != NULL, NULL);

  return (const gchar *const *) (&priv->argv[1]);
}

/*
 * _cby_entry_point_get_audio_resource_type:
 * @self: the Canterbury entry point
 *
 * Returns: audio resource type for this entry point.
 */
CanterburyAudioType
_cby_entry_point_get_audio_resource_type (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->audio_resource_type;
}

/*
 * _cby_entry_point_get_audio_channel:
 * @self: the Canterbury entry point
 *
 * Returns: (transfer none): audio_channel for this entry point.
 */
const gchar *
_cby_entry_point_get_audio_channel (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->audio_channel;
}

/*
 * _cby_entry_point_get_audio_resource_owner_id:
 * @self: the Canterbury entry point
 *
 * Returns: (transfer none): audio resource owner id for this entry point.
 */
const gchar *
_cby_entry_point_get_audio_resource_owner_id (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->audio_resource_owner_id;
}

/*
 * _cby_entry_point_get_category_label:
 * @self: the Canterbury entry point
 *
 * Returns: (transfer none): category_label for this entry point.
 */
const gchar *
_cby_entry_point_get_category_label (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->category_label;
}

/**
 * cby_entry_point_get_display_name:
 * @self: the Canterbury entry point
 *
 * This function returns a non mangled display name such as `Music`.
 *
 * Returns: (transfer none): Display name for this entry point.
 *
 * Since: 0.1703.4
 */
const gchar *
cby_entry_point_get_display_name (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_ENTRY_POINT (self), NULL);

  return priv->display_name;
}

/*
 * _cby_entry_point_get_mangled_display_name:
 * @self: the Canterbury entry point
 *
 * This function differs from cby_entry_point_get_display_name by
 * the fact that it returns a Mangled display name such as `M U S I C`.
 *
 * Returns: (transfer none): Mangled display name for this entry point.
 */
const gchar *
_cby_entry_point_get_mangled_display_name (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->mangled_display_name;
}

/*
 * _cby_entry_point_get_internet_bandwidth_priority:
 * @self: the Canterbury entry point
 *
 * Returns: Internet bandwith priority
 */
CanterburyBandwidthPriority
_cby_entry_point_get_internet_bandwidth_priority (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->internet_bandwidth_priority;
}

/**
 * cby_entry_point_get_supported_types:
 * @self: the Canterbury entry point
 *
 * Retrieves the list of content types that this application claims to support.
 * If this information is not provided by the environment, this function will
 * return NULL.
 *
 * Returns: (transfer none): List of application supported types
 *
 * Since: 0.1703.4
 */
const gchar *const *
cby_entry_point_get_supported_types (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_ENTRY_POINT (self), NULL);

  return (const gchar *const *)priv->supported_types;
}

/*
 * _cby_entry_point_get_splash_screen:
 * @self: the Canterbury entry point
 *
 * Returns the splash screen.
 * See <https://phabricator.apertis.org/T3497>.
 *
 * Returns: Application splash screen
 */
const gchar *
_cby_entry_point_get_splash_screen (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->splash_screen;
}

/*
 * _cby_entry_point_get_service_type:
 * @self: the Canterbury entry point
 *
 * Returns: Service type for this entry point.
 */
CbyServiceType
_cby_entry_point_get_service_type (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->service_type;
}

/*
 * _cby_entry_point_get_app_info:
 * @self: the Canterbury entry point
 *
 * Gets the app_info for this entry point.
 *
 * Returns: (transfer none): app_info for this entry point.
 */
GAppInfo *
_cby_entry_point_get_app_info (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->app_info;
}

/**
 * _cby_entry_point_get_process_info:
 * @self: the Canterbury entry point
 *
 * Gets the process_info for this entry point.
 *
 * Returns: (transfer none): process_info for this entry point.
 */
CbyProcessInfo *
_cby_entry_point_get_process_info (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  return priv->process_info;
}

/**
 * cby_entry_point_get_string:
 * @self: the Canterbury entry point
 * @key: app info key
 *
 * Look up a string value in the `[Desktop Entry]` group of the #GKeyFile
 * backing @self.
 *
 * Returns: (transfer full): A newly allocated string or NULL if the key is not found
 *
 * Since: 0.1703.4
 */
gchar *
cby_entry_point_get_string (CbyEntryPoint *self, const gchar *key)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_ENTRY_POINT (self), NULL);
  g_return_val_if_fail (key != NULL, NULL);
  g_return_val_if_fail (G_IS_DESKTOP_APP_INFO (priv->app_info), NULL);

  return g_desktop_app_info_get_string (G_DESKTOP_APP_INFO (priv->app_info), key);
}

/**
 * cby_entry_point_get_icon:
 * @self: the Canterbury entry point
 *
 * Gets the icon for this entry point.
 *
 * Returns: (transfer none): icon for this entry point or %NULL if not defined.
 *
 * Since: 0.1703.4
 */
GIcon *
cby_entry_point_get_icon (CbyEntryPoint *self)
{
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);

  g_return_val_if_fail (CBY_IS_ENTRY_POINT (self), NULL);

  if (G_IS_APP_INFO (priv->app_info))
    return g_app_info_get_icon (priv->app_info);

  return NULL;
}

static void
activate_cb (GObject *source_object,
             GAsyncResult *result,
             gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;

  tuple = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                         result, &error);

  if (tuple == NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);
}

static void
activate_get_bus_cb (GObject *source_object,
                     GAsyncResult *result,
                     gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GDBusConnection) session_bus = NULL;
  GVariant *args = g_task_get_task_data (task);

  session_bus = g_bus_get_finish (result, &error);

  if (session_bus == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_dbus_connection_call (session_bus, CBY_SESSION_BUS_NAME,
                          CBY_OBJECT_PATH_PRIVATE_ENTRY_POINTS1,
                          CBY_INTERFACE_PRIVATE_ENTRY_POINTS1,
                          "ActivateEntryPoint",
                          args,
                          G_VARIANT_TYPE_UNIT,
                          G_DBUS_CALL_FLAGS_NONE,
                          -1,
                          g_task_get_cancellable (task),
                          activate_cb,
                          g_object_ref (task));
}

/**
 * cby_entry_point_activate_async:
 * @self: the entry point
 * @platform_data: (nullable): a %G_VARIANT_TYPE_VARDICT with hints for the
 *  application
 *  framework to pass to the activated application. If this is a floating
 *  reference, this function takes ownership of it.
 * @cancellable: (nullable): can be used to stop waiting for a result
 * @callback: (nullable): called when the entry point has been activated
 *  or the attempt to activate it has failed
 * @user_data: (closure): user data for @callback
 *
 * Activate the entry point. For a graphical program, this might mean
 * creating a new window or bringing an existing window to the foreground.
 *
 * Since: 0.1703.5
 */
void
cby_entry_point_activate_async (CbyEntryPoint *self,
                                GVariant *platform_data,
                                GCancellable *cancellable,
                                GAsyncReadyCallback callback,
                                gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);
  GVariant *args; /* floating */

  g_return_if_fail (CBY_IS_ENTRY_POINT (self));
  g_return_if_fail (platform_data == NULL ||
                    g_variant_is_of_type (platform_data,
                                          G_VARIANT_TYPE_VARDICT));

  /* Floating reference, will be sunk when we put it in the arguments */
  if (platform_data == NULL)
    platform_data = g_variant_new_array (G_VARIANT_TYPE ("{sv}"), NULL, 0);

  /* Floating reference, will be sunk when we put it in the GTask */
  args = g_variant_new ("(s@a{sv})", priv->id, platform_data);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, cby_entry_point_activate_async);
  g_task_set_task_data (task, g_variant_ref_sink (args),
                        (GDestroyNotify) g_variant_unref);
  g_bus_get (G_BUS_TYPE_SESSION, cancellable, activate_get_bus_cb,
             g_object_ref (task));
}

/**
 * cby_entry_point_activate_finish:
 * @self: the entry point
 * @result: the result passed to the callback
 * @error: used to raise an error on failure
 *
 * Interpret the result of cby_entry_point_activate_async().
 *
 * Returns: %TRUE on success
 *
 * Since: 0.1703.5
 */
gboolean
cby_entry_point_activate_finish (CbyEntryPoint *self,
                                 GAsyncResult *result,
                                 GError **error)
{
  g_return_val_if_fail (CBY_IS_ENTRY_POINT (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
                                                  cby_entry_point_activate_async),
                        FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
open_cb (GObject *source_object,
         GAsyncResult *result,
         gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;

  tuple = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                         result, &error);

  if (tuple == NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);
}

static void
open_get_bus_cb (GObject *source_object,
                 GAsyncResult *result,
                 gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GDBusConnection) session_bus = NULL;
  GVariant *args = g_task_get_task_data (task);

  session_bus = g_bus_get_finish (result, &error);

  if (session_bus == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_dbus_connection_call (session_bus, CBY_SESSION_BUS_NAME,
                          CBY_OBJECT_PATH_PRIVATE_ENTRY_POINTS1,
                          CBY_INTERFACE_PRIVATE_ENTRY_POINTS1,
                          "OpenURIs",
                          args,
                          G_VARIANT_TYPE_UNIT,
                          G_DBUS_CALL_FLAGS_NONE,
                          -1,
                          g_task_get_cancellable (task),
                          open_cb,
                          g_object_ref (task));
}

/**
 * cby_entry_point_open_uris_async:
 * @self: the entry point
 * @uris: one or more URIs to open
 * @platform_data: (nullable): a %G_VARIANT_TYPE_VARDICT with hints for
 *  the application framework to pass to the activated application. If this
 *  is a floating reference, this function takes ownership of it.
 * @cancellable: (nullable): can be used to stop waiting for a result
 * @callback: (nullable): called when the entry point has been activated
 *  or the attempt to activate it has failed
 * @user_data: (closure): user data for @callback
 *
 * Activate the entry point, and tell it to open the given @uris.
 * For a graphical program, this might mean creating a new window or
 * bringing an existing window to the foreground.
 *
 * This method does not take any special steps to make the URIs accessible
 * by the entry point. If they are not, opening them will fail. Similarly,
 * it makes no attempt to confirm that the URIs are of a scheme or content
 * type that the entry point is able to open.
 *
 * This method is intended to be called by the Apertis content handover
 * service (Didcot).
 *
 * Since: 0.1703.5
 */
void
cby_entry_point_open_uris_async (CbyEntryPoint *self,
                                 const gchar * const *uris,
                                 GVariant *platform_data,
                                 GCancellable *cancellable,
                                 GAsyncReadyCallback callback,
                                 gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  CbyEntryPointPrivate *priv = cby_entry_point_get_instance_private (self);
  GVariant *args; /* floating */

  g_return_if_fail (CBY_IS_ENTRY_POINT (self));
  g_return_if_fail (uris != NULL);
  g_return_if_fail (uris[0] != NULL);
  g_return_if_fail (platform_data == NULL ||
                    g_variant_is_of_type (platform_data,
                                          G_VARIANT_TYPE_VARDICT));

  /* Floating reference, will be sunk when we put it in the arguments */
  if (platform_data == NULL)
    platform_data = g_variant_new_array (G_VARIANT_TYPE ("{sv}"), NULL, 0);

  /* Floating reference, will be sunk when we put it in the GTask */
  args = g_variant_new ("(s^as@a{sv})", priv->id, uris, platform_data);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, cby_entry_point_open_uris_async);
  g_task_set_task_data (task, g_variant_ref_sink (args),
                        (GDestroyNotify) g_variant_unref);
  g_bus_get (G_BUS_TYPE_SESSION, cancellable, open_get_bus_cb,
             g_object_ref (task));
}

/**
 * cby_entry_point_open_uris_finish:
 * @self: the entry point
 * @result: the result passed to the callback
 * @error: used to raise an error on failure
 *
 * Interpret the result of cby_entry_point_open_uris_async().
 *
 * Returns: %TRUE on success
 *
 * Since: 0.1703.5
 */
gboolean
cby_entry_point_open_uris_finish (CbyEntryPoint *self,
                                  GAsyncResult *result,
                                  GError **error)
{
  g_return_val_if_fail (CBY_IS_ENTRY_POINT (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
                                                  cby_entry_point_open_uris_async),
                        FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

/**
 * cby_entry_point_open_uri_async:
 * @self: the entry point
 * @uri: a URI to open
 * @platform_data: (nullable): a %G_VARIANT_TYPE_VARDICT with hints for
 *  the application framework to pass to the activated application. If
 *  this is a floating reference, this function takes ownership of it.
 * @cancellable: (nullable): can be used to stop waiting for a result
 * @callback: (nullable): called when the entry point has been activated
 *  or the attempt to activate it has failed
 * @user_data: (closure): user data for @callback
 *
 * Convenience version of cby_entry_point_open_uris_async() that takes a
 * single URI argument.
 *
 * Since: 0.1703.5
 */
void
cby_entry_point_open_uri_async (CbyEntryPoint *self,
                                const gchar *uri,
                                GVariant *platform_data,
                                GCancellable *cancellable,
                                GAsyncReadyCallback callback,
                                gpointer user_data)
{
  const gchar *uris[] = { NULL, NULL };

  uris[0] = uri;
  return cby_entry_point_open_uris_async (self, uris, platform_data,
                                          cancellable, callback, user_data);
}

/**
 * cby_entry_point_open_uri_finish:
 * @self: the entry point
 * @result: the result passed to the callback
 * @error: used to raise an error on failure
 *
 * Interpret the result of cby_entry_point_open_uri_async().
 *
 * Returns: %TRUE on success
 *
 * Since: 0.1703.5
 */
gboolean
cby_entry_point_open_uri_finish (CbyEntryPoint *self,
                                 GAsyncResult *result,
                                 GError **error)
{
  return cby_entry_point_open_uris_finish (self, result, error);
}
