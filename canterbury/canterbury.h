/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CANTERBURY_CANTERBURY_H__
#define __CANTERBURY_CANTERBURY_H__

#define __CANTERBURY_IN_META_HEADER__
#include <canterbury/bundle.h>
#include <canterbury/enumtypes.h>
#include <canterbury/errors.h>
#include <canterbury/paths.h>
#include <canterbury/process-info.h>
#undef __CANTERBURY_IN_META_HEADER__

#endif
