/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CANTERBURY_PROCESS_INFO_H__
#define __CANTERBURY_PROCESS_INFO_H__

#if !defined(__CANTERBURY_IN_META_HEADER__) && !defined(CANTERBURY_COMPILATION)
#error "Only <canterbury/canterbury.h> can be included directly"
#endif

#include <sys/types.h>

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

/**
 * CbyProcessType:
 * @CBY_PROCESS_TYPE_UNKNOWN: The type of process could not be determined
 *  or does not fall into one of the standard categories
 * @CBY_PROCESS_TYPE_PLATFORM: The process is part of the platform
 *  (for example `systemd`, `dbus-daemon` or `canterbury`)
 * @CBY_PROCESS_TYPE_BUILT_IN_BUNDLE: The process is part of a built-in
 *  application bundle; it is included in the platform image, but behaves
 *  like an independent application (for example the `Rhayader` web browser
 *  and `mildenhall-settings` settings manager in the reference Apertis
 *  platform)
 * @CBY_PROCESS_TYPE_STORE_BUNDLE: The process is part of a store application
 *  bundle, either preinstalled in the factory image or downloaded from an
 *  app-store
 *
 * The type of process represented by a #CbyProcessInfo.
 *
 * Since: 0.4.0
 */
typedef enum {
    CBY_PROCESS_TYPE_UNKNOWN = 0,
    CBY_PROCESS_TYPE_PLATFORM,
    CBY_PROCESS_TYPE_BUILT_IN_BUNDLE,
    CBY_PROCESS_TYPE_STORE_BUNDLE
} CbyProcessType;

#define CBY_TYPE_PROCESS_INFO (cby_process_info_get_type ())

G_DECLARE_FINAL_TYPE (CbyProcessInfo, cby_process_info, CBY, PROCESS_INFO,
    GObject)

#define CBY_PROCESS_INFO_NO_USER_ID (G_MAXUINT32)

CbyProcessInfo *cby_process_info_get_self (void);

CbyProcessInfo *cby_process_info_new_for_apparmor_context (
    const gchar *context);
CbyProcessInfo *cby_process_info_new_for_apparmor_label (const gchar *label);
CbyProcessInfo *cby_process_info_new_for_apparmor_label_and_user (const gchar *label,
                                                                  guint user_id);
CbyProcessInfo *cby_process_info_new_for_path_and_user (const gchar *path,
                                                        guint user_id);

void cby_process_info_new_for_dbus_invocation_async (
    GDBusMethodInvocation *invocation,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);
CbyProcessInfo *cby_process_info_new_for_dbus_invocation_finish (
    GAsyncResult *res,
    GError **error);

void cby_process_info_new_for_dbus_sender_async (
    const gchar *sender,
    GDBusConnection *connection,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);
CbyProcessInfo *cby_process_info_new_for_dbus_sender_finish (
    GAsyncResult *res,
    GError **error);

const gchar *cby_process_info_get_apparmor_label (CbyProcessInfo *self);
const gchar *cby_process_info_get_bundle_id (CbyProcessInfo *self);
const gchar *cby_process_info_get_persistence_path (CbyProcessInfo *self);
CbyProcessType cby_process_info_get_process_type (CbyProcessInfo *self);
guint cby_process_info_get_user_id (CbyProcessInfo *self);
const gchar *cby_process_info_get_downloads_path (CbyProcessInfo *self);

G_END_DECLS

#endif
