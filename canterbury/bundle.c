/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "canterbury/bundle.h"
#include "canterbury/bundle-internal.h"

#include <string.h>

#include <gio/gio.h>

#include "canterbury/messages-internal.h"
#include "canterbury/process-info.h"

#include "canterbury/platform/paths.h"

/**
 * SECTION:bundle.h
 * @title: Bundle related utilities
 * @short_description: Utility functions to deal with bundles
 */

/**
 * cby_is_bundle_id:
 * @maybe_id: a potential bundle identifier
 *
 * Check whether @maybe_id is a syntactically valid application bundle
 * identifier. Valid application bundle identifiers follow the same rules
 * as D-Bus interface names.
 *
 * Returns: %TRUE if @maybe_id is in fact a bundle ID
 *
 * Since: 0.3.1
 */
gboolean
cby_is_bundle_id (const gchar *maybe_id)
{
  return g_dbus_is_interface_name (maybe_id);
}

/*
 * _cby_parse_installation_path:
 * @path: the absolute path to the directory where the static
 *  files of an app bundle are installed (for example /usr/Applications/eye),
 *  or a filename within that directory (for example
 *  /usr/Applications/eye/bin/eye), or an AppArmor label that takes the
 *  form of an absolute filename within that directory
 * @out_bundle_id: (out) (transfer full) (nullable) (optional): used to
 *  return the bundle ID corresponding to that path if it is a
 *  built-in or store app bundle, or %NULL otherwise
 *
 * Parse the installation path of an app executable to determine whether
 * it is part of a built-in app bundle, part of a store app bundle,
 * part of the platform, or a path that cannot be identified as any of those.
 * If it is an app bundle, also output its bundle ID.
 *
 * Returns: whether the path corresponds to a built-in app bundle,
 *  a store app bundle, a platform component, or none of those
 */
CbyProcessType
_cby_parse_installation_path (const gchar *path, gchar **out_bundle_id)
{
  gsize prefix_length = 0;
  gchar *id = NULL;
  CbyProcessType type = CBY_PROCESS_TYPE_UNKNOWN;

  g_return_val_if_fail (path != NULL, CBY_PROCESS_TYPE_UNKNOWN);
  g_return_val_if_fail (g_path_is_absolute (path), CBY_PROCESS_TYPE_UNKNOWN);

  /* Programs' AppArmor profiles normally reflect their filenames.
   * Store application bundles are installed in a subdirectory
   * of /Applications/ named for the bundle ID */
  if (g_str_has_prefix (path, CBY_PATH_PREFIX_STORE_BUNDLE "/"))
    {
      type = CBY_PROCESS_TYPE_STORE_BUNDLE;
      prefix_length = strlen (CBY_PATH_PREFIX_STORE_BUNDLE "/");
    }
  /* Built-in application bundles are installed in a subdirectory
   * of /usr/Applications/ named for the bundle ID */
  else if (g_str_has_prefix (path, CBY_PATH_PREFIX_BUILT_IN_BUNDLE "/"))
    {
      type = CBY_PROCESS_TYPE_BUILT_IN_BUNDLE;
      prefix_length = strlen (CBY_PATH_PREFIX_BUILT_IN_BUNDLE "/");
    }
  /* Confined platform processes have profiles reflecting their filenames
   * in /usr (but not /usr/Applications), /bin, /sbin or /lib. Platform
   * processes are not part of any particular app bundle. */
  else if (g_str_has_prefix (path, "/usr/") ||
           g_str_has_prefix (path, "/bin/") ||
           g_str_has_prefix (path, "/sbin/") ||
           g_str_has_prefix (path, "/lib/"))
    {
      type = CBY_PROCESS_TYPE_PLATFORM;
    }
  /* Otherwise we conservatively consider the type to be UNKNOWN */

  if (prefix_length != 0 && out_bundle_id != NULL)
    {
      const gchar *id_start = path + prefix_length;
      const gchar *slash = strchr (id_start, '/');

      if (slash == NULL)
        {
          /* /Applications/foo -> foo
           *      id_start-^ */
          id = g_strdup (id_start);
        }
      else
        {
          /* /Applications/foo/bin/bar -> foo
           *      id_start-^  ^-slash */
          id = g_strndup (id_start, slash - id_start);
        }
    }

  if (id != NULL && !cby_is_bundle_id (id))
    {
      if (type == CBY_PROCESS_TYPE_BUILT_IN_BUNDLE)
        {
          /* TODO: For now we tolerate built-in app bundles with syntatically invalid bundle IDs.
           * When all built-in apps and agents have syntatically valid
           * bundle IDs, stop special treating them */
          WARNING ("Path \"%s\" contains a syntactically invalid bundle ID \"%s\"",
                   path, id);
        }
      else
        {
          /* We don't warn here to avoid breaking tests */
          DEBUG ("In path \"%s\", treating syntactically invalid bundle ID \"%s\" as not a real bundle",
                 path, id);
          g_clear_pointer (&id, g_free);
          type = CBY_PROCESS_TYPE_UNKNOWN;
        }
    }

  if (out_bundle_id == NULL)
    g_free (id);
  else
    *out_bundle_id = id;

  return type;
}

/**
 * cby_get_bundle_id:
 *
 * Return the bundle ID of the current process. If the current process
 * is not running in the security context of an application bundle,
 * return %NULL instead.
 *
 * Returns: (nullable): the bundle ID of the current process, or %NULL
 *  if it is not part of an application bundle.
 *
 * Since: 0.3.1
 */
const gchar *
cby_get_bundle_id (void)
{
  CbyProcessInfo *me;

  me = cby_process_info_get_self ();
  g_return_val_if_fail (CBY_IS_PROCESS_INFO (me), NULL);
  return cby_process_info_get_bundle_id (me);
}

/**
 * cby_is_entry_point_id:
 * @maybe_id: a potential entry point identifier
 *
 * Check whether @maybe_id is a syntactically valid application entry point
 * identifier. Valid application entry point identifiers follow the same rules
 * as application bundle identifiers.
 *
 * Returns: %TRUE if @maybe_id is in fact an entry point ID
 *
 * Since: 0.1612.1
 */
gboolean
cby_is_entry_point_id (const gchar *maybe_id)
{
  return g_dbus_is_interface_name (maybe_id);
}
