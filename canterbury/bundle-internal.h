/*<private_header>*/
/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CANTERBURY_BUNDLE_INTERNAL_H__
#define __CANTERBURY_BUNDLE_INTERNAL_H__

#include "canterbury/process-info.h"

CbyProcessType _cby_parse_installation_path (const gchar *path,
                                             gchar **out_bundle_id);

#endif
