/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "audio-mgr"
#include "audio_mgr_if.h"
#include "audio_mgr.h"

#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "canterbury/gdbus/canterbury.h"
#include "canterbury/messages-internal.h"
#include "canterbury/platform/entry-point-internal.h"

typedef struct _AudioInvoke AudioInvoke;
struct _AudioInvoke
{
  guint uinAudioResourceType;
  gchar *pAppName;
  guint32 uinSinkIndex;
  gchar *pMethodName;
};

AudioMgrPriv pAudioMgrPriv;
CanterburyAudioMgr* pAudioMgrDbusObj = NULL;
static gboolean check_if_requested_app_has_priority( guint uinAudioResourceType , const gchar* pAppName );
static gboolean check_if_requested_app_has_audio_resource_permission( const gchar* pAppName );
static gboolean check_if_requested_app_can_be_played( const gchar* pAppName );
static void replace_current_audio_source (void);
static gboolean pAudioInvoke (void);
static gboolean audio_release (void);
static void play_current_audio_source_from_stack (void);
static void remove_current_audio_source_from_stack (void);
static void free_audio_app_info( AudioAppEntryInfo* pAudioEntry );
static gboolean check_if_requested_app_is_already_playing (const gchar *pAppName,
    const gchar *pMethodName);

static void v_automotive_proxy_audio_invoke (const gchar *pMethodName,
                                             const gchar *pAppName,
                                             guint32      uinSinkIndex,
                                             guint        uinAudioResourceType);
static void v_process_audio_iface_signal (gint audio_signal);
static void v_initialize_audio_sources (void);
static guint get_current_audio_resource_type (const gchar *pAppName);
static void v_initialize_audio_priority_hash_table (void);
static void audio_response_signal (gchar *pAppName,
                                   guint  audio_result);

static gboolean audio_request_clb( CanterburyAudioMgr *object,
                                   GDBusMethodInvocation   *invocation,
                                   const gchar             *pAppName,
                                   gpointer                pUserData)
{

	audio_mgr_debug ("\n **************************************  AUDIO MGR Audio request ************************************************ \n" );
	/* Step 1 :: check if requested app has the correct rights for the audio resource */
	audio_mgr_debug (" \n AUDIO MGR: check if requested app has the correct rights for the audio resource \n" );
	if ( check_if_requested_app_can_be_played ( pAppName ) )
	{
		audio_mgr_debug (" \n AUDIO MGR: Step 1 :: requested audio is a playable app \n" );

		/* Step 2 :: free the prev requested application info*/
		free_audio_app_info( &pAudioMgrPriv.RequestedAppInfo );
		audio_mgr_debug (" \n AUDIO MGR: Step 2 ::  free the prev requested application info \n" );

		/* Step 3 :: store the information about the requested application */
		pAudioMgrPriv.RequestedAppInfo.pAppName = g_strdup(pAppName);
		pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType = get_current_audio_resource_type( pAppName );
		audio_mgr_debug (" \n AUDIO MGR: Step 3 :: store the information about the requested application  \n" );

		if( FALSE == check_if_requested_app_is_already_playing(pAppName , CANTERBURY_APP_AUDIO_REQUEST ) )
		{
			audio_mgr_debug (" \n AUDIO MGR: Step 4 :: Audio request called for an app which is currently not playing \n" );
			if( pAudioInvoke( ) )
			{
				v_automotive_proxy_audio_invoke(CANTERBURY_APP_AUDIO_REQUEST , pAppName , 0 , pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType );
				canterbury_audio_mgr_complete_audio_request( object , invocation );
			}
			else
			{
				g_dbus_method_invocation_return_dbus_error (invocation,
						"canterbury.AudioMgr.Request.Failed",
						"Requested Audio app does cannot be played");
				return TRUE;
			}
		}
		else
		{
			gchar* request_app_name = g_strdup(pAppName);
			canterbury_audio_mgr_complete_audio_request( object , invocation );
			audio_response_signal( request_app_name , CANTERBURY_AUDIO_PLAY );
			audio_mgr_debug (" AUDIO MGR: requested_app_is_already_playing \n" );
		}
		audio_mgr_debug (" *********************************************  AUDIO MGR ***************************************************** \n" );
	}
	else
	{
		g_dbus_method_invocation_return_dbus_error (invocation,
				"canterbury.AudioMgr.Request.Failed",
				"Requested Audio app does not have the priority");
		return TRUE;
	}
	return TRUE;
}

static gboolean audio_release_clb( CanterburyAudioMgr *object,
                                   GDBusMethodInvocation   *invocation,
                                   const gchar             *pAppName,
                                   gpointer                pUserData)
{
	audio_mgr_debug ("\n ***************************************  AUDIO MGR:: Audio Release *********************************************** \n" );
	/* Step 1 :: check if requested app has the correct rights for the audio resource */
	if ( check_if_requested_app_can_be_played (pAppName) )
	{
		audio_mgr_debug (" \n AUDIO MGR: Step 1 :: requested audio is a playable app \n" );

		/* Step 2 :: free the prev requested application info*/
		free_audio_app_info( &pAudioMgrPriv.RequestedAppInfo );
		audio_mgr_debug (" \n AUDIO MGR: Step 2 ::  free the prev requested application info \n" );

		/* Step 3 :: store the information about the requested application */
		pAudioMgrPriv.RequestedAppInfo.pAppName = g_strdup(pAppName);
		pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType = get_current_audio_resource_type( pAppName );
		audio_mgr_debug (" \n AUDIO MGR: Step 3 :: store the information about the requested application  \n" );

		if( audio_release( ) )
		{
			audio_mgr_debug (" AUDIO MGR: Step 6 :: audio_release success \n" );
			//v_automotive_proxy_audio_invoke(CANTERBURY_APP_AUDIO_RELEASE , pAppName , 0 , pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType );

			audio_mgr_debug ("  \n AUDIO MGR: Step 7 :: remove the interrupt source from the stack \n" );
			remove_current_audio_source_from_stack();

			audio_mgr_debug ("  \n AUDIO MGR: Step 8 :: play the normal source if available in the stack \n" );
			play_current_audio_source_from_stack();
			canterbury_audio_mgr_complete_audio_release( object , invocation );
		}
		else
		{
			g_dbus_method_invocation_return_dbus_error (invocation,
					"canterbury.AudioMgr.Release.Failed",
					"Requested Audio cannot be released");
			return TRUE;
		}
	}
	else
	{
		g_dbus_method_invocation_return_dbus_error (invocation,
				"canterbury.AudioMgr.Release.Failed",
				"Requested Audio app does not have the priority");
		return TRUE;
	}
	return TRUE;
}

static void
audio_response_signal (gchar *pAppName,
                       guint  uinAudioResult)
{
	canterbury_audio_mgr_emit_audio_response( pAudioMgrDbusObj ,
			pAppName,
			uinAudioResult );
	g_free(pAppName);
}

void v_audio_pitch_signal( gdouble pitch )
{
	canterbury_audio_mgr_emit_voice_pitch( pAudioMgrDbusObj ,pitch );
}

void
init_audio_mgr (GDBusConnection *connection)
{
    DEBUG ("enter");

    v_initialize_audio_sources ();

    v_initialize_pulse_audio_subscription();


	pAudioMgrDbusObj = canterbury_audio_mgr_skeleton_new();

	g_signal_connect (pAudioMgrDbusObj,
			"handle-audio-request",
			G_CALLBACK (audio_request_clb),
			NULL);

	g_signal_connect (pAudioMgrDbusObj,
			"handle-audio-release",
			G_CALLBACK (audio_release_clb),
			NULL);

	/* Exports interface launcher_object at object_path "/canterbury/AudioMgr" on connection */
	if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (pAudioMgrDbusObj),
				connection,
				"/org/apertis/Canterbury/AudioMgr",
				NULL))
	{
		audio_mgr_debug("export error \n");
	}
}

/*****************************************************************
 * function: automotive_set_audio_sources()
 * make a local repository of the available audio sources on X86.
 * Collect the Application ID, Application Name and Source type.
 ******************************************************************/
static void
v_initialize_audio_sources (void)
{
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (GPtrArray) entry_points = NULL;
  guint i;
  CbyEntryPoint *entry_point = NULL;

  entry_point_index = launch_mgr_get_entry_point_index ();

  pAudioMgrPriv.appEntriesDB = NULL;

  entry_points = cby_entry_point_index_get_entry_points (entry_point_index);
  for (i = 0; i < entry_points->len; i++)
    {
      AudioEntry *pAudioEntry = g_new0 (AudioEntry, 1);

      entry_point = g_ptr_array_index (entry_points, i);
      pAudioEntry = g_new0 (AudioEntry, 1);
      pAudioEntry->AppName = g_strdup (cby_entry_point_get_id (entry_point));
      pAudioEntry->uinAudioResourceType = _cby_entry_point_get_audio_resource_type (entry_point);
      if (_cby_entry_point_get_audio_channel (entry_point) != NULL)
        pAudioEntry->pAudioChannelName = g_strdup (_cby_entry_point_get_audio_channel (entry_point));
      else
        pAudioEntry->pAudioChannelName = g_strdup ("error") ;
      audio_mgr_debug ("AUDIO MGR: App name %s , Audio resource type %d \n", pAudioEntry->AppName , pAudioEntry->uinAudioResourceType);
      pAudioMgrPriv.appEntriesDB =  g_list_append ( pAudioMgrPriv.appEntriesDB , pAudioEntry );
    }

	v_initialize_audio_priority_hash_table();
	pAudioMgrPriv.pAudioStack = NULL;
}

void
audio_mgr_update_with_new_audio_source (const gchar *app_name,
                                        guint audio_resource_type)
{
  GList *l = NULL;
  AudioEntry *audio_entry = NULL;

  for (l = pAudioMgrPriv.appEntriesDB; l != NULL; l = g_list_next (l))
    {
      audio_entry = l->data;
      if (g_strcmp0 (audio_entry->AppName, app_name) == 0)
        {
          audio_entry->uinAudioResourceType = audio_resource_type;
          return;
        }
    }

  audio_entry = g_new0 (AudioEntry, 1);
  audio_entry->AppName = g_strdup (app_name);
  audio_entry->uinAudioResourceType = audio_resource_type;
  pAudioMgrPriv.appEntriesDB =  g_list_append (pAudioMgrPriv.appEntriesDB, audio_entry);
}

void
audio_mgr_update_with_audio_channel_name (const gchar *app_name,
                                          const gchar *audio_channel_name)
{
  GList *l = NULL;
  AudioEntry *audio_entry = NULL;

  for (l = pAudioMgrPriv.appEntriesDB; l != NULL; l = g_list_next (l))
    {
      audio_entry = l->data;
      if (g_strcmp0 (audio_entry->AppName, app_name) == 0)
        {
          g_free (audio_entry->pAudioChannelName);
          audio_entry->pAudioChannelName = g_strdup (audio_channel_name);
          return;
        }
    }

  audio_entry = g_new0 (AudioEntry, 1);
  audio_entry->AppName = g_strdup (app_name);
  audio_entry->pAudioChannelName = g_strdup (audio_channel_name) ;
  pAudioMgrPriv.appEntriesDB = g_list_append (pAudioMgrPriv.appEntriesDB, audio_entry);
}

void
audio_mgr_update_with_removed_audio_source (const gchar *app_name)
{
  GList *l = NULL;
  AudioEntry *audio_entry = NULL;

  for (l = pAudioMgrPriv.appEntriesDB; l != NULL; l = g_list_next (l))
    {
      audio_entry = l->data;
      if (g_strcmp0 (audio_entry->AppName, app_name) == 0)
        {
          free_audio_app_info (&pAudioMgrPriv.RequestedAppInfo);
          audio_mgr_debug ("Step 1: free the prev requested application info\n");

          pAudioMgrPriv.RequestedAppInfo.pAppName = g_strdup (app_name);
          pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType =
              get_current_audio_resource_type (app_name);
          audio_mgr_debug ("Step 2: store the information about the requested application\n");

          if (audio_release ())
            {
              audio_mgr_debug ("Step 3: audio release success\n");
              v_automotive_proxy_audio_invoke (CANTERBURY_APP_AUDIO_RELEASE, app_name, 0,
                                               pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType);

              audio_mgr_debug ("Step 4: remove the interrupt source from the stack\n");
              remove_current_audio_source_from_stack ();

              audio_mgr_debug ("Step 5: play the normal source if available in the stack\n");
              play_current_audio_source_from_stack ();
            }

          pAudioMgrPriv.appEntriesDB = g_list_remove (pAudioMgrPriv.appEntriesDB, audio_entry);
          g_free (audio_entry->AppName);
          break;
      }
  }
}

static void
v_insert_audio_resource_into_hash_table (guint prio,
                                         guint audio_resource)
{
	guint *priority;
	gint *uinAudioResourceType;

	priority = g_new0 (guint, 1);
	*priority = prio;
	uinAudioResourceType = g_new0 (gint, 1);
	*uinAudioResourceType = (gint)audio_resource;

	g_hash_table_insert(   pAudioMgrPriv.priority_hash_table,
			uinAudioResourceType,
			priority
			);
}

static void
v_initialize_audio_priority_hash_table (void)
{
	pAudioMgrPriv.priority_hash_table = g_hash_table_new( g_int_hash ,  g_int_equal );
	v_insert_audio_resource_into_hash_table( 0xFF , CANTERBURY_AUDIO_RESOURCE_NONE );
	v_insert_audio_resource_into_hash_table( 0x03 , CANTERBURY_AUDIO_RESOURCE_MUSIC );
	v_insert_audio_resource_into_hash_table( 0x03 , CANTERBURY_AUDIO_RESOURCE_INTERRUPT );
	v_insert_audio_resource_into_hash_table( 0x01 , CANTERBURY_AUDIO_RESOURCE_PHONE );
	v_insert_audio_resource_into_hash_table( 0x03 , CANTERBURY_AUDIO_RESOURCE_VIDEO );
	v_insert_audio_resource_into_hash_table( 0x03 , CANTERBURY_AUDIO_RESOURCE_RECORD );
	v_insert_audio_resource_into_hash_table( 0x03 , CANTERBURY_AUDIO_RESOURCE_EXTERNAL );
}

/********************************************************************************
 * function: check_if_requested_app_has_audio_resource_permission()
 * It finds out the Audio app. name of the current app. making an Audio request
 ********************************************************************************/
static gboolean check_if_requested_app_has_audio_resource_permission( const gchar* pAppName )
{
	guint cnt;
	guint list_length = g_list_length( pAudioMgrPriv.appEntriesDB );
	for( cnt = 0 ; cnt< list_length ; cnt++)
	{
		AudioEntry *pAudioEntry = (AudioEntry*)g_list_nth_data ( pAudioMgrPriv.appEntriesDB , cnt );
		audio_mgr_debug ("AUDIO MGR: App name frm db %s  req app %s\n" , pAudioEntry->AppName , pAppName);
		if( !strcmp( pAudioEntry->AppName , pAppName ) )
			return TRUE;
	}

	audio_mgr_debug ("AUDIO MGR: check_if_requested_app_has_audio_resource_permission failed list length\n");
	return FALSE;
}

gchar *
audio_mgr_get_current_active_audio_source (void)
{
  AudioAppEntryInfo *current_active_audio_entry = NULL;

  current_active_audio_entry =
      (AudioAppEntryInfo *) g_list_nth_data (pAudioMgrPriv.pAudioStack, 0);
  if ((current_active_audio_entry != NULL) &&
      (current_active_audio_entry->uinAudioStatus == CANTERBURY_AUDIO_PLAY))
    return g_strdup (current_active_audio_entry->pAppName);
  return NULL;
}

static gboolean
b_hash_find_int_key (gpointer key,
    gpointer value,
    gpointer pUserData)
{
    return g_int_equal  (key, pUserData);
}

/********************************************************************************
 * function: check_if_requested_app_has_priority()
 * It finds out the  current app. making an Audio request has the desired priority
 ********************************************************************************/
static gboolean check_if_requested_app_has_priority( guint uinAudioResourceType , const gchar* pAppName )
{
	AudioAppEntryInfo* pCurrentActiveAudioEntry = NULL;
	guint* pCurrentAppsPriority = NULL;
	guint* pRequestedAppsPriority = NULL;

	pCurrentActiveAudioEntry = g_list_nth_data (pAudioMgrPriv.pAudioStack, 0);

	if(pCurrentActiveAudioEntry)
	{
		pCurrentAppsPriority = g_hash_table_find (  pAudioMgrPriv.priority_hash_table ,
                                                 b_hash_find_int_key,
                                                 &pCurrentActiveAudioEntry->uinAudioResourceType );
	}
	else
	{
	  gint uinAudioResourceTypelocal = CANTERBURY_AUDIO_RESOURCE_NONE;
		pCurrentAppsPriority = g_hash_table_find ( pAudioMgrPriv.priority_hash_table ,
                                                b_hash_find_int_key,
                                                &uinAudioResourceTypelocal );
	}

	pRequestedAppsPriority = g_hash_table_find ( pAudioMgrPriv.priority_hash_table ,
                                                b_hash_find_int_key,
                                                &uinAudioResourceType );

	audio_mgr_debug ("AUDIO MGR: requested prio %d , current prio %d \n" , *pRequestedAppsPriority , *pCurrentAppsPriority );
	if( *pRequestedAppsPriority <= *pCurrentAppsPriority)
		return TRUE;
	else
		return FALSE;
}

gchar *
audio_mgr_get_current_normal_audio_source (void)
{
  guint len = g_list_length (pAudioMgrPriv.pAudioStack);
  if (len != 0)
    {
      AudioAppEntryInfo *current_active_audio_entry =
          (AudioAppEntryInfo *) g_list_nth_data (pAudioMgrPriv.pAudioStack, len - 1);
      if (current_active_audio_entry != NULL)
        {
          guint resource_type;

          resource_type = get_current_audio_resource_type (current_active_audio_entry->pAppName);
          if ((resource_type == CANTERBURY_AUDIO_RESOURCE_MUSIC) ||
              (resource_type == CANTERBURY_AUDIO_RESOURCE_VIDEO))
            {
              return g_strdup (current_active_audio_entry->pAppName);
            }
        }
    }
  return NULL;
}

static guint
get_current_audio_resource_type (const gchar *pAppName)
{
	guint cnt;
	guint list_length = g_list_length( pAudioMgrPriv.appEntriesDB );
	for( cnt = 0 ; cnt<list_length ; cnt++)
	{
		AudioEntry *pAudioEntry = (AudioEntry*)g_list_nth_data ( pAudioMgrPriv.appEntriesDB , cnt );
		if( !strcmp( pAudioEntry->AppName , pAppName ) )
			return pAudioEntry->uinAudioResourceType;
	}

	return CANTERBURY_AUDIO_RESOURCE_NONE;
}

/***************************************************************************
 * function: v_automotive_proxy_audio_invoke()
 * Fn. sends the AudioRequest and AudioRelease methods to automotive proxy
 ***************************************************************************/
static void
v_automotive_proxy_audio_invoke(const gchar *pMethodName,
                                const gchar *pAppName,
                                guint32      uinSinkIndex,
                                guint        uinAudioResourceType)
{
  if (pMethodName == NULL)
    return;

  if (!strcmp (pMethodName, CANTERBURY_APP_AUDIO_REQUEST))
    {
      // check if the requested app is an external source
      if (uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_EXTERNAL)
        {
          audio_mgr_debug ("\n AUDIO MGR: Step X :: send play signal to the external application %s \n", pAppName);
          v_send_external_app_signal(uinSinkIndex , CANTERBURY_AUDIO_PLAY);
        }
      else
        {
          audio_mgr_debug ("\n AUDIO MGR: Step X :: send play signal to the internal application %s \n", pAppName);
          v_process_audio_iface_signal(CANTERBURY_AUDIO_PLAY);
        }
    }
  else
    {
      // check if the requested app is an external source
      if (uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_EXTERNAL)
        {
          audio_mgr_debug ("\n AUDIO MGR: Step X :: send stop signal to the external application %s \n", pAppName);
          v_send_external_app_signal(uinSinkIndex , CANTERBURY_AUDIO_STOP);
        }
      else
        {
          if ((uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_INTERRUPT)
            || (uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_PHONE))
            {
              audio_mgr_debug ("\n AUDIO MGR: Step X ::  send stop signal to the internal interrupt application  %s \n", pAppName);
              v_process_audio_iface_signal(CANTERBURY_AUDIO_STOP);
            }
          else
            {
              audio_mgr_debug ("\n AUDIO MGR: Step X ::  send pause signal to the internal normal application  %s \n", pAppName);
              v_process_audio_iface_signal(CANTERBURY_AUDIO_PAUSE);
            }
        }
    }
}

/***************************************************************************
 * function: v_process_audio_iface_signal()
 * Clb fn invoked by the automotive proxy indicating the audio app play status
 ***************************************************************************/
static void
v_process_audio_iface_signal (gint uinAudioSignal)
{
	gboolean bSendResponse = FALSE;
	AudioAppEntryInfo* pCurrentActiveAudioEntry =NULL;

	pCurrentActiveAudioEntry = (AudioAppEntryInfo*)g_list_nth_data ( pAudioMgrPriv.pAudioStack , 0 );
	if(pCurrentActiveAudioEntry == NULL)
		return;

	switch(uinAudioSignal)
	{
		case CANTERBURY_AUDIO_PLAY:
			audio_mgr_debug (" AUDIO MGR: CANTERBURY_AUDIO_PLAY\n");
			pCurrentActiveAudioEntry->uinAudioStatus = CANTERBURY_AUDIO_PLAY;
			bSendResponse = TRUE;
			break;
		case CANTERBURY_AUDIO_STOP:
			audio_mgr_debug (" AUDIO MGR: AUDIO_STOP \n");
			pCurrentActiveAudioEntry->uinAudioStatus = CANTERBURY_AUDIO_STOP;
			bSendResponse = TRUE;
			break;
		case CANTERBURY_AUDIO_PAUSE:
			audio_mgr_debug (" AUDIO MGR: Audio signal pause forwarded to HMI\n");
			pCurrentActiveAudioEntry->uinAudioStatus = CANTERBURY_AUDIO_STOP;
			bSendResponse = TRUE;
			break;
		default:
			break;
	}

	if(bSendResponse == TRUE)
	{
		audio_response_signal( g_strdup(pCurrentActiveAudioEntry->pAppName) ,uinAudioSignal );
	}
}

static gboolean check_if_requested_app_can_be_played( const gchar* pAppName )
{
	guint uinAudioResourceType = get_current_audio_resource_type( pAppName );
	if(
			( FALSE == check_if_requested_app_has_audio_resource_permission( pAppName  ) )
			||
			( FALSE == check_if_requested_app_has_priority( uinAudioResourceType , pAppName ) )
	  )
	{
		audio_mgr_debug (" AUDIO MGR: requested app cannot be played: current audio invalid \n");
		return FALSE;
	}
	return TRUE;
}

static void free_audio_app_info( AudioAppEntryInfo* pAudioEntry )
{
	if(pAudioEntry->pAppName)
		g_free( pAudioEntry->pAppName );
	pAudioEntry->pAppName = NULL;

	if(pAudioEntry->PlayingTitle)
		g_free( pAudioEntry->PlayingTitle );
	pAudioEntry->PlayingTitle = NULL;

	if(pAudioEntry->PlayingArtist)
		g_free( pAudioEntry->PlayingArtist );
	pAudioEntry->PlayingArtist = NULL;

	pAudioEntry->uinAudioStatus = CANTERBURY_AUDIO_STOP;

	pAudioEntry->uinSinkIndex = 0x0;

}

static gboolean request_for_audio_change_clb( gpointer pData )
{
	AudioInvoke* pAudioInvoke = pData ;
	v_automotive_proxy_audio_invoke( pAudioInvoke->pMethodName , pAudioInvoke->pAppName , pAudioInvoke->uinSinkIndex , pAudioInvoke->uinAudioResourceType );
	g_free(pAudioInvoke->pMethodName);
	g_free(pAudioInvoke->pAppName);

	return FALSE;
}

static void
handle_the_new_state_change_for_external_app (CanterburyAppState NewState,
    gboolean bApplicationAvailableInAudioStack,
    AudioAppEntryInfo* pCurrentActiveAudioEntry,
    const gchar *pAppName)
{
  switch( NewState )
  {
    case CANTERBURY_APP_STATE_RESTART:
    case CANTERBURY_APP_STATE_SHOW:
      {
        if(bApplicationAvailableInAudioStack)
        {
          if( pCurrentActiveAudioEntry->uinAudioStatus == CANTERBURY_AUDIO_STOP )
          {
            audio_mgr_debug ("\n AUDIO MGR: Step C :: play the external audio source \n"  );
            //g_timeout_add_seconds(3 , play_external_application , pCurrentActiveAudioEntry ) ;
            v_external_app_audio_invoke_clb( pCurrentActiveAudioEntry->pAppName , CANTERBURY_APP_AUDIO_REQUEST , pCurrentActiveAudioEntry->uinSinkIndex );
          }
        }
        else
        {
          guint uinClientCnt = 0;
          ExternalAudioEntry* pAudioEntry = NULL;
          for( uinClientCnt = 0 ; uinClientCnt< g_list_length( pAudioMgrPriv.pExtAudioEntryList ) ; uinClientCnt++)
          {
            pAudioEntry = g_list_nth_data ( pAudioMgrPriv.pExtAudioEntryList , uinClientCnt );
            if( ( !strcmp( pAppName , pAudioEntry->AppName ) ) ||
                ( ( !strcmp( pAudioEntry->AppName, "browser" ) && !strcmp( pAppName , "ALSA plug-in [npviewer.bin]" ) ) ) )
            {
              audio_mgr_debug ("\n AUDIO MGR: Step C :: External audio source is not available in audio stack , play it  sink index %d  App_index %d  mute_status %d\n" ,  pAudioEntry->uinSinkIndex , pAudioEntry->App_index , pAudioEntry->mute_status );
              v_external_app_audio_invoke_clb( pAudioEntry->AppName , CANTERBURY_APP_AUDIO_REQUEST , pAudioEntry->uinSinkIndex );
              //g_timeout_add_seconds(3 , play_external_application , pCurrentActiveAudioEntry ) ;
              break;
            }
          }
        }
      }
      break;

    case CANTERBURY_APP_STATE_PAUSE:
      if(bApplicationAvailableInAudioStack)
      {
        if( pCurrentActiveAudioEntry->uinAudioStatus == CANTERBURY_AUDIO_PLAY )
        {
          audio_mgr_debug ("\n AUDIO MGR: Step C :: pause the external audio source \n"  );
          //g_timeout_add_seconds(3 , pause_external_application , pCurrentActiveAudioEntry ) ;
          v_external_app_audio_invoke_clb( pCurrentActiveAudioEntry->pAppName , CANTERBURY_APP_AUDIO_REQUEST , pCurrentActiveAudioEntry->uinSinkIndex );
        }
      }
      break;

    case CANTERBURY_APP_STATE_START:
    case CANTERBURY_APP_STATE_BACKGROUND:
    case CANTERBURY_APP_STATE_OFF:
    case CANTERBURY_APP_STATE_INVALID:
    default:
      break;
  }
}

void
inform_app_new_state_to_audio_mgr (const gchar *pAppName,
                                   CanterburyAppState NewState)
{
	audio_mgr_debug ("\n AUDIO MGR: Step A :: inform_app_new_state_to_audio_mgr %s , NewState %d \n" , pAppName , NewState );
	if ( ( check_if_requested_app_can_be_played( pAppName )  &&
				( get_current_audio_resource_type(pAppName) == CANTERBURY_AUDIO_RESOURCE_EXTERNAL ) ) )
	{
		guint cnt;
		AudioAppEntryInfo* pCurrentActiveAudioEntry = NULL;
		gboolean bApplicationAvailableInAudioStack = FALSE;
		for ( cnt = 0; cnt < g_list_length( pAudioMgrPriv.pAudioStack ) ; cnt++ )
		{
			pCurrentActiveAudioEntry = (AudioAppEntryInfo*)g_list_nth_data ( pAudioMgrPriv.pAudioStack , cnt );
			if( ( !strcmp( pCurrentActiveAudioEntry->pAppName, pAppName ) ) ||
					( ( !strcmp( pCurrentActiveAudioEntry->pAppName, "browser" ) && !strcmp( pAppName , "ALSA plug-in [npviewer.bin]" ) ) ) )
			{
				audio_mgr_debug ("\n AUDIO MGR: Step B :: application available in audio stack \n"  );
				bApplicationAvailableInAudioStack = TRUE;
				break;
			}
		}

		handle_the_new_state_change_for_external_app(NewState , bApplicationAvailableInAudioStack , pCurrentActiveAudioEntry , pAppName);

	}
}

static void remove_current_audio_source_from_stack (void)
{
	AudioAppEntryInfo* pCurrentActiveAudioEntry = NULL;
	pCurrentActiveAudioEntry = (AudioAppEntryInfo*)g_list_nth_data ( pAudioMgrPriv.pAudioStack , 0 );
	if(pCurrentActiveAudioEntry)
	{
		pAudioMgrPriv.pAudioStack = g_list_remove ( pAudioMgrPriv.pAudioStack ,pCurrentActiveAudioEntry );
		/*  free the prev current application info*/
		free_audio_app_info( pCurrentActiveAudioEntry );
		pCurrentActiveAudioEntry = NULL;
	}
	pCurrentActiveAudioEntry = (AudioAppEntryInfo*)g_list_nth_data ( pAudioMgrPriv.pAudioStack , 0 );
  if(pCurrentActiveAudioEntry)
	{
		set_current_active_audio_source(pCurrentActiveAudioEntry->pAppName);
	}
	else
	{
	  set_current_active_audio_source("None");
	}
}

static void replace_current_audio_source (void)
{
	AudioAppEntryInfo* pCurrentActiveAudioEntry = NULL;
	if( g_list_length( pAudioMgrPriv.pAudioStack ) )
	{
		remove_current_audio_source_from_stack();

		pCurrentActiveAudioEntry = g_new0( AudioAppEntryInfo , 1);
		pCurrentActiveAudioEntry->pAppName = g_strdup(pAudioMgrPriv.RequestedAppInfo.pAppName );
		pCurrentActiveAudioEntry->uinAudioResourceType = pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType;
		pAudioMgrPriv.pAudioStack =  g_list_prepend ( pAudioMgrPriv.pAudioStack , pCurrentActiveAudioEntry );
		set_current_active_audio_source(pCurrentActiveAudioEntry->pAppName);
	}
}

static void insert_requested_audio_source (void)
{
	AudioAppEntryInfo* pCurrentActiveAudioEntry = g_new0( AudioAppEntryInfo , 1);

	pCurrentActiveAudioEntry->pAppName = g_strdup(pAudioMgrPriv.RequestedAppInfo.pAppName );
	pCurrentActiveAudioEntry->uinAudioResourceType = pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType;
	pAudioMgrPriv.pAudioStack =  g_list_prepend ( pAudioMgrPriv.pAudioStack , pCurrentActiveAudioEntry );
	set_current_active_audio_source(pCurrentActiveAudioEntry->pAppName);
}

static gboolean
check_if_requested_app_is_already_playing (const gchar *pAppName,
    const gchar *pMethodName)
{
	AudioAppEntryInfo* pCurrentActiveAudioEntry = NULL;
	if( g_list_length( pAudioMgrPriv.pAudioStack ) )
	{
		/* the current playing application will be on top of the audio stack */
		pCurrentActiveAudioEntry = (AudioAppEntryInfo*)g_list_nth_data ( pAudioMgrPriv.pAudioStack , 0 );
	}

	if( pCurrentActiveAudioEntry )
	{
		if(
				( !strcmp( pMethodName , CANTERBURY_APP_AUDIO_REQUEST )
				  &&
				  ( !strcmp( pCurrentActiveAudioEntry->pAppName , pAppName) ) )
				&&
				( pCurrentActiveAudioEntry->uinAudioStatus  == CANTERBURY_AUDIO_PLAY )
		  )
			return TRUE;
	}

	return FALSE;
}

static gboolean audio_release (void)
{
	AudioAppEntryInfo* pCurrentActiveAudioEntry = NULL;

	audio_mgr_debug ("AUDIO MGR: Step 4 :: Audio release request has arrived\n");
	if( g_list_length( pAudioMgrPriv.pAudioStack ) )
	{
		/* the current playing application will be on top of the audio stack */
		pCurrentActiveAudioEntry = (AudioAppEntryInfo*)g_list_nth_data ( pAudioMgrPriv.pAudioStack , 0 );
	}

	if( (pCurrentActiveAudioEntry ) )
	{
		if( pCurrentActiveAudioEntry->uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_EXTERNAL )
		{
			if ( ( !strcmp( pCurrentActiveAudioEntry->pAppName , pAudioMgrPriv.RequestedAppInfo.pAppName) ) &&
					( pCurrentActiveAudioEntry->uinAudioStatus  == CANTERBURY_AUDIO_PLAY ) )
			{
				audio_mgr_debug ("\n AUDIO MGR: Step 5 :: Application is of type external source, release the audio \n" );
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			if (
					( !strcmp( pCurrentActiveAudioEntry->pAppName , pAudioMgrPriv.RequestedAppInfo.pAppName) )
					&&
					( pCurrentActiveAudioEntry->uinAudioStatus  == CANTERBURY_AUDIO_PLAY )
					&&
					( ( pCurrentActiveAudioEntry->uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_INTERRUPT  ) ||
					  ( pCurrentActiveAudioEntry->uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_PHONE )
					)
			   )
			{
				audio_mgr_debug ("\n AUDIO MGR: Step 5 :: Application is of type interrupt source, release the audio \n" );
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
	return FALSE;
}

static void play_current_audio_source_from_stack (void)
{
	AudioAppEntryInfo* pCurrentActiveAudioEntry = NULL;
	if( g_list_length( pAudioMgrPriv.pAudioStack ) )
	{
		/* the current playing application will be on top of the audio stack */
		pCurrentActiveAudioEntry = (AudioAppEntryInfo*)g_list_nth_data ( pAudioMgrPriv.pAudioStack , 0 );
		v_automotive_proxy_audio_invoke(CANTERBURY_APP_AUDIO_REQUEST , pCurrentActiveAudioEntry->pAppName , pCurrentActiveAudioEntry->uinSinkIndex , pCurrentActiveAudioEntry->uinAudioResourceType );
	}
}

static gboolean
setup_audio_route_for_interrupt_source (
    const AudioAppEntryInfo *pCurrentActiveAudioEntry)
{
  /* Step 7 :: check if current audio source is of type interrupt or normal */
  if( ( pCurrentActiveAudioEntry->uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_INTERRUPT ) ||
      ( pCurrentActiveAudioEntry->uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_PHONE ) )
  {
    audio_mgr_debug ("\n AUDIO MGR: Step 7 :: replace current interrupt source with new interrupt source \n" );
    /* Step 8 :: replace current interrupt source with new interrupt source */
    replace_current_audio_source();
  }
  else
  {
    audio_mgr_debug ("\n AUDIO MGR: Step 7 :: check if current source is the only source in the audio stack \n" );
    /* Step 8 :: check if current source is the only source in the audio stack */
    if( g_list_length( pAudioMgrPriv.pAudioStack ) == 1 )
    {
      audio_mgr_debug ("\n AUDIO MGR: Step 8 :: place the new interrupt source on top of the current audio source \n" );
      /* Step 9 :: place the new interrupt source on top of the current audio source */
      insert_requested_audio_source();
    }
    else
    {
      g_printerr("something is wrong in the audio stack, ... \n");
      return FALSE;
    }
  }
  return TRUE;
}

static gboolean
setup_audio_route_for_normal_source (
    const AudioAppEntryInfo *pCurrentActiveAudioEntry)
{
  /* the requested source is of type normal */
  audio_mgr_debug ("\n AUDIO MGR: Step 7 :: check if current audio source is of type interrupt or normal  \n" );
  /* Step 7 :: check if current audio source is of type interrupt or normal */
  if( ( pCurrentActiveAudioEntry->uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_INTERRUPT ) ||
      ( pCurrentActiveAudioEntry->uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_PHONE ) )
  {
    audio_mgr_debug ("\n AUDIO MGR: Step 8 :: check if current source is the only source in the audio stack  \n" );
    /* Step 8 :: check if current source is the only source in the audio stack */
    if( g_list_length( pAudioMgrPriv.pAudioStack ) == 1 )
    {
      audio_mgr_debug ("\n AUDIO MGR: Step 9 :: replace current interrupt source with new normal source \n" );
      /* Step 9 :: replace current interrupt source with new normal source */
      replace_current_audio_source();
    }
    else
    {
      audio_mgr_debug ("\n AUDIO MGR: Step 10 :: remove the current interrupt source from the stack \n" );
      remove_current_audio_source_from_stack();

      audio_mgr_debug ("\n AUDIO MGR: Step 11 :: replace current normal source with new normal source \n" );
      /* Step 11 :: replace current normal source with new normal source */
      replace_current_audio_source();
    }
  }
  else
  {
    audio_mgr_debug (" \n AUDIO MGR: Step 8 :: check if current source is the only source in the audio stack \n" );
    /* Step 8 :: check if current source is the only source in the audio stack */
    if( g_list_length( pAudioMgrPriv.pAudioStack ) == 1 )
    {
      audio_mgr_debug ("\n AUDIO MGR: Step 9 :: replace current normal source with new normal source \n" );
      /* Step 9 :: replace current normal source with new normal source*/
      replace_current_audio_source();
    }
    else
    {
      g_printerr("something is wrong in the audio stack, ... \n");
      return FALSE;
    }
  }
  return TRUE;
}

static gboolean
pAudioInvoke (void)
{
	/* Step 4 :: get the current playing application info */
	AudioAppEntryInfo* pCurrentActiveAudioEntry = NULL;
	if( g_list_length( pAudioMgrPriv.pAudioStack ) )
	{
		/* the current playing application will be on top of the audio stack */
		pCurrentActiveAudioEntry = (AudioAppEntryInfo*)g_list_nth_data ( pAudioMgrPriv.pAudioStack , 0 );
	}

	if( pCurrentActiveAudioEntry )
	{
		audio_mgr_debug ("\n AUDIO MGR: Step 5 :: pause the current playing audio \n" );
		/* Step 5 :: pause the current playing audio */
		v_automotive_proxy_audio_invoke( CANTERBURY_APP_AUDIO_RELEASE , pCurrentActiveAudioEntry->pAppName , pCurrentActiveAudioEntry->uinSinkIndex , pCurrentActiveAudioEntry->uinAudioResourceType );

		audio_mgr_debug ("\n AUDIO MGR: Step 6 :: check if requested audio source is of type interrupt or normal \n" );
		/* Step 6 :: check if requested audio source is of type interrupt or normal */
		if( ( pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_INTERRUPT ) ||
				( pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_PHONE ) )
		{
		  return setup_audio_route_for_interrupt_source(pCurrentActiveAudioEntry);
		}
		else
		{
		  return setup_audio_route_for_normal_source(pCurrentActiveAudioEntry);
		}
	}
	else  /* if( pCurrentActiveAudioEntry ) */
	{
		audio_mgr_debug ("\n AUDIO MGR: Step 5 :: The audio stack is empty , put the new application on top \n" );
		/* The audio stack is empty , put the new application on top */
		insert_requested_audio_source();
	}
	return TRUE;
}

void
v_external_app_audio_invoke_clb (const gchar *pAppName,
    const gchar *pMethodName,
    guint32 uinSinkIndex)
{
	audio_mgr_debug ("\n *********************************************  AUDIO MGR ***************************************************** \n" );
	if( ( pAppName == NULL) || (pMethodName == NULL) )
		return;
	/* Step 1 :: check if requested app has the correct rights for the audio resource */
	if ( check_if_requested_app_can_be_played (pAppName) )
	{
		audio_mgr_debug (" \n AUDIO MGR: Step 1 :: requested audio is a playable app \n" );
		/* Step 2 :: free the prev requested application info*/
		free_audio_app_info( &pAudioMgrPriv.RequestedAppInfo );
		audio_mgr_debug (" \n AUDIO MGR: Step 2 ::  free the prev requested application info \n" );

		/* Step 3 :: store the information about the requested application */
		pAudioMgrPriv.RequestedAppInfo.pAppName = g_strdup(pAppName);
		pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType = get_current_audio_resource_type( pAppName );
		audio_mgr_debug (" \n AUDIO MGR: Step 3 :: store the information about the requested application  \n" );

		if( !strcmp( pMethodName , CANTERBURY_APP_AUDIO_REQUEST ) )
		{
			if( audio_release( ) )
			{
				v_automotive_proxy_audio_invoke( pMethodName , pAppName , uinSinkIndex , pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType );
			}
			else
			{
				g_printerr (" AUDIO MGR: v_external_app_audio_invoke_clb failed Audio release not possible \n");
			}
		}
		else
		{
			if( pAudioInvoke( ) )
			{
				AudioInvoke* pAudioInvoke = g_new0(AudioInvoke , 1);
				pAudioInvoke->pAppName = g_strdup(pAppName);
				pAudioInvoke->uinAudioResourceType = pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType;
				pAudioInvoke->pMethodName = g_strdup(pMethodName);
				pAudioInvoke->uinSinkIndex = uinSinkIndex;
				g_timeout_add_seconds(3 , request_for_audio_change_clb , pAudioInvoke );
				//v_automotive_proxy_audio_invoke( pMethodName , pAppName , uinSinkIndex , pAudioMgrPriv.RequestedAppInfo.uinAudioResourceType );
			}
			else
			{
				g_printerr (" AUDIO MGR: v_external_app_audio_invoke_clb failed \n");
			}
		}
	}
	audio_mgr_debug ("\n *********************************************  AUDIO MGR ***************************************************** \n" );
}

