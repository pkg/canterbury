/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CANTERBURY_SRC_APPARMOR_H
#define CANTERBURY_SRC_APPARMOR_H

#include <glib.h>
#include <sys/apparmor.h>

G_BEGIN_DECLS

/*
 * Do not use %CBY_APPARMOR_PARSE_ACTION_REMOVE_ONLY_DO_THIS_IN_TESTS
 * under normal circumstances. If a profile is removed, any surviving
 * processes that are running under that profile become unconfined,
 * which would be very bad if they are untrusted code.
 */
typedef enum
{
  CBY_APPARMOR_PARSE_ACTION_REPLACE,
  CBY_APPARMOR_PARSE_ACTION_REMOVE_ONLY_DO_THIS_IN_TESTS,
} CbyAppArmorParseAction;

gboolean _cby_apparmor_load_stub_profile (const gchar *profile_name,
                                          GError **error);

gboolean _cby_apparmor_load_profile_from_string (const gchar *profile_text,
                                                 GError **error);

typedef struct _CbyAppArmorStore CbyAppArmorStore;

CbyAppArmorStore *_cby_apparmor_store_open (const gchar *profile_path,
                                            GError **error);
void _cby_apparmor_store_close (CbyAppArmorStore *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (CbyAppArmorStore, _cby_apparmor_store_close)

gboolean _cby_apparmor_store_disable_profile (CbyAppArmorStore *self,
                                              const gchar *bundle_id,
                                              GError **error);

gboolean _cby_apparmor_store_enable_and_load_profile (CbyAppArmorStore *self,
                                                      const gchar *profile_source,
                                                      const gchar *bundle_id,
                                                      GError **error);

gboolean _cby_apparmor_store_invalidate_ubercache (CbyAppArmorStore *self,
                                                   GError **error);

gboolean _cby_apparmor_store_ensure_and_load_ubercache (CbyAppArmorStore *self,
                                                        GError **error);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (aa_kernel_interface, aa_kernel_interface_unref)

G_END_DECLS

#endif
