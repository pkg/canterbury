/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "win-mgr"
#include "win_mgr_if.h"

#include <sys/time.h>
#include <unistd.h>

#include <systemd/sd-daemon.h>

#include "app_mgr_if.h"
#include "canterbury/errors.h"
#include "canterbury/gdbus/canterbury.h"
#include "canterbury/messages-internal.h"
#include "last_user_mode_if.h"
#include "launch_mgr_if.h"
#include "util.h"

CanterburyMutterPlugin*   pMutterProxy = NULL;

typedef enum
{
  TRISTATE_UNSET = -1,
  TRISTATE_FALSE,
  TRISTATE_TRUE,
} Tristate;

static gchar *deferred_active_title = NULL;
static gchar *deferred_active_category = NULL;
static Tristate deferred_last_user_mode_status = TRISTATE_UNSET;
static GTask *init_task = NULL;

gboolean
b_start_app_switch( const gchar* pAppType ,
                    const gchar* pLaunchedFrom ,
                    guint uinSpecialParams  ,
                    const gchar *pSplashScreenPath,
                    GAsyncReadyCallback app_switch_clb,
                    gpointer pUserData )
{
  if(pMutterProxy)
  {
	  canterbury_mutter_plugin_call_start_app_switch(pMutterProxy ,
                                               pAppType ,
                                               pLaunchedFrom,
                                               uinSpecialParams,
                                               pSplashScreenPath,
                                               NULL ,
                                               app_switch_clb,
                                               pUserData);
    return TRUE;
  }
  else
  {
    g_warning( "pMutterProxy unavailable \n" );
    return FALSE;
  }
}

void
window_mgr_set_active (const gchar *category,
                       const gchar *title)
{
  if (pMutterProxy != NULL)
    {
      canterbury_mutter_plugin_set_app_category (pMutterProxy, category);
      canterbury_mutter_plugin_set_app_name (pMutterProxy, title);
    }
  else
    {
      g_free (deferred_active_title);
      deferred_active_title = g_strdup (title);
      g_free (deferred_active_category);
      deferred_active_category = g_strdup (category);
    }
}

void
v_hide_splash_screen (void)
{
  if (pMutterProxy != NULL)
    canterbury_mutter_plugin_call_hide_splash_screen (pMutterProxy, NULL, NULL,
                                                      NULL);
  else
    g_warning ("Compositor unavailable");
}

void
v_display_off(gboolean bWithAnimation)
{
  if (pMutterProxy != NULL)
    canterbury_mutter_plugin_call_display_off_sync (pMutterProxy,
                                                    bWithAnimation,
                                                    NULL, NULL);
  else
    g_warning ("Compositor unavailable");
}

void
v_last_user_mode_status(gboolean bStatus)
{
  if (pMutterProxy != NULL)
    {
      canterbury_mutter_plugin_call_last_user_mode_status (pMutterProxy,
                                                           bStatus, NULL, NULL,
                                                           NULL);
    }
  else
    {
      /* If the compositor comes up later, we can tell it then */
      deferred_last_user_mode_status =
          (bStatus ? TRISTATE_TRUE : TRISTATE_FALSE);
    }
}

static void
app_switch_completed( CanterburyMutterPlugin  *object,
                      gpointer        user_data)
{
  reset_launch_progress_timer();
  activity_mgr_update_app_on_top ();
}

static void mutter_proxy_clb( GObject *source_object,
                              GAsyncResult *res,
                              gpointer pUserData)
{
  GError *error;
  /* finishes the proxy creation and gets the proxy ptr */
  pMutterProxy = canterbury_mutter_plugin_proxy_new_finish(res , &error);

  if(pMutterProxy == NULL)
  {
	  g_warning("error %s \n",g_dbus_error_get_remote_error(error));
  }
  else
  {
    g_signal_connect (pMutterProxy,
                      "app-switch-completed",
                      G_CALLBACK (app_switch_completed),
                      pUserData);

    /* If the last-user-mode module told us to push a status to the
     * compositor before we were ready, catch up now */
    switch (deferred_last_user_mode_status)
      {
      case TRISTATE_FALSE:
        canterbury_mutter_plugin_call_last_user_mode_status (pMutterProxy,
                                                             FALSE, NULL,
                                                             NULL, NULL);
        break;

      case TRISTATE_TRUE:
        canterbury_mutter_plugin_call_last_user_mode_status (pMutterProxy,
                                                             TRUE, NULL,
                                                             NULL, NULL);
        break;

      case TRISTATE_UNSET:
      default:
        /* nothing to be done */
        break;
      }

    /* We have dealt with it now */
    deferred_last_user_mode_status = TRISTATE_UNSET;

    if (deferred_active_title != NULL)
      {
        canterbury_mutter_plugin_set_app_name (pMutterProxy,
                                               deferred_active_title);
        g_clear_pointer (&deferred_active_title, g_free);
      }

    if (deferred_active_category != NULL)
      {
        canterbury_mutter_plugin_set_app_category (pMutterProxy,
                                                   deferred_active_category);
        g_clear_pointer (&deferred_active_category, g_free);
      }
  }

  if (init_task != NULL)
    {
      g_task_return_boolean (init_task, TRUE);
      g_clear_object (&init_task);
    }
}

static void
wm_name_appeared (GDBusConnection *connection,
                 const gchar     *name,
                 const gchar     *name_owner,
                 gpointer         pUserData)
{
	win_mgr_debug("name_appeared \n");

  /* Asynchronously creates a proxy for the D-Bus interface */
	canterbury_mutter_plugin_proxy_new (
                                    connection,
                                    G_DBUS_PROXY_FLAGS_NONE,
                                    "org.apertis.Canterbury.Mutter.Plugin",
                                    "/org/apertis/Canterbury/Mutter/Plugin",
                                    NULL,
                                    mutter_proxy_clb,
                                    NULL);
}

static void
wm_name_vanished(GDBusConnection *connection,
                 const gchar     *name,
                 gpointer         pUserData)
{
	if (NULL != pMutterProxy) {
		DEBUG ("%s disappeared from D-Bus", name);

		g_object_unref(pMutterProxy);
		pMutterProxy = NULL;
		/*added to kill all apps in case the window manager is restarting and app manager has not restarted*/
		if (_cby_is_target ())
		{
			DEBUG ("Restarting app manager in response");
			v_restart_app_manager();
		}
		else
		{
			DEBUG ("Not restarting app manager in SDK environment");
		}

	} else {
		if (init_task != NULL)
		{
			g_task_return_new_error (init_task, CBY_ERROR,
				CBY_ERROR_FAILED, "%s not found", name);
			g_clear_object (&init_task);
		}
	}
}

void
win_mgr_init_async (GDBusConnection *session_bus,
                    GCancellable *cancellable,
                    GAsyncReadyCallback callback,
                    gpointer user_data)
{
  /* Compositor integration can only be initialized once */
  g_return_if_fail (init_task == NULL);

  init_task = g_task_new (NULL, cancellable, callback, user_data);
  g_task_set_source_tag (init_task, win_mgr_init_async);

  g_bus_watch_name_on_connection (session_bus,
                                  "org.apertis.Canterbury.Mutter.Plugin",
                                  G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                                  wm_name_appeared,
                                  wm_name_vanished,
                                  NULL, NULL);
}

gboolean
win_mgr_init_finish (GAsyncResult *result,
                     GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, NULL), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result, win_mgr_init_async),
                        FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

void
win_mgr_minimize_current_application_window (const gchar *application_name,
    const gchar *pLaunchedClient)
{
  /* find the window ID of the application on top */
   if (launch_mgr_find_running_app (application_name) == NULL)
   {
     g_printerr("WIN MGR:: application not running\n");
     return;
   }
    /* invoke a minimize clb in the mutter plugin */
  if (pMutterProxy != NULL)
    canterbury_mutter_plugin_call_minimize_current_application (pMutterProxy,
                                                                pLaunchedClient,
                                                                NULL, NULL,
                                                                NULL);
  else
    g_warning ("Compositor unavailable");
}

void win_mgr_maximize_application_window(const gchar* pAppName)
{
  if (pMutterProxy != NULL)
    canterbury_mutter_plugin_call_maximize_application (pMutterProxy,
                                                        pAppName, NULL, NULL,
                                                        NULL);
  else
    g_warning ("Compositor unavailable");
}
