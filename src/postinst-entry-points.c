/*
 * canterbury-postinst: configure an app-bundle after mounting it for the
 * first time
 *
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>
#include "canterbury/messages-internal.h"

#include "util.h"

static GOptionEntry entries[] = { { NULL } };

/*
 * Check https://appdev.apertis.org/documentation/bundle-spec.html#icon-for-the-bundle
 * for more info on icons for app-bundles.
 */
static const guint supported_icon_sizes[] = {
  8, 16, 22, 24, 32, 36, 42, 48, 64, 72, 96, 128, 192, 256, 512
};
static const gchar *supported_icon_formats[] = { "png", "svg" };

int
main (int argc, char **argv)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GOptionContext) context = NULL;
  g_autoptr (GDir) dir = NULL;
  g_autoptr (GPtrArray) icon_names = NULL;
  g_autofree gchar *bundle_prefix = NULL;
  g_autofree gchar *bundle_apps_path = NULL;
  g_autofree gchar *extensions_apps_path = NULL;
  g_autofree gchar *bundle_icons_path = NULL;
  g_autofree gchar *extensions_icons_path = NULL;
  g_auto (CbyFileDescriptor) extensions_apps_fd = -1;
  g_auto (CbyFileDescriptor) extensions_icons_fd = -1;
  gint error_exit_status = 1;
  const gchar *bundle_id;
  const gchar *name;

  _cby_setenv_disable_services ();

  context = g_option_context_new ("- configure an application bundle");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      error_exit_status = 2;
      goto error;
    }

  if (argc < 2)
    {
      g_printerr ("%s: a bundle to be configured is required\n", g_get_prgname ());
      return 2;
    }

  if (argc > 2)
    {
      g_printerr ("%s: too many arguments\n", g_get_prgname ());
      return 2;
    }

  bundle_id = argv[1];

  if (!cby_is_bundle_id (bundle_id))
    {
      g_printerr ("%s: not a bundle ID: \"%s\"\n", g_get_prgname (),
                  bundle_id);
      return 2;
    }

  bundle_prefix = g_build_filename (CBY_PATH_PREFIX_STORE_BUNDLE, bundle_id, NULL);

  /* Install entry points (.desktop) */
  bundle_apps_path = g_build_filename (bundle_prefix, "share", "applications", NULL);

  extensions_apps_path = g_build_filename (CBY_PATH_SYSTEM_EXTENSIONS, "applications", NULL);

  extensions_apps_fd = _cby_open_dir_at (NULL, AT_FDCWD,
                                         extensions_apps_path,
                                         CBY_PATH_FLAGS_NONE, &error);
  if (extensions_apps_fd < 0)
    goto error;

  /* List of possible icon names for app-bundle - to be used when creating symlinks for icons */
  icon_names = g_ptr_array_new_with_free_func (g_free);
  /* We should always look for an icon for the entire bundle */
  g_ptr_array_add (icon_names, g_strdup (bundle_id));

  INFO ("Searching for entry points for app-bundle \"%s\" on \"%s\"", bundle_id, bundle_apps_path);
  dir = g_dir_open (bundle_apps_path, 0, &error);
  if (dir == NULL)
    {
      if (_cby_ignore_enoent (&error))
        {
          INFO ("No entry point found");
        }
      else
        {
          g_prefix_error (&error, "Unable to open \"%s\" for reading: ", bundle_apps_path);
          goto error;
        }
    }
  else
    {
      /* Iterate over CBY_PATH_PREFIX_STORE_BUNDLE/<bundle_id>/share/applications
       * searching for entry points (.desktop) and symlink them into
       * CBY_PATH_SYSTEM_EXTENSIONS/applications.
       */
      for (name = g_dir_read_name (dir);
           name != NULL;
           name = g_dir_read_name (dir))
        {
          if (g_str_has_suffix (name, ".desktop"))
            {
              g_autofree gchar *target = NULL;
              g_autofree gchar *link_name = NULL;
              g_autofree gchar *entry_point_id = NULL;
              char *dot;

              target = g_build_filename (bundle_apps_path, name, NULL);

              INFO ("Checking if entry point \"%s\" has a valid id", target);

              entry_point_id = g_strdup (name);
              dot = strrchr (entry_point_id, '.');
              g_assert (dot != NULL);
              *dot = '\0';

              if (!cby_is_entry_point_id (entry_point_id))
                {
                  WARNING ("Invalid id \"%s\" for entry point \"%s\" - ignoring entry point",
                           entry_point_id, target);
                  continue;
                }

              link_name = g_build_filename (extensions_apps_path, name, NULL);
              INFO ("Creating entry point symbolic link: \"%s\" → \"%s\"",
                    link_name, target);

              if (!_cby_durable_symlink_at (target,
                                            extensions_apps_path,
                                            extensions_apps_fd,
                                            name,
                                            CBY_PATH_FLAGS_OVERWRITE,
                                            &error))
                {
                  g_prefix_error (&error, "Unable to symlink entry point into place: ");
                  goto error;
                }

              /* We already added bundle_id to the list of icon names */
              if (g_strcmp0 (entry_point_id, bundle_id) != 0)
                g_ptr_array_add (icon_names, g_steal_pointer (&entry_point_id));
            }
        }
    }

  g_clear_pointer (&dir, g_dir_close);

  /* Install icons */
  bundle_icons_path = g_build_filename (bundle_prefix, "share", "icons", NULL);

  extensions_icons_path = g_build_filename (CBY_PATH_SYSTEM_EXTENSIONS, "icons", NULL);

  extensions_icons_fd = _cby_open_dir_at (NULL, AT_FDCWD,
                                          extensions_icons_path,
                                          CBY_PATH_FLAGS_NONE, &error);
  if (extensions_icons_fd < 0)
    goto error;

  INFO ("Searching for icons for app-bundle \"%s\" on \"%s\"", bundle_id, bundle_icons_path);
  dir = g_dir_open (bundle_icons_path, 0, &error);
  if (dir == NULL)
    {
      if (_cby_ignore_enoent (&error))
        {
          INFO ("No entry point found");
        }
      else
        {
          g_prefix_error (&error, "Unable to open \"%s\" for reading: ", bundle_icons_path);
          goto error;
        }
    }
  else
    {
      /* Iterate over CBY_PATH_PREFIX_STORE_BUNDLE/<bundle_id>/share/icons
       * searching for icons for all available entry points and symlink them
       * into CBY_PATH_SYSTEM_EXTENSIONS/icons/<theme>/<size>/apps.
       */
      for (name = g_dir_read_name (dir);
           name != NULL;
           name = g_dir_read_name (dir))
        {
          g_autofree gchar *path = NULL;
          const gchar *theme_name;
          GStatBuf stat_buf;
          guint i;

          path = g_build_filename (bundle_icons_path, name, NULL);
          if (g_lstat (path, &stat_buf) == -1 || !S_ISDIR (stat_buf.st_mode))
            continue;

          theme_name = name;

          for (i = 0; i < icon_names->len; i++)
            {
              const gchar *icon_name = g_ptr_array_index (icon_names, i);
              guint j;

              for (j = 0; j < G_N_ELEMENTS (supported_icon_sizes); ++j)
                {
                  g_autofree gchar *icon_size = NULL;
                  guint k;

                  icon_size = g_strdup_printf ("%dx%d", supported_icon_sizes[j], supported_icon_sizes[j]);

                  for (k = 0; k < G_N_ELEMENTS (supported_icon_formats); ++k)
                    {
                      g_autofree gchar *icon_filename = NULL;
                      g_autofree gchar *target = NULL;
                      const gchar *icon_format = supported_icon_formats[k];

                      icon_filename = g_strdup_printf ("%s.%s", icon_name, icon_format);
                      target = g_build_filename (bundle_icons_path, theme_name, icon_size,
                                                 "apps", icon_filename, NULL);
                      if (g_file_test (target, G_FILE_TEST_EXISTS))
                        {
                          g_auto (CbyFileDescriptor) extensions_icon_theme_apps_fd = -1;
                          g_autofree gchar *extensions_icon_theme_apps_path = NULL;
                          g_autofree gchar *apps_path = NULL;
                          g_autofree gchar *link_name = NULL;

                          apps_path = g_build_filename (theme_name, icon_size, "apps", NULL);
                          extensions_icon_theme_apps_path = g_build_filename (extensions_icons_path,
                                                                              apps_path, NULL);
                          /* Make sure the dir exists */
                          if (g_mkdir_with_parents (extensions_icon_theme_apps_path, 0755) == -1)
                            {
                              gint saved_errno = errno;

                              g_set_error (&error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                                           "Unable to create \"%s\": %s",
                                           extensions_icon_theme_apps_path,
                                           g_strerror (saved_errno));
                              goto error;
                            }

                          extensions_icon_theme_apps_fd = _cby_open_dir_at (extensions_icons_path,
                                                                            extensions_icons_fd,
                                                                            apps_path,
                                                                            CBY_PATH_FLAGS_NONE,
                                                                            &error);
                          if (extensions_icon_theme_apps_fd < 0)
                            goto error;

                          link_name = g_build_filename (extensions_icon_theme_apps_path,
                                                        icon_filename, NULL);
                          INFO ("Creating icon symbolic link: \"%s\" → \"%s\"", link_name, target);
                          if (!_cby_durable_symlink_at (target,
                                                        extensions_icon_theme_apps_path,
                                                        extensions_icon_theme_apps_fd,
                                                        icon_filename,
                                                        CBY_PATH_FLAGS_OVERWRITE,
                                                        &error))
                            {
                              g_prefix_error (&error, "Unable to symlink icon into place: ");
                              goto error;
                            }
                        }
                    }
                }
            }
        }
    }

  g_assert (error == NULL);
  return 0;

error:
  g_printerr ("%s\n", error->message);
  return error_exit_status;
}
