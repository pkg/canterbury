/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __WIN_MGR_IF__
#define __WIN_MGR_IF__

#include <glib.h>
#include <gio/gio.h>

G_BEGIN_DECLS

void win_mgr_init_async (GDBusConnection *session_bus,
                         GCancellable *cancellable,
                         GAsyncReadyCallback callback,
                         gpointer user_data);
gboolean win_mgr_init_finish (GAsyncResult *result,
                              GError **error);

gboolean
b_start_app_switch( const gchar* pAppType ,
                    const gchar* pLaunchedFrom ,
                    guint uinSpecialParams  ,
                    const gchar *pSplashScreenPath,
                    GAsyncReadyCallback app_switch_clb,
                    gpointer pUserData );

void window_mgr_set_active (const gchar *category,
                            const gchar *display_name);

void v_hide_splash_screen (void);

void
v_display_off(gboolean bWithAnimation);

void win_mgr_minimize_current_application_window (const gchar *application_name,
    const gchar *pLaunchedClient);

void win_mgr_maximize_application_window (const gchar *app_name);

void reset_launch_progress_timer (void);

void
v_last_user_mode_status(gboolean bStatus);

G_END_DECLS

#endif //__WIN_MGR_IF__
