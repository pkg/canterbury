/*
 * canterbury-prerm: deconfigure an app-bundle before unmounting it
 *
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <gio/gio.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <sys/apparmor.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>
#include "canterbury/messages-internal.h"

#include "apparmor.h"
#include "util.h"

static GOptionEntry entries[] = { { NULL } };

/*
 * Install stub AppArmor profiles so that processes from this bundle
 * can't usefully continue to run (T2614). We assume here that Ribchester
 * already asked Canterbury to kill any such processes (T2621).
 */
static gboolean
stub_out_apparmor_profiles (const gchar *bundle_id,
                            GError **error)
{
  g_autofree gchar *profile_prefix =
      g_strdup_printf ("%s/%s", CBY_PATH_PREFIX_STORE_BUNDLE, bundle_id);
  g_autofree gchar *profile_name =
      g_strdup_printf ("%s/%s/**", CBY_PATH_PREFIX_STORE_BUNDLE, bundle_id);
  g_autofree gchar *profiles_path = NULL;
  g_autofree gchar *line = NULL;
  g_autofree gchar *mount_point = NULL;
  g_autoptr (GFile) profiles = NULL;
  g_autoptr (GFileInputStream) reader = NULL;
  g_autoptr (GDataInputStream) line_reader = NULL;

  if (!_cby_apparmor_load_stub_profile (profile_name, error))
    return FALSE;

  /* In practice this is /sys/fs/kernel/security/apparmor */
  if (aa_find_mountpoint (&mount_point) != 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to find AppArmor securityfs mount point: %s",
                   g_strerror (saved_errno));
      return FALSE;
    }

  /* In practice this is /sys/fs/kernel/security/apparmor/profiles, which
   * is a file containing AppArmor contexts (label + mode), 1 per line */
  profiles_path = g_build_filename (mount_point, "profiles", NULL);
  profiles = g_file_new_for_path (profiles_path);
  reader = g_file_read (profiles, NULL, error);

  if (reader == NULL)
    return FALSE;

  line_reader = g_data_input_stream_new (G_INPUT_STREAM (reader));

  for (line = g_data_input_stream_read_line (line_reader, NULL, NULL, NULL);
       line != NULL;
       line = g_data_input_stream_read_line (line_reader, NULL, NULL, NULL))
    {
      if (g_str_has_prefix (line, profile_prefix))
        {
          /* Hmm, this could be a problem: check more carefully */
          char *mode = NULL;
          size_t len = strlen (profile_prefix);

          if (aa_splitcon (line, &mode) != line)
            {
              int saved_errno = errno;

              g_set_error (error, G_IO_ERROR,
                           g_io_error_from_errno (saved_errno),
                           "Unable to understand AppArmor profile \"%s\": %s",
                           line, g_strerror (saved_errno));
              return FALSE;
            }
          else if (!g_str_has_prefix (line, profile_prefix))
            {
              g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_DATA,
                           "Parsing AppArmor profile \"%s\" had weird result: "
                           "no longer starts with \"%s\"",
                           line, profile_prefix);
              return FALSE;
            }
          else if (strcmp (line, profile_name) == 0)
            {
              /* This is fine, we already replaced it; do nothing */
            }
          else if (line[len] != '/' &&
                   line[len] != '\0')
            {
              /* False positive: we are looking for
               * /Applications/com.example.Foo and we found
               * /Applications/com.example.Foobar. Do nothing */
            }
          else
            {
              /* We don't want to just replace this one, because it might
               * invalidate the contents of the pseudo-file, and in any case
               * the app bundle specification says this shouldn't happen.
               * The safe thing to do is to just refuse.
               * See also https://phabricator.apertis.org/D4290 */
              g_set_error (error, G_IO_ERROR, G_IO_ERROR_EXISTS,
                           "Unexpected AppArmor profile \"%s\" found", line);
              return FALSE;
            }
        }

      g_clear_pointer (&line, g_free);
    }

  return TRUE;
}

static gboolean
disable_apparmor_profiles (const gchar *bundle_id,
                           GError **error)
{
  g_autoptr (CbyAppArmorStore) apparmor = NULL;

  if (!stub_out_apparmor_profiles (bundle_id, error))
    return FALSE;

  apparmor = _cby_apparmor_store_open (CBY_PATH_SYSTEM_EXTENSIONS "/apparmor.d",
                                       error);

  if (apparmor == NULL)
    return FALSE;

  return _cby_apparmor_store_disable_profile (apparmor, bundle_id, error);
}

int
main (int argc, char **argv)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GOptionContext) context = NULL;
  g_autofree gchar *stamp = NULL;
  const gchar *bundle_id;
  int saved_errno;

  _cby_setenv_disable_services ();

  context = g_option_context_new ("- deconfigure an application bundle");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return 2;
    }

  if (argc < 2)
    {
      g_printerr ("%s: a bundle to be removed is required\n", g_get_prgname ());
      return 2;
    }

  if (argc > 2)
    {
      g_printerr ("%s: too many arguments\n", g_get_prgname ());
      return 2;
    }

  bundle_id = argv[1];

  if (!cby_is_bundle_id (bundle_id))
    {
      g_printerr ("%s: not a bundle ID: \"%s\"\n", g_get_prgname (),
                  bundle_id);
      return 2;
    }

  /* TODO: stop doing this when we do something useful instead */
  stamp = g_strdup_printf ("%s/%s.stamp", PKGCACHEDIR, bundle_id);

  if (g_unlink (stamp) != 0)
    {
      saved_errno = errno;

      if (saved_errno != ENOENT)
        WARNING ("%s: unable to unlink \"%s\": %s",
                 g_get_prgname (), stamp, g_strerror (saved_errno));
    }

  if (!disable_apparmor_profiles (bundle_id, &error))
    {
      CRITICAL ("%s: unable to disable AppArmor profiles: %s",
                g_get_prgname (), error->message);
      g_clear_error (&error);
      return 1;
    }

  /* Drop privileges by executing a less special executable */
  execl (PKGLIBEXECDIR "/canterbury-prerm-entry-points", /* program to run */
         PKGLIBEXECDIR "/canterbury-prerm-entry-points", /* argv[0] */
         bundle_id,
         NULL);

  /* Still here? Then something has gone very wrong */
  saved_errno = errno;
  g_printerr ("%s: unable to execute canterbury-prerm-entry-points: %s\n",
              g_get_prgname (), g_strerror (saved_errno));
  return 1;
}
