/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CANTERBURY_SRC_SERVICE_MANAGER_H
#define CANTERBURY_SRC_SERVICE_MANAGER_H

#include <glib.h>

#include "canterbury/platform/entry-point-index.h"

G_BEGIN_DECLS

#define CBY_TYPE_SERVICE_MANAGER (cby_service_manager_get_type ())
G_DECLARE_FINAL_TYPE (CbyServiceManager, cby_service_manager, CBY, SERVICE_MANAGER, GObject)

CbyServiceManager *_cby_service_manager_new (GDBusConnection *session_bus,
                                             CbyEntryPointIndex *entry_point_index,
                                             GError **error);

CbyEntryPointIndex *_cby_service_manager_get_entry_point_index (CbyServiceManager *self);
GDBusConnection *_cby_service_manager_get_session_bus (CbyServiceManager *self);

gboolean _cby_service_manager_has_service (CbyServiceManager *self,
                                           CbyEntryPoint *entry_point);
const gchar *_cby_service_manager_get_service_name (CbyServiceManager *self,
                                                    CbyEntryPoint *entry_point);

void _cby_service_manager_start_service_async (CbyServiceManager *self,
                                               CbyEntryPoint *entry_point,
                                               GCancellable *cancellable,
                                               GAsyncReadyCallback callback,
                                               gpointer user_data);
GPid _cby_service_manager_start_service_finish (CbyServiceManager *self,
                                                GAsyncResult *result,
                                                GError **error);
void _cby_service_manager_stop_service_async (CbyServiceManager *self,
                                              CbyEntryPoint *entry_point,
                                              GCancellable *cancellable,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data);
gboolean _cby_service_manager_stop_service_finish (CbyServiceManager *self,
                                                   GAsyncResult *result,
                                                   GError **error);
void _cby_service_manager_signal_service_async (CbyServiceManager *self,
                                                CbyEntryPoint *entry_point,
                                                gint signum,
                                                GCancellable *cancellable,
                                                GAsyncReadyCallback callback,
                                                gpointer user_data);
gboolean _cby_service_manager_signal_service_finish (CbyServiceManager *self,
                                                     GAsyncResult *result,
                                                     GError **error);

void _cby_service_manager_reload_units_async (CbyServiceManager *self,
                                              GCancellable *cancellable,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data);
gboolean _cby_service_manager_reload_units_finish (CbyServiceManager *self,
                                                   GAsyncResult *result,
                                                   GError **error);

void _cby_service_manager_start_agents_async (CbyServiceManager *self,
                                              GCancellable *cancellable,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data);
gboolean _cby_service_manager_start_agents_finish (CbyServiceManager *self,
                                                   GAsyncResult *result,
                                                   GError **error);

void _cby_service_manager_stop_bundle_async (CbyServiceManager *self,
                                             const gchar *bundle_id,
                                             GCancellable *cancellable,
                                             GAsyncReadyCallback callback,
                                             gpointer user_data);
gboolean _cby_service_manager_stop_bundle_finish (CbyServiceManager *self,
                                                  GAsyncResult *result,
                                                  GError **error);

G_END_DECLS

#endif
