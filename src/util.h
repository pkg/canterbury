/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CANTERBURY_SRC_UTIL_H
#define CANTERBURY_SRC_UTIL_H

#include <glib.h>
#include <glib/gstdio.h>

G_BEGIN_DECLS

void _cby_setenv_disable_services (void);

gboolean _cby_ignore_enoent (GError **errorp);

/* FIXME: maybe upstream this into glib-unix when we have more implementation
 * experience with it, or replace this with libglnx */
/*
 * CbyFileDescriptor:
 *
 * An open file descriptor, or -1 to indicate the absence of a file descriptor.
 *
 * This is just a typedef for `int`, but unlike `int`, it can be used in
 * `g_auto (CbyFileDescriptor)` to have the fd close automatically when it
 * goes out of scope.
 */
typedef int CbyFileDescriptor;

static inline void
_cby_file_descriptor_close (CbyFileDescriptor fd)
{
  g_close (fd, NULL);
}

G_DEFINE_AUTO_CLEANUP_FREE_FUNC (CbyFileDescriptor, _cby_file_descriptor_close,
                                 -1)

CbyFileDescriptor _cby_file_descriptor_copy (CbyFileDescriptor fd,
                                             GError **error);

/*
 * cby_file_descriptor_steal:
 * @fdp: a pointer to a file descriptor, which will be set to -1
 *
 * The equivalent of g_steal_pointer() for file descriptors.
 *
 * Returns: what the value of `*fdp` was on entry
 */
static inline CbyFileDescriptor
_cby_file_descriptor_steal (CbyFileDescriptor *fdp)
{
  CbyFileDescriptor tmp = *fdp;

  *fdp = -1;
  return tmp;
}

/*
 * cby_file_descriptor_clear:
 * @fdp: a pointer to a file descriptor, which will be set to -1
 *
 * The equivalent of g_clear_object() for file descriptors.
 */
static inline void
_cby_file_descriptor_clear (CbyFileDescriptor *fdp)
{
  CbyFileDescriptor tmp = *fdp;

  *fdp = -1;
  g_close (tmp, NULL);
}

/*
 * CbyPathFlags:
 * @CBY_PATH_FLAGS_NONE: Nothing special
 * @CBY_PATH_FLAGS_NOFOLLOW_SYMLINKS: If the last component of a path is a
 *  symbolic link, act on the symbolic link instead of the file it refers to.
 * @CBY_PATH_FLAGS_OVERWRITE: Be willing to overwrite an existing file.
 *
 * Flags affecting path resolution.
 */
typedef enum
{
  CBY_PATH_FLAGS_NONE = 0,
  CBY_PATH_FLAGS_NOFOLLOW_SYMLINKS = (1 << 0),
  CBY_PATH_FLAGS_OVERWRITE = (1 << 1),
} CbyPathFlags;

CbyFileDescriptor _cby_open_dir_at (const gchar *log_path,
                                    CbyFileDescriptor dirfd,
                                    const gchar *path,
                                    CbyPathFlags flags,
                                    GError **error);

gboolean _cby_durable_symlink_at (const gchar *target,
                                  const gchar *log_parent_path,
                                  CbyFileDescriptor parent_fd,
                                  const gchar *link_name,
                                  CbyPathFlags flags,
                                  GError **error);

gboolean _cby_is_target (void);

G_END_DECLS

#endif
