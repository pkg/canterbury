/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "hardkeys-mgr"
#include "hard_keys.h"

#include "canterbury/messages-internal.h"

#include <string.h>

typedef struct _BackHandling BackHandling;

struct _BackHandling
{
  gboolean bBackPressEvent;
  /* stack of applications which have been launched since the maximized
   * app launcher was last seen */
  GSList *pAppBackStack;
  /* Entry point IDs of applications who have registered for back press
   * events from app manager.
   * Used as a set with g_hash_table_add: { owned string => itself } */
  GHashTable *registrations;
  guint uinBackPressedTimeoutTimer;
};

static BackHandling backHandler;

static gint app_position_in_back_stack (CbyEntryPoint *entry_point);
static void launch_previous_application_from_back_stack (void);
static void delete_element_from_back_stack (guint count);

static gboolean b_app_manager_handle_back_press_switch_to_previous_app(gpointer pUserData);


void
back_handler_init (void)
{
  backHandler.bBackPressEvent = FALSE;
  backHandler.pAppBackStack = NULL;
  backHandler.registrations =
    g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
}

/**
 * cby_back_stack_iter_init:
 * @iter: an uninitialized iterator structure on the stack
 *
 * Initialize @iter, similar to g_hash_table_iter_init().
 */
void
cby_back_stack_iter_init (CbyBackStackIter *iter)
{
  iter->_link = backHandler.pAppBackStack;
}

/**
 * cby_back_stack_iter_next:
 * @iter: an iterator structure initialized with cby_back_stack_iter_init()
 * @entry: (out) (optional) (transfer none): the next entry if %TRUE is
 *  returned
 *
 * Iterate over @iter, similar to g_hash_table_iter_next(). The first
 * result will be the most recently used app in the stack (top of the stack),
 * and subsequent results are increasingly old.
 *
 * If %FALSE is returned, then @entry is undefined and the iterator
 * becomes invalid.
 *
 * |[<!-- language="C" -->
 * CbyBackStackIter iter;
 * BackStack *entry;
 *
 * cby_back_stack_iter_init (&iter);
 *
 * while (cby_back_stack_iter_next (&iter, &entry))
 *   do_something_with (entry);
 * ]|
 *
 * Returns: %TRUE if @entry was filled in, %FALSE if there are no more items
 */
gboolean
cby_back_stack_iter_next (CbyBackStackIter *iter, BackStack **entry)
{
  GSList *link;

  g_return_val_if_fail (iter != NULL, FALSE);
  link = iter->_link;

  if (link == NULL)
    return FALSE;

  if (entry != NULL)
    *entry = link->data;

  iter->_link = link->next;
  return TRUE;
}

void
back_stack_remove_registration (CbyEntryPoint *entry_point)
{
  const gchar *id = cby_entry_point_get_id (entry_point);

  g_hash_table_remove (backHandler.registrations, id);
}

static gboolean
check_if_current_app_registered_for_back_events (const gchar *entry_point_id)
{
  if (g_hash_table_contains (backHandler.registrations, entry_point_id))
    {
      // current app on top had registered for back press events , send a response to the app to react accordingly
       DEBUG ("App on top had registered for back press events: \"%s\"" ,
              entry_point_id);
       hard_keys_emit_back_press_register_response (entry_point_id);

       if ( backHandler.uinBackPressedTimeoutTimer > 0)
       {
          g_source_remove ( backHandler.uinBackPressedTimeoutTimer);
          backHandler.uinBackPressedTimeoutTimer = 0;
       }

       // a flag to used to make sure that when app manager receives a back consumed clb, its for an actual back button press event and not some junk
       backHandler.bBackPressEvent = TRUE;

       //back press should always work, we give the application 1 second to respond to back consumed event, if it doesnt respond by then, we simply put the previous app on top
       backHandler.uinBackPressedTimeoutTimer = g_timeout_add_seconds(1,b_app_manager_handle_back_press_switch_to_previous_app, NULL);

      return TRUE;
    }
  else
    {
      return FALSE;
    }
}

void
back_stack_remove_entry (CbyEntryPoint *entry_point)
{
  gint back_stack_position = app_position_in_back_stack (entry_point);

  if (back_stack_position != -1)
    delete_element_from_back_stack (back_stack_position);
}

static void
launch_previous_application_from_back_stack_cb (GObject *source_object,
                                                GAsyncResult *result,
                                                gpointer user_data)
{
  g_autoptr (CbyEntryPoint) entry_point = user_data;
  g_autoptr (GError) error = NULL;

  if (!app_mgr_launch_new_application_finish (result, &error))
    WARNING ("Unable to launch \"%s\": %s",
             cby_entry_point_get_id (entry_point), error->message);
}

static void
launch_previous_application_from_back_stack (void)
{
  const gchar *entry_point_id;
  BackStack *pAppInfo;
  g_autoptr (GError) error = NULL;

  // the top application in the back stack is the current app on top, so remove it from the stack and load the next app to be launched
  if(b_is_current_view_launcher() == TRUE )
  {
    app_mgr_debug(" reset_current_launcher_view \n" );
    v_reset_current_launcher_view();
  }
  else
  {
    delete_element_from_back_stack( 0 );
  }

  /* there should still be *something* on the stack */
  g_return_if_fail (backHandler.pAppBackStack != NULL);
  pAppInfo = backHandler.pAppBackStack->data;
  g_return_if_fail (pAppInfo != NULL);

  entry_point_id = cby_entry_point_get_id (pAppInfo->entry_point);
  app_mgr_launch_new_application_async (entry_point_id, NULL, NULL,
                                        (const gchar * const *) pAppInfo->pArgvList,
                                        CANTERBURY_BACK_KEY, 0xff, NULL,
                                        launch_previous_application_from_back_stack_cb,
                                        g_object_ref (pAppInfo->entry_point));
}

void
show_launcher_app (void)
{
  launch_mgr_activate_launcher (CANTERBURY_BACK_KEY);
}

/*********************************************************************************************
 * Function:    handle_back_press_clb
 * Description: Call back function invoked when launcher sends a back message to the app mgr.
 *
 * Return:      none
 ********************************************************************************************/
gboolean handle_back_press_clb( CanterburyHardKeys *object,
                                GDBusMethodInvocation   *invocation,
                                gpointer                 pUserData )
{
  const gchar *pCurrentAppNnTop = NULL;
  gboolean backpress_reg_app_found = FALSE;

  hard_keys_mgr_debug("***back clb received \n");
  if(is_app_launch_in_progress())
  {
     hard_keys_mgr_debug("*** Launch In Progress \n");
    canterbury_hard_keys_complete_back_press(object , invocation);
    return TRUE;
  }

  hide_global_popups();

  /* get the application running on top */
  pCurrentAppNnTop = activity_mgr_get_app_on_top();

  /* If there is no app on top, there's nothing to do. */
  if (pCurrentAppNnTop == NULL)
    {
      canterbury_hard_keys_complete_back_press (object, invocation);
      return TRUE;
    }

   if (launch_mgr_find_running_app (pCurrentAppNnTop) == NULL)
   {
     canterbury_hard_keys_complete_back_press(object , invocation);
     return TRUE;
   }

  /* check if the launcher already on top */
  if( !strcmp( pCurrentAppNnTop ,  LAUNCHER_APP_NAME ) && ( b_is_current_view_launcher() == FALSE ) )
  {
    hard_keys_mgr_debug("*** launcher on top \n");
    canterbury_hard_keys_complete_back_press(object , invocation);
    hard_keys_emit_back_press_register_response (LAUNCHER_APP_NAME);
    hard_keys_emit_back_press_response (FALSE);
    return TRUE;
  }

  if(b_is_current_view_launcher() == TRUE )
  {
    hard_keys_mgr_debug("*** current view is launcher, launch prev app from back stack \n");
    launch_previous_application_from_back_stack();
    return TRUE;
  }

  // check if the current application on top has registered for the back press events
  backpress_reg_app_found = check_if_current_app_registered_for_back_events(pCurrentAppNnTop);
  hard_keys_mgr_debug("*** app registration for back press events %d \n" , backpress_reg_app_found);
  if( backpress_reg_app_found == FALSE )
  { // current app on top didnt register for back press events, so bring back previous application on top
    if( 1 < g_slist_length( backHandler.pAppBackStack ) )
    {
      BackStack* pAppInfo = g_slist_nth_data ( backHandler.pAppBackStack , 0 );
      const gchar *id = cby_entry_point_get_id (pAppInfo->entry_point);

      if (g_strcmp0 (pCurrentAppNnTop, id) == 0)
      {
        hard_keys_mgr_debug("*** alaunch prev app from back stack \n" );
        launch_previous_application_from_back_stack();
      }
      else
      {
        WARNING ("Activity manager says \"%s\" is the app on top, but found "
                 "\"%s\" on top of non-empty back stack",
                 pCurrentAppNnTop, id);
        clear_back_stack();
      }
    }
    else
    { //back stack is empty, put launcher on top
      if( 1 == g_slist_length( backHandler.pAppBackStack ) )
      {
        BackStack* pAppInfo = g_slist_nth_data ( backHandler.pAppBackStack , 0 );
        const gchar *id = cby_entry_point_get_id (pAppInfo->entry_point);

        if (g_strcmp0 (pCurrentAppNnTop, id) == 0)
        {
          delete_element_from_back_stack(0);
        }
        else
        {
          WARNING ("Activity manager says \"%s\" is the app on top, but found "
                   "\"%s\" as the only back stack entry",
                   pCurrentAppNnTop, id);
          clear_back_stack();
        }
      }

      show_launcher_app();
      canterbury_hard_keys_complete_back_press(object , invocation);
      hard_keys_emit_back_press_response (TRUE);
      // ask the window manager to minimize the current app on top and swipe in Launcher
      hard_keys_mgr_debug("minimize current app %s \n" ,pCurrentAppNnTop );
      win_mgr_minimize_current_application_window( pCurrentAppNnTop , CANTERBURY_BACK_KEY);
      return TRUE;
    }
  }
  canterbury_hard_keys_complete_back_press(object , invocation);
  return TRUE;
}

gboolean handle_register_back_press_clb( CanterburyHardKeys *object,
                                         GDBusMethodInvocation   *invocation,
                                         const gchar             *entry_point_id,
                                         gpointer                 pUserData )
{
  /* FIXME: we should check that the given entry point actually belongs
   * to the caller, if this whole API isn't removed first */
  g_hash_table_add (backHandler.registrations, g_strdup (entry_point_id));
  canterbury_hard_keys_complete_register_back_press( object ,invocation );
  return TRUE;
}

gboolean handle_back_press_consumed_clb( CanterburyHardKeys *object,
                                         GDBusMethodInvocation   *invocation,
                                         const gchar             *entry_point_id,
                                         gboolean                consumed_status,
                                         gpointer                pUserData )
{
  if ( backHandler.uinBackPressedTimeoutTimer > 0)
  {
    g_source_remove ( backHandler.uinBackPressedTimeoutTimer);
     backHandler.uinBackPressedTimeoutTimer = 0;
  }

  if( !consumed_status )
  	b_app_manager_handle_back_press_switch_to_previous_app(NULL);

  canterbury_hard_keys_complete_back_press_consumed(object ,invocation);
	return TRUE;
}

static gboolean b_app_manager_handle_back_press_switch_to_previous_app(gpointer pUserData)
{
/* do this till the global view manager is ready */
  const gchar *pCurrentAppNnTop;
  /* get the application running on top */
  pCurrentAppNnTop = activity_mgr_get_app_on_top();

  /* If there is no app on top, there's nothing to do. */
  if (pCurrentAppNnTop == NULL)
    return G_SOURCE_REMOVE;

  hard_keys_mgr_debug("***b_app_manager_handle_back_press_switch_to_previous_app \n" );
  if( backHandler.bBackPressEvent == FALSE )
  {
    g_printerr("consumed event sent without back key press \n");
    return FALSE;
  }
  else
  {
    // bring back previous application on top
    if( 1 < g_slist_length( backHandler.pAppBackStack ) )
    {
        BackStack* pAppInfo = g_slist_nth_data ( backHandler.pAppBackStack , 0 );
        const gchar *id = cby_entry_point_get_id (pAppInfo->entry_point);

        if (g_strcmp0 (pCurrentAppNnTop, id) == 0)
        {
          launch_previous_application_from_back_stack();
        }
        else
        {
          WARNING ("Activity manager says \"%s\" is the app on top, but found "
                   "\"%s\" on top of non-empty back stack",
                   pCurrentAppNnTop, id);
          clear_back_stack();
        }
    }
    else
    {
      if( 1 == g_slist_length( backHandler.pAppBackStack ) )
      {
        BackStack* pAppInfo = g_slist_nth_data ( backHandler.pAppBackStack , 0 );
        const gchar *id = cby_entry_point_get_id (pAppInfo->entry_point);

        if (g_strcmp0 (pCurrentAppNnTop, id) == 0)
        {
          delete_element_from_back_stack(0);
        }
        else
        {
          WARNING ("Activity manager says \"%s\" is the app on top, but found "
                   "\"%s\" as the only back stack entry",
                   pCurrentAppNnTop, id);
          clear_back_stack();
        }
      }
      show_launcher_app();
      hard_keys_emit_back_press_response (TRUE);
      hard_keys_mgr_debug("minimize current app %s \n" ,pCurrentAppNnTop );
      // ask the window manager to minimize the current app on top and swipe in Launcher
      win_mgr_minimize_current_application_window(pCurrentAppNnTop, CANTERBURY_BACK_KEY);
    }
  }
  backHandler.bBackPressEvent = FALSE;
  return FALSE;
}

static void
delete_element_from_back_stack (guint count)
{
    GSList* back_stack_nth_item = g_slist_nth( backHandler.pAppBackStack , count );
    if( back_stack_nth_item->data != NULL )
    {
      BackStack* pAppInfo = back_stack_nth_item->data;
      const gchar *id = cby_entry_point_get_id (pAppInfo->entry_point);

      DEBUG ("Removing item #%d: \"%s\"", count, id);

      g_clear_object (&pAppInfo->entry_point);

      if(pAppInfo->pArgvList!=NULL)
      {
        g_strfreev(pAppInfo->pArgvList);
        pAppInfo->pArgvList=NULL;
      }
      if(NULL != pAppInfo)
      {
        g_free(pAppInfo);
        pAppInfo = NULL;
      }
    }
    backHandler.pAppBackStack = g_slist_delete_link ( backHandler.pAppBackStack , back_stack_nth_item );
    hard_keys_mgr_debug("list len %d \n", g_slist_length( backHandler.pAppBackStack ));
}

void
clear_back_stack (void)
{
  guint cnt , stack_length;
  stack_length = g_slist_length( backHandler.pAppBackStack );
  hard_keys_mgr_debug("***length of back stack %d \n" , g_slist_length( backHandler.pAppBackStack ));
  for( cnt = 0 ; cnt < stack_length ; cnt++ )
  {
    delete_element_from_back_stack( 0 );
  }
  backHandler.pAppBackStack = NULL;
}

static BackStack *
back_stack_find_entry (CbyEntryPoint *entry_point)
{
  guint back_stack_len = g_slist_length (backHandler.pAppBackStack);
  guint cnt;

  for (cnt = 0; cnt < back_stack_len; cnt++)
    {
      BackStack *app_info = g_slist_nth_data (backHandler.pAppBackStack, cnt);

      if (app_info->entry_point == entry_point)
        return app_info;
    }
  return NULL;
}

static gint
app_position_in_back_stack (CbyEntryPoint *entry_point)
{
  guint back_stack_len = g_slist_length (backHandler.pAppBackStack);
  guint cnt;

  for (cnt = 0 ; cnt < back_stack_len; cnt++)
    {
      BackStack *app_info = g_slist_nth_data (backHandler.pAppBackStack, cnt);

      if (app_info->entry_point == entry_point)
        return cnt;
    }
  return -1;
}

static gint
app_position_in_back_reverse_stack (CbyEntryPoint *entry_point)
{
  guint back_stack_len = g_slist_length( backHandler.pAppBackStack ) -1;
  guint cnt;

  if (back_stack_len > 0)
    {
      for (cnt = back_stack_len; cnt > 0; cnt--)
        {
          BackStack *app_info = g_slist_nth_data (backHandler.pAppBackStack, cnt);

          if (app_info->entry_point == entry_point)
            return cnt;
        }
    }
  return -1;
}

static const gchar *
app_previously_launched_from (CbyEntryPoint *entry_point)
{
  guint back_stack_len = g_slist_length (backHandler.pAppBackStack);
  guint cnt;

  for (cnt = 0 ; cnt < back_stack_len; cnt++)
    {
      BackStack *app_info = g_slist_nth_data (backHandler.pAppBackStack, cnt);

      if (app_info->entry_point == entry_point)
        return app_info->pLaunchedFrom;
    }
  return CANTERBURY_UNKNOWN_CLIENT;
}

void
back_stack_update_entry (CbyEntryPoint *entry_point,
                         const gchar * const *back_stack_argv,
                         const gchar *launched_from)
{
  const gchar *entry_point_id = cby_entry_point_get_id (entry_point);
  gboolean delete_app_from_back_stack = TRUE;
  gint back_stack_position;
  BackStack *app_info;
  BackStack *new_app_info;

  hard_keys_mgr_debug ("Updating back stack for application %s launched from %s - back stack current len %d\n" ,
                       entry_point_id, launched_from,
                       g_slist_length (backHandler.pAppBackStack));

  if (b_is_current_view_launcher () == TRUE)
    {
      hard_keys_mgr_debug ("Reset current launcher view\n");
      v_reset_current_launcher_view ();
    }

  /* look for the new application in the back stack.*/
  app_info = back_stack_find_entry (entry_point);

  /* check if the current app on top of back stack is already
   * available somewhere else, if so delete it */
  /* this is required only if there are more than one entry in the
   * back stack */
  if (g_slist_length (backHandler.pAppBackStack) > 1)
    {
      BackStack *top_app = g_slist_nth_data (backHandler.pAppBackStack, 0);

      back_stack_position =
        app_position_in_back_reverse_stack (top_app->entry_point);

      hard_keys_mgr_debug ("Current back stack pos %d for application %s\n",
                           back_stack_position, entry_point_id);
      if ((back_stack_position != -1) && (back_stack_position != 0))
        {
          hard_keys_mgr_debug ("The current application %s on top of back stack is available "
                               "somewhere else in back stack (pos %d), delete it from back stack\n",
                               entry_point_id, back_stack_position);
          delete_element_from_back_stack (0);
        }
    }

  back_stack_position = app_position_in_back_stack (entry_point);
  /* Check if the back stack already has this app and if this app
   * is launched from home screen and delete it otherwise */
  if ((back_stack_position == (gint) (g_slist_length (backHandler.pAppBackStack) - 1)) &&
      (g_strcmp0 (app_previously_launched_from (entry_point), CANTERBURY_HOME_SCREEN_CLIENT) == 0) &&
      (g_slist_length (backHandler.pAppBackStack) != 1))
    {
      delete_app_from_back_stack = FALSE;
      hard_keys_mgr_debug ("Application %s launched from home screen, dont delete it from back stack\n",
                           entry_point_id);
    }

  /* If the application is available in the back stack,
   * then put that application on top of the stack */
  if ((delete_app_from_back_stack) &&
      (app_info != NULL) &&
      (back_stack_position != -1))
    {
      hard_keys_mgr_debug ("Removing application %s from back stack at position %d\n",
                           entry_point_id, back_stack_position);
      delete_element_from_back_stack (back_stack_position);
    }

  new_app_info = g_new0 (BackStack, 1);
  new_app_info->entry_point = g_object_ref (entry_point);
  new_app_info->pArgvList = g_strdupv ((gchar**) back_stack_argv);
  new_app_info->pLaunchedFrom = g_strdup (launched_from);
  backHandler.pAppBackStack = g_slist_prepend (backHandler.pAppBackStack, new_app_info);
  hard_keys_mgr_debug ("Added application %s to back stack at position %d\n",
                       entry_point_id, g_slist_length (backHandler.pAppBackStack));
}

gboolean
handle_disable_back_press_clb (CanterburyHardKeys *object,
                               GDBusMethodInvocation *invocation,
                               const gchar *entry_point_id,
                               gboolean disable_status,
                               gpointer user_data G_GNUC_UNUSED)
{
  canterbury_hard_keys_emit_disable_back_press_response (object,
                                                         disable_status);
  canterbury_hard_keys_complete_disable_back_press(object,invocation);
  return TRUE;
}

