/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __APP_MGR_IF__
#define __APP_MGR_IF__

#include <glib.h>

/* a macro uses strcmp() */
#include <string.h>

#include "activity_mgr_if.h"
#include "canterbury/gdbus/canterbury_app_handler.h"
#include "canterbury/platform/entry-point.h"

G_BEGIN_DECLS

/*
* include all defines here
*/

#define LAUNCHER_APP_NAME                                 "Launcher"

/*
* include all enum declarations here
*/

void app_mgr_set_app_on_top (CbyEntryPoint *entry_point);

void hide_global_popups (void);

void emit_new_app_state (const gchar *entry_point_id,
                         CanterburyAppState state,
                         const gchar * const *arguments);

void
v_set_system_shutdown_signal(gboolean bSignal);

gboolean v_get_system_shutdown_signal (void);

void app_mgr_launch_new_application_async (const gchar *entry_point_id,
                                           const gchar * const *uris,
                                           GVariant *platform_data,
                                           const gchar * const *legacy_arguments,
                                           const gchar *launched_from,
                                           guint special_args,
                                           GCancellable *cancellable,
                                           GAsyncReadyCallback callback,
                                           gpointer user_data);
gboolean app_mgr_launch_new_application_finish (GAsyncResult *result,
                                                GError **error);

void set_current_active_audio_source (const gchar *pAppName);

gboolean b_get_last_user_mode_status (void);

void v_restart_app_manager (void);

#define app_mgr_debug(a ...) \
  g_log ("app-mgr", G_LOG_LEVEL_DEBUG, a)

#define launch_mgr_debug(a ...) \
  g_log ("launch-mgr", G_LOG_LEVEL_DEBUG, a)

#define activity_mgr_debug(a ...) \
  g_log ("activity-mgr", G_LOG_LEVEL_DEBUG, a)

#define audio_mgr_debug(a ...) \
  g_log ("audio-mgr", G_LOG_LEVEL_DEBUG, a)

#define hard_keys_mgr_debug(a ...) \
  g_log ("hard-keys-mgr", G_LOG_LEVEL_DEBUG, a)

#define appstore_mgr_debug(a ...) \
  g_log ("appstore-mgr", G_LOG_LEVEL_DEBUG, a)

#define launcher_mgr_debug(a ...) \
  g_log ("launcher-mgr", G_LOG_LEVEL_DEBUG, a)

#define data_mgr_debug(a ...) \
  g_log ("data-mgr", G_LOG_LEVEL_DEBUG, a)

#define app_launch_mgr_debug(a ...) \
  g_log ("app-launch-mgr", G_LOG_LEVEL_DEBUG, a)

#define launch_mgr_db_debug(a ...) \
  g_log ("app-launch-db-mgr", G_LOG_LEVEL_DEBUG, a)

#define win_mgr_debug(a ...) \
  g_log ("win-mgr", G_LOG_LEVEL_DEBUG, a)

G_END_DECLS

#endif //__APP_MGR_IF__
