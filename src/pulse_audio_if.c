/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "audio-mgr"
#include "audio_mgr.h"

#include <stdio.h>
#include <string.h>

extern AudioMgrPriv pAudioMgrPriv;

static pa_stream *createMonitorStreamForSource (uint32_t source_idx,
                                                uint32_t stream_idx);
static void v_ps_subscribe_cb (pa_context                   *c,
                               pa_subscription_event_type_t  t,
                               uint32_t                      index,
                               void                         *pUserData);
static void context_state_callback(pa_context *pa_ctx, void *pUserData);
static void sink_cb(pa_context *, const pa_sink_info *i, int eol, void *pUserData);
static void source_cb(pa_context *, const pa_source_info *i, int eol, void *pUserData);
static void sink_input_cb(pa_context *, const pa_sink_input_info *i, int eol, void *pUserData);
static void source_output_cb(pa_context *, const pa_source_output_info *i, int eol, void *pUserData);
static void client_cb(pa_context *, const pa_client_info *i, int eol, void *pUserData);
static void sink_input_mute_by_index( pa_context *c, int success , void *pUserData );
static const gchar *get_app_name_from_channel_name (gchar *channel_name);
//static void ext_stream_restore_read_cb( pa_context *,  const pa_ext_stream_restore_info *i,  int eol, void *pUserData);
//static void ext_stream_restore_subscribe_cb(pa_context *c, void *pUserData) ;

static void read_callback(pa_stream *s, size_t length, void *pUserData);
static void suspended_callback(pa_stream *s, void *pUserData);

void v_initialize_pulse_audio_subscription (void)
{
	int return_val ;
	pAudioMgrPriv.pMainLoop = pa_glib_mainloop_new(NULL);
	g_assert(pAudioMgrPriv.pMainLoop);

	pAudioMgrPriv.pCtxt = pa_context_new(pa_glib_mainloop_get_api(pAudioMgrPriv.pMainLoop), NULL);
	g_assert(pAudioMgrPriv.pCtxt);

	return_val = pa_context_connect(pAudioMgrPriv.pCtxt, NULL, PA_CONTEXT_NOFAIL, NULL);

	pa_context_set_state_callback(pAudioMgrPriv.pCtxt, context_state_callback, NULL);

	pAudioMgrPriv.pExtAudioEntryList = NULL;

	pAudioMgrPriv.lastPeak = 0;
	pAudioMgrPriv.stream = NULL;

	if (return_val  < 0)
	{
		audio_mgr_debug(("v_initialize_pulse_audio_subscription failed \n"));
		pa_context_unref(pAudioMgrPriv.pCtxt);
		pa_glib_mainloop_free(pAudioMgrPriv.pMainLoop);
		pAudioMgrPriv.pCtxt = NULL;
		pAudioMgrPriv.pMainLoop = NULL;
	}

}

static gboolean mute_sink (gpointer data )
{
	guint32 *sink = (guint32*)data;
	pa_context_set_sink_input_mute( pAudioMgrPriv.pCtxt , *sink , 1 , sink_input_mute_by_index , NULL );
	g_free(sink);
	return FALSE;
}

static gboolean unmute_sink (gpointer data )
{
	guint32 *sink = (guint32*)data;
	pa_context_set_sink_input_mute( pAudioMgrPriv.pCtxt , *sink , 0 , sink_input_mute_by_index , NULL );
	g_free(sink);
	return FALSE;
}

void v_send_external_app_signal( guint32 uinSinkIndex , guint audio_signal )
{
	guint32 *sink_idx = g_new(guint32 , 1);

	AudioAppEntryInfo* pCurrentActiveAudioEntry =NULL;
	pCurrentActiveAudioEntry = (AudioAppEntryInfo*)g_list_nth_data ( pAudioMgrPriv.pAudioStack , 0 );
	switch(audio_signal)
	{
		case CANTERBURY_AUDIO_PLAY:
			audio_mgr_debug("\n AUDIO MGR: Step Y :: v_send_external_app_signal CANTERBURY_AUDIO_PLAY sink index %d \n" , uinSinkIndex );
			*sink_idx = uinSinkIndex;
			g_timeout_add(0 , unmute_sink , sink_idx);
			pCurrentActiveAudioEntry->uinAudioStatus = CANTERBURY_AUDIO_PLAY;
			pCurrentActiveAudioEntry->uinSinkIndex = uinSinkIndex;
			break;

		case CANTERBURY_AUDIO_STOP:
			audio_mgr_debug("\n AUDIO MGR: Step Y :: v_send_external_app_signal AUDIO_STOP  sink index %d \n" , uinSinkIndex );
			pCurrentActiveAudioEntry->uinAudioStatus = CANTERBURY_AUDIO_STOP;
			pCurrentActiveAudioEntry->uinSinkIndex = uinSinkIndex;
			*sink_idx = uinSinkIndex;
			g_timeout_add(0 , mute_sink , sink_idx);
			break;

		case CANTERBURY_AUDIO_PAUSE:
			pCurrentActiveAudioEntry->uinAudioStatus = CANTERBURY_AUDIO_PAUSE;
			break;

		default:
			g_return_if_reached ();
	}
}

static void
pa_context_success_clb (pa_context *pa_context,
    int success,
    void *pUserData)
{
	audio_mgr_debug("pa_context_success_clb %d  \n" , success);

}

static void context_state_callback(pa_context *pa_context, void *pUserData)
{
	audio_mgr_debug(("context_state_callback \n"));
	g_assert(pa_context);
	switch (pa_context_get_state(pa_context))
	{
		case PA_CONTEXT_READY:
			{
				pa_operation *ptr = NULL;

				pa_context_set_subscribe_callback(pa_context, v_ps_subscribe_cb, NULL);

				if (!(ptr = pa_context_subscribe( pa_context,
								(pa_subscription_mask_t)
								(PA_SUBSCRIPTION_MASK_SINK|
								 PA_SUBSCRIPTION_MASK_SOURCE|
								 PA_SUBSCRIPTION_MASK_SINK_INPUT|
								 PA_SUBSCRIPTION_MASK_SOURCE_OUTPUT|
								 PA_SUBSCRIPTION_MASK_CLIENT
								),
								pa_context_success_clb,
								NULL)))
				{
					audio_mgr_debug(("pa_context_subscribe() failed \n"));
					return;
				}
				pa_operation_unref(ptr);

				if (!(ptr = pa_context_get_client_info_list(pa_context, client_cb, (void *) "NONE") ) )
				{
					audio_mgr_debug(("pa_context_client_info_list() failed \n"));
					return;
				}
				pa_operation_unref(ptr);

				if (!(ptr = pa_context_get_sink_info_list(pa_context, sink_cb, (void *) "NONE")))
				{
					audio_mgr_debug(("pa_context_get_sink_info_list() failed \n"));
					return;
				}
				pa_operation_unref(ptr);

				if (!(ptr = pa_context_get_source_info_list(pa_context, source_cb, (void *) "NONE")))
				{
					audio_mgr_debug(("pa_context_get_source_info_list() failed \n"));
					return;
				}
				pa_operation_unref(ptr);

				if (!(ptr = pa_context_get_sink_input_info_list(pa_context, sink_input_cb, (void *) "NONE")))
				{
					audio_mgr_debug(("pa_context_get_sink_input_info_list() failed \n"));
					return;
				}
				pa_operation_unref(ptr);

				if (!(ptr = pa_context_get_source_output_info_list(pa_context, source_output_cb, (void *) "NONE")))
				{
					audio_mgr_debug(("pa_context_get_source_output_info_list() failed \n"));
					return;
				}
				pa_operation_unref(ptr);
#if 0
				/* This call is not always supported */
				if ((ptr = pa_ext_stream_restore_read(pa_context, ext_stream_restore_read_cb, "NONE")))
				{
					pa_operation_unref(ptr);

					pa_ext_stream_restore_set_subscribe_cb(pa_context, ext_stream_restore_subscribe_cb, "NONE");

					if ((ptr = pa_ext_stream_restore_subscribe(pa_context, 1, NULL, NULL)))
						pa_operation_unref(ptr);

				}
				else
					audio_mgr_debug("Failed to initialize stream_restore extension: %s \n", pa_strerror(pa_context_errno(pAudioMgrPriv.pCtxt)));
#endif

				break;
			}

		case PA_CONTEXT_FAILED:
			audio_mgr_debug("Connection failed \n");
			return;

		case PA_CONTEXT_TERMINATED:
		case PA_CONTEXT_UNCONNECTED:
		case PA_CONTEXT_CONNECTING:
		case PA_CONTEXT_AUTHORIZING:
		case PA_CONTEXT_SETTING_NAME:
		default:
			return;
	}
}
static void sink_input_mute_by_index( pa_context *c, int success , void *pUserData )
{
	audio_mgr_debug("sink_input_mute_by_index status %d \n" , success);
}

static void source_input_mute_by_index( pa_context *c, int success , void *pUserData )
{
	audio_mgr_debug("source_input_mute_by_index status %d \n" , success);
}

static gchar *
get_event_type (pa_subscription_event_type_t t)
{
  gchar* event_type = NULL;
	if ((t & PA_SUBSCRIPTION_EVENT_TYPE_MASK) == PA_SUBSCRIPTION_EVENT_REMOVE)
	{
		event_type = g_strdup("REMOVE");
	}
	else if ((t & PA_SUBSCRIPTION_EVENT_TYPE_MASK) == PA_SUBSCRIPTION_EVENT_CHANGE)
	{
		event_type = g_strdup("CHANGE");
	}
	else if ((t & PA_SUBSCRIPTION_EVENT_TYPE_MASK) == PA_SUBSCRIPTION_EVENT_NEW)
	{
		event_type = g_strdup("NEW");
	}
	else
	{
		g_error("remove sink i/p unknown %ux \n" , (t & PA_SUBSCRIPTION_EVENT_TYPE_MASK));
	}
	return event_type;
}

static void
handle_sink_input_event (pa_context *c,
    uint32_t index ,
    gchar *event_type)
{
  pa_operation *o;
  if (!(o = pa_context_get_sink_input_info(c, index, sink_input_cb, event_type)))
  {
    audio_mgr_debug("pa_context_get_sink_input_info() failed \n");
    return;
  }
  pa_operation_unref(o);

  if( ! strcmp(event_type , "REMOVE" ) )
  {
    // break;
    guint client_cnt = 0;
    ExternalAudioEntry* audio_entry = NULL;
    guint list_len = g_list_length( pAudioMgrPriv.pExtAudioEntryList );
    for( client_cnt = 0 ; client_cnt< list_len ; client_cnt++)
    {
      audio_entry = g_list_nth_data ( pAudioMgrPriv.pExtAudioEntryList , client_cnt );
      if( audio_entry->uinSinkIndex == index )
      {
        audio_mgr_debug("client_cb:: remove the sink index \n");
        //  v_external_app_audio_invoke_clb(get_app_name_from_channel_name(audio_entry->AppName) , CANTERBURY_APP_AUDIO_RELEASE , audio_entry->uinSinkIndex );
        pAudioMgrPriv.pExtAudioEntryList = g_list_remove (pAudioMgrPriv.pExtAudioEntryList , audio_entry );
        if(audio_entry->AppName)
        {
          g_free(audio_entry->AppName);
          audio_entry->AppName = NULL;
        }
        g_free(audio_entry);
        audio_entry = NULL;
        break;
      }
    }
  }
}

static void
v_ps_subscribe_cb (pa_context                   *c,
                   pa_subscription_event_type_t  t,
                   uint32_t                      index,
                   void                         *pUserData)
{
	gchar* event_type = NULL;
	event_type = get_event_type(t);

	if( index == 0 )
		return;

	audio_mgr_debug("***index %d ** switch %d *\n" ,  index , t & PA_SUBSCRIPTION_EVENT_FACILITY_MASK);
	switch (t & PA_SUBSCRIPTION_EVENT_FACILITY_MASK)
	{
		case PA_SUBSCRIPTION_EVENT_SINK:
			{

				pa_operation *o;
				if (!(o = pa_context_get_sink_info_by_index(c, index, sink_cb, event_type)))
				{
					audio_mgr_debug(("pa_context_get_sink_info_by_index() failed \n"));
					return;
				}
				pa_operation_unref(o);
			}
			break;

		case PA_SUBSCRIPTION_EVENT_SOURCE:
			{

				pa_operation *o;
				if (!(o = pa_context_get_source_info_by_index(c, index, source_cb, event_type)))
				{
					audio_mgr_debug("pa_context_get_source_info_by_index() failed \n");
					return;
				}
				pa_operation_unref(o);
			}
			break;

		case PA_SUBSCRIPTION_EVENT_SINK_INPUT:
			{
			  handle_sink_input_event(c , index , event_type);
			}
			break;

		case PA_SUBSCRIPTION_EVENT_SOURCE_OUTPUT:
			{

				pa_operation *o;
				if (!(o = pa_context_get_source_output_info(c, index, source_output_cb, event_type)))
				{
					audio_mgr_debug("pa_context_get_sink_input_info() failed \n");
					return;
				}
				pa_operation_unref(o);
			}
			break;

		case PA_SUBSCRIPTION_EVENT_CLIENT:
			{
				pa_operation *o;
				if (!(o = pa_context_get_client_info(c, index, client_cb, event_type)))
				{
					audio_mgr_debug("pa_context_get_client_info() failed \n");
					return;
				}

				if( ! strcmp(event_type , "REMOVE" ) )
				{
					guint client_cnt = 0;
					ExternalAudioEntry* audio_entry = NULL;
					guint list_len = g_list_length( pAudioMgrPriv.pExtAudioEntryList );
					for( client_cnt = 0 ; client_cnt< list_len ; client_cnt++)
					{
						audio_entry = g_list_nth_data ( pAudioMgrPriv.pExtAudioEntryList , client_cnt );
						if( audio_entry->App_index == index )
						{
							audio_mgr_debug("client_cb:: remove the client index \n");
							pAudioMgrPriv.pExtAudioEntryList = g_list_remove (pAudioMgrPriv.pExtAudioEntryList , audio_entry );
							if(audio_entry->AppName)
							{
								g_free(audio_entry->AppName);
								audio_entry->AppName = NULL;
							}
							g_free(audio_entry);
							audio_entry = NULL;
							break;
						}
					}
				}
				pa_operation_unref(o);
			}
			break;

		case PA_SUBSCRIPTION_EVENT_SERVER:
			break;

		case PA_SUBSCRIPTION_EVENT_CARD:
			break;

		default :
	  	audio_mgr_debug(" default %d \n" , t & PA_SUBSCRIPTION_EVENT_FACILITY_MASK);
			break;
	}
}

static void sink_cb(pa_context *ctx, const pa_sink_info *sink_info, int eol, void *pUserData)
{
	audio_mgr_debug("sink_cb \n");
	if (eol < 0)
	{
		if (pa_context_errno(pAudioMgrPriv.pCtxt) == PA_ERR_NOENTITY)
			return;

		audio_mgr_debug("Sink callback failure\n");
		return;
	}
	if(sink_info)
	{
		audio_mgr_debug("sink_cb:: index %d , name %s , descr %s , volume %ud , mute %d , state %d pUserData %s \n" , sink_info->index , sink_info->name ,
				sink_info->description , sink_info->volume.values[0] , sink_info->mute , sink_info->state , (gchar *)pUserData);
	}
	if (eol > 0)
	{
		return;
	}
	//w->updateSink(*i);
}

static void source_cb(pa_context *ctx, const pa_source_info *src_info, int eol, void *pUserData)
{
	audio_mgr_debug("source_cb \n");
	if (eol < 0)
	{
		if (pa_context_errno(pAudioMgrPriv.pCtxt) == PA_ERR_NOENTITY)
			return;

		audio_mgr_debug("Source callback failure\n");
		return;
	}

	if(src_info)
	{
		audio_mgr_debug("source_cb:: index %d , name %s , descr %s , volume %ud , mute %d , state %d pUserData %s \n" , src_info->index , src_info->name , src_info->description ,
				src_info->volume.values[0], src_info->mute , src_info->state , (gchar*)pUserData );

		if(FALSE ==g_strcmp0(pUserData , "NONE"))
		 pa_context_set_source_mute_by_index( pAudioMgrPriv.pCtxt, src_info->index , TRUE , source_input_mute_by_index , NULL );
	}

	if (eol > 0)
	{
		return;
	}

}

static pa_stream *
createMonitorStreamForSource (uint32_t source_idx,
                              uint32_t stream_idx)
{
  pa_stream *stream;
  pa_sample_spec ss;
  ss.channels = 1;
  ss.format = PA_SAMPLE_FLOAT32;
  ss.rate = 25;

  if (!(stream = pa_stream_new(pAudioMgrPriv.pCtxt, "Peak detect", &ss, NULL)))
  {
      audio_mgr_debug("Failed to connect monitoring stream");
      return NULL;
  }

  pa_stream_set_read_callback(stream, read_callback, NULL);
  pa_stream_set_suspended_callback(stream, suspended_callback, NULL);

  return stream;
}

static void read_callback(pa_stream *s, size_t length, void *pUserData) {

    const void *data;
    double v;

    if (pa_stream_peek(s, &data, &length) < 0) {
        audio_mgr_debug("Failed to read data from stream");
        return;
    }

    assert(length > 0);
    assert(length % sizeof(float) == 0);

    v = ((const float*) data)[length / sizeof(float) -1];

    pa_stream_drop(s);

    if (v < 0)
        v = 0;
    if (v > 1)
        v = 1;


    if (pAudioMgrPriv.lastPeak >= DECAY_STEP)
        if (v < pAudioMgrPriv.lastPeak - DECAY_STEP)
            v = pAudioMgrPriv.lastPeak - DECAY_STEP;

    pAudioMgrPriv.lastPeak = v;
    v_audio_pitch_signal(pAudioMgrPriv.lastPeak);
    audio_mgr_debug("V=%f\n", v);
}

static void suspended_callback(pa_stream *s, void *pUserData) {

    audio_mgr_debug("Stream Destroyed for Source Index %d!!!",pa_stream_get_device_index(s));

//    if (pa_stream_is_suspended(s))
//        pa_stream_unref(s);

}


static void sink_input_cb(pa_context *ctx, const pa_sink_input_info *sink_input, int eol, void *pUserData)
{
	audio_mgr_debug("sink_input_cb \n");
	if(sink_input)
	{
		audio_mgr_debug("sink_input_cb:: client index %d , sink index %d , name %s , volume %ud , mute %d  , pUserData %s \n" ,
				sink_input->client , sink_input->index , sink_input->name , sink_input->volume.values[0] , sink_input->mute , (gchar*)pUserData);

		if( (pUserData != NULL ) && !strcmp(pUserData , "NEW") )
		{
			guint client_cnt = 0;
			ExternalAudioEntry* audio_entry = NULL;
			guint list_len = g_list_length( pAudioMgrPriv.pExtAudioEntryList );
			audio_mgr_debug("sink_input_cb NEW \n" );
			pa_context_set_sink_input_mute(pAudioMgrPriv.pCtxt , sink_input->index , 0 , sink_input_mute_by_index , NULL );
			for( client_cnt = 0 ; client_cnt< list_len ; client_cnt++)
			{
				audio_entry = g_list_nth_data ( pAudioMgrPriv.pExtAudioEntryList , client_cnt );
				audio_mgr_debug("sink_input_cb App_index %d \n" , audio_entry->App_index);
				if( audio_entry->App_index == sink_input->client )
				{
					const gchar* app_name;

					audio_mgr_debug("sink_input_cb:: sink index %d \n", sink_input->index);
					audio_entry->uinSinkIndex = sink_input->index;
					app_name = get_app_name_from_channel_name (audio_entry->AppName);
					if(app_name != NULL)
					{
						pa_context_set_sink_input_mute(pAudioMgrPriv.pCtxt , sink_input->index , 1 , sink_input_mute_by_index , NULL );
						v_external_app_audio_invoke_clb( app_name ,CANTERBURY_APP_AUDIO_REQUEST , audio_entry->uinSinkIndex );
					}
					break;
				}
			}
		}
	}
	else  if( (pUserData != NULL ) && !strcmp(pUserData , "REMOVE") )
	{

	}

	if (eol < 0)
	{
		if (pa_context_errno(pAudioMgrPriv.pCtxt) == PA_ERR_NOENTITY)
			return;

		audio_mgr_debug("Sink input callback failure");
		return;
	}
}

static void source_output_cb(pa_context *ctx, const pa_source_output_info *src_output_info, int eol, void *pUserData)
{
	audio_mgr_debug("source_output_cb \n");
	if( FALSE == g_strcmp0((gchar*)pUserData , "REMOVE") )
	{
    audio_mgr_debug("Source output callback REMOVE \n");
    if(pAudioMgrPriv.stream)
    {
      pa_stream_set_read_callback(pAudioMgrPriv.stream, NULL, NULL);
      pa_stream_unref(pAudioMgrPriv.stream);
      pAudioMgrPriv.stream = NULL;

      if(src_output_info)
       pa_context_set_source_mute_by_index( pAudioMgrPriv.pCtxt, src_output_info->source , TRUE , source_input_mute_by_index , NULL );
    }
    return;
	}

	if (eol < 0)
	{
		if (pa_context_errno(pAudioMgrPriv.pCtxt) == PA_ERR_NOENTITY)
			return;

		audio_mgr_debug("Source output callback failure\n");
		return;
	}


	if(src_output_info)
	{
	  char dev[16];
	  pa_stream_flags_t flags;
	  pa_buffer_attr attr;

	  if( FALSE == g_strcmp0((gchar*)pUserData , "NEW") )
	  {
	    if((NULL==pAudioMgrPriv.stream))
	     pAudioMgrPriv.stream = createMonitorStreamForSource(src_output_info->source, -1);
      pa_context_set_source_mute_by_index( pAudioMgrPriv.pCtxt, src_output_info->source , FALSE , source_input_mute_by_index , NULL );

      memset(&attr, 0, sizeof(attr));
      attr.fragsize = sizeof(float);
      attr.maxlength = (uint32_t) -1;
	    snprintf(dev, sizeof(dev), "%u", src_output_info->source);

      flags = (pa_stream_flags_t) (PA_STREAM_DONT_MOVE | PA_STREAM_PEAK_DETECT | PA_STREAM_ADJUST_LATENCY | PA_STREAM_NOFLAGS);
      if (pa_stream_connect_record(pAudioMgrPriv.stream, dev, &attr, flags) < 0)
      {
        g_warning("Failed to connect monitoring stream %d\n", src_output_info->source);
      }
	  }
		audio_mgr_debug("source_output_cb:: index %d , source index %d name %s pUserData %s\n" , src_output_info->index , src_output_info->source , src_output_info->name , (gchar*)pUserData);
	}

	if (eol > 0)
	{
		return;
	}
}

static const gchar* get_app_name_from_channel_name(gchar* channel_name)
{
	guint cnt;

	for( cnt = 0 ; cnt< g_list_length( pAudioMgrPriv.appEntriesDB ) ; cnt++)
	{
		AudioEntry *audio_entry = (AudioEntry*)g_list_nth_data ( pAudioMgrPriv.appEntriesDB , cnt );
		audio_mgr_debug ("AUDIO MGR: App name frm db %s  channel name %s req app %s\n" , audio_entry->AppName ,  audio_entry->pAudioChannelName , channel_name);

		if( audio_entry->uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_EXTERNAL )
		{
			gchar *channel_name_local;

			if( !strcmp( audio_entry->pAudioChannelName , channel_name ) )
			{
				audio_mgr_debug ("AUDIO MGR: valid external app name %s found \n", channel_name);
				return audio_entry->AppName;
			}

			channel_name_local = g_strconcat ("ALSA plug-in [", audio_entry->pAudioChannelName, "]" , NULL);
			if( !strcmp( channel_name_local , channel_name ) )
			{
				audio_mgr_debug ("AUDIO MGR: valid external app name %s found \n", channel_name);
				g_free(channel_name_local);
				return audio_entry->AppName;
			}
			g_free(channel_name_local);

			if (activity_mgr_has_entry_point (audio_entry->AppName))
			{
				audio_mgr_debug ("AUDIO MGR: valid external app name %s found \n", channel_name);
				return audio_entry->AppName;
			}
		}
	}

	return NULL;
}

static gboolean
check_for_valid_external_app_name (const gchar *app_name)
{
	guint cnt;
	for( cnt = 0 ; cnt< g_list_length( pAudioMgrPriv.appEntriesDB ) ; cnt++)
	{
		AudioEntry *audio_entry = (AudioEntry*)g_list_nth_data ( pAudioMgrPriv.appEntriesDB , cnt );
		audio_mgr_debug ("AUDIO MGR: App name frm db %s  channel name %s req app %s\n" , audio_entry->AppName ,  audio_entry->pAudioChannelName , app_name);

		if(  audio_entry->uinAudioResourceType == CANTERBURY_AUDIO_RESOURCE_EXTERNAL )
		{
			gchar *channel_name;

			if( !strcmp( audio_entry->pAudioChannelName , app_name ) )
			{
				audio_mgr_debug ("AUDIO MGR: valid external app name %s found \n", app_name);
				return TRUE;
			}
			channel_name = g_strconcat ("ALSA plug-in [", audio_entry->pAudioChannelName, "]" , NULL);
			if( !strcmp( channel_name , app_name ) )
			{
				audio_mgr_debug ("AUDIO MGR: valid external app name %s found \n", app_name);
				g_free(channel_name);
				return TRUE;
			}
			g_free(channel_name);
		}
		else
		{
			gchar *channel_name;

			if(  !strcmp( audio_entry->pAudioChannelName , app_name ) )
			{
				audio_mgr_debug ("AUDIO MGR: valid internal app name %s found \n", app_name);
				return FALSE;
			}
			channel_name = g_strconcat ("ALSA plug-in [", audio_entry->pAudioChannelName, "]" , NULL);
			if( !strcmp( channel_name , app_name ) )
			{
				audio_mgr_debug ("AUDIO MGR: valid internal app name %s found \n", app_name);
				g_free(channel_name);
				return FALSE;
			}
			g_free(channel_name);
		}
	}
	return TRUE;
}

static void handle_client_audio_statue_change(const pa_client_info *client_info)
{
  if( check_for_valid_external_app_name(client_info->name) )
  {
    guint client_cnt = 0;
    ExternalAudioEntry* audio_entry = NULL;
    guint list_len = g_list_length( pAudioMgrPriv.pExtAudioEntryList );
    for( client_cnt = 0 ; client_cnt< list_len ; client_cnt++)
    {
      //audio_mgr_debug("client_cb:: client index %d  \n" , audio_entry->App_index);
      audio_entry = g_list_nth_data ( pAudioMgrPriv.pExtAudioEntryList , client_cnt );
      if( audio_entry->App_index == client_info->index )
      {
        audio_mgr_debug("client_cb:: client name %s \n", client_info->name);
        if( audio_entry->AppName == NULL )
          audio_entry->AppName = g_strdup(client_info->name);
        break;
      }
    }
  }
  else
  {
    guint client_cnt = 0;
    ExternalAudioEntry* audio_entry = NULL;
    guint list_len = g_list_length( pAudioMgrPriv.pExtAudioEntryList );
    for( client_cnt = 0 ; client_cnt< list_len ; client_cnt++)
    {
      audio_entry = g_list_nth_data ( pAudioMgrPriv.pExtAudioEntryList , client_cnt );
      if( audio_entry->App_index == client_info->index )
      {
        pAudioMgrPriv.pExtAudioEntryList = g_list_remove(pAudioMgrPriv.pExtAudioEntryList , audio_entry );
        if(audio_entry->AppName)
          g_free(audio_entry->AppName);
        g_free(audio_entry);
        break;
      }
    }
  }
}

static void client_cb(pa_context *ctx, const pa_client_info *client_info, int eol, void *pUserData)
{
	audio_mgr_debug(" client_cb \n");
	if(client_info)
	{
		if( !strcmp(pUserData , "NEW") )
		{
			ExternalAudioEntry *new_audio_entry;

			audio_mgr_debug(" client_cb NEW app name <%s> \n", client_info->name);
			new_audio_entry = g_new0 (ExternalAudioEntry , 1);
			new_audio_entry->App_index = client_info->index;
			new_audio_entry->AppName =NULL;
			// if( check_for_valid_external_app_name(client_info->name) )
			//    new_audio_entry->AppName = g_strdup(client_info->name);
			new_audio_entry->uinSinkIndex = 0;
			pAudioMgrPriv.pExtAudioEntryList = g_list_append( pAudioMgrPriv.pExtAudioEntryList , new_audio_entry );
		}
		else if( !strcmp(pUserData , "CHANGE") )
		{
			audio_mgr_debug(" client_cb CHANGE \n");
			handle_client_audio_statue_change(client_info);
		}
		else
		{
			if( ! strcmp(pUserData , "REMOVE" ) )
			{
				guint client_cnt = 0;
				ExternalAudioEntry* audio_entry = NULL;
				for( client_cnt = 0 ; client_cnt< g_list_length( pAudioMgrPriv.pExtAudioEntryList ) ; client_cnt++)
				{
					audio_entry = g_list_nth_data ( pAudioMgrPriv.pExtAudioEntryList , client_cnt );
					if( audio_entry->App_index == client_info->index )
					{
						audio_mgr_debug("client_cb:: remove the client index \n");
						pAudioMgrPriv.pExtAudioEntryList = g_list_remove (pAudioMgrPriv.pExtAudioEntryList , audio_entry );
						if(audio_entry->AppName)
							g_free(audio_entry->AppName);
						g_free(audio_entry);
						break;
					}
				}
			}
		}

		audio_mgr_debug("client_cb:: index %d , name <`%s`> pUserData %s\n" , client_info->index , client_info->name , (gchar*)pUserData );
	}

	if( !strcmp(pUserData , "REMOVE") )
	{

	}

	if (eol < 0)
	{
		if (pa_context_errno(pAudioMgrPriv.pCtxt) == PA_ERR_NOENTITY)
			return;

		audio_mgr_debug("Client callback failure");
		return;
	}
}

#if 0
static void ext_stream_restore_read_cb( pa_context *ctx,  const pa_ext_stream_restore_info *stream_restore_info,  int eol, void *pUserData)
{
	audio_mgr_debug(" ext_stream_restore_read_cb \n" );
	if (eol < 0)
	{
		audio_mgr_debug("Failed to initialize stream_restore extension: %s \n", pa_strerror(pa_context_errno(pAudioMgrPriv.pCtxt)) );
		return;
	}
	if(stream_restore_info)
		audio_mgr_debug(" ext_stream_restore_read_cb :: stream identifier %s The sink/source of the stream %s, pUserData %s \n" ,
				stream_restore_info->name ,  stream_restore_info->device , (gchar*)pUserData);
}


static void ext_stream_restore_subscribe_cb(pa_context *ctx, void *pUserData)
{
	pa_operation *o;
	audio_mgr_debug(" ext_stream_restore_subscribe_cb \n");
	if (!(o = pa_ext_stream_restore_read(ctx, ext_stream_restore_read_cb, NULL)))
	{
		audio_mgr_debug("pa_ext_stream_restore_read() failed \n");
		return;
	}

	pa_operation_unref(o);
}
#endif
