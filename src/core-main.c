/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * canterbury-core: minimal version of canterbury
 *
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <string.h>

#include <glib.h>
#include <gio/gio.h>
#include <systemd/sd-daemon.h>

#include "canterbury/errors.h"
#include "canterbury/platform/component-index.h"
#include "canterbury/platform/entry-point-index.h"
#include "canterbury/platform/environment.h"
#include "canterbury/messages-internal.h"

#include "per-user-bridge.h"
#include "service-manager.h"
#include "util.h"

static void
init_completed_cb (GObject *nil G_GNUC_UNUSED,
                   GAsyncResult *result,
                   gpointer user_data)
{
  GTask *task = G_TASK (result);

  if (g_task_had_error (task))
    g_main_loop_quit (user_data);
  else
    DEBUG ("Initialization completed");
}

static void
start_agents_cb (GObject *source,
                 GAsyncResult *result,
                 gpointer user_data)
{
  CbyServiceManager *service_manager = CBY_SERVICE_MANAGER (source);
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  int ret;

  if (!_cby_service_manager_start_agents_finish (service_manager, result,
                                                 &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  ret = sd_notify (0, "READY=1");

  if (ret > 0)
    {
      DEBUG ("notified systemd that we are ready");
      g_task_return_boolean (task, TRUE);
    }
  else if (ret == 0)
    {
      DEBUG ("ready, but not started by systemd");
      g_task_return_boolean (task, TRUE);
    }
  else
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                               "Unable to notify systemd that we are ready: %s",
                               strerror (-ret));
      return;
    }
}

static void
reload_units_cb (GObject *source,
                 GAsyncResult *result,
                 gpointer user_data)
{
  CbyServiceManager *service_manager = CBY_SERVICE_MANAGER (source);
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;

  if (!_cby_service_manager_reload_units_finish (service_manager, result,
                                                 &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  _cby_service_manager_start_agents_async (service_manager, NULL,
                                           start_agents_cb,
                                           g_object_ref (task));
}

static void
per_user_bridge_cb (GObject *source,
                    GAsyncResult *result,
                    gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GTask) task = user_data;
  CbyServiceManager *service_manager = g_task_get_task_data (task);

  if (!cby_per_user_bridge_register_finish (CBY_PER_USER_BRIDGE (source),
                                            result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  _cby_service_manager_reload_units_async (service_manager, NULL,
                                           reload_units_cb,
                                           g_object_ref (task));
}

int
main (int argc,
      char *argv[])
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (GDBusConnection) session_bus = NULL;
  g_autoptr (CbyComponentIndex) component_index = NULL;
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (CbyPerUserBridge) per_user_bridge = NULL;
  g_autoptr (CbyServiceManager) service_manager = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GMainLoop) main_loop = NULL;

  _cby_setenv_disable_services ();
  cby_init_environment ();

  session_bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);

  if (session_bus == NULL)
    {
      CRITICAL ("Unable to connect to D-Bus: %s", error->message);
      return 1;
    }

  component_index = cby_component_index_new (CBY_COMPONENT_INDEX_FLAGS_NONE,
                                             &error);

  if (component_index == NULL)
    {
      CRITICAL ("Unable to load component index: %s", error->message);
      return 1;
    }

  entry_point_index = cby_entry_point_index_new (component_index);

  service_manager = _cby_service_manager_new (session_bus, entry_point_index,
                                              &error);

  if (service_manager == NULL)
    {
      CRITICAL ("Unable to set up service manager: %s", error->message);
      return 1;
    }

  main_loop = g_main_loop_new (NULL, FALSE);
  task = g_task_new (NULL, NULL, init_completed_cb, main_loop);
  /* Later steps will need the CbyServiceManager */
  g_task_set_task_data (task, g_object_ref (service_manager), g_object_unref);

  /* Register to receive TerminateBundle() before we start launching anything,
   * so that anything we launch, we are definitely able to terminate. */
  per_user_bridge = cby_per_user_bridge_new (service_manager);
  cby_per_user_bridge_register_async (per_user_bridge, per_user_bridge_cb,
                                      g_object_ref (task));

  g_main_loop_run (main_loop);

  if (g_task_get_completed (task) &&
      !g_task_propagate_boolean (task, &error))
    {
      CRITICAL ("Unable to finish initialization: %s", error->message);
      return 1;
    }

  return 0;
}
