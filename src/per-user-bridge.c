/*
 * Copyright © 2017 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "per-user-bridge.h"
#include "canterbury/messages-internal.h"

struct _CbyPerUserBridge
{
  CbyPerUserAppManager1Skeleton skeleton;
  guint name_ownership;
  GDBusConnection *system_bus;
  CbyPrivilegedAppHelper1 *priv_app_helper_proxy;
  GTask *register_task;
  CbyServiceManager *service_manager;
};

G_DEFINE_TYPE (CbyPerUserBridge, cby_per_user_bridge, CBY_TYPE_PER_USER_APP_MANAGER1_SKELETON)

enum
{
  PROP_SERVICE_MANAGER = 1,
};

static void
terminate_bundle_final_cb (GObject *source_object,
                           GAsyncResult *result,
                           gpointer user_data)
{
  CbyPerUserAppManager1 *iface = CBY_PER_USER_APP_MANAGER1 (source_object);
  g_autoptr (GDBusMethodInvocation) invocation = user_data;
  g_autoptr (GError) error = NULL;

  if (g_task_propagate_boolean (G_TASK (result), &error))
    cby_per_user_app_manager1_complete_terminate_bundle (iface, invocation);
  else
    g_dbus_method_invocation_return_gerror (invocation, error);
}

static void
stop_bundle_cb (GObject *source_object,
                GAsyncResult *result,
                gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  CbyPerUserBridge *self = g_task_get_source_object (task);

  if (!_cby_service_manager_stop_bundle_finish (self->service_manager, result,
                                                &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_return_boolean (task, TRUE);
}

static void
terminate_bundle_process_info_cb (GObject *nil G_GNUC_UNUSED,
                                  GAsyncResult *result,
                                  gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (CbyProcessInfo) process_info =
      cby_process_info_new_for_dbus_invocation_finish (result, &error);
  CbyPerUserBridge *self = g_task_get_source_object (task);
  const gchar *label;
  const gchar *bundle_id = g_task_get_task_data (task);

  if (process_info == NULL)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_ACCESS_DENIED,
                               "Unable to identify caller: %s",
                               error->message);
      return;
    }

  if (cby_process_info_get_process_type (process_info) !=
      CBY_PROCESS_TYPE_PLATFORM)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_ACCESS_DENIED,
                               "TerminateBundle may only be called by "
                               "platform processes");
      return;
    }

  label = cby_process_info_get_apparmor_label (process_info);

  if (g_strcmp0 (label, "unconfined") != 0 &&
      g_strcmp0 (label, "/usr/bin/ribchester") != 0 &&
      g_strcmp0 (label, "/usr/lib/ribchester/ribchester-core") != 0)
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_ACCESS_DENIED,
                               "TerminateBundle may only be called by "
                               "Ribchester or an unconfined process");
      return;
    }

  _cby_service_manager_stop_bundle_async (self->service_manager,
                                          bundle_id, NULL, stop_bundle_cb,
                                          g_object_ref (task));
}

static gboolean
cby_per_user_bridge_terminate_bundle (CbyPerUserBridge *self,
                                      GDBusMethodInvocation *invocation,
                                      const gchar *bundle_id,
                                      gpointer nil G_GNUC_UNUSED)
{
  g_autoptr (GTask) task = g_task_new (self, NULL,
                                       terminate_bundle_final_cb,
                                       g_object_ref (invocation));

  if (!cby_is_bundle_id (bundle_id))
    {
      g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT,
                               "Not a valid bundle ID: \"%s\"", bundle_id);
      return TRUE; /* handled */
    }

  g_task_set_task_data (task, g_strdup (bundle_id), g_free);
  cby_process_info_new_for_dbus_invocation_async (invocation, NULL,
      terminate_bundle_process_info_cb, g_object_ref (task));
  return TRUE; /* handled */
}

static void
cby_per_user_bridge_constructed (GObject *object)
{
  CbyPerUserBridge *self = CBY_PER_USER_BRIDGE (object);

  g_signal_connect (self, "handle-terminate-bundle",
                    G_CALLBACK (cby_per_user_bridge_terminate_bundle), NULL);
}

static void
cby_per_user_bridge_get_property (GObject *object,
                                  guint prop_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
  CbyPerUserBridge *self = CBY_PER_USER_BRIDGE (object);

  switch (prop_id)
    {
    case PROP_SERVICE_MANAGER:
      g_value_set_object (value, self->service_manager);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
cby_per_user_bridge_set_property (GObject *object,
                                  guint prop_id,
                                  const GValue *value,
                                  GParamSpec *pspec)
{
  CbyPerUserBridge *self = CBY_PER_USER_BRIDGE (object);

  switch (prop_id)
    {
    case PROP_SERVICE_MANAGER:
      /* construct-only */
      g_assert (self->service_manager == NULL);
      self->service_manager = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
cby_per_user_bridge_dispose (GObject *object)
{
  CbyPerUserBridge *self = CBY_PER_USER_BRIDGE (object);

  if (self->name_ownership != 0)
    {
      g_bus_unwatch_name (self->name_ownership);
      self->name_ownership = 0;
    }

  g_clear_object (&self->system_bus);
  g_clear_object (&self->priv_app_helper_proxy);
  g_clear_object (&self->register_task);
  g_clear_object (&self->service_manager);

  G_OBJECT_CLASS (cby_per_user_bridge_parent_class)->dispose (object);
}

static void
cby_per_user_bridge_class_init (CbyPerUserBridgeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = cby_per_user_bridge_constructed;
  object_class->dispose = cby_per_user_bridge_dispose;
  object_class->get_property = cby_per_user_bridge_get_property;
  object_class->set_property = cby_per_user_bridge_set_property;

  /**
   * CbyPerUserBridge:service-manager:
   *
   * The service manager used to stop services.
   */
  g_object_class_install_property (object_class, PROP_SERVICE_MANAGER,
      g_param_spec_object (
          "service-manager", "Service manager",
          "Service manager used to stop services", CBY_TYPE_SERVICE_MANAGER,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

static void
cby_per_user_bridge_init (CbyPerUserBridge *self)
{
}

CbyPerUserBridge *
cby_per_user_bridge_new (CbyServiceManager *service_manager)
{
  g_return_val_if_fail (CBY_IS_SERVICE_MANAGER (service_manager), NULL);

  return g_object_new (CBY_TYPE_PER_USER_BRIDGE,
                       "service-manager", service_manager,
                       NULL);
}

static void
system_bus_cb (GDBusConnection *system_bus,
               const gchar *trying_to_own_name,
               gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  CbyPerUserBridge *self = CBY_PER_USER_BRIDGE (user_data);

  g_assert (self->system_bus == NULL);
  self->system_bus = g_object_ref (system_bus);

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self),
                                         self->system_bus,
                                         CBY_OBJECT_PATH_PER_USER_APP_MANAGER1,
                                         &error))
    {
      g_task_return_error (self->register_task, g_steal_pointer (&error));
      g_clear_object (&self->register_task);
      return;
    }
}

static void
register_user_app_manager_cb (GObject *source,
                              GAsyncResult *result,
                              gpointer user_data)
{
  g_autoptr (CbyPerUserBridge) self = user_data;
  g_autoptr (GError) error = NULL;

  if (!cby_privileged_app_helper1_call_register_user_manager_finish (
          self->priv_app_helper_proxy, result, &error))
    {
      /* We don't fail here (yet) since we might not have a new enough
       * version of Ribchester to accept this method call from us */
      WARNING ("Unable to register with Ribchester, it might be too old: %s",
               error->message);
    }

  g_task_return_boolean (self->register_task, TRUE);
}

static void
privileged_app_helper_proxy_cb (GObject *source,
                                GAsyncResult *result,
                                gpointer user_data)
{
  g_autoptr (CbyPerUserBridge) self = user_data;
  g_autoptr (GError) error = NULL;

  self->priv_app_helper_proxy =
      cby_privileged_app_helper1_proxy_new_finish (result, &error);

  if (self->priv_app_helper_proxy == NULL)
    {
      g_task_return_error (self->register_task, g_steal_pointer (&error));
      g_clear_object (&self->register_task);
      return;
    }

  cby_privileged_app_helper1_call_register_user_manager (
      self->priv_app_helper_proxy, NULL, register_user_app_manager_cb,
      g_object_ref (self));
}

static void
name_acquired_cb (GDBusConnection *system_bus,
                  const gchar *name,
                  gpointer user_data)
{
  CbyPerUserBridge *self = CBY_PER_USER_BRIDGE (user_data);

  cby_privileged_app_helper1_proxy_new (
      system_bus,
      (G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES |
       G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS),
      CBY_SYSTEM_BUS_NAME_RIBCHESTER,
      CBY_OBJECT_PATH_PRIVILEGED_APP_HELPER1,
      NULL,
      privileged_app_helper_proxy_cb,
      g_object_ref (self));
}

static void
name_lost_cb (GDBusConnection *system_bus,
              const gchar *name,
              gpointer user_data)
{
  CbyPerUserBridge *self = CBY_PER_USER_BRIDGE (user_data);

  /* If we are still trying to register, then we've failed. If we already
   * registered, it's OK that we lost the name - it means another Canterbury
   * instance has taken it from us. */
  if (self->register_task != NULL)
    {
      g_autoptr (GTask) task = g_steal_pointer (&self->register_task);

      if (system_bus == NULL)
        g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                                 "Unable to connect to system bus");
      else
        g_task_return_new_error (task, CBY_ERROR, CBY_ERROR_FAILED,
                                 "Unable to acquire bus name \"%s\"", name);
    }
  else
    {
      DEBUG ("Lost name \"%s\", presumably taken by another instance", name);
    }
}

void
cby_per_user_bridge_register_async (CbyPerUserBridge *self,
                                    GAsyncReadyCallback callback,
                                    gpointer user_data)
{
  g_return_if_fail (CBY_IS_PER_USER_BRIDGE (self));
  /* Only allow this function to be called once */
  g_return_if_fail (self->name_ownership == 0);
  g_return_if_fail (self->register_task == NULL);
  g_return_if_fail (self->system_bus == NULL);

  self->register_task = g_task_new (self, NULL, callback, user_data);
  g_task_set_source_tag (self->register_task,
                         cby_per_user_bridge_register_async);

  self->name_ownership =
      g_bus_own_name (G_BUS_TYPE_SYSTEM,
                      CBY_SYSTEM_BUS_NAME_PER_USER_APP_MANAGER1,
                      (G_BUS_NAME_OWNER_FLAGS_ALLOW_REPLACEMENT |
                       G_BUS_NAME_OWNER_FLAGS_REPLACE),
                      system_bus_cb,
                      name_acquired_cb,
                      name_lost_cb,
                      self, NULL);
}

gboolean
cby_per_user_bridge_register_finish (CbyPerUserBridge *self,
                                     GAsyncResult *result,
                                     GError **error)
{
  g_return_val_if_fail (CBY_IS_PER_USER_BRIDGE (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
                                                  cby_per_user_bridge_register_async),
                        FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}
