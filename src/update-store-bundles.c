/*
 * canterbury-update-store-bundles: update system state after store
 * app-bundle maintenance
 *
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>
#include "canterbury/messages-internal.h"

#include "apparmor.h"
#include "util.h"

static GOptionEntry entries[] = { { NULL } };

static gboolean
populate_apparmor_ubercache (GError **error)
{
  g_autoptr (CbyAppArmorStore) apparmor = NULL;

  apparmor = _cby_apparmor_store_open (CBY_PATH_SYSTEM_EXTENSIONS "/apparmor.d",
                                       error);

  if (apparmor == NULL)
    return FALSE;

  if (!_cby_apparmor_store_invalidate_ubercache (apparmor, error))
    return FALSE;

  return _cby_apparmor_store_ensure_and_load_ubercache (apparmor, error);
}

int
main (int argc, char **argv)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GOptionContext) context = NULL;
  int exit_status;
  gboolean failed = FALSE;
  const gchar * const update_component_index_argv[] = {
      BINDIR "/canterbury-update-component-index",
      NULL
  };

  _cby_setenv_disable_services ();

  context = g_option_context_new ("- update system-wide caches");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return 2;
    }

  if (argc > 1)
    {
      g_printerr ("%s: too many arguments\n", g_get_prgname ());
      return 2;
    }

  /* We are no longer in a situation where we can abort app-bundle
   * installation by this point, so if this fails, we might as well try to
   * struggle onwards. */
  if (!populate_apparmor_ubercache (&error))
    {
      g_printerr ("%s: %s\n", g_get_prgname (), error->message);
      g_clear_error (&error);
      failed = TRUE;
    }

  /* Drop privileges by executing a less special executable to update
   * the component index. We run this as a subprocess so that we can
   * exit unsuccessfully (for the AppArmor update, above) even if this
   * succeeds. */
  if (!g_spawn_sync (NULL,
                     (gchar **) update_component_index_argv,
                     NULL,
                     G_SPAWN_DEFAULT,
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     &exit_status,
                     &error))
    {
      g_printerr ("%s: unable to start \"%s\": %s\n", g_get_prgname (),
                  update_component_index_argv[0], error->message);
      g_clear_error (&error);
      failed = TRUE;
    }
  else if (!g_spawn_check_exit_status (exit_status, &error))
    {
      g_printerr ("%s: while running \"%s\": %s\n", g_get_prgname (),
                  update_component_index_argv[0], error->message);
      g_clear_error (&error);
      failed = TRUE;
    }

  if (failed)
    return 1;
  else
    return 0;
}
