/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "hardkeys-mgr"
#include "hard_keys.h"

#include <string.h>

/*********************************************************************************************
 * Function:    handle_home_press_clb
 * Description: Call back function invoked when launcher sends a home message to the app mgr.
 *
 * Parameters:  none
 * Return:      none
 ********************************************************************************************/
gboolean handle_home_press_clb ( CanterburyHardKeys *object,
                                 GDBusMethodInvocation   *invocation,
                                 gpointer                 pUserData )
{
 const gchar *pCurrentAppNnTop = NULL;
 if(is_app_launch_in_progress())
 {
    canterbury_hard_keys_complete_home_press(object , invocation);
    return TRUE;
 }
 /* get the application running on top */
 pCurrentAppNnTop = activity_mgr_get_app_on_top();
 hide_global_popups();

   /* check if the launcher already on top */
 if (!g_strcmp0 (pCurrentAppNnTop,  LAUNCHER_APP_NAME))
 {
   hard_keys_mgr_debug("*** launcher on top \n");
   canterbury_hard_keys_complete_home_press(object, invocation);
   hard_keys_emit_home_press_response (FALSE);
   return TRUE;
 }
 canterbury_hard_keys_complete_home_press(object, invocation);
 display_launcher_app( pCurrentAppNnTop , CANTERBURY_HOME_KEY);
 return TRUE;
}


