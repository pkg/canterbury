/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "activity-mgr"
#include "activity_mgr_if.h"

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "app_mgr_if.h"
#include "audio_mgr_if.h"
#include "canterbury/messages-internal.h"
#include "launch_mgr_if.h"
#include "launcher_mgr_if.h"
#include "win_mgr_if.h"

#define ACTIVITY_MGR_MAX_APPS 7
#define ACTIVITY_MGR_MAX_KILL_APPS 10

typedef enum
{
  ACTIVITY_INITIALIZE_STARTUP_APPS,
  ACTIVITY_START_APP,
  ACTIVITY_PAUSE_APP,
  ACTIVITY_STOP_APP,
  ACTIVITY_RESTART_APP,
  ACTIVITY_HIDE_APP,
  ACTIVITY_SHOW_APP,
  ACTIVITY_OFF_APP,
  ACTIVITY_KILL_APP,
} ActivityMgrStates;

typedef struct
{
  ActivityMgrStates NewState;
  CbyEntryPoint *entry_point;
  GPid pid;
  CanterburyAppBkgState uinBkgState;
  gchar **arguments;
  gchar *pLaunchedFrom;
  guint uinSpecialParams;
} ActivityStateInfo;

static void activity_state_info_free (ActivityStateInfo *NewAppInfo);

typedef struct _ActivityMgrPrivate ActivityMgrPrivate;
typedef enum _ActivityMgrErrorType ActivityMgrErrorType;

struct _ActivityMgrPrivate
{
  GDBusConnection *session_bus;
  GHashTable *pending_terminations;
  GList *pAppActivityStack;
};

enum _ActivityMgrErrorType
{
  ACTIVITY_SUCCESS,
  ACTIVITY_APP_ALREADY_ON_TOP,
  ACTIVITY_ERROR_NO_APP_NAME,
  ACTIVITY_ERROR_WRONG_ACTIVITY_STATE,
  ACTIVITY_ERROR_STACK_FULL,
  ACTIVITY_ERROR_APP_NOT_STOPPED,
  ACTIVITY_ERROR_APP_NOT_RESTARTED,
  ACTIVITY_ERROR_APP_NOT_KILLED,
};

ActivityMgrPrivate ActivityMgrPriv;

static ActivityMgrErrorType activity_mgr_start_new_app (ActivityStateInfo *pNewAppData);
static ActivityMgrErrorType activity_mgr_show_app (ActivityStateInfo *pNewAppData);
static ActivityMgrErrorType activity_mgr_pause_new_app (ActivityStateInfo *pAppToPause);
static ActivityMgrErrorType activity_mgr_unpause_new_app (ActivityStateInfo *pAppToRestart);
static ActivityMgrErrorType activity_mgr_stop_new_app (ActivityStateInfo *pAppToStop);
static ActivityMgrErrorType activity_mgr_kill_app (ActivityStateInfo *pAppToKill);
static ActivityMgrErrorType activity_mgr_off_app (ActivityStateInfo *pAppToOff);
static void inform_app_new_state_to_application (CanterburyAppState  new_app_state,
                                                 ActivityStateInfo  *app_state_info);

/*
 * A GDestroyNotify to cancel a GSource.
 */
static void
cancel_source_cb (gpointer data)
{
  g_source_remove (GPOINTER_TO_UINT (data));
}

void
activity_mgr_init (GDBusConnection *session_bus)
{
  g_return_if_fail (G_IS_DBUS_CONNECTION (session_bus));

  ActivityMgrPriv.session_bus = g_object_ref (session_bus);
  ActivityMgrPriv.pending_terminations =
    g_hash_table_new_full (g_direct_hash, g_direct_equal, g_object_unref,
                           cancel_source_cb);
}

static void
activity_mgr_dbus_activate_cb (GObject *source_object,
                               GAsyncResult *result,
                               gpointer user_data)
{
  g_autoptr (CbyEntryPoint) entry_point = user_data;
  g_autoptr (GVariant) tuple = NULL;
  g_autoptr (GError) error = NULL;
  const gchar *id;

  g_return_if_fail (CBY_IS_ENTRY_POINT (entry_point));
  id = cby_entry_point_get_id (entry_point);

  tuple = g_dbus_connection_call_finish (ActivityMgrPriv.session_bus,
                                         result, &error);

  if (tuple != NULL)
    DEBUG ("Successfully activated %s", id);
  else
    DEBUG ("Failed to activate %s, but proceeding anyway: %s", id,
           error->message);

  activity_mgr_entry_point_activated (entry_point);
}

static void
activity_mgr_dbus_activate (CbyEntryPoint *entry_point,
                            const gchar *const *uris,
                            GVariant *platform_data)
{
  const gchar *id = cby_entry_point_get_id (entry_point);
  g_autofree gchar *object_path = g_strdup_printf ("/%s", id);
  g_autoptr (GVariant) platform_data_ref = NULL;

  if (platform_data == NULL)
    platform_data = g_variant_new_array (G_VARIANT_TYPE ("{sv}"), NULL, 0);

  platform_data_ref = g_variant_ref_sink (platform_data);

  /* Convert the entry point ID into a valid object path. This can't
   * fail for a DBusActivatable entry point, if CbyEntryPoint did its
   * checks correctly. */
  g_return_if_fail (g_dbus_is_interface_name (id));
  /* Every interface name is a valid bus name, so this can't fail. */
  g_return_if_fail (g_dbus_is_name (id));
  g_strdelimit (object_path, ".", '/');
  /* This assertion cannot fail, because every interface name makes a
   * valid object path when you prepend "/" and replace "." with "/". */
  g_return_if_fail (g_variant_is_object_path (object_path));

  if (uris != NULL)
    {
      /* Floating reference, sunk by g_dbus_connection_call */
      GVariant *floating_args =
          g_variant_new ("(^as@a{sv})", uris, platform_data);

      g_dbus_connection_call (ActivityMgrPriv.session_bus,
                              id, object_path, "org.freedesktop.Application",
                              "Open",
                              floating_args,
                              G_VARIANT_TYPE_UNIT,
                              G_DBUS_CALL_FLAGS_NONE,
                              -1, NULL,
                              activity_mgr_dbus_activate_cb,
                              g_object_ref (entry_point));
    }
  else
    {
      /* Floating reference, sunk by g_dbus_connection_call */
      GVariant *floating_args = g_variant_new ("(@a{sv})", platform_data);

      g_dbus_connection_call (ActivityMgrPriv.session_bus,
                              id, object_path, "org.freedesktop.Application",
                              "Activate",
                              floating_args,
                              G_VARIANT_TYPE_UNIT,
                              G_DBUS_CALL_FLAGS_NONE,
                              -1, NULL,
                              activity_mgr_dbus_activate_cb,
                              g_object_ref (entry_point));
    }
}

/**
 * activity_mgr_get_app_on_top:
 * Returns:   the current application on display
 *
 * activity_mgr_get_app_on_top looks into the list of applications currently running and returns the
 * application which is currently occupying the display
 *
**/
const gchar*
activity_mgr_get_app_on_top( void )
{
  ActivityStateInfo *top;

  if (ActivityMgrPriv.pAppActivityStack == NULL)
    return NULL;

  top = ActivityMgrPriv.pAppActivityStack->data;

  return cby_entry_point_get_id (top->entry_point);
}

/**
 * activity_mgr_has_entry_point:
 *
 * Returns: %TRUE if @entry_point_id appears in the app stack.
**/
gboolean
activity_mgr_has_entry_point (const gchar *entry_point_id)
{
   GList *pActivityStack;

   g_return_val_if_fail (entry_point_id != NULL, FALSE);

   for (pActivityStack = ActivityMgrPriv.pAppActivityStack;
        pActivityStack != NULL;
        pActivityStack = pActivityStack->next)
     {
        ActivityStateInfo* pAppData = (ActivityStateInfo*)pActivityStack->data;

        if (strcmp (entry_point_id,
                    cby_entry_point_get_id (pAppData->entry_point)) == 0)
          return TRUE;
     }

  return FALSE;
}

/*
 * @pActivityStateInfo: (transfer full): the state transition to carry out
 */
static ActivityMgrErrorType
activity_state_info_handle_sync (ActivityStateInfo *pActivityStateInfo)
{
    //activity_mgr_debug( "ACTIVITY MGR: new activity state %d stack length %d\n" , pActivityStateInfo->NewState  , g_list_length( ActivityMgrPriv.pAppActivityStack ) );
    ActivityMgrErrorType ErrorStatus = ACTIVITY_SUCCESS;
    const gchar *id = cby_entry_point_get_id (pActivityStateInfo->entry_point);

    switch( pActivityStateInfo->NewState )
    {
    case ACTIVITY_INITIALIZE_STARTUP_APPS:
        /* This state is dedicated to only the startup applications like launcher and status bar */
        activity_mgr_debug ("Entry point <%s> added to activity stack\n", id);
        if (!strcmp (id, LAUNCHER_APP_NAME))
        {
          ActivityMgrPriv.pAppActivityStack = g_list_prepend (ActivityMgrPriv.pAppActivityStack , pActivityStateInfo );
          app_mgr_set_app_on_top (pActivityStateInfo->entry_point);
        }
        else
            ActivityMgrPriv.pAppActivityStack = g_list_append (ActivityMgrPriv.pAppActivityStack , pActivityStateInfo );
        break;

    case ACTIVITY_HIDE_APP:
        inform_app_new_state_to_application( CANTERBURY_APP_STATE_BACKGROUND , pActivityStateInfo);
        break;

    case ACTIVITY_SHOW_APP:
        ErrorStatus = activity_mgr_show_app( pActivityStateInfo );
        /* the window manager should handle this and show the provided app on top */
        break;
    case ACTIVITY_START_APP:
        ErrorStatus = activity_mgr_start_new_app(pActivityStateInfo);
        break;

    case ACTIVITY_STOP_APP:
        ErrorStatus = activity_mgr_stop_new_app(pActivityStateInfo);
        break;

    case ACTIVITY_PAUSE_APP:
        ErrorStatus = activity_mgr_pause_new_app(pActivityStateInfo);
        break;

    case ACTIVITY_RESTART_APP:
        ErrorStatus = activity_mgr_unpause_new_app(pActivityStateInfo);
        break;

    case ACTIVITY_KILL_APP:
        ErrorStatus = activity_mgr_kill_app(pActivityStateInfo);
        break;

    case ACTIVITY_OFF_APP:
        ErrorStatus = activity_mgr_off_app(pActivityStateInfo);
        break;

    default:
        g_printerr(" ACTIVITY MGR:  Wrong activity state defined %d \n" , pActivityStateInfo->NewState);
        ErrorStatus = ACTIVITY_ERROR_WRONG_ACTIVITY_STATE;
        break;
    }
    //activity_mgr_debug( "ACTIVITY MGR: exit  activity_state_machine_handler \n");
    return ErrorStatus;
}

static gboolean
activity_state_info_handle_idle_cb (gpointer p)
{
  ActivityMgrErrorType e = activity_state_info_handle_sync (p);

  if (e != ACTIVITY_SUCCESS)
    DEBUG ("Error state %d", e);

  return G_SOURCE_REMOVE;
}

static void
activity_state_info_handle_in_idle (ActivityStateInfo *state)
{
  g_idle_add_full (G_PRIORITY_DEFAULT, activity_state_info_handle_idle_cb,
                   state, NULL);
}

static void
send_argument_updates_to_app (ActivityStateInfo *pNewAppData)
{
  if (pNewAppData->arguments != NULL && pNewAppData->arguments[0] != NULL)
  {
    if( pNewAppData->uinBkgState ==  CANTERBURY_BKG_STATE_STOPPED )
      inform_app_new_state_to_application( CANTERBURY_APP_STATE_RESTART , pNewAppData);
    else
      inform_app_new_state_to_application( CANTERBURY_APP_STATE_SHOW , pNewAppData);
  }
}

static void
handle_next_state_of_current_app_on_top (ActivityStateInfo *pCurrentAppNnTop)
{
  CanterburyExecutableType type =
    _cby_entry_point_get_executable_type (pCurrentAppNnTop->entry_point);

  if (type == CANTERBURY_EXECUTABLE_TYPE_STARTUP_APPLICATION)
  {
      //do nothing, it remains on top
      activity_mgr_debug( "ACTIVITY MGR: current app is of type startup \n"  );
  }
  else if( pCurrentAppNnTop->uinBkgState ==  CANTERBURY_BKG_STATE_RUNNING )
  {
      activity_mgr_debug( "ACTIVITY MGR: application in hide state \n"   );
      pCurrentAppNnTop->NewState = ACTIVITY_HIDE_APP;
      if (activity_state_info_handle_sync (pCurrentAppNnTop) != ACTIVITY_SUCCESS)
          g_printerr( "ACTIVITY MGR: Failed to set the current app to background state  \n"  );
  }
  else  if( pCurrentAppNnTop->uinBkgState ==  CANTERBURY_BKG_STATE_STOPPED )
  {
      pCurrentAppNnTop->NewState = ACTIVITY_PAUSE_APP;
      if (activity_state_info_handle_sync (pCurrentAppNnTop) != ACTIVITY_SUCCESS)
          g_printerr( "ACTIVITY MGR: Failed to set the current app to pause state  \n"  );
  }
  else if( pCurrentAppNnTop->uinBkgState ==  CANTERBURY_BKG_STATE_KILLED )
  {
      pCurrentAppNnTop->NewState = ACTIVITY_OFF_APP;
      if (activity_state_info_handle_sync (pCurrentAppNnTop) != ACTIVITY_SUCCESS)
          g_printerr( "ACTIVITY MGR: Failed to kill the current app  \n"  );
  }
}

static void
set_all_attributes_to_restart_new_app (ActivityStateInfo *pNewAppData,
    ActivityStateInfo* pAppToRestart)
{
  gboolean bAppToKillStillRunningInBkg = FALSE;
  const gchar *new_id = cby_entry_point_get_id (pNewAppData->entry_point);

  if (g_hash_table_remove (ActivityMgrPriv.pending_terminations,
                           pNewAppData->entry_point))
    {
      DEBUG ("Cancelled pending termination of %s", new_id);
      bAppToKillStillRunningInBkg = TRUE;
    }

  if( ( pNewAppData->uinBkgState == CANTERBURY_BKG_STATE_RUNNING ) || bAppToKillStillRunningInBkg )
  {
      /* Application already running in the background*/
      pAppToRestart->NewState = ACTIVITY_SHOW_APP;
      pAppToRestart->uinSpecialParams = pNewAppData->uinSpecialParams;
      if(pAppToRestart->pLaunchedFrom != NULL)
        g_free(pAppToRestart->pLaunchedFrom);

      pAppToRestart->pLaunchedFrom = g_strdup(pNewAppData->pLaunchedFrom);

      inform_app_new_state_to_application( CANTERBURY_APP_STATE_SHOW , pNewAppData);
      inform_app_new_state_to_audio_mgr (new_id, CANTERBURY_APP_STATE_SHOW);
      activity_mgr_debug( "ACTIVITY MGR: application already running in the bkg  \n"  );
  }
  else
  {
    /* Application is stopped in the background*/
    pAppToRestart->NewState = ACTIVITY_RESTART_APP;
    pAppToRestart->uinSpecialParams = pNewAppData->uinSpecialParams;
    if(pAppToRestart->pLaunchedFrom != NULL)
      g_free(pAppToRestart->pLaunchedFrom);

    pAppToRestart->pLaunchedFrom = g_strdup(pNewAppData->pLaunchedFrom);

    g_clear_pointer (&pAppToRestart->arguments, g_strfreev);

    if (_cby_entry_point_get_dbus_activatable (pNewAppData->entry_point))
      pAppToRestart->arguments = g_strdupv (pNewAppData->arguments);
  }
  /* free the memory created for the already existing app */
  activity_state_info_free (pNewAppData);
}

static ActivityStateInfo *activity_mgr_suggest_app_to_kill (void);

static ActivityMgrErrorType
activity_mgr_start_new_app (ActivityStateInfo *pNewAppData)
{
    ActivityStateInfo* pCurrentAppNnTop;
    ActivityMgrErrorType ReturnStatus = ACTIVITY_SUCCESS;
    guint uinStackCounter = 0;
    guint uinListLen;
    gboolean bAppToBeKilledFound = FALSE;
    const gchar *new_id = cby_entry_point_get_id (pNewAppData->entry_point);

    if (pNewAppData->entry_point == NULL)
    {
        g_printerr("ACTIVITY MGR: Application name to be started not defined \n");
        return ACTIVITY_ERROR_NO_APP_NAME;
    }

    uinListLen = g_list_length( ActivityMgrPriv.pAppActivityStack );
    /* get the application running on top */
    pCurrentAppNnTop = (ActivityStateInfo*)g_list_nth_data ( ActivityMgrPriv.pAppActivityStack , 0 );
    if( pCurrentAppNnTop == NULL )
    {
        activity_mgr_debug("ACTIVITY MGR: App on top not found. App must be launched in hidden state before launcher \n");
        ActivityMgrPriv.pAppActivityStack = g_list_prepend (ActivityMgrPriv.pAppActivityStack , pNewAppData );
        return ACTIVITY_SUCCESS;
    }
    activity_mgr_debug ("ACTIVITY MGR: current app on top %s, new app %s\n",
                        cby_entry_point_get_id (pCurrentAppNnTop->entry_point),
                        new_id);
    /* Step 1: check if the application on top is the requested app to be launched */
    if (pCurrentAppNnTop->entry_point == pNewAppData->entry_point)
    {
        activity_mgr_debug("ACTIVITY MGR: New Application already on top \n");
        /* check if the application has any arguments, and if so,
         * send a message to the application with those new arguments */
        send_argument_updates_to_app(pNewAppData);
        /* free the memory created for the already existing app */
        activity_state_info_free (pNewAppData);
        return ACTIVITY_SUCCESS;
    }
    /* End of Step 1 */

    /* Step 2: check what needs to be done with the application which is currently running on top based on its background state information */
    handle_next_state_of_current_app_on_top(pCurrentAppNnTop);
    /* End of Step 2 */

    /* Step 3: check if the new requested application is already available in the activity stack, if so restart that application */
    uinStackCounter = 0;
    while( uinListLen > uinStackCounter )
    {
        ActivityStateInfo* pAppToRestart = NULL;
        pAppToRestart = (ActivityStateInfo*)g_list_nth_data( ActivityMgrPriv.pAppActivityStack , uinStackCounter );
        if (pAppToRestart->entry_point == pNewAppData->entry_point)
        {
          set_all_attributes_to_restart_new_app(pNewAppData , pAppToRestart);

          /* restart the available application */
          return activity_state_info_handle_sync (pAppToRestart);
        }
        uinStackCounter++;
    };
    /* End of Step 3 */

    /* Step 4: New application not in stack,Check if the activity stack is full, if so iterate through the bottom to find the first stopped application and kill it */
    if( ACTIVITY_MGR_MAX_APPS <= uinListLen )
    {
      ActivityStateInfo *pAppToKillData;

      activity_mgr_debug( "ACTIVITY MGR: activity stack is full, delete the application at the bottom \n"  );
      pAppToKillData = activity_mgr_suggest_app_to_kill ();
      if( pAppToKillData != NULL )
      {
          pAppToKillData->NewState = ACTIVITY_OFF_APP;
          ReturnStatus = activity_state_info_handle_sync (pAppToKillData);
          bAppToBeKilledFound = TRUE;
      }
      else
      {
          ReturnStatus = ACTIVITY_ERROR_STACK_FULL;
          g_warning("ACTIVITY MGR : **** Activity stack is full , and all the applications in the stack are running, none of them are in stopped state... This should not happen, please check ****\n");
      }
    }
    /* End of Step 4 */

    /* Step 5: insert the new application on top of the stack */
    if( ( ACTIVITY_MGR_MAX_APPS >=  uinListLen ) || ( bAppToBeKilledFound == TRUE ) )
    {
        ActivityMgrPriv.pAppActivityStack = g_list_prepend (ActivityMgrPriv.pAppActivityStack , pNewAppData );
        set_launcher_menu_entry_to_focus (pNewAppData->entry_point);
        activity_mgr_debug ("ACTIVITY MGR: new application on top: %s\n",
                            new_id);
        activity_mgr_show_app (pNewAppData);
    }
    else
    {
        ReturnStatus = ACTIVITY_ERROR_STACK_FULL;
        activity_mgr_debug( "ACTIVITY MGR: activity stack is full, cannot add more application onto stack stack len %d max stack size %d  \n" , uinListLen ,ACTIVITY_MGR_MAX_APPS );
    }
    /* End of Step 5 */

    return ReturnStatus;
}

/*
 * Return the ActivityStateInfo for a running app that can be killed with
 * a minimum of disruption.
 */
static ActivityStateInfo *
activity_mgr_suggest_app_to_kill (void)
{
  /* get the user launched application which is at the bottom of the stack */
  ActivityStateInfo* pAppToKillData = NULL;
  guint uinStackCounter = 1;
  gboolean bAppToBeKilledFound = FALSE;
  gchar *current_active_audio_source = audio_mgr_get_current_active_audio_source ();
  guint uinListLen = g_list_length (ActivityMgrPriv.pAppActivityStack);

  /* loop through the activity stack from bottom to look for the application in the stopped state to kill that application */
  while( (bAppToBeKilledFound != TRUE ) && ( uinListLen >  uinStackCounter ) )
  {
      const gchar *id;

      pAppToKillData = (ActivityStateInfo*)g_list_nth_data( ActivityMgrPriv.pAppActivityStack , (uinListLen - uinStackCounter) );
      if( pAppToKillData == NULL)
      {
          g_warning( "ACTIVITY MGR:pAppToKillData null at %d , something went wrong in the activity stack \n", (uinListLen - uinStackCounter) );
          uinStackCounter++;
          continue;
      }
      id = cby_entry_point_get_id (pAppToKillData->entry_point);
      activity_mgr_debug ("ACTIVITY MGR:check if %s can be killed\n", id);
      activity_mgr_debug( "ACTIVITY MGR:  bkg state %d \n", pAppToKillData->uinBkgState);
      activity_mgr_debug( "ACTIVITY MGR: position %d \n", (uinListLen- uinStackCounter) );

      if ((pAppToKillData->uinBkgState == CANTERBURY_BKG_STATE_STOPPED) &&
          (pAppToKillData->NewState != ACTIVITY_OFF_APP) &&
          (pAppToKillData->NewState != ACTIVITY_KILL_APP) &&
          ((current_active_audio_source == NULL) ||
           (g_strcmp0 (current_active_audio_source, id) != 0)))
        {
          activity_mgr_debug ("ACTIVITY MGR:bAppToBeKilledFound %s\n", id);
          bAppToBeKilledFound = TRUE;
          break;
        }
      uinStackCounter++;
  };

  g_free (current_active_audio_source);

  if( bAppToBeKilledFound == FALSE )
  {
    current_active_audio_source = audio_mgr_get_current_active_audio_source ();
    /* get the user launched application which is at the bottom of the stack */
    uinStackCounter = 1;
    /* loop through the activity stack from bottom to look for the application in the running state to kill that application */
    while( (bAppToBeKilledFound != TRUE ) && ( uinListLen >  uinStackCounter ) )
      {
        CanterburyExecutableType type;
        const gchar *id;

        pAppToKillData = (ActivityStateInfo*)g_list_nth_data( ActivityMgrPriv.pAppActivityStack , (uinListLen - uinStackCounter)  );
        if( pAppToKillData == NULL)
        {
            g_warning( "ACTIVITY MGR:pAppToKillData null at %d , something went wrong in the activity stack \n", (uinListLen - uinStackCounter) );
            uinStackCounter++;
            continue;
        }
        id = cby_entry_point_get_id (pAppToKillData->entry_point);

        activity_mgr_debug ("ACTIVITY MGR: check if %s position %d bkg state %d can be killed\n",
                            id, (uinListLen - uinStackCounter), pAppToKillData->uinBkgState);

        type =
          _cby_entry_point_get_executable_type (pAppToKillData->entry_point);

        if ((pAppToKillData->uinBkgState == CANTERBURY_BKG_STATE_RUNNING) &&
            (pAppToKillData->NewState != ACTIVITY_OFF_APP) &&
            (pAppToKillData->NewState != ACTIVITY_KILL_APP) &&
            (type != CANTERBURY_EXECUTABLE_TYPE_STARTUP_APPLICATION))
          {
            if ((current_active_audio_source == NULL) ||
                (g_strcmp0 (current_active_audio_source, id) != 0))
              {
                activity_mgr_debug ("ACTIVITY MGR: bAppToBeKilledFound %s\n", id);
                bAppToBeKilledFound = TRUE;
                break;
              }
          }
        uinStackCounter++;
      }

    g_free (current_active_audio_source);
  }

  if( bAppToBeKilledFound == FALSE )
    return NULL;
  else
    return pAppToKillData;
}

static ActivityMgrErrorType
activity_mgr_stop_new_app (ActivityStateInfo *pAppToStop)
{
    ActivityMgrErrorType ErrorStatus = ACTIVITY_SUCCESS;
    const gchar *id;

    if (pAppToStop->entry_point == NULL)
    {
        g_printerr("ACTIVITY MGR: Application name to be stopped not defined \n");
        return ACTIVITY_ERROR_NO_APP_NAME;
    }

    id = cby_entry_point_get_id (pAppToStop->entry_point);
    if (g_strcmp0 (id, activity_mgr_get_app_on_top()) == 0)
    {
        g_printerr("ACTIVITY MGR: Application to be stopped is currently on top \n");
        return ACTIVITY_APP_ALREADY_ON_TOP;
    }

    if (!launch_mgr_signal_running_app (pAppToStop->entry_point, pAppToStop->pid, SIGSTOP))
    {
        ErrorStatus = ACTIVITY_ERROR_APP_NOT_STOPPED ;
        activity_mgr_debug ("ACTIVITY MGR: The entry point \"%s\" couldnt be stopped\n", id);
    }
    return ErrorStatus;
}

static gboolean
activity_mgr_apply_deferred_state (gpointer user_data)
{
    g_autoptr (CbyEntryPoint) entry_point = g_object_ref (user_data);
    guint uinStackCounter = 0;
    guint app_activity_stack_len;
    ActivityStateInfo *pAppToKill = NULL;
    ActivityStateInfo *pCurrentAppNnTop = NULL;
    const gchar *id;

    /* This is the point of no return: it is no longer possible to cancel
     * this termination. Don't let the GHashTable cancel the GSource -
     * we want to take responsibility for returning G_SOURCE_REMOVE. */
    if (g_hash_table_steal (ActivityMgrPriv.pending_terminations, entry_point))
      {
        /* The hash table was holding a ref to the entry point. We have
         * stolen that ref, so release it now. */
        g_object_unref (entry_point);
      }

    g_return_val_if_fail (CBY_IS_ENTRY_POINT (entry_point),
                          G_SOURCE_REMOVE);

    id = cby_entry_point_get_id (entry_point);
    DEBUG ("%s", id);

    app_activity_stack_len = g_list_length (ActivityMgrPriv.pAppActivityStack);

    while( app_activity_stack_len  > uinStackCounter )
    {
        pAppToKill = (ActivityStateInfo*)g_list_nth_data( ActivityMgrPriv.pAppActivityStack , uinStackCounter );
        if (pAppToKill->entry_point == entry_point)
        {
          DEBUG ("Found %s in activity stack", id);
          break;
        }
        uinStackCounter++;
    };

    if( uinStackCounter >= app_activity_stack_len )
    {
       DEBUG ("Did not find %s in activity stack", id);
       return G_SOURCE_REMOVE;
    }

    pCurrentAppNnTop = (ActivityStateInfo*)g_list_nth_data ( ActivityMgrPriv.pAppActivityStack , 0 );
    if ((pCurrentAppNnTop->entry_point != NULL) && (pAppToKill->entry_point != NULL) && pCurrentAppNnTop->entry_point != pAppToKill->entry_point)
    {
      ActivityMgrErrorType e;

      /* This function is always called from a state transition
       * handler or from a timeout callback, so there is no point in
       * delaying this with another idle. */
      DEBUG ("Applying state %d now", pAppToKill->NewState);
      e = activity_state_info_handle_sync (pAppToKill);

      if (e != ACTIVITY_SUCCESS)
        DEBUG ("Failed with code %d", e);
    }
    else
    {
        DEBUG ("Not applying state %d because %s is at the top of the stack",
               pAppToKill->NewState, id);
    }
    return G_SOURCE_REMOVE;
}

/*
 * Notify application processes that the state of the given app should change.
 *
 * If the given app is the app on top of the stack, additionally reset the
 * launch progress timer.
 */
static void
inform_app_new_state_to_application (CanterburyAppState  NewAppState,
                                     ActivityStateInfo  *pAppStateInfo)
{
  g_auto (GStrv) arguments = NULL;
  const gchar *id;
  const gchar *entry_point_on_top;

  id = cby_entry_point_get_id (pAppStateInfo->entry_point);
  DEBUG ("%s", id);

  switch (NewAppState)
    {
    case CANTERBURY_APP_STATE_RESTART:
    case CANTERBURY_APP_STATE_SHOW:
      /* I think the intention here is that after we have activated an
       * app once with non-default arguments (like a list of URLs to
       * open), we want subsequent activations to not have any special
       * arguments so that the app knows that it should reopen in the
       * same state it was in last time? */
      arguments = g_steal_pointer (&pAppStateInfo->arguments);

      /* We don't send NewAppState to DBusActivatable entry points, because
       * they already had an Activate() or Open() call, which is enough */
      if (!_cby_entry_point_get_dbus_activatable (pAppStateInfo->entry_point))
          emit_new_app_state (id, NewAppState,
                              (const gchar * const *) arguments);
      break;

    case CANTERBURY_APP_STATE_BACKGROUND:
    case CANTERBURY_APP_STATE_OFF:
    case CANTERBURY_APP_STATE_PAUSE:
      /* Signal that the graphical program has been put in the background or
       * that it will be killed or SIGSTOP'd soon. The NewAppState signal
       * is deprecated and will eventually have to be removed, because it
       * breaks app-bundle confidentiality (tells bundle A that bundle B is
       * installed, see T2783), so don't send it to DBusActivatable entry
       * points so that the API presented to those does not have to be broken
       * in future.
       *
       * TODO: Design a replacement if needed (T3438) */
      if (!_cby_entry_point_get_dbus_activatable (pAppStateInfo->entry_point))
        emit_new_app_state (id, NewAppState, NULL);
      break;

    case CANTERBURY_APP_STATE_START:
      /* Not valid here, we only emit this from RegisterMyApp().
       * Fall through */
    case CANTERBURY_APP_STATE_INVALID:
    default:
      g_return_if_reached ();
    }

  entry_point_on_top = activity_mgr_get_app_on_top ();

  if (g_strcmp0 (entry_point_on_top, id) == 0)
    reset_launch_progress_timer ();
}

static void
activity_state_info_free (ActivityStateInfo *NewAppInfo)
{
  if(NewAppInfo!=NULL)
  {
    g_clear_object (&NewAppInfo->entry_point);
    g_strfreev (NewAppInfo->arguments);

    g_free( NewAppInfo );
  }
}

static ActivityMgrErrorType
activity_mgr_pause_new_app (ActivityStateInfo *pAppToPause)
{
    if (pAppToPause->entry_point == NULL)
    {
        g_printerr("ACTIVITY MGR: Application name to be paused not defined \n");
        return ACTIVITY_ERROR_NO_APP_NAME;
    }

    activity_mgr_debug ("ACTIVITY MGR: activity_mgr_pause_app app name %s\n",
                        cby_entry_point_get_id (pAppToPause->entry_point));
    inform_app_new_state_to_application( CANTERBURY_APP_STATE_PAUSE , pAppToPause );
    inform_app_new_state_to_audio_mgr (cby_entry_point_get_id (pAppToPause->entry_point),
                                       CANTERBURY_APP_STATE_PAUSE);

    pAppToPause->NewState = ACTIVITY_STOP_APP;

    if (!_cby_entry_point_get_dbus_activatable (pAppToPause->entry_point))
      {
        /* Don't actually send SIGSTOP for 3 seconds, to give it a chance to
         * save its state */
        g_timeout_add_seconds_full (G_PRIORITY_DEFAULT, 3,
                                    activity_mgr_apply_deferred_state,
                                    g_object_ref (pAppToPause->entry_point),
                                    g_object_unref);
      }
    else
      {
        /* D-Bus-activatable programs currently have no way to be told they
         * are going to be paused, so just do it. In practice it would be
         * problematic to pause such a program in any case, because its
         * incoming messages would build up in the dbus-daemon.
         * If a replacement for this is needed, see T3438. */
        activity_mgr_apply_deferred_state (pAppToPause->entry_point);
      }

    return ACTIVITY_SUCCESS;
}

static ActivityMgrErrorType
activity_mgr_off_app (ActivityStateInfo *pAppToOff)
{
    guint source;

    if (pAppToOff->entry_point == NULL)
    {
        g_printerr("ACTIVITY MGR: Application name to be paused not defined \n");
        return ACTIVITY_ERROR_NO_APP_NAME;
    }

    activity_mgr_debug ("ACTIVITY MGR: activity_mgr_kill app name %s\n",
                        cby_entry_point_get_id (pAppToOff->entry_point));
    inform_app_new_state_to_application (CANTERBURY_APP_STATE_OFF, pAppToOff);
    pAppToOff->NewState = ACTIVITY_KILL_APP;

    if (!_cby_entry_point_get_dbus_activatable (pAppToOff->entry_point))
      {
        /* Don't actually terminate it for 3 seconds, to give it a chance to
         * save its state */
        source =
          g_timeout_add_seconds_full (G_PRIORITY_DEFAULT, 3,
                                      activity_mgr_apply_deferred_state,
                                      g_object_ref (pAppToOff->entry_point),
                                      g_object_unref);

        /* Save the source ID so we can cancel termination if the app gets
         * re-activated later. This will restart any grace period that might
         * already have been running, by cancelling its GSource. */
        g_hash_table_replace (ActivityMgrPriv.pending_terminations,
                              g_object_ref (pAppToOff->entry_point),
                              GUINT_TO_POINTER (source));
      }
    else
      {
        /* D-Bus-activatable programs are expected to have daemon-like
         * graceful exit behaviour - if cleanup is needed, they should use
         * an async-signal-safe mechanism like g_unix_signal_add() to
         * handle SIGTERM - so just stop them immediately. systemd will
         * automatically escalate to SIGKILL if they don't exit promptly. */
        activity_mgr_apply_deferred_state (pAppToOff->entry_point);
      }

    return ACTIVITY_SUCCESS;
}

static ActivityMgrErrorType
activity_mgr_show_app (ActivityStateInfo *pAppToShow)
{
  ActivityMgrErrorType ErrorStatus = ACTIVITY_SUCCESS;
  const gchar *id;

  if (pAppToShow->entry_point == NULL)
  {
      g_printerr("ACTIVITY MGR: Application name to be shown not defined \n");
      return ACTIVITY_ERROR_NO_APP_NAME;
  }
  id = cby_entry_point_get_id (pAppToShow->entry_point);
  ActivityMgrPriv.pAppActivityStack = g_list_remove (  ActivityMgrPriv.pAppActivityStack , pAppToShow );
  ActivityMgrPriv.pAppActivityStack = g_list_prepend (ActivityMgrPriv.pAppActivityStack , pAppToShow );

  activity_mgr_debug ("ACTIVITY MGR: app name %s\n", id);
  if (launch_mgr_find_running_app (id) != NULL)
  {
    if (g_strcmp0 (id, LAUNCHER_APP_NAME))
    {
      const char *type_str = "APPLICATION";

      if (_cby_entry_point_get_executable_type (pAppToShow->entry_point) ==
          CANTERBURY_EXECUTABLE_TYPE_EXT_APP)
        type_str = "EXT_APPLICATION";

      activity_mgr_debug( "mutter_plugin_call_start_app_switch  \n" );
      b_start_app_switch (type_str,
                          pAppToShow->pLaunchedFrom,
                          pAppToShow->uinSpecialParams,
                          CANTERBURY_DBUS_UNKNOWN,
                          NULL,
                          NULL);

      activity_mgr_debug ("****** win_mgr_maximize_application_window back press enable %s\n", pAppToShow->pLaunchedFrom);
      win_mgr_maximize_application_window (id);
    }
  }
  return ErrorStatus;
}

static ActivityMgrErrorType
activity_mgr_kill_app (ActivityStateInfo *pAppToKill)
{
    ActivityMgrErrorType ErrorStatus = ACTIVITY_SUCCESS;
    const gchar *top_app_name;
    const gchar *id;

    if (pAppToKill->entry_point == NULL)
    {
        g_printerr("ACTIVITY MGR: Application name to be killed not defined \n");
        return ACTIVITY_ERROR_NO_APP_NAME;
    }
    id = cby_entry_point_get_id (pAppToKill->entry_point);

    top_app_name = activity_mgr_get_app_on_top ();
    activity_mgr_debug( "ACTIVITY MGR:: activity_mgr_kill_app top_app_name %s\n" , top_app_name);
    if (g_strcmp0 (top_app_name, id) == 0)
    {
        activity_mgr_debug( "ACTIVITY MGR:: application already on top \n" );
        return ACTIVITY_APP_ALREADY_ON_TOP;
    }

    if (launch_mgr_find_running_app (top_app_name) == NULL)
    {
        guint source;

        activity_mgr_debug( "ACTIVITY MGR:: ***** win id of app null **** \n" );

        if (!_cby_entry_point_get_dbus_activatable (pAppToKill->entry_point))
          {
            /* Don't actually terminate it for 3 seconds, to give it a chance to
             * save its state */
            source =
              g_timeout_add_seconds_full (G_PRIORITY_DEFAULT, 3,
                                          activity_mgr_apply_deferred_state,
                                          g_object_ref (pAppToKill->entry_point),
                                          g_object_unref);

            /* Save the source ID so we can cancel termination if the app gets
             * re-activated later. This will restart any grace period that might
             * already have been running, by cancelling its GSource. */
            g_hash_table_replace (ActivityMgrPriv.pending_terminations,
                                  g_object_ref (pAppToKill->entry_point),
                                  GUINT_TO_POINTER (source));
          }
        else
          {
            /* D-Bus-activatable programs are expected to have daemon-like
             * graceful exit behaviour - if cleanup is needed, they should use
             * an async-signal-safe mechanism like g_unix_signal_add() to
             * handle SIGTERM - so just stop them immediately. systemd will
             * automatically escalate to SIGKILL if they don't exit
             * promptly. */
            activity_mgr_apply_deferred_state (pAppToKill->entry_point);
          }

        return ACTIVITY_ERROR_APP_NOT_KILLED;
    }

    ActivityMgrPriv.pAppActivityStack = g_list_remove (  ActivityMgrPriv.pAppActivityStack , pAppToKill );
    if (!launch_mgr_kill_running_app (pAppToKill->entry_point, pAppToKill->pid))
    {
        ErrorStatus = ACTIVITY_ERROR_APP_NOT_KILLED ;
        activity_mgr_debug ("ACTIVITY MGR: The entry point \"%s\" couldnt be killed\n", id);
    }
    activity_state_info_free (pAppToKill);

    return ErrorStatus;
}

static ActivityMgrErrorType
activity_mgr_unpause_new_app (ActivityStateInfo *pAppToRestart)
{
    const gchar *id;

    if (pAppToRestart->entry_point == NULL)
    {
        g_printerr("ACTIVITY MGR: Application name to be restarted not defined \n");
        return ACTIVITY_ERROR_NO_APP_NAME;
    }
    id = cby_entry_point_get_id (pAppToRestart->entry_point);

    if (!launch_mgr_signal_running_app (pAppToRestart->entry_point, pAppToRestart->pid, SIGCONT))
      activity_mgr_debug ("ACTIVITY MGR: The entry point \"%s\" couldnt be resumed\n", id);
    ActivityMgrPriv.pAppActivityStack = g_list_remove (  ActivityMgrPriv.pAppActivityStack , pAppToRestart );
    ActivityMgrPriv.pAppActivityStack = g_list_prepend (ActivityMgrPriv.pAppActivityStack , pAppToRestart );
    activity_mgr_debug ("ACTIVITY MGR: new application on top: %s\n", id);
    inform_app_new_state_to_application( CANTERBURY_APP_STATE_RESTART , pAppToRestart);
    inform_app_new_state_to_audio_mgr (id, CANTERBURY_APP_STATE_RESTART);
    pAppToRestart->NewState = ACTIVITY_SHOW_APP;
    return activity_state_info_handle_sync (pAppToRestart);
}

void
activity_mgr_update_app_on_top (void)
{
  if (ActivityMgrPriv.pAppActivityStack != NULL)
    {
      ActivityStateInfo *top = ActivityMgrPriv.pAppActivityStack->data;

      app_mgr_set_app_on_top (top->entry_point);
    }
}

gchar *
activity_mgr_force_kill_something (void)
{
  ActivityStateInfo *victim;
  const gchar *victim_id;

  /* Try to select something non-disruptive first. This skips the app on
   * the top of the stack, and apps that are flagged to run in the
   * background. */
  victim = activity_mgr_suggest_app_to_kill ();

  /* If that didn't work, try selecting the app on the top of the stack.
   * (FIXME: why?) */
  if (victim == NULL && ActivityMgrPriv.pAppActivityStack != NULL)
    {
      WARNING ("Could not find a non-disruptive app to kill, killing top of "
               "stack instead");
      victim = ActivityMgrPriv.pAppActivityStack->data;
    }

  /* If there's nothing on top of the stack, give up. */
  if (victim == NULL)
    return NULL;

  victim_id = cby_entry_point_get_id (victim->entry_point);

  /* If the top of the stack is the Launcher (which is meant to stay at the
   * bottom, so it's probably the only thing in the stack), give up. */
  if (g_strcmp0 (victim_id, LAUNCHER_APP_NAME) == 0)
    return NULL;

  victim->NewState = ACTIVITY_KILL_APP;

  if (!launch_mgr_kill_running_app (victim->entry_point, victim->pid))
    {
      WARNING ("Failed to kill entry point %s (pid %d)",
               victim_id, victim->pid);
    }

  return g_strdup (victim_id);
}

/*
 * If @entry_point is in the activity stack, remove it.
 */
void
activity_mgr_remove_entry_point (CbyEntryPoint *entry_point)
{
  GList *iter;

  g_return_if_fail (CBY_IS_ENTRY_POINT (entry_point));

  DEBUG ("%s", cby_entry_point_get_id (entry_point));

  for (iter = ActivityMgrPriv.pAppActivityStack;
       iter != NULL;
       iter = iter->next)
    {
      ActivityStateInfo *entry = iter->data;

      if (entry->entry_point == entry_point)
        {
          if (iter == ActivityMgrPriv.pAppActivityStack)
            {
              /* TODO: In the function from which this was adapted, there was
               * commented-out code here to move the Launcher onto the top.
               * Should we do something like that here? */
              DEBUG ("The application on top was killed");
            }

          ActivityMgrPriv.pAppActivityStack =
            g_list_remove_link (ActivityMgrPriv.pAppActivityStack, iter);

          activity_state_info_free (iter->data);
          g_list_free_1 (iter);
          return;
        }
    }
}

/*
 * Called when the given entry point has been activated, either via D-Bus
 * activation or by direct launching of the executable that resulted in
 * it calling the older RegisterMyApp() method.
 */
void
activity_mgr_entry_point_activated (CbyEntryPoint *entry_point)
{
  if (_cby_entry_point_get_splash_screen (entry_point))
    v_hide_splash_screen ();
}

static void
start_app_switch_cb (GObject *source_object,
                     GAsyncResult *res,
                     gpointer user_data)
{
  DEBUG ("StartAppSwitch() finished");
  activity_state_info_handle_in_idle (user_data);
}

void
activity_mgr_schedule_activation (CbyEntryPoint *entry_point,
                                  GPid pid,
                                  const gchar *const *uris,
                                  GVariant *platform_data,
                                  const gchar *const *argv,
                                  const gchar *launched_from,
                                  guint special_params,
                                  ActivateFlags flags)
{
  ActivityStateInfo *info;
  CanterburyExecutableType type;
  gboolean existing = ((flags & ACTIVATE_FLAGS_NEW) == 0);
  g_autoptr (GVariant) platform_data_ref = NULL;

  if (platform_data != NULL)
    platform_data_ref = g_variant_ref_sink (platform_data);

  if (_cby_entry_point_get_dbus_activatable (entry_point))
    activity_mgr_dbus_activate (entry_point, uris, platform_data);

  info = g_new0 (ActivityStateInfo, 1);
  type = _cby_entry_point_get_executable_type (entry_point);

  if (type == CANTERBURY_EXECUTABLE_TYPE_STARTUP_APPLICATION && !existing)
    info->NewState = ACTIVITY_INITIALIZE_STARTUP_APPS;
  else
    info->NewState = ACTIVITY_START_APP;

  info->entry_point = g_object_ref (entry_point);
  info->pid = pid;
  info->uinBkgState = _cby_entry_point_get_background_state (entry_point);
  info->uinSpecialParams = special_params;
  info->pLaunchedFrom = g_strdup (launched_from);

  if (!_cby_entry_point_get_dbus_activatable (entry_point) && argv != NULL)
    {
      /* +1 because ActivityStateInfo does not include the executable name
       * in the arguments */
      info->arguments = g_strdupv ((gchar **) argv + 1);
    }

  if (existing)
    {
      DEBUG ("Activating existing process");
      activity_state_info_handle_in_idle (info);
    }
  else if (type == CANTERBURY_EXECUTABLE_TYPE_STARTUP_APPLICATION)
    {
      DEBUG ("Activating startup app");
      activity_state_info_handle_in_idle (info);
    }
  else
    {
      DEBUG ("launch normal app");

      if (ActivityMgrPriv.pAppActivityStack == NULL)
        {
          DEBUG ("Launcher not yet up: launching in hidden mode");
          activity_state_info_handle_in_idle (info);
        }
      else
        {
          const gchar *splash = CANTERBURY_DBUS_UNKNOWN;
          const gchar *type_string = "APPLICATION";

          if (_cby_entry_point_get_splash_screen (entry_point) != NULL)
            splash = _cby_entry_point_get_splash_screen (entry_point);

          if (_cby_entry_point_get_executable_type (entry_point) ==
              CANTERBURY_EXECUTABLE_TYPE_EXT_APP)
            type_string = "EXT_APPLICATION";

          if (!b_start_app_switch (type_string, launched_from,
                                   special_params, splash,
                                   start_app_switch_cb, info))
            activity_state_info_handle_in_idle (info);
        }
    }
}
